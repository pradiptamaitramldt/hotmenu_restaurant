//
//  UserDefaultsManager.swift
//  CellularBackupContacts
//
//  Created by Mukesh Singh on 15/10/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation

enum UserDefaultsKey: String {
    
    case acccessToken = "access_token"//this is rawValue
    case tokenType = "token_type"
    
    case companyName = "company_name"
    case email = "email"
    case fullName = "fullName"
    case isPhoneVerified = "isMobileVerified"
    case phone = "phone"
    case profileImage = "picture"
    case rating = "rating"
    case userId = "user_id"
    case loginId = "loginId"
    case scope = "scope"
    case isdCode = "isdCode"
    case sid = "sid"
    case isdCodeId = "isdCodeId"
    case gender = "gender"
    
    case deviceToken = "device_token"
    
    
    case isDocmentUploaded = "is_docment_uploaded"
    
    case driverStatus = "driver_status"
}

class UserDefaultsManager: NSObject {
    
    public static let shared = UserDefaultsManager()
    private let userDefaults = UserDefaults.standard
    
    public func setStringValue(_ value: String, forKey: UserDefaultsKey) {
        self.userDefaults.set(value, forKey: forKey.rawValue)
    }
    
    public func getStringValue(forKey: UserDefaultsKey) -> String {
        if let value = self.userDefaults.value(forKey: forKey.rawValue) as? String {
            return value
        } else {
            return ""
        }
    }
    
    /*
    func getUserID() -> String {
        if let userID = self.userDefaults.value(forKey: "user_id") as? String {
            return userID
        } else {
            return ""
        }
    }
    
    func setUserID(userID: String) {
        self.userDefaults.set(userID, forKey: "user_id")
    }
    
    func getAuthToken() -> String {
        if let authToken = self.userDefaults.value(forKey: "auth_token") as? String {
            return authToken
        } else {
            return ""
        }
    }
    
    func setAuthToken(authToken: String) {
        self.userDefaults.set(authToken, forKey: "auth_token")
    }
    
    func getDeviceToken() -> String {
        if let deviceToken = self.userDefaults.value(forKey: "device_token") as? String {
            return deviceToken
        } else {
            return ""
        }
    }
    
    func setDeviceToken(authToken: String) {
        self.userDefaults.set(authToken, forKey: "device_token")
    }
    
    func clearAllValues() {
        for key in Array(self.userDefaults.dictionaryRepresentation().keys) {
            self.userDefaults.removeObject(forKey: key)
        }
    }
    */
    func clearAllValuesExceptDeviceToken() {
        for key in Array(self.userDefaults.dictionaryRepresentation().keys) {
            if( (key != "device_token") && (key != "device_owner_user_id") ) {
                self.userDefaults.removeObject(forKey: key)
            }
        }
    }
    
}
