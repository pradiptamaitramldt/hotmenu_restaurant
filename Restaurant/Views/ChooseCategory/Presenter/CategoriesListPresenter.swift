//
//  CategoriesListPresenter.swift
//  Restaurant
//
//  Created by Souvik on 06/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

class CategoriesListPresenter: NSObject {
    var arrRestaurantTypes : [VendorType] = []

    func getAllPossibleResturantTypes(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getRestaurantTypes, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: nil, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetRestaurantTypesModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.arrRestaurantTypes = (resData?.responseData?.vendorTypes)!
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
            }
        }
    }
}
