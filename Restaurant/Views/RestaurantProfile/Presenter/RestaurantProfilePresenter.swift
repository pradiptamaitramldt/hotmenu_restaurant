//
//  RestaurantProfilePresenter.swift
//  Restaurant
//
//  Created by Souvik on 10/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class RestaurantProfilePresenter: NSObject {

    let objAddTiming = AddTimingRequestDataModel()

    var objRestaurantDetail = GetRestaurantDetailsResponseData()
    var arrRestaurantTypes : [VendorType] = []

    func getRestaurantProfileDetails(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!, "vendorId" : UserDefaultValues.vendorID]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getRestaurantProfile, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetRestaurantDetailsModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            guard let data = resData?.responseData else{
                                completion(false,(resData?.message!)!)
                                return
                            }
                            self.objRestaurantDetail = data
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)

            }
        }
    }
    
    func chageLogoImage(image : UIImage?, completion : @escaping(_ success : Bool ,_ message : String) -> Void){

        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId"  : UserDefaultValues.vendorID,
                                     "image"     : image!]
        WebServiceManager.shared.requestMultipartAPIForTwoImages(url: WebServiceConstants.changeLogo, parameter: param, httpMethodType: .post, isAuthorized: true, authToken: UserDefaultValues.authToken) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, "Request Timeout.")

            }
        }

      }
    
    func chageCACImage(image : UIImage?, completion : @escaping(_ success : Bool ,_ message : String) -> Void){

        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId"  : UserDefaultValues.vendorID,
                                     "image"     : image!]
        WebServiceManager.shared.requestMultipartAPIForTwoImages(url: WebServiceConstants.licenceUpload, parameter: param, httpMethodType: .post, isAuthorized: true, authToken: UserDefaultValues.authToken) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, "Request Timeout.")

            }
        }

      }

    
    func chageBannerImage(image : UIImage?, completion : @escaping(_ success : Bool ,_ message : String) -> Void){

        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId"  : UserDefaultValues.vendorID,
                                     "image"     : image!]
        WebServiceManager.shared.requestMultipartAPIForTwoImages(url: WebServiceConstants.changeBanner, parameter: param, httpMethodType: .post, isAuthorized: true, authToken: UserDefaultValues.authToken) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, "Request Timeout.")

            }
        }

      }
    
    func editRestaurantInfo(email : String, countryCode : String, phone : String,name : String, type : String, manager : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        if name.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            if type.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
//                if manager.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                    if countryCode.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                        if Utility.isValidMobileNumber(phone){

                            let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                                            "vendorId"  : UserDefaultValues.vendorID,
                                                            "restaurantName"     : name,
                                                            "managerName" : manager,
                                                            "restaurantType" : type,
                                                            "restaurantEmail" : email,
                                                            "countryCode" : countryCode,
                                                            "restaurantPhone" : phone]
                            WebServiceManager.shared.requestAPI(url: WebServiceConstants.changeRestaurantInfo, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
                                if let responseData = data{
                                    let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                                        if resData != nil{
                                            if resData?.statuscode! == ResponseCode.success.rawValue{
                                                completion(true,(resData?.message!)!)
                                            }else{
                                                completion(false,(resData?.message!)!)
                                            }
                                        }else{
                                            completion(false, err!.localizedDescription)
                                        }
                                    }
                                }else{
                                    completion(false, ErrorInfo.netWorkError.rawValue)
                                }
                            }

                        }else{
                            completion(false, "Please enter Your Mobile No. (Mobile number should be contain 10-11 digits)")
                        }
                    }else{
                        completion(false, "Please select Country Code")
                        
                    }
//                }else{
//                    completion(false, "Manager name cannot be empty.")
//                }
            }else{
                completion(false, "Please choose a Restaurant Type")
            }
        }else{
            completion(false, "Restaurant name cannot be empty.")
            
        }
        
    }
    
    
    func editRestaurantEmail(email : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
                if Utility.isValidEmail(email){
                    let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                                    "vendorId"  : UserDefaultValues.vendorID,
                                                    "restaurantEmail" : email]
                    WebServiceManager.shared.requestAPI(url: WebServiceConstants.changeRestaurantEmailInfo, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
                        if let responseData = data{
                            let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                                if resData != nil{
                                    if resData?.statuscode! == ResponseCode.success.rawValue{
                                        completion(true,(resData?.message!)!)
                                    }else{
                                        completion(false,(resData?.message!)!)
                                    }
                                }else{
                                    completion(false, err!.localizedDescription)
                                }
                            }
                        }else{
                            completion(false, ErrorInfo.netWorkError.rawValue)
                        }
                    }
                }else{
                    completion(false, "Please add a valid email ID.")
                }
        
    }
    
    func editRestaurantPhoneInfo(countryCode : String, phone : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
         if countryCode.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            if Utility.isValidMobileNumber(phone){
                     let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                                     "vendorId"  : UserDefaultValues.vendorID,
                                                     "countryCode"     : countryCode,
                                                     "restaurantPhone" : phone]
                     WebServiceManager.shared.requestAPI(url: WebServiceConstants.changeRestaurantPhoneInfo, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
                         if let responseData = data{
                             let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                                 if resData != nil{
                                     if resData?.statuscode! == ResponseCode.success.rawValue{
                                         completion(true,(resData?.message!)!)
                                     }else{
                                         completion(false,(resData?.message!)!)
                                     }
                                 }else{
                                     completion(false, err!.localizedDescription)
                                 }
                             }
                         }else{
                             completion(false, ErrorInfo.netWorkError.rawValue)
                         }
                     }
                 }else{
                     completion(false, "Please enter Your Mobile No. (Mobile number should be contain 10-11 digits)")
                 }
         }else{
             completion(false, "Please select Country Code")

         }
         
     }
    
    func editTiming(isRestaurantClosed : String,preOrder : String,address : String, latitude : String, longitude : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let jsonEncoder = JSONEncoder()
        do{
            let jsonData = try jsonEncoder.encode(self.objAddTiming.restaurantTime)
            let json = String(data: jsonData, encoding: .utf8)//String(data: jsonData, encoding: String.Encoding.utf8)
            print(json!)
            if address.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                let param : [String: Any] = ["restaurantTime" : json!,
                                             "vendorId"  : UserDefaultValues.vendorID,
                                             "customerId"     : UserDefaultValues.userID!,
                                             "location" : address,
                                             "latitude" : latitude,
                                             "longitude" : longitude,
                                             "preOrder" : preOrder,
                                             "restaurantClose" : isRestaurantClosed ]
                WebServiceManager.shared.requestAPI(url: WebServiceConstants.editRestaurantTiming, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
                    if let responseData = data{
                        let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                            if resData != nil{
                                if resData?.statuscode! == ResponseCode.success.rawValue{
                                    completion(true,(resData?.message!)!)
                                }else{
                                    completion(false,(resData?.message!)!)
                                }
                            }else{
                                completion(false, err!.localizedDescription)
                            }
                        }
                    }else{
                        completion(false, ErrorInfo.netWorkError.rawValue)
                    }
                }
            }else{
                completion(false, "Please add restaurant Address")
            }
        }catch{
            
        }

        
    }
    
    func getAllPossibleResturantTypes(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getRestaurantTypes, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: nil, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetRestaurantTypesModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.arrRestaurantTypes = (resData?.responseData?.vendorTypes)!
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }
        }
    }

}
