//
//  GetRestaurantDetailsModel.swift
//  Restaurant
//
//  Created by Souvik on 10/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation



// MARK: - GetRestaurantDetailsModel
class GetRestaurantDetailsModel: Codable {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: GetRestaurantDetailsResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: GetRestaurantDetailsResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class GetRestaurantDetailsResponseData: Codable {
    var vendorTime: [VendorTime]?
    var vendorLatlong: VendorLatlong?
    var vendor: Vendor?
    var imageURL: String?
    var cacLicense : Bool?

    enum CodingKeys: String, CodingKey {
        case vendorTime, vendorLatlong, vendor, cacLicense
        case imageURL = "imageUrl"
    }
    init(){
        
    }

    init(vendorTime: [VendorTime]?, vendorLatlong: VendorLatlong?, vendor: Vendor?, imageURL: String?, cacLicense : Bool?) {
        self.vendorTime = vendorTime
        self.vendorLatlong = vendorLatlong
        self.vendor = vendor
        self.imageURL = imageURL
        self.cacLicense = cacLicense
    }
}

// MARK: - Vendor
class Vendor: Codable {
    var location: Location?
    var rating: Float?
    var licenceImage,preOrder, address: String?
    var isActive: Bool?
    var vendorOpenCloseTime: [VendorOpenCloseTime]?
    var id, managerName, restaurantType: String?
    var contactEmail: String?
    var contactPhone: Int?
    var countryCode, vendorDescription, logo, banner: String?
    var createdAt, updatedAt: String?
    var v: Int?

    enum CodingKeys: String, CodingKey {
        case location, rating, licenceImage, address, isActive, vendorOpenCloseTime, preOrder
        case id = "_id"
        case managerName, restaurantType, contactEmail, contactPhone, countryCode
        case vendorDescription = "description"
        case logo, banner, createdAt, updatedAt
        case v = "__v"
    }

    init(location: Location?, rating: Float?, licenceImage: String?, address: String?, preOrder : String?, isActive: Bool?, vendorOpenCloseTime: [VendorOpenCloseTime]?, id: String?, managerName: String?, restaurantType: String?, contactEmail: String?, contactPhone: Int?, countryCode: String?, vendorDescription: String?, logo: String?, banner: String?, createdAt: String?, updatedAt: String?, v: Int?) {
        self.location = location
        self.rating = rating
        self.licenceImage = licenceImage
        self.address = address
        self.isActive = isActive
        self.vendorOpenCloseTime = vendorOpenCloseTime
        self.id = id
        self.managerName = managerName
        self.restaurantType = restaurantType
        self.contactEmail = contactEmail
        self.contactPhone = contactPhone
        self.countryCode = countryCode
        self.vendorDescription = vendorDescription
        self.logo = logo
        self.banner = banner
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.v = v
        self.preOrder = preOrder
    }
}
// MARK: - Location
class RestaurantLocation: Codable {
    var type: String?
    var coordinates: [Double]?

    init(type: String?, coordinates: [Double]?) {
        self.type = type
        self.coordinates = coordinates
    }
}

// MARK: - VendorOpenCloseTime
class VendorOpenCloseTime: Codable {
    var isActive: Bool?
    var id, vendorID, day: String?
    var openTime, closeTime, v: Int?
    var createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case isActive
        case id = "_id"
        case vendorID = "vendorId"
        case day, openTime, closeTime
        case v = "__v"
        case createdAt, updatedAt
    }

    init(isActive: Bool?, id: String?, vendorID: String?, day: String?, openTime: Int?, closeTime: Int?, v: Int?, createdAt: String?, updatedAt: String?) {
        self.isActive = isActive
        self.id = id
        self.vendorID = vendorID
        self.day = day
        self.openTime = openTime
        self.closeTime = closeTime
        self.v = v
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}

// MARK: - VendorLatlong
class VendorLatlong: Codable {
    var vendorLat, vendorLong: Double?

    init(vendorLat: Double?, vendorLong: Double?) {
        self.vendorLat = vendorLat
        self.vendorLong = vendorLong
    }
}

// MARK: - VendorTime
class VendorTime: Codable {
    var day, openTime, closeTime: String?

    init(day: String?, openTime: String?, closeTime: String?) {
        self.day = day
        self.openTime = openTime
        self.closeTime = closeTime
    }
}
