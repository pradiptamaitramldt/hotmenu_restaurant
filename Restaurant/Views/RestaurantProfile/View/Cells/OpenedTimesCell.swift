//
//  OpenedTimesCell.swift
//  Restaurant
//
//  Created by Souvik on 10/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class OpenedTimesCell: UICollectionViewCell {
    @IBOutlet weak var labelDay: CustomNormalLabel!
    @IBOutlet weak var labelOpeningTime: CustomNormalLabel!
    @IBOutlet weak var labelClosingTime: CustomNormalLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
