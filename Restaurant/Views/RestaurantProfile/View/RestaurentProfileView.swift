//
//  RestaurentProfileView.swift
//  Restaurant
//
//  Created by Souvik on 09/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class RestaurentProfileView: BaseTableViewController, SideMenuItemContent, Storyboardable   {
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var viewRating: CosmosView!
    @IBOutlet weak var labelName: CustomBoldFontLabel!
    @IBOutlet weak var labelManagerName: CustomNormalLabel!
    @IBOutlet weak var collectionViewTypes: UICollectionView!
    
    @IBOutlet weak var labelEmail: CustomNormalLabel!
    @IBOutlet weak var labelContactNumber: CustomNormalLabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var buttonModify: NormalBoldButton!
    @IBOutlet weak var labelAddress: CustomNormalLabel!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionViewTimes: UICollectionView!
    @IBOutlet weak var label_RestaurantClosed: CustomBoldFontLabel!
    
    

    let presenter = RestaurantProfilePresenter()
    var bottom_menu = BottomMenu()
    var viewBottom = UIView()
    var arrTypes : [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionViewTimes.dataSource = self
        self.collectionViewTimes.delegate = self
        self.collectionViewTimes.register(UINib(nibName: "OpenedTimesCell", bundle: nil), forCellWithReuseIdentifier: "openedTimesCell")
        self.collectionViewTypes.dataSource = self
        self.collectionViewTypes.delegate = self

//        self.tableView.estimatedRowHeight = UITableView.automaticDimension

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let bottomSafeArea: CGFloat
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows[0]
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            //            topSafeArea = safeFrame.minY
            bottomSafeArea = window.frame.maxY - safeFrame.maxY
        } else {
            //            topSafeArea = topLayoutGuide.length
            bottomSafeArea = bottomLayoutGuide.length
        }
        self.viewBottom = UIView(frame: CGRect(x: 0.0, y: self.view.bounds.height - (bottomSafeArea) - 80.0, width: self.view.bounds.width, height: 80.0))
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:viewBottom.bounds.size.width,height:viewBottom.bounds.size.height)
        viewBottom.addSubview(bottom_menu)
        bottom_menu.userSeleted()
        bottom_menu.delegate = self
        if let window = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first {
            window.addSubview(viewBottom)
        }

        if UserDefaultValues.userID != nil{
            //AMShimmer.start(for: self.bannerImage)
            self.presenter.getRestaurantProfileDetails { (status, message) in
                self.stopLoaderAnimation()
                DispatchQueue.main.async {
                    self.showRestaurantDetails()
                }
            }

        }
        
    }

    @IBAction func btnBackDidClick(_ sender: Any) {
        showSideMenu()

    }
    
    @IBAction func buttonModifyDidClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "restaurantEditProfileView") as! RestaurantEditProfileView
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonSeeReviewsDidClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "reviewView") as! ReviewView
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    //MARK: - Self methods -
     
    func showRestaurantDetails(){
        let obj = self.presenter.objRestaurantDetail
        if obj.vendor != nil{
            self.bannerImage.sd_setImage(with: URL(string: obj.imageURL! + (obj.vendor?.banner!)!))
            self.imageLogo.sd_setImage(with: URL(string: obj.imageURL! + (obj.vendor?.logo!)!))
            self.viewRating.rating = Double(obj.vendor!.rating ?? 0.0)
//            self.labelName.text = String(describing: "\(obj.vendor?.restaurantName! ?? "")")
            self.labelManagerName.text = String(describing: "Manager : \(obj.vendor?.managerName! ?? "")")
            self.labelEmail.text = String(describing: "Email : \(obj.vendor?.contactEmail! ?? "")")
            self.labelContactNumber.text = String(describing: "Contact Number : \(obj.vendor?.contactPhone! ?? 0)")
            self.labelAddress.text = obj.vendor?.address!
            let initialLocation = CLLocation(latitude: obj.vendorLatlong!.vendorLat!, longitude: obj.vendorLatlong!.vendorLong!)
            self.centerMapOnLocation(location: initialLocation)
            self.arrTypes = (obj.vendor?.restaurantType!.split{$0 == ","}.map(String.init))!
            self.collectionViewTypes.reloadData()
            self.collectionViewTimes.reloadData()
            self.collectionViewHeightConstraint.constant = CGFloat(Double(obj.vendorTime!.count) * 30.0)
            self.tableView.reloadData()
            if obj.vendorTime?.count == 0{
                self.label_RestaurantClosed.isHidden = false
            }else{
                self.label_RestaurantClosed.isHidden = true
                
            }
            

        }

    }
    
     func centerMapOnLocation(location: CLLocation) {
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
        DispatchQueue.main.async {
            self.mapView.setRegion(region, animated: true)
            let annotation = MKPointAnnotation()
            annotation.coordinate = location.coordinate
            self.mapView.addAnnotation(annotation)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let obj = self.presenter.objRestaurantDetail

        switch indexPath.row {
        case 0:
            return 282.0
            case 1:
                return 302.0 + CGFloat(Double(obj.vendorTime?.count ?? 0) * 30.0)
            case 2:
                return 211.0
            case 3:
                return 200.0


        default:
            return 0.0
        }
    }

}
//MARK: bottomMenu delegate
extension RestaurentProfileView: bottomMenuDelegate{
    func selectedButton(selectedItem:NSInteger){
        
        if selectedItem == 1 {
            //home
            //            let st = UIStoryboard.init(name: "DashBoardView", bundle: nil)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 0
            UIApplication.shared.windows[0].rootViewController = viewController
        }else if selectedItem == 2{
            //menu
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 1
            UIApplication.shared.windows[0].rootViewController = viewController

        }
        else if selectedItem == 3{
            //OrderList
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 6
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }else{
            //userprofile
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 5
            UIApplication.shared.windows[0].rootViewController = viewController
        }
    }
    
}

extension RestaurentProfileView : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewTimes{
            return self.presenter.objRestaurantDetail.vendorTime?.count ?? 0
        }else{
            return self.arrTypes.count

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionViewTimes{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "openedTimesCell", for: indexPath) as! OpenedTimesCell
            guard let time = self.presenter.objRestaurantDetail.vendorTime?[indexPath.item] else {
                return cell
            }
            cell.labelDay.text = time.day!
            cell.labelOpeningTime.text = time.openTime!
            cell.labelClosingTime.text = time.closeTime!
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "orderTypeCollectionViewCell", for: indexPath) as! OrderTypeCollectionViewCell
            cell.label_Name.text = "\(self.arrTypes[indexPath.item])"
            cell.contentView.backgroundColor = UIColor.black
            cell.label_Name.textColor = .white
            return cell
        }

    }
    
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        if collectionView == collectionViewTimes{
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }else{
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        }


    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == collectionViewTimes{
            return CGSize(width:collectionView.frame.size.width,height:30.0)
        }else{
            let category = self.arrTypes[indexPath.item]

            let teStStr = "\(category)"
            let size : CGSize = (teStStr.size(withAttributes:
                [NSAttributedString.Key.font: ceraProNormalFontSize as Any]))
            return CGSize(width:size.width+16,height:26.0)

        }

        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        if collectionView == collectionViewTimes{
            return 0.0
        }else{
            return 10.0

        }

        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        if collectionView == collectionViewTimes{
            return 0.0
        }else{
            return 10.0

        }

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }

    

}

