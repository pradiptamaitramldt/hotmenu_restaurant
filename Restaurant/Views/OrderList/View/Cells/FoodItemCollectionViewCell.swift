//
//  FoodItemCollectionViewCell.swift
//  Restaurant
//
//  Created by Souvik on 13/04/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class FoodItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var label_FoodItemName: CustomNormalLabel!
    @IBOutlet weak var label_Count: CustomNormalLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
