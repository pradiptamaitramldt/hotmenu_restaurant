//
//  ExtrasCollectionViewCell.swift
//  Restaurant
//
//  Created by Souvik on 20/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class ExtrasCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var labelExtra: CustomNormalLabel!
    
    @IBOutlet weak var label_Count: CustomNormalLabel!
    @IBOutlet weak var label_Price: CustomNormalLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
