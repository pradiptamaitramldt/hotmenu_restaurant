//
//  CartItemCell.swift
//  Restaurant
//
//  Created by Souvik on 09/04/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class OrderedtemCell: UITableViewCell {
    @IBOutlet weak var label_MenuItemName: CustomBoldFontLabel!
    @IBOutlet weak var label_ItemCount: CustomNormalLabel!
    @IBOutlet weak var label_ItemPrice: CustomBoldFontLabel!
    @IBOutlet weak var view_Container: UIView!
    @IBOutlet weak var collectionViewOptions: UICollectionView!
    @IBOutlet weak var collectionViewExtras: UICollectionView!
    @IBOutlet weak var labelOptions: CustomNormalLabel!
    @IBOutlet weak var labelExtras: CustomNormalLabel!
    
    var objOrder = OrderDetail(item: "", itemImage: "", quantity: 0, itemPrice: 0, totalPrice: 0, createdAt: "")
    var itemCount = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.collectionViewExtras.dataSource = self
//        self.collectionViewExtras.delegate = self
//        self.collectionViewExtras.register(UINib(nibName: "ExtrasCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "extrasCollectionViewCell")
//
//        self.collectionViewOptions.dataSource = self
//        self.collectionViewOptions.delegate = self
//        self.collectionViewOptions.register(UINib(nibName: "OptionsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "optionsCollectionViewCell")
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    


}

//extension OrderedtemCell : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
//    
//    func numberOfSections(in collectionView: UICollectionView) -> Int
//    {
//        return 1
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if collectionView == collectionViewOptions{
//            return self.objOrder.itemOptions?.count ?? 0
//        }else{
//            return self.objOrder.orderExtras?.count ?? 0
//            
//        }
//        
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
//    {
//        if collectionView == collectionViewOptions{
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "optionsCollectionViewCell", for: indexPath) as! OptionsCollectionViewCell
//            cell.labelOption.text = self.objOrder.itemOptions?[indexPath.item].arrOptions?[0].name ?? ""
//            return cell
//        }else{
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "extrasCollectionViewCell", for: indexPath) as! ExtrasCollectionViewCell
//            cell.labelExtra.text = self.objOrder.orderExtras?[indexPath.item].itemName ?? ""
//            cell.label_Count.text = "X \(String(describing: (self.objOrder.orderExtras?[indexPath.item].quantity)!))"
//            cell.label_Price.text = "\(returnInCurrencyFormat(amount: (Float((self.objOrder.orderExtras?[indexPath.item].quantity)!) * (self.objOrder.orderExtras?[indexPath.item].price)!)))"
//            return cell
//
//        }
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
//    {
//        
//        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//        
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//    {
//        return CGSize(width: collectionView.bounds.width, height: 30.0)
//        
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
//    {
//        return 0.0
//        
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
//    {
//        return 0.0
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
//    {
//        
//    }
//}

