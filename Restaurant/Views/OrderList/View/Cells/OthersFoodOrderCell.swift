//
//  OthersFoodOrderCell.swift
//  Restaurant
//
//  Created by Souvik on 13/04/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

protocol OtherFoodItemDelegate {
    func assignDeliveryBoy(orderID : String, status : String)
}

class OthersFoodOrderCell: UITableViewCell {
    @IBOutlet weak var label_OrderID: CustomNormalLabel!
    @IBOutlet weak var label_OrderStatus: CustomNormalLabel!
    @IBOutlet weak var collectionView_FoodItems: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var label_Time: CustomNormalLabel!
    @IBOutlet weak var tableView_FoodItems: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonAssign: UIButton!
    
    var objOrder : OrdersArr?
    var deliveryBoy : DeliveryBoyData?
    var delegate : OtherFoodItemDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        var tableViewHeight : CGFloat = 0
        for foodItem in objOrder?.orderDetails ?? []{
//            if foodItem.orderExtras?.count  ?? 0 > 0 || foodItem.itemOptions?.count ?? 0 > 0{
//                let optionheight = Float(foodItem.itemOptions?.count ?? 0) * 30.0
//                let extraHeight = Float(foodItem.orderExtras?.count ?? 0) * 30.0
//                tableViewHeight += optionheight > extraHeight ? CGFloat(55.0 + optionheight) : CGFloat(55.0 + extraHeight)
//            }else{
//                tableViewHeight += 40.0
//            }
            tableViewHeight += 40.0
        }

//        self.collectionView_FoodItems.register(UINib(nibName: "FoodItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "foodItemCollectionViewCell")
//        self.collectionView_FoodItems.dataSource = self
//        self.collectionView_FoodItems.delegate = self
//        self.collectionViewHeight.constant = tableViewHeight//CGFloat((Double(objOrder?.orderDetails?.count ?? 0)*30.0))
        self.tableView_FoodItems.dataSource = self
        self.tableView_FoodItems.delegate = self
        self.tableView_FoodItems.register(UINib(nibName: "OrderedtemCell", bundle: nil), forCellReuseIdentifier: "orderedtemCell")
        self.tableViewHeight.constant = tableViewHeight//CGFloat((Double(objOrder?.orderDetails?.count ?? 0)*30.0))

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func button_ContactCustomerCilck(_ sender: Any) {
    }
    @IBAction func buttonAssignDeliveryBoyAction(_ sender: Any) {
        self.delegate?.assignDeliveryBoy(orderID: (objOrder?.orderID!)!, status: OrderStatusAction.ready.rawValue)
    }
    
    
}

extension OthersFoodOrderCell : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objOrder?.orderDetails?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "foodItemCollectionViewCell", for: indexPath) as! FoodItemCollectionViewCell
        let objFoodItem = objOrder?.orderDetails![indexPath.item]
        cell.label_Count.text = "\(objFoodItem?.quantity! ?? 0) (Qty)"
        cell.label_FoodItemName.text = objFoodItem?.item!
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        return CGSize(width:collectionView.bounds.width,height:30.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0.0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
}

extension OthersFoodOrderCell : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objOrder?.orderDetails?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let foodItem = objOrder?.orderDetails?[indexPath.row]
//        if foodItem!.orderExtras?.count  ?? 0 > 0 || foodItem!.itemOptions?.count ?? 0 > 0{
//            let optionheight = Float(foodItem!.itemOptions?.count ?? 0) * 30.0
//            let extraHeight = Float(foodItem!.orderExtras?.count ?? 0) * 30.0
//            return optionheight > extraHeight ? CGFloat(55.0 + optionheight) : CGFloat(55.0 + extraHeight)
//        }else{
//            return 40.0
//        }
        return 40.0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderedtemCell") as! OrderedtemCell
        let foodItem = objOrder?.orderDetails?[indexPath.row]
        cell.label_MenuItemName.text = foodItem?.item ?? ""
        cell.label_ItemCount.text = "( \(foodItem?.quantity ?? 0) )"
        cell.label_ItemPrice.text = "\(returnInCurrencyFormat(amount: Float((foodItem?.itemPrice!)!)))"
        cell.itemCount = foodItem?.quantity ?? 0
        cell.objOrder = foodItem!
        cell.collectionViewOptions.reloadData()
        cell.collectionViewExtras.reloadData()
        cell.labelExtras.isHidden = true
        cell.labelOptions.isHidden = true
//        if foodItem!.orderExtras?.count ?? 0 > 0 {
//            cell.labelExtras.isHidden = false
//        }else{
//            cell.labelExtras.isHidden = true
//        }
        cell.labelExtras.isHidden = true
//        if foodItem!.itemOptions?.count ?? 0 > 0{
//            cell.labelOptions.isHidden = false
//        }else{
//            cell.labelOptions.isHidden = true
//        }
        cell.labelOptions.isHidden = true

        return cell
    }
    
    
}


