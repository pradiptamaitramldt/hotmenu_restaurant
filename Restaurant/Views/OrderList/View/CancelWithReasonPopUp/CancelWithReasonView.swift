//
//  AddMenuView.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

protocol CancelWithReasonDelegate{
    func btnSubMitClicked(resaon : String, orderID : String)
}

class CancelWithReasonView: UIView {
    @IBOutlet weak var textViewreason: UITextView!
    

    var delegate : CancelWithReasonDelegate?
    var orderID : String = ""
    
    override init(frame: CGRect) {
       super.init(frame: frame)
        self.loadViewFromNib()
     }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadViewFromNib()

    }

    func loadViewFromNib() {
        let view = UINib(nibName: "CancelWithReasonView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
        let delayTime = DispatchTime.now() + 0.1
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
            self.loadView()
            
        })
    }
    
    func loadView(){

    }
    
    @IBAction func btnCrossDidClick(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    
    @IBAction func button_SubmitCLick(_ sender: Any) {
        self.delegate?.btnSubMitClicked(resaon: self.textViewreason.text!, orderID: self.orderID)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    }

