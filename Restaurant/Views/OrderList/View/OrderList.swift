//
//  OrderList.swift
//  Restaurant
//
//  Created by Souvik on 12/04/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

enum OrderStatus : String{
    case all = "All"
    case new = "New"
    case ready = "Accept"
    case assign = "Assigned"
    case pickedup = "PickedUp"
    case delivered = "Delivered"
    case cancelled = "Cancelled"
}

enum OrderStatusAction : String{
    case cancelled = "CANCELLED"
    case ready = "READY"
    case pickedup = "PICKEDUP"
    case delivered = "DELIVERED"
    
}

class OrderTypeCollectionViewCell : UICollectionViewCell{
    
    @IBOutlet weak var label_Name: UILabel!
    @IBOutlet weak var buttonCancel: UIButton!
}

class OrderList: BaseViewController, SideMenuItemContent, Storyboardable  {
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var collectionView_Orders: UICollectionView!
    @IBOutlet weak var tableView_FoodItems: UITableView!
    @IBOutlet weak var notificationBell: UIBarButtonItem!
    
    var bottom_menu = BottomMenu()
    var presenter = OrderListPresenter()
    var selectedStatusType : String = OrderStatus.all.rawValue
    var cancelOrderPopup : CancelWithReasonView?
    var didGetValue = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = String(describing: "\(UserDefaultValues.userName)")
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.viewBottom.bounds.size.width,height:self.viewBottom.bounds.size.height)
        self.viewBottom.addSubview(bottom_menu)
        bottom_menu.notesSeleted()
        bottom_menu.delegate = self
        self.collectionView_Orders.dataSource = self
        self.collectionView_Orders.delegate = self
        self.tableView_FoodItems.register(UINib(nibName: "NewFoodOrderCell", bundle: nil), forCellReuseIdentifier: "newFoodOrderCell")
        self.tableView_FoodItems.register(UINib(nibName: "AcceptedFoodOrderCell", bundle: nil), forCellReuseIdentifier: "acceptedFoodOrderCell")
        self.tableView_FoodItems.register(UINib(nibName: "OthersFoodOrderCell", bundle: nil), forCellReuseIdentifier: "othersFoodOrderCell")
        
        self.tableView_FoodItems.dataSource = self
        self.tableView_FoodItems.delegate = self
        self.tableView_FoodItems.rowHeight = UITableView.automaticDimension
        self.tableView_FoodItems.estimatedRowHeight = 200.0
        NotificationCenter.default.addObserver(self, selector: #selector(moveToDetails(_:)), name: .gotPush, object: nil)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        didGetValue = false
        self.notificationBell.addBadge(number: 0)
        self.selectedStatusType = UserDefaultValues.selectedType
        UserDefaultValues.selectedType = OrderStatus.all.rawValue
        AMShimmer.start(for: tableView_FoodItems)
       self.presenter.getAllStatusList { (status, message) in
          DispatchQueue.main.async {
                AMShimmer.stop(for: self.tableView_FoodItems)
               if status{
                    //success
                    self.didGetValue = true
                    self.collectionView_Orders.reloadData()
                 }else{
                    self.didGetValue = false
                    self.showAlertWith(message: message) {
                    }
                }
                switch self.selectedStatusType {
                case OrderStatus.all.rawValue:
                    self.collectionView_Orders.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
                case OrderStatus.new.rawValue:
                    self.collectionView_Orders.selectItem(at: IndexPath(item: 1, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
                case OrderStatus.ready.rawValue:
                    self.collectionView_Orders.selectItem(at: IndexPath(item: 2, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
                case OrderStatus.pickedup.rawValue:
                    self.collectionView_Orders.selectItem(at: IndexPath(item: 3, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
                case OrderStatus.delivered.rawValue:
                    self.collectionView_Orders.selectItem(at: IndexPath(item: 4, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
                case OrderStatus.cancelled.rawValue:
                    self.collectionView_Orders.selectItem(at: IndexPath(item: 5, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
                    
                default:
                    print("jjfd")
                }
                //self.startLoaderAnimation()
                self.presenter.getOrderList(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: self.selectedStatusType) { (status, message) in
                   // self.stopLoaderAnimation()
                    DispatchQueue.main.async {
                        if status{
                            self.tableView_FoodItems.reloadData()
                        }else{
                            self.showAlertWith(message: message) {
                            }
                        }
                        
                    }
                    
                }
            }
        }
    }
    @IBAction func btnBellDidClick(_ sender: Any) {
        let storybrd = UIStoryboard(name: "NotificationSettingsView", bundle: nil)
        let vc = storybrd.instantiateViewController(identifier: "notificationsListView") as! NotificationsListView
        vc.isopenFromOrderList = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btn_MenuDidClick(_ sender: Any) {
        showSideMenu()
    }
    
    @objc func moveToDetails(_ notificationData: Notification){
        self.notificationBell.updateBadge(number: UserDefaultValues.badgeCount)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    func showCancelPopup(orderID: String){
        cancelOrderPopup = CancelWithReasonView(frame: self.view.bounds)
        cancelOrderPopup!.delegate = self
        cancelOrderPopup?.orderID = orderID
        self.view.addSubview(cancelOrderPopup!)
        self.view.bringSubviewToFront(cancelOrderPopup!)
        
    }
}

extension OrderList: CancelWithReasonDelegate{
    
    func btnSubMitClicked(resaon: String, orderID: String) {
        if resaon == ""{
            self.showAlert(message: "Please add some reasons for cancelling the order.")
        }else{
            self.startLoaderAnimation()
            
            self.presenter.cancelOrder(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: OrderStatusAction.cancelled.rawValue, delayTime: "", orderId: orderID, reason: resaon) { (status, message) in
                self.stopLoaderAnimation()
                DispatchQueue.main.async {
                    if status{
                        self.cancelOrderPopup?.removeFromSuperview()
                        self.showAlertWith(message: message) {
                            AMShimmer.start(for: self.tableView_FoodItems)
                            self.presenter.getOrderList(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: self.selectedStatusType) { (status, message) in
                                AMShimmer.stop(for: self.tableView_FoodItems)
                                DispatchQueue.main.async {
                                    if status{
                                        self.tableView_FoodItems.reloadData()
                                    }else{
                                        self.showAlertWith(message: message) {
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

//MARK: bottomMenu delegate
extension OrderList: bottomMenuDelegate{
    func selectedButton(selectedItem:NSInteger){
        
        if selectedItem == 1 {
            //home
            //            let st = UIStoryboard.init(name: "DashBoardView", bundle: nil)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 0
            UIApplication.shared.windows[0].rootViewController = viewController
        }else if selectedItem == 2{
            //menu
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 1
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }
        else if selectedItem == 3{
            //OrderList
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 6
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }else{
            //userprofile
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 5
            UIApplication.shared.windows[0].rootViewController = viewController
        }
    }
    
}


extension OrderList : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.presenter.arrAllStatus.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "orderTypeCollectionViewCell", for: indexPath) as! OrderTypeCollectionViewCell
        if cell.isSelected{
            cell.contentView.backgroundColor = UIColor.black
            cell.label_Name.textColor = .white
        }else{
            cell.contentView.backgroundColor = UIColor.white
            cell.label_Name.textColor = .black
        }
        cell.label_Name.text = self.presenter.arrAllStatus[indexPath.item]
        AMShimmer.stop(for: cell.label_Name)
        AMShimmer.stop(for: cell.buttonCancel)
        AMShimmer.stop(for: cell.contentView)
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 2, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        let teStStr = self.presenter.arrAllStatus[indexPath.item]
        let size : CGSize = (teStStr.size(withAttributes:
            [NSAttributedString.Key.font: ceraProNormalFontSize as Any]))
        return CGSize(width:size.width+16,height:30.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10.0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! OrderTypeCollectionViewCell
        cell.contentView.backgroundColor = UIColor.black
        cell.label_Name.textColor = .white
        collectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
        self.selectedStatusType = self.presenter.arrAllStatus[indexPath.item]
       AMShimmer.start(for: self.tableView_FoodItems)
        self.presenter.getOrderList(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: self.selectedStatusType) { (status, message) in
           
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.tableView_FoodItems)
                
                if status{
                    self.tableView_FoodItems.reloadData()
                }else{
                    self.showAlertWith(message: message) {
                    }
                }
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as! OrderTypeCollectionViewCell? else
        {
            return
        }
        cell.contentView.backgroundColor = UIColor.white
        cell.label_Name.textColor = .black
        
    }
    
    
}

extension OrderList : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if didGetValue {
             return self.presenter.arrOrders.count
        }
        else {
            return 3
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if didGetValue {
            let objOrder = self.presenter.arrOrders[indexPath.row]
            switch objOrder.orderStatus{
            case OrderStatus.new.rawValue:
                //New Order
                let cell = tableView.dequeueReusableCell(withIdentifier: "newFoodOrderCell") as! NewFoodOrderCell
                var tableViewHeight : CGFloat = 0
                for foodItem in objOrder.orderDetails ?? []{
                    //                if foodItem.orderExtras?.count  ?? 0 > 0 || foodItem.itemOptions?.count ?? 0 > 0{
                    //                    let optionheight = Float(foodItem.itemOptions?.count ?? 0) * 30.0
                    //                    let extraHeight = Float(foodItem.orderExtras?.count ?? 0) * 30.0
                    //                    tableViewHeight += optionheight > extraHeight ? CGFloat(55.0 + optionheight) : CGFloat(55.0 + extraHeight)
                    //                }else{
                    //                    tableViewHeight += 40.0
                    //                }
                    tableViewHeight += 40.0
                }
                
                cell.tableViewHeight.constant = tableViewHeight
                cell.collectionViewHeight.constant = tableViewHeight//CGFloat((Double(objOrder.orderDetails!.count)*30.0))
                cell.tableViewHeight.constant = tableViewHeight//CGFloat((Double(objOrder.orderDetails!.count)*30.0))
                cell.objOrder = objOrder
                cell.label_OrderID.text = objOrder.orderNo!
                cell.label_OrderStatus.text = "Status : \(objOrder.orderStatus!)"
                cell.textField_PrepTime.text = ""//objOrder.preparationTime
                cell.textField_PrepTime.delegate = self
                cell.label_Time.text = "Order Time : \(objOrder.orderStatusTime!)"
                cell.delegate = self
                AMShimmer.stop(for: cell.button_Accept)
                AMShimmer.stop(for: cell.button_Cancel)
                return cell
                //        case OrderStatus.accepted.rawValue:
                //            //Accepted Order
                //            let cell = tableView.dequeueReusableCell(withIdentifier: "acceptedFoodOrderCell") as! AcceptedFoodOrderCell
                //            var tableViewHeight : CGFloat = 0
                //
                //            for foodItem in objOrder.orderDetails ?? []{
                //                if foodItem.orderExtras?.count  ?? 0 > 0 || foodItem.itemOptions?.count ?? 0 > 0{
                //                    let optionheight = Float(foodItem.itemOptions?.count ?? 0) * 30.0
                //                    let extraHeight = Float(foodItem.orderExtras?.count ?? 0) * 30.0
                //                    tableViewHeight += optionheight > extraHeight ? CGFloat(55.0 + optionheight) : CGFloat(55.0 + extraHeight)
                //                }else{
                //                    tableViewHeight += 40.0
                //                }
                //            }
                //
                //            cell.tableViewHeight.constant = tableViewHeight
                //            cell.collectionViewHeight.constant = tableViewHeight//CGFloat((Double(objOrder.orderDetails!.count)*30.0))
                //
                //            cell.objOrder = objOrder
                //            cell.label_OrderID.text = objOrder.orderNo!
                //            cell.label_OrderStatus.text = "Status : \(objOrder.orderStatus!)"
                //            cell.label_Time.text = "Accepted Time : \(objOrder.orderStatusTime!)"
                //
                //            cell.delegate = self
                //            cell.stackViewForReady.isHidden = true
                //            cell.stackViewForAccepted.isHidden = false
                //            return cell
                //        case OrderStatus.delayed.rawValue:
                //            //Delayed Order
                //            let cell = tableView.dequeueReusableCell(withIdentifier: "acceptedFoodOrderCell") as! AcceptedFoodOrderCell
                //            var tableViewHeight : CGFloat = 0
                //
                //            for foodItem in objOrder.orderDetails ?? []{
                //                if foodItem.orderExtras?.count  ?? 0 > 0 || foodItem.itemOptions?.count ?? 0 > 0{
                //                    let optionheight = Float(foodItem.itemOptions?.count ?? 0) * 30.0
                //                    let extraHeight = Float(foodItem.orderExtras?.count ?? 0) * 30.0
                //                    tableViewHeight += optionheight > extraHeight ? CGFloat(55.0 + optionheight) : CGFloat(55.0 + extraHeight)
                //                }else{
                //                    tableViewHeight += 40.0
                //                }
                //            }
                //
                //            cell.tableViewHeight.constant = tableViewHeight
                //            cell.collectionViewHeight.constant = tableViewHeight//CGFloat((Double(objOrder.orderDetails!.count)*30.0))
                //
                //            cell.objOrder = objOrder
                //            cell.label_OrderID.text = objOrder.orderNo!
                //            cell.label_OrderStatus.text = "Status : \(objOrder.orderStatus!)"
                //            cell.delegate = self
                //            cell.label_Time.text = "Deleyed At : \(objOrder.orderStatusTime!)"
                //            cell.stackViewForReady.isHidden = true
                //            cell.stackViewForAccepted.isHidden = false
                //
            //            return cell
            case OrderStatus.ready.rawValue,OrderStatus.assign.rawValue :
                //Ready Order
                let cell = tableView.dequeueReusableCell(withIdentifier: "acceptedFoodOrderCell") as! AcceptedFoodOrderCell
                var tableViewHeight : CGFloat = 0
                
                for foodItem in objOrder.orderDetails ?? []{
                    //                if foodItem.orderExtras?.count  ?? 0 > 0 || foodItem.itemOptions?.count ?? 0 > 0{
                    //                    let optionheight = Float(foodItem.itemOptions?.count ?? 0) * 30.0
                    //                    let extraHeight = Float(foodItem.orderExtras?.count ?? 0) * 30.0
                    //                    tableViewHeight += optionheight > extraHeight ? CGFloat(55.0 + optionheight) : CGFloat(55.0 + extraHeight)
                    //                }else{
                    //                    tableViewHeight += 40.0
                    //                }
                    tableViewHeight += 40.0
                }
                
                cell.tableViewHeight.constant = tableViewHeight
                cell.collectionViewHeight.constant = tableViewHeight//CGFloat((Double(objOrder.orderDetails!.count)*30.0))
                
                cell.objOrder = objOrder
                cell.label_OrderID.text = objOrder.orderNo!
                cell.label_OrderStatus.text = "Status : \(objOrder.orderStatus!)"
                cell.delegate = self
                cell.label_Time.text = "Ready At : \(objOrder.orderStatusTime!)"
                if  objOrder.orderStatus == OrderStatus.assign.rawValue {
                    cell.stackViewForReady.isHidden = false
                    cell.stackViewForAccepted.isHidden = true
                }
                else {
                    cell.stackViewForReady.isHidden = true
                    cell.stackViewForAccepted.isHidden = false
                }
                AMShimmer.stop(for: cell.assignButton)
                AMShimmer.stop(for: cell.completedButton)
                AMShimmer.stop(for: cell.outForDeliverButton)
                return cell
                
            default :
                print("")
                let cell = tableView.dequeueReusableCell(withIdentifier: "othersFoodOrderCell") as! OthersFoodOrderCell
                var tableViewHeight : CGFloat = 0
                
                for foodItem in objOrder.orderDetails ?? []{
                    //                if foodItem.orderExtras?.count  ?? 0 > 0 || foodItem.itemOptions?.count ?? 0 > 0{
                    //                    let optionheight = Float(foodItem.itemOptions?.count ?? 0) * 30.0
                    //                    let extraHeight = Float(foodItem.orderExtras?.count ?? 0) * 30.0
                    //                    tableViewHeight += optionheight > extraHeight ? CGFloat(55.0 + optionheight) : CGFloat(55.0 + extraHeight)
                    //                }else{
                    //                    tableViewHeight += 40.0
                    //                }
                    tableViewHeight += 40.0
                }
                cell.tableViewHeight.constant = tableViewHeight
                cell.collectionViewHeight.constant = tableViewHeight//CGFloat((Double(objOrder.orderDetails!.count)*30.0))
                cell.objOrder = objOrder
                cell.label_OrderID.text = objOrder.orderNo!
                cell.label_OrderStatus.text = "Status : \(objOrder.orderStatus!)"
                cell.label_Time.text = "Time : \(objOrder.orderStatusTime!)"
                AMShimmer.stop(for: cell.buttonAssign)
                return cell
        }
        
            
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "othersFoodOrderCell") as! OthersFoodOrderCell
            return cell
        }
    }
    
    
}

extension OrderList : NewOrderCellDelegate{
    func acceptOrder(orderID: String, status: String, cell: NewFoodOrderCell) {
        let time = cell.textField_PrepTime.text ?? ""
        if time == "" {
            self.showAlertWith(message: "Please enter preparation time") {
            }
        }else{
            if status == OrderStatusAction.cancelled.rawValue{
                self.showCancelPopup(orderID: orderID)
            }else{
                AMShimmer.start(for: self.tableView_FoodItems)
                self.presenter.changeOrderStatus(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: status, delayTime: time, orderId: orderID) { (status, message) in
                    
                    DispatchQueue.main.async {
                        AMShimmer.stop(for: self.tableView_FoodItems)
                        if status{
                            self.showAlertWith(message: message) {
                                
                                self.presenter.getOrderList(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: self.selectedStatusType) { (status, message) in
                                    self.stopLoaderAnimation()
                                    DispatchQueue.main.async {
                                        if status{
                                            self.tableView_FoodItems.reloadData()
                                        }else{
                                            self.showAlertWith(message: message) {
                                            }
                                        }
                                        
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    func cancelOrder(orderID: String, status: String) {
        self.showCancelPopup(orderID: orderID)
        
    }
    
    func contactCustomer(phone: String) {
        if let url = URL(string: "telprompt://\(phone)") {
            let application = UIApplication.shared
            guard application.canOpenURL(url) else {
                return
            }
            application.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
}


extension OrderList : AcceptedCellDelegate{
    func readyOrder(orderID: String, status: String) {
//        if status == OrderStatusAction.ready.rawValue{
//            self.startLoaderAnimation()
//
//            self.presenter.changeOrderStatus(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: status, delayTime: "", orderId: orderID) { (status, message) in
//                self.stopLoaderAnimation()
//                DispatchQueue.main.async {
//                    if status{
//                        self.showAlertWith(message: message) {
//                            self.startLoaderAnimation()
//                            self.presenter.getOrderList(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: self.selectedStatusType) { (status, message) in
//                                self.stopLoaderAnimation()
//                                DispatchQueue.main.async {
//                                    if status{
//                                        self.tableView_FoodItems.reloadData()
//                                    }else{
//                                        self.showAlertWith(message: message) {
//                                        }
//                                    }
//
//                                }
//                            }
//
//                        }
//                    }
//                }
//            }
//
//        }else{
//
//        }
        
        let storybrd = UIStoryboard(name: "OrderList", bundle: nil)
        let vc = storybrd.instantiateViewController(identifier: "DeliveryBoyList") as! DeliveryBoyList
        vc.orderID = orderID
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func outForDeliveryOrder(orderID: String, status: String) {
        if status == OrderStatusAction.delivered.rawValue{
            self.startLoaderAnimation()
            
            self.presenter.changeOrderStatus(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: status, delayTime: "", orderId: orderID) { (status, message) in
                self.stopLoaderAnimation()
                DispatchQueue.main.async {
                    if status{
                        self.showAlertWith(message: message) {
                            self.startLoaderAnimation()
                            self.presenter.getOrderList(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: self.selectedStatusType) { (status, message) in
                                self.stopLoaderAnimation()
                                DispatchQueue.main.async {
                                    if status{
                                        self.tableView_FoodItems.reloadData()
                                    }else{
                                        self.showAlertWith(message: message) {
                                        }
                                    }
                                    
                                }
                            }
                            
                        }
                    }
                }
            }
            
        }else{
            
        }
        
    }
    
    func makeAsCompleteOrder(orderID: String, status: String) {
        if status == OrderStatusAction.delivered.rawValue{
            self.startLoaderAnimation()
            
            self.presenter.changeOrderStatus(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: status, delayTime: "", orderId: orderID) { (status, message) in
                self.stopLoaderAnimation()
                DispatchQueue.main.async {
                    if status{
                        self.showAlertWith(message: message) {
                            self.startLoaderAnimation()
                            self.presenter.getOrderList(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: self.selectedStatusType) { (status, message) in
                                self.stopLoaderAnimation()
                                DispatchQueue.main.async {
                                    if status{
                                        self.tableView_FoodItems.reloadData()
                                    }else{
                                        self.showAlertWith(message: message) {
                                        }
                                    }
                                    
                                }
                            }
                            
                        }
                    }
                }
            }
            
        }else{
            
        }
        
    }
    
    func makeAsDelayedOrder(orderID: String, status: String) {
        let alert = UIAlertController(title: "HotMenu", message: "Please enter delay time. (In minutes)", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "Enter delay time"
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            self.startLoaderAnimation()
            
            self.presenter.changeOrderStatus(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: status, delayTime: (textField?.text!)!, orderId: orderID) { (status, message) in
                self.stopLoaderAnimation()
                DispatchQueue.main.async {
                    if status{
                        self.showAlertWith(message: message) {
                            self.startLoaderAnimation()
                            self.presenter.getOrderList(customerID: UserDefaultValues.userID!, vendorID: UserDefaultValues.vendorID, status: self.selectedStatusType) { (status, message) in
                                self.stopLoaderAnimation()
                                DispatchQueue.main.async {
                                    if status{
                                        self.tableView_FoodItems.reloadData()
                                    }else{
                                        self.showAlertWith(message: message) {
                                        }
                                    }
                                    
                                }
                            }
                            
                        }
                    }
                }
            }
        }))
        let cancel = UIAlertAction(title: "Cancel", style: .destructive) { (alt) in
        }
        alert.addAction(cancel)
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
}
extension OrderList: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
