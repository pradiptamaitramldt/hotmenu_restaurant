//
//  GetOrderListAPI.swift
//  Restaurant
//
//  Created by Souvik on 13/04/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//


import Foundation

// MARK: - GetOrderListAPIModel
class GetOrderListAPIModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: OrderListResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct OrderListResponseData: Codable {
    let ordersArr: [OrdersArr]?
}

// MARK: - OrdersArr
struct OrdersArr: Codable {
    let orderNo, orderStatus, orderStatusTime, orderID: String?
    let customerName, countryCode, customerPhone: String?
    let orderDetails: [OrderDetail]?

    enum CodingKeys: String, CodingKey {
        case orderNo, orderStatus, orderStatusTime
        case orderID = "orderId"
        case customerName, countryCode, customerPhone, orderDetails
    }
}

// MARK: - OrderDetail
struct OrderDetail: Codable {
    let item: String?
    let itemImage: String?
    let quantity, itemPrice, totalPrice: Int?
    let createdAt: String?
}

