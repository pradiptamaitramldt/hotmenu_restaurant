//
//  GetOrderStatusListAPI.swift
//  Restaurant
//
//  Created by Souvik on 13/04/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

// MARK: - GetOrderStatusListAPI
class GetOrderStatusListAPI: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: OrderStatusLisResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: OrderStatusLisResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class OrderStatusLisResponseData: Codable {
    let orderStatus: [String]?

    init(orderStatus: [String]?) {
        self.orderStatus = orderStatus
    }
}
