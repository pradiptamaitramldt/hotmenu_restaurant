//
//  OrderListPresenter.swift
//  Restaurant
//
//  Created by Souvik on 13/04/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class OrderListPresenter: NSObject {

    var arrAllStatus : [String] = []
    var arrOrders : [OrdersArr] = []
    
    func getAllStatusList(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getRestaurantOrderStausListAPI, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetOrderStatusListAPI.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.arrAllStatus.removeAll()
                            self.arrAllStatus = (resData?.responseData?.orderStatus!)!
//                            self.arrAllStatus.insert("ALL", at: 0)
                            print(resData)
                            print(self.arrAllStatus)
                            completion(true,"")
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
            }
        }
    }
    
    func getOrderList(customerID: String, vendorID : String, status: String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId" : vendorID,
                                     "orderStatus" : status]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getOrderListAPI, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetOrderListAPIModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.arrOrders.removeAll()
                            guard let arrOrders = resData?.responseData?.ordersArr else{
                                completion(false,(resData?.message!)!)
                                return
                            }
                            self.arrOrders = arrOrders
                            completion(true,"")
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)

            }
        }
    }
    
    func changeOrderStatus(customerID: String, vendorID : String, status: String,delayTime : String, orderId : String, reason : String = "", completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId" : vendorID,
                                     "prepTime" : delayTime,
                                     "orderId" : orderId]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.chanegeOrderStatusAPI, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
            }
        }
    }
    
    func cancelOrder(customerID: String, vendorID : String, status: String,delayTime : String, orderId : String, reason : String = "", completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId" : vendorID,
                                     "cancellationReason" : reason,
                                     "orderId" : orderId]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.cancelOrderAPI, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)

            }
        }
        }

}
