//
//  NotificationSettingsView.swift
//  Restaurant
//
//  Created by Souvik on 04/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class NotificationSettingsView: BaseViewController, SideMenuItemContent, Storyboardable{
  @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var label1: CustomNormalLabel!
    @IBOutlet weak var label2: CustomNormalLabel!
    @IBOutlet weak var label3: CustomNormalLabel!
    @IBOutlet weak var label4: CustomNormalLabel!
    
    
    
    let presenter = NotificationSettingPresenter()
    var gradientLabelSwitch1 : LabelSwitch?
    var gradientLabelSwitch2 : LabelSwitch?
    var gradientLabelSwitch3 : LabelSwitch?
    var gradientLabelSwitch4 : LabelSwitch?
    var bottom_menu = BottomMenu()

    override func viewDidLoad() {
        super.viewDidLoad()
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.viewBottom.bounds.size.width,height:self.viewBottom.bounds.size.height)
        self.viewBottom.addSubview(bottom_menu)
        bottom_menu.userSeleted()
        bottom_menu.delegate = self

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startLoaderAnimation()
        self.presenter.getSetting { (status, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.AddAllSwitchButtons()
            }

        }

    }
    
    @IBAction func button_menuDidClick(_ sender: Any) {
        showSideMenu()
    }

    func AddAllSwitchButtons(){
        

        let ls1 = LabelSwitchConfig(text: "On",
                              textColor: .white,
                                   font: ceraProBoldNormalFontSize,
                                   gradientColors: [UIColor(named: "NavigationBarColor")!.cgColor, UIColor(named: "ThemeColor")!.cgColor], startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1, y: 0.5))
        
        let rs1 = LabelSwitchConfig(text: "Off",
                              textColor: .white,
                                   font: ceraProBoldNormalFontSize,
                                   gradientColors: [UIColor(named: "ThemeColor")!.cgColor, UIColor(named: "NavigationBarColor")!.cgColor], startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1, y: 0.5))
        
        
        gradientLabelSwitch1 = LabelSwitch(center: CGPoint(x: view.frame.size.width - 50, y: self.label1.center.y), leftConfig: ls1, rightConfig: rs1, defaultState: .L)
        gradientLabelSwitch1?.tag = 1
        gradientLabelSwitch1?.delegate = self
        gradientLabelSwitch1?.curState = self.presenter.objSetting.newOrder! ? .R : .L
        gradientLabelSwitch1?.circleShadow = false
        gradientLabelSwitch1?.fullSizeTapEnabled = true
        self.view.addSubview(gradientLabelSwitch1!)
        
        
        gradientLabelSwitch2 = LabelSwitch(center: CGPoint(x: view.frame.size.width - 50, y:self.label2.center.y), leftConfig: ls1, rightConfig: rs1, defaultState: .L)
        gradientLabelSwitch2?.tag = 2
        gradientLabelSwitch2?.delegate = self
        gradientLabelSwitch2?.curState = self.presenter.objSetting.preOrder! ? .R : .L
        gradientLabelSwitch2?.circleShadow = false
        gradientLabelSwitch2?.fullSizeTapEnabled = true
        self.view.addSubview(gradientLabelSwitch2!)

        
        gradientLabelSwitch3 = LabelSwitch(center: CGPoint(x: view.frame.size.width - 50, y: self.label3.center.y), leftConfig: ls1, rightConfig: rs1, defaultState: .L)
        gradientLabelSwitch3?.tag = 3
        gradientLabelSwitch3?.delegate = self
        gradientLabelSwitch3?.curState = self.presenter.objSetting.orderNotification! ? .R : .L
        gradientLabelSwitch3?.circleShadow = false
        gradientLabelSwitch3?.fullSizeTapEnabled = true
        self.view.addSubview(gradientLabelSwitch3!)

        gradientLabelSwitch4 = LabelSwitch(center: CGPoint(x: view.frame.size.width - 50, y: self.label4.center.y), leftConfig: ls1, rightConfig: rs1, defaultState: .L)
        gradientLabelSwitch4?.tag = 4
        gradientLabelSwitch4?.delegate = self
        gradientLabelSwitch4?.curState = self.presenter.objSetting.orderModification! ? .R : .L
        gradientLabelSwitch4?.circleShadow = false
        gradientLabelSwitch4?.fullSizeTapEnabled = true
        self.view.addSubview(gradientLabelSwitch4!)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK: bottomMenu delegate
extension NotificationSettingsView: bottomMenuDelegate{
    func selectedButton(selectedItem:NSInteger){
        
        if selectedItem == 1 {
            //home
            //            let st = UIStoryboard.init(name: "DashBoardView", bundle: nil)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 0
            UIApplication.shared.windows[0].rootViewController = viewController
        }else if selectedItem == 2{
            //menu
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 1
            UIApplication.shared.windows[0].rootViewController = viewController

        }
        else if selectedItem == 3{
            //OrderList
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 6
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }else{
            //userprofile
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 5
            UIApplication.shared.windows[0].rootViewController = viewController
        }
    }
    
}

extension NotificationSettingsView: LabelSwitchDelegate {
    func switchChangToState(sender: LabelSwitch) {
        var status : Bool = true
        switch sender.curState {
        case .L: print("left state")
        status = false
        case .R: print("right state")
        status = true
        }
        
        switch sender.tag {
        case 1:
            self.presenter.objSetting.newOrder = status
        case 2:
            self.presenter.objSetting.preOrder = status
        case 3:
            self.presenter.objSetting.orderNotification = status
        case 4:
            self.presenter.objSetting.orderModification = status
            
        default:
            print("")
        }
        self.startLoaderAnimation()
        self.presenter.updateSetting { (status, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.showAlert(message: message)

            }

        }
    }
}
