//
//  NotificationSettingPresenter.swift
//  Restaurant
//
//  Created by Souvik on 16/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit



class NotificationSettingPresenter: NSObject {

    var objSetting = NotificationSettingModel()
    func updateSetting(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let jsonEncoder = JSONEncoder()
        do{
            let jsonData = try jsonEncoder.encode(self.objSetting)
            let json = String(data: jsonData, encoding: .utf8)//String(data: jsonData, encoding: String.Encoding.utf8)
            print(json!)
            let param : [String: Any] = ["notificationData" : json!,
                                         "vendorId"  : UserDefaultValues.vendorID,
                                         "customerId"     : UserDefaultValues.userID!]
            WebServiceManager.shared.requestAPI(url: WebServiceConstants.changeNotificationSetting, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
                if let responseData = data{
                    let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                        if resData != nil{
                            if resData?.statuscode! == ResponseCode.success.rawValue{
                                completion(true,(resData?.message!)!)
                            }else{
                                completion(false,(resData?.message!)!)
                            }
                        }else{
                            completion(false, err!.localizedDescription)
                        }
                    }
                }else{
                    completion(false, ErrorInfo.netWorkError.rawValue)
                }
            }
        }catch{
            
        }
        
        
    }
    
    func getSetting(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["vendorId"  : UserDefaultValues.vendorID,
                                     "customerId"     : UserDefaultValues.userID!]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getNotificationSetting, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetNotificationSettingModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.objSetting = (resData?.responseData!.usernotSetting)!
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
            }
        }

        
    }

}


