//
//  GetNotificationSettingModel.swift
//  Restaurant
//
//  Created by Souvik on 16/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

// MARK: - GetNotificationSettingModel
class GetNotificationSettingModel: Codable {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: GetNotificationSettingResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: GetNotificationSettingResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class GetNotificationSettingResponseData: Codable {
    var usernotSetting: NotificationSettingModel?

    init(usernotSetting: NotificationSettingModel?) {
        self.usernotSetting = usernotSetting
    }
}

class NotificationSettingModel: Codable{
    var newOrder: Bool? = true
    var preOrder: Bool? = true
    var orderNotification: Bool? = true
    var orderModification: Bool? = true
    
    enum CodingKeys: String, CodingKey {
        case newOrder = "NewOrder"
        case preOrder = "PreOrder"
        case orderNotification = "OrderNotification"
        case orderModification = "OrderModification"
    }
    
    init(){
        
    }
    
    init(newOrder: Bool?, preOrder: Bool?, orderNotification: Bool?, orderModification: Bool?) {
        self.newOrder = newOrder
        self.preOrder = preOrder
        self.orderNotification = orderNotification
        self.orderModification = orderModification
    }
    
}

