//
//  PrivacyPolicyView.swift
//  Restaurant
//
//  Created by Souvik on 06/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class WhoWeAreView: BaseViewController, SideMenuItemContent, Storyboardable  {
    
    @IBOutlet weak var textViewContent: UITextView!
    
    var presenter = PrivacyPolicyPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startLoaderAnimation()
        self.presenter.getCRMContents(pageName: "VendorWhoWeAre") { (status, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.textViewContent.attributedText = self.presenter.objPrivacyContent.htmlToAttributedString
            }
            
        }

    }
    
    @IBAction func btnSideMenuDidClick(_ sender: Any) {
        self.showSideMenu()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
