//
//  ManagePaymentPresenter.swift
//  Restaurant
//
//  Created by Souvik on 05/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class ManagePaymentPresenter: NSObject {

    var arrAllBanks : [Bank] = []
    var objAccountDetails : Account?
    
    func getPaymentInformation(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!, "vendorId" : UserDefaultValues.vendorID]

        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getBankDetails, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(), parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let convertedData = WebServiceManager.shared.decryptResponseDataToDataForLogin(data: responseData)
                Utility.log(convertedData)
                do {
                    let objResponse = try JSONDecoder().decode(GetAllBanksModel.self, from: convertedData!)
                    self.objAccountDetails = (objResponse.responseData?.account!)!
                    self.arrAllBanks = objResponse.responseData?.bank ?? []

                    completion(true, objResponse.message!)

                } catch {
                    do {
                        let objResponse = try JSONDecoder().decode(GetAllBanksModel.self, from: convertedData!)
                        completion(false, objResponse.message!)
                    } catch {
                        print(error.localizedDescription)
                    }
                }
                
            }
        }
    }
    
    
    func updatePaymentInformation(businessName : String, bankCode : String, accountNumber : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        
        if businessName.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            if bankCode.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                if accountNumber.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                        let objInfo = PostAccountDetailsRequestModel()
                        objInfo.customerId = UserDefaultValues.userID
                        objInfo.vendorId = UserDefaultValues.vendorID
                        objInfo.bankCode = bankCode
                        objInfo.businessName = businessName
                        objInfo.accountNo = accountNumber
                        do{
                            let jsonEncoder = JSONEncoder()
                            let jsonData = try jsonEncoder.encode(objInfo)
                            
                            do {
                                let json = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
                                let requestObj = WebServiceManager.shared.encryptJsonToString(json: json as! [String : Any])
                                WebServiceManager.shared.requestAPI(url: WebServiceConstants.addPaymentInfo, httpHeader: WebServiceHeaderGenerator.generateHeaderWithPublicKeywith(dataStr: requestObj), httpMethodType: .post) { (data, err) in
                                    if let responseData = data{
                                        let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                                            if resData != nil{
                                                if resData?.statuscode! == ResponseCode.success.rawValue{
                                                    completion(true,(resData?.message!)!)
                                                }else{
                                                    completion(false,(resData?.message!)!)
                                                }
                                            }else{
                                                completion(false, err!.localizedDescription)
                                            }
                                        }
                                    }else{
                                        completion(false, ErrorInfo.netWorkError.rawValue)

                                    }
                                }
                            } catch let myJSONError {
                                Utility.log(myJSONError)
                            }
                        }catch{
                            Utility.log("Encoding Error")
                        }
                        
                }else{
                    completion(false, "Please add your account number")
                }
            }else{
                completion(false, "Please add your bank code by adding bank name.")
            }
        }else{
            completion(false, "Please enter your Business name")
        }
        
    
    }
    
    func getBankList(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getBankList, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(), parameter: ["vendorId": UserDefaultValues.vendorID, "customerId" : UserDefaultValues.userID!], httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let convertedData = WebServiceManager.shared.decryptResponseDataToData(data: responseData)
                Utility.log(convertedData)
                do {
                    let objResponse = try JSONDecoder().decode(GetAllBanksModel.self, from: convertedData!)
                    self.arrAllBanks = objResponse.responseData?.bank ?? []
                    completion(true, "Banks List")
                } catch {
                    do {
                        let objResponse = try JSONDecoder().decode(GetAllBanksModel.self, from: convertedData!)
                        completion(false, objResponse.message!)
                    } catch {
                        print(error.localizedDescription)
                    }
                }
                
            }
        }
    }


    }
