//
//  ManagePaymentsView.swift
//  Restaurant
//
//  Created by Souvik on 04/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class ManagePaymentsView: BaseViewController, SideMenuItemContent, Storyboardable   {
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var textFieldName: CustomTextField!
    @IBOutlet weak var viewBankCode: UIView!
    @IBOutlet weak var viewBankName: UIView!
    @IBOutlet weak var textFieldBankName: CustomTextField!
    @IBOutlet weak var textFieldBankCode: CustomTextField!
    @IBOutlet weak var viewAccountNumber: UIView!
    @IBOutlet weak var textFieldAccountNumber: CustomTextField!
    @IBOutlet weak var btnSubmit: NormalBoldButton!
    
    @IBOutlet weak var tableViewBanks: UITableView!
    @IBOutlet weak var tableViewHeightConstraints: NSLayoutConstraint!
    
    var presenter = ManagePaymentPresenter()
    var arrBankTemp : [Bank] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewBanks.isHidden = true
        self.tableViewBanks.dataSource = self
        self.tableViewBanks.delegate = self
 
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startLoaderAnimation()
         self.presenter.getPaymentInformation { (status, message) in
             self.stopLoaderAnimation()
             DispatchQueue.main.async {
                if self.presenter.objAccountDetails?.accountNo == nil{
                    self.btnSubmit.setTitle("Add", for: .normal)
                }else{
                    self.textFieldName.text = self.presenter.objAccountDetails!.businessName ?? ""
                    self.textFieldBankCode.text = self.presenter.objAccountDetails!.bankCode ?? ""
                    self.textFieldAccountNumber.text = self.presenter.objAccountDetails!.accountNo ?? ""
                    for item in self.presenter.arrAllBanks{
                        if item.bankCode == self.presenter.objAccountDetails!.bankCode ?? ""{
                            self.textFieldBankName.text = item.bankName ?? ""
                        }
                    }
                    self.textFieldName.isUserInteractionEnabled = false
                    self.textFieldBankName.isUserInteractionEnabled = false
                    self.textFieldAccountNumber.isUserInteractionEnabled = false
                    self.btnSubmit.setTitle("Edit", for: .normal)
                }

             }

         }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewName.setRoundedCornered(height: viewName.frame.size.height)
        self.viewBankCode.setRoundedCornered(height: viewBankCode.frame.size.height)
        self.viewAccountNumber.setRoundedCornered(height: viewAccountNumber.frame.size.height)
        self.viewBankName.setRoundedCornered(height: viewBankName.frame.size.height)

    }
    
    @IBAction func btnMenuDidClick(_ sender: Any) {
        self.showSideMenu()
    }
    
    @IBAction func btnSubmitDidClick(_ sender: Any) {
        let title = (sender as! UIButton).titleLabel?.text!
        if title == "Add"{
            self.startLoaderAnimation()
            self.presenter.updatePaymentInformation(businessName: self.textFieldName.text!, bankCode: self.textFieldBankCode.text!, accountNumber: self.textFieldAccountNumber.text!) { (status, message) in
                self.stopLoaderAnimation()
                DispatchQueue.main.async {
                    self.showAlertWith(message: message) {
                    if status{
                            self.btnSubmit.setTitle("Edit", for: .normal)
                            self.textFieldName.isUserInteractionEnabled = false
                            self.textFieldBankName.isUserInteractionEnabled = false
                            self.textFieldAccountNumber.isUserInteractionEnabled = false

                        }
                    }

                }
            }
        }else if title == "Edit"{
            self.textFieldName.isUserInteractionEnabled = true
            self.textFieldBankName.isUserInteractionEnabled = true
            self.textFieldAccountNumber.isUserInteractionEnabled = true
            self.btnSubmit.setTitle("Submit", for: .normal)

        }else if title == "Submit"{
            self.startLoaderAnimation()
            self.presenter.updatePaymentInformation(businessName: self.textFieldName.text!, bankCode: self.textFieldBankCode.text!, accountNumber: self.textFieldAccountNumber.text!) { (status, message) in
                self.stopLoaderAnimation()
                DispatchQueue.main.async {
                    self.showAlertWith(message: message) {
                    if status{
                            self.btnSubmit.setTitle("Edit", for: .normal)
                            self.textFieldName.isUserInteractionEnabled = false
                            self.textFieldBankName.isUserInteractionEnabled = false
                            self.textFieldAccountNumber.isUserInteractionEnabled = false

                        }
                    }

                }
            }

        }
    }
    
    func filterBankList(filterString: String) {
        self.arrBankTemp = self.presenter.arrAllBanks
        if filterString.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            self.arrBankTemp = self.arrBankTemp.filter {
                $0.bankName?.range(of: filterString, options: .caseInsensitive) != nil
            }
        }
        self.tableViewBanks.reloadData()
        return
    }

    
}

extension ManagePaymentsView : UITextFieldDelegate{

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldBankName{
            self.tableViewBanks.isHidden = false
            self.filterBankList(filterString: textField.text!)
            self.tableViewHeightConstraints.constant = (CGFloat(self.arrBankTemp.count) * 35.0) > 200.0 ? 200.0 : (CGFloat(self.arrBankTemp.count) * 35.0)

            return true
        }else{
            return true
        }
        
    }

}


extension ManagePaymentsView : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBankTemp.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bankNamesCell") as! BankNamesCell
        let bank = self.arrBankTemp[indexPath.row]
        cell.labelBankName.text = bank.bankName ?? ""
        cell.labelCode.text = bank.bankCode ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bank = self.arrBankTemp[indexPath.row]

        self.textFieldBankName.text = bank.bankName ?? ""
        self.textFieldBankCode.text = bank.bankCode ?? ""
        self.tableViewBanks.isHidden = true
        
    }
    
}
