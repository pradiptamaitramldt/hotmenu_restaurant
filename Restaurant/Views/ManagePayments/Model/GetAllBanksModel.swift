//
//  GetAllBanksModel.swift
//  Restaurant
//
//  Created by Souvik on 05/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

import Foundation

// MARK: - GetAllBanksModel
class GetAllBanksModel: Codable {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: GetAllBanksResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: GetAllBanksResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class GetAllBanksResponseData: Codable {
    var bank: [Bank]?
    var account: Account?

    init(bank: [Bank]?, account: Account?) {
        self.bank = bank
        self.account = account
    }
}

// MARK: - Account
class Account: Codable {
    var id, vendorID, accountNo, bankCode: String?
    var businessName, createdAt, updatedAt: String?
    var v: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case vendorID = "vendorId"
        case accountNo, bankCode, businessName, createdAt, updatedAt
        case v = "__v"
    }
    
    init() {
        
    }
    
    init(id: String?, vendorID: String?, accountNo: String?, bankCode: String?, businessName: String?, createdAt: String?, updatedAt: String?, v: Int?) {
        self.id = id
        self.vendorID = vendorID
        self.accountNo = accountNo
        self.bankCode = bankCode
        self.businessName = businessName
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.v = v
    }
}

// MARK: - Bank
class Bank: Codable {
    var id, bankName, bankCode: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case bankName, bankCode
    }

    init(id: String?, bankName: String?, bankCode: String?) {
        self.id = id
        self.bankName = bankName
        self.bankCode = bankCode
    }
}
