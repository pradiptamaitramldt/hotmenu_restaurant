//
//  PostAccountDetailsRequestModel.swift
//  Restaurant
//
//  Created by Souvik on 05/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

class PostAccountDetailsRequestModel : Codable{
    var vendorId : String?
    var customerId : String?
    var bankCode : String?
    var accountNo : String?
    var businessName : String?
    
}
