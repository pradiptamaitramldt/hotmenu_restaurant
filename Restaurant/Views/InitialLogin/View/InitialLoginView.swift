//
//  InitialLoginView.swift
//  Restaurant
//
//  Created by Bit Mini on 07/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class InitialLoginView: BaseTableViewController {
    @IBOutlet weak var btn_LoginWithFacebook: RoundedButton!
    @IBOutlet weak var btn_LoginwithGmail: RoundedButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var textViewTerms: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.btn_LoginWithFacebook.setRoundedCornered(height: btn_LoginWithFacebook.frame.size.height)
        self.btn_LoginwithGmail.setRoundedCornered(height: btn_LoginwithGmail.frame.size.height)
        backView.layer.shadowOpacity = 0.3
        backView.layer.shadowOffset = CGSize(width: 0, height: 0)
        backView.layer.shadowRadius = 4.0
        backView.layer.cornerRadius = 2.0
        self.setLinkToWebView()

    }
    // Mark: IBActions -
    
    @IBAction func btn_SignUpDidClick(_ sender: Any) {
    }
    
    @IBAction func btn_LoginClick(_ sender: Any) {
//        let vc = self.storyboard?.instantiateViewController(identifier: "loginWithEmailView") as! LoginWithEmailView
//        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btn_GuestuserClick(_ sender: Any) {
        let mainStoryBoard = UIStoryboard(name: "Home", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "homeSceenNavigationController")// as! HomeSceenNavigationController
        self.present(redViewController, animated: true, completion: nil)

    }
    
    @IBAction func loginWithFacebook_Click(_ sender: Any) {
    }
    
    @IBAction func btn_LoginWithGmailClick(_ sender: Any) {
    }
    
    
    
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 80, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 80, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 80, currentScrenHeight: self.view.bounds.height)
        case 3:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 69, currentScrenHeight: self.view.bounds.height)
        case 4:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 100, currentScrenHeight: self.view.bounds.height)
        case 5:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 100, currentScrenHeight: self.view.bounds.height)
        case 6:
            return 253.0
        default:
            return 0.0
        }
    }

    
    func setLinkToWebView(){
        let grcolor = #colorLiteral(red: 0.6588235294, green: 0.768627451, blue: 0.4784313725, alpha: 1)

        let attributedString = NSMutableAttributedString(string: "By continuing you agree to our T&Cs. Please also check out our Privacy Policy.\n\n We use your data to offer you a personalised experience and to better understand and improve our services. For more information see here.", attributes: [NSAttributedString.Key.font: UIFont(name: "CeraPro-Medium", size: 13)!,
        NSAttributedString.Key.foregroundColor:UIColor.black])
        let linkRange1 = (attributedString.string as NSString).range(of: "T&Cs.")
        attributedString.addAttribute(NSAttributedString.Key.link, value: "https://www.google.com", range: linkRange1)
        
        let linkRange2 = (attributedString.string as NSString).range(of: "Privacy Policy.")
        attributedString.addAttribute(NSAttributedString.Key.link, value: "https://www.yahoo.com", range: linkRange2)

        let linkRange3 = (attributedString.string as NSString).range(of: "see here.")
        attributedString.addAttribute(NSAttributedString.Key.link, value: "https://www.youtube.com", range: linkRange3)

        let linkAttributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.foregroundColor: grcolor
        ]

        // textView is a UITextView
        let fullAttributedString = NSMutableAttributedString()
        fullAttributedString.append(attributedString)

        textViewTerms.linkTextAttributes = linkAttributes
        textViewTerms.attributedText = fullAttributedString
        textViewTerms.isUserInteractionEnabled = true
        textViewTerms.isEditable = false

        
//        let plainAttributedString = NSMutableAttributedString(string: "By continuing you agree to our ", attributes: [NSAttributedString.Key.font: UIFont(name: "ceraPro-Regular", size: 17)!,
//        NSAttributedString.Key.foregroundColor:UIColor.black])
//        let string = "T&Cs."
//        let attributedLinkString = NSMutableAttributedString(string: string, attributes:[NSAttributedString.Key.link: URL(string: "http://www.google.com")!, NSAttributedString.Key.font: UIFont(name: "ceraPro-Regular", size: 17)!/*,NSAttributedString.Key.foregroundColor:UIColor.red(red: 168.0, green: 198.0, blue: 122.0, alpha: 1.0)*/])
//        let fullAttributedString = NSMutableAttributedString()
//        fullAttributedString.append(plainAttributedString)
//        fullAttributedString.append(attributedLinkString)
////        attributedLabel.isUserInteractionEnabled = true
////        attributedLabel.attributedText = fullAttributedString
//        textViewTerms.isUserInteractionEnabled = true
//        textViewTerms.isEditable = false
//        textViewTerms.attributedText = fullAttributedString
//        
//        
//         let text = NSMutableAttributedString(string: "I agree to and accept A-Team's ")
//         text.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 12), range: NSMakeRange(0, text.length))
//         let selectablePart = NSMutableAttributedString(string: "Terms and Privacy Policy")
//         selectablePart.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 12), range: NSMakeRange(0, selectablePart.length))
//         // Add an underline to indicate this portion of text is selectable (optional)
//         selectablePart.addAttribute(NSUnderlineStyleAttributeName, value: 1, range: NSMakeRange(0,selectablePart.length))
//         selectablePart.addAttribute(NSUnderlineColorAttributeName, value: UIColor.black, range: NSMakeRange(0, selectablePart.length))
//         // Add an NSLinkAttributeName with a value of an url or anything else
//         selectablePart.addAttribute(NSLinkAttributeName, value: "http://www.google.com", range: NSMakeRange(0,selectablePart.length))
//         // Combine the non-selectable string with the selectable string
//         text.append(selectablePart)
//         // Center the text (optional)
//         let paragraphStyle = NSMutableParagraphStyle()
//         paragraphStyle.alignment = NSTextAlignment.center
//         text.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, text.length))
//         // To set the link text color (optional)
//         text_ViewTerms.linkTextAttributes = [NSForegroundColorAttributeName:#colorLiteral(red: 0.1736440659, green: 0.3124960065, blue: 0.5841408968, alpha: 1), NSFontAttributeName: UIFont.systemFont(ofSize: 12)]
//         // Set the text view to contain the attributed text
//         text_ViewTerms.attributedText = text
//         // Disable editing, but enable selectable so that the link can be selected
//         text_ViewTerms.isEditable = false
//         text_ViewTerms.isSelectable = true
//         // Set the delegate in order to use textView(_:shouldInteractWithURL:inRange)
//         text_ViewTerms.delegate = self

     }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//extension RegistrationViewController : UITextViewDelegate{
//
//   func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
//
//       if let url = NSURL(string: "http://www.google.com"){
//           UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
//       }
//       return false
//   }
//}
//
//extension UIApplication {
//   var statusBarView: UIView? {
//       return value(forKey: "statusBar") as? UIView
//   }
//}
