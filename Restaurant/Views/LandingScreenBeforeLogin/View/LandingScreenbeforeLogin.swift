//
//  LandingScreenbeforeLogin.swift
//  Restaurant
//
//  Created by Bit Mini on 07/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class LandingScreenbeforeLogin: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
            let vc = self.storyboard?.instantiateViewController(identifier: "navigationControllerBeforeLogin")
            self.present(vc!, animated: true, completion: nil)
        }
    }
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
