//
//  GetAllMenuItemsResponseModel.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let getAllMenuItemsResponseModel = try? newJSONDecoder().decode(GetAllMenuItemsResponseModel.self, from: jsonData)

import Foundation

// MARK: - GetAllMenuItemsResponseModel
class GetAllMenuItemsResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: GetAllMenuItemsResponse?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: GetAllMenuItemsResponse?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class GetAllMenuItemsResponse: Codable {
    let itemlist: [Itemlist]?

    init(itemlist: [Itemlist]?) {
        self.itemlist = itemlist
    }
}

// MARK: - Itemlist
class Itemlist: Codable {
    let item: MenuItem?
    let itemExtra: [ItemExtra]?

    init(item: MenuItem?, itemExtra: [ItemExtra]?) {
        self.item = item
        self.itemExtra = itemExtra
    }
}

// MARK: - Item
class MenuItem: Codable {
    let isActive: Bool?
    let id, itemName, categoryID, vendorID: String?
    let type: String?
    let price:Float?
    let waitingTime: Int?
    let menuImage, createdAt, updatedAt: String?
    let v: Int?

    enum CodingKeys: String, CodingKey {
        case isActive
        case id = "_id"
        case itemName
        case categoryID = "categoryId"
        case vendorID = "vendorId"
        case type, price, waitingTime, menuImage, createdAt, updatedAt
        case v = "__v"
    }

    init(isActive: Bool?, id: String?, itemName: String?, categoryID: String?, vendorID: String?, type: String?, price: Float?, waitingTime: Int?, menuImage: String?, createdAt: String?, updatedAt: String?, v: Int?) {
        self.isActive = isActive
        self.id = id
        self.itemName = itemName
        self.categoryID = categoryID
        self.vendorID = vendorID
        self.type = type
        self.price = price
        self.waitingTime = waitingTime
        self.menuImage = menuImage
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.v = v
    }
}

// MARK: - ItemExtra
class ItemExtra: Codable {
    var id, itemName: String?
    var price: Double?
    var isActive: Bool?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case itemName, price, isActive
    }

    init(id: String?, itemName: String?, price: Double?, isActive : Bool?) {
        self.id = id
        self.itemName = itemName
        self.price = price
        self.isActive = isActive
    }
}
