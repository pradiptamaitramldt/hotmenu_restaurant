//
//  GetPreOptionsModel.swift
//  Restaurant
//
//  Created by Souvik on 16/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

// MARK: - GetPreOptionsModel
class GetPreOptionsModel: Codable {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: GetPreOptionsResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: GetPreOptionsResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class GetPreOptionsResponseData: Codable {
    var options: [OptionNames]?

    init(options: [OptionNames]?) {
        self.options = options
    }
}

// MARK: - Option
class OptionNames: Codable {
    var name: String?

    init(name: String?) {
        self.name = name
    }
}
