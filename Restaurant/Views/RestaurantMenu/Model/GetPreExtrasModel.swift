//
//  GetPreExtrasModel.swift
//  Restaurant
//
//  Created by Souvik on 22/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

import Foundation

// MARK: - GetPreExtrasModel
class GetPreExtrasModel: Codable {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: GetPreExtrasResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: GetPreExtrasResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class GetPreExtrasResponseData: Codable {
    var extras: [OptionNames]?

    init(extras: [OptionNames]?) {
        self.extras = extras
    }
}

