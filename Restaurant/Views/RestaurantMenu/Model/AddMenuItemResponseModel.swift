//
//  AddMenuItemResponseModel.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

// MARK: - AddMenuItemResponseModel
class AddMenuItemResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: [AddMenuItemResponse]?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: [AddMenuItemResponse]?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseDatum
class AddMenuItemResponse: Codable {
    let isActive: Bool?
    let id, itemID, itemName, responseDatumDescription: String?
    let ingredients, recipe: String?
    let price: Float?
    let v: Int?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case isActive
        case id = "_id"
        case itemID = "itemId"
        case itemName
        case responseDatumDescription = "description"
        case ingredients, recipe, price
        case v = "__v"
        case createdAt, updatedAt
    }

    init(isActive: Bool?, id: String?, itemID: String?, itemName: String?, responseDatumDescription: String?, ingredients: String?, recipe: String?, price: Float?, v: Int?, createdAt: String?, updatedAt: String?) {
        self.isActive = isActive
        self.id = id
        self.itemID = itemID
        self.itemName = itemName
        self.responseDatumDescription = responseDatumDescription
        self.ingredients = ingredients
        self.recipe = recipe
        self.price = price
        self.v = v
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}
