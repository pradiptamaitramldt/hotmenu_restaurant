//
//  GetMenuDetailsModel.swift
//  Restaurant
//
//  Created by Souvik on 08/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let getMenuDetailsModel = try? newJSONDecoder().decode(GetMenuDetailsModel.self, from: jsonData)

import Foundation

// MARK: - GetMenuDetailsModel
class GetMenuDetailsModel: Codable {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: GetMenuDetailResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: GetMenuDetailResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
    
}

// MARK: - ResponseData
class GetMenuDetailResponseData: Codable {
    var item: MenuFoodItem?
    var itemExtra: [ItemExtra]?
    
    init() {
        
    }

    init(item: MenuFoodItem?, itemExtra: [ItemExtra]?) {
        self.item = item
        self.itemExtra = itemExtra
    }
}

// MARK: - Item
class MenuFoodItem: Codable {
    var itemName, categoryID, vendorID, type: String?
    var price: Double?
    var waitingTime: Int?
    var menuImage: String?
    var itemOptions: [ItemOption]?
    var discountType: String?
    var discountAmount: Double?
    var validityFrom, validityTo: String?
    var isActive: Bool?
    var imageURL: String?
    var id: String?
    var itemExtra: [ItemExtra]?

    enum CodingKeys: String, CodingKey {
        case itemName
        case categoryID = "categoryId"
        case vendorID = "vendorId"
        case type, price, waitingTime, menuImage, itemOptions, discountType, discountAmount, validityFrom, validityTo, isActive
        case imageURL = "imageUrl"
        case id = "_id"
        case itemExtra
    }

    init(itemName: String?, categoryID: String?, vendorID: String?, type: String?, price: Double?, waitingTime: Int?, menuImage: String?, itemOptions: [ItemOption]?, discountType: String?, discountAmount: Double?, validityFrom: String?, validityTo: String?, isActive: Bool?, imageURL: String?, id: String?, itemExtra: [ItemExtra]?) {
        self.itemName = itemName
        self.categoryID = categoryID
        self.vendorID = vendorID
        self.type = type
        self.price = price
        self.waitingTime = waitingTime
        self.menuImage = menuImage
        self.itemOptions = itemOptions
        self.discountType = discountType
        self.discountAmount = discountAmount
        self.validityFrom = validityFrom
        self.validityTo = validityTo
        self.isActive = isActive
        self.imageURL = imageURL
        self.id = id
        self.itemExtra = itemExtra
    }
}

