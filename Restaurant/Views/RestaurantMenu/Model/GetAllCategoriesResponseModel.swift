//
//  GetAllCategoriesResponseModel.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation
import Foundation

// MARK: - GetAllCategoriesResponseModel
class GetAllCategoriesResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: [AllCategoriesResponseData]?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: [AllCategoriesResponseData]?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseDatum
class AllCategoriesResponseData: Codable {
    let id, name: String?
    let image: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name, image
    }
}
