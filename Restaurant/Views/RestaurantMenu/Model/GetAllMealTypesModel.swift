//
//  GetAllMealTypesModel.swift
//  Restaurant
//
//  Created by Pradipta Maitra on 19/10/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation

// MARK: - AllMealTypeResponseModel
struct AllMealTypeResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: [MealTypeResponseDatum]?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseDatum
struct MealTypeResponseDatum: Codable {
    let id, type: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case type
    }
}
