//
//  GetAllMenuListModel.swift
//  Restaurant
//
//  Created by Souvik on 04/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//




import Foundation

// MARK: - GetAllMenuListModel
class GetAllMenuListModel: Codable {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: GetAllMenuLisResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: GetAllMenuLisResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class GetAllMenuLisResponseData: Codable {
    var categoryList: [CategoryList]?
    var itemlist: [FoodItemlist]?
    
    init() {
        
    }
    init(categoryList: [CategoryList]?, itemlist: [FoodItemlist]?) {
        self.categoryList = categoryList
        self.itemlist = itemlist
    }
}

// MARK: - CategoryList
class CategoryList: Codable {
    var categoryName, id: String?
    var inItemCount: Int?

    enum CodingKeys: String, CodingKey {
        case categoryName
        case id = "_id"
        case inItemCount
    }

    init(categoryName: String?, id: String?, inItemCount: Int?) {
        self.categoryName = categoryName
        self.id = id
        self.inItemCount = inItemCount
    }
}

// MARK: - Itemlist
class FoodItemlist: Codable {
    var category: Category?
    var items: [FoodItem]?

    init(category: Category?, items: [FoodItem]?) {
        self.category = category
        self.items = items
    }
}

// MARK: - Category
class Category: Codable {
    var id, categoryName: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case categoryName
    }

    init(id: String?, categoryName: String?) {
        self.id = id
        self.categoryName = categoryName
    }
}

// MARK: - Item
class FoodItem: Codable {
    
    let isActive: Bool?
    let id, itemName, categoryID: String?
    let mealTypeID: MealTypeID?
    let type: String?
    let price, waitingTime: Int?
    let menuImage: String?
    let imageURL: String?
    let itemOptions: [ItemOption]?
    let discountType: String?
    let discountAmount: Int?
    let validityFrom, validityTo, createdAt, updatedAt: String?
    let itemExtras: [ItemExtra]?

    enum CodingKeys: String, CodingKey {
        case isActive
        case id = "_id"
        case itemName
        case categoryID = "categoryId"
        case type, price, waitingTime, menuImage, validityFrom, validityTo, createdAt, updatedAt, itemExtras, itemOptions, discountType, discountAmount
        case mealTypeID = "mealTypeId"
        case imageURL = "imageUrl"
    }

    init(isActive: Bool?, id: String?, itemName: String?, categoryID: String?, type: String?, price: Int?, waitingTime: Int?, menuImage: String?, validityFrom: String?, validityTo: String?, createdAt: String?, updatedAt: String?, itemExtras: [ItemExtra]?, itemOptions: [ItemOption]?, discountType: String?, discountAmount: Int?, mealTypeID: MealTypeID?, imageURL: String?) {
        self.isActive = isActive
        self.id = id
        self.itemName = itemName
        self.categoryID = categoryID
        self.type = type
        self.price = price
        self.waitingTime = waitingTime
        self.menuImage = menuImage
        self.validityFrom = validityFrom
        self.validityTo = validityTo
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.itemExtras = itemExtras
        self.itemOptions = itemOptions
        self.discountType = discountType
        self.discountAmount = discountAmount
        self.mealTypeID = mealTypeID
        self.imageURL = imageURL
    }
}

// MARK: - ItemOption
struct ItemOption: Codable {
    var arrOptions: [ArrOption]
    let optionTitle: String?
    let isActive: IsActive?
}

// MARK: - ArrOption
struct ArrOption: Codable {
    var name: String?
    var isActive: Bool?
}

// MARK: - MealTypeID
struct MealTypeID: Codable {
    let id, type: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case type
    }
}

enum IsActive: Codable {
    case bool(Bool)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Bool.self) {
            self = .bool(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(IsActive.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for IsActive"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .bool(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}
