//
//  RestaurentMenuPresenter.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit


class SubMenuItemToAdd : Codable {
    var name : String = ""
    var price : String = ""
    var isActive : Bool = false
    init() {
        
    }
    
    init(name : String, price : String, isActive : Bool) {
        self.name = name
        self.price = price
        self.isActive = isActive
    }
}

class Option : Codable {
    var optionTitle : String = ""
    var isActive : Bool = true
    var arrOptions : [ArrOption] = []

    init() {
        
    }
    
    init(optionTitle : String, arrOptions : [ArrOption]) {
          self.optionTitle = optionTitle
          self.arrOptions = arrOptions
      }
}

class OptionsItemToAdd : Codable {
    var name : String = ""
    var isActive : Bool = false
    init() {
        
    }
    
    init(name : String, isActive : Bool) {
        self.name = name
        self.isActive = isActive
    }
}


class RestaurentMenuPresenter: NSObject {
    
    var arrAllCategories : [AllCategoriesResponseData] = []
    var arrSubMenuItems : [SubMenuItemToAdd] = []
    var selectedCategoryID : String = ""
    var selectedMealTyeID : String = ""
    var selectedItemType : String = "NON VEG"
    var objAllMenuItemsHandler = GetAllMenuLisResponseData()
    var arrAllOptionItems : [ItemOption] = []
    var discountType = "FLAT"
    var objSelectedMenuForEdit = GetMenuDetailResponseData()
    var arrOptionNames : [OptionNames] = []
    var arrExtrasNames : [OptionNames] = []
    var arrMealTypes : [MealTypeResponseDatum] = []
    
    func getAllFoodCategories(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
//        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getAllCategories, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: nil, httpMethodType: .get) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetAllCategoriesResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.arrAllCategories.removeAll()
                            self.arrAllCategories = (resData?.responseData!)!
                            print("arrAllCategories",self.arrAllCategories)
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)

            }
        }
    }
//    func getAllPossibleResturantTypes(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
//        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getRestaurantTypes, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: nil, httpMethodType: .post) { (data, err) in
//            if let responseData = data{
//                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetRestaurantTypesModel.self) { (resData, err) in
//                    if resData != nil{
//                        if resData?.statuscode! == ResponseCode.success.rawValue{
//                            self.arrAllCategories = (resData?.responseData?.vendorTypes)!
//                            completion(true,(resData?.message!)!)
//                        }else{
//                            completion(false,(resData?.message!)!)
//                        }
//                    }else{
//                        completion(false, err!.localizedDescription)
//                    }
//                }
//            }
//        }
//    }

    
    func getAllAddedMenuItems(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId" : UserDefaultValues.vendorID]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getAllMenuItem, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetAllMenuListModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.objAllMenuItemsHandler = ((resData?.responseData!)!)
                            var itemTotalCount = 0
                            for cat in self.objAllMenuItemsHandler.itemlist ?? []{
                                itemTotalCount += cat.items?.count ?? 0
                            }
                            self.objAllMenuItemsHandler.categoryList?.insert(CategoryList(categoryName: "All", id: "", inItemCount: itemTotalCount), at: 0)
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
            }
        }
    }
   
    func getAllMealTypes(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
    //        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!]
            WebServiceManager.shared.requestAPI(url: WebServiceConstants.getAllMealTypes, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: nil, httpMethodType: .get) { (data, err) in
                if let responseData = data{
                    let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: AllMealTypeResponseModel.self) { (resData, err) in
                        if resData != nil{
                            if resData?.statuscode! == ResponseCode.success.rawValue{
                                self.arrMealTypes.removeAll()
                                self.arrMealTypes = (resData?.responseData!)!
                                print("arrMealTypes",self.arrMealTypes)
                                completion(true,(resData?.message!)!)
                            }else{
                                completion(false,(resData?.message!)!)
                            }
                        }else{
                            completion(false, err!.localizedDescription)
                        }
                    }
                }else{
                    completion(false, ErrorInfo.netWorkError.rawValue)

                }
            }
        }
    
    func getAllPreOptions(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId" : UserDefaultValues.vendorID]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getPreOptions, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetPreOptionsModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.arrOptionNames = resData?.responseData?.options ?? []
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
                
            }
        }
    }

    func getAllPreExtras(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId" : UserDefaultValues.vendorID]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getPreExtras, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetPreExtrasModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.arrExtrasNames = resData?.responseData?.extras ?? []
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
            }
        }
    }

    func deleteMenuItem(itemId : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId" : UserDefaultValues.vendorID, "itemId" : itemId]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.deleteMenuItem, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
                
            }
        }
    }
    
    func addMenuItem(itemName : String, itemCategoryID : String, mealTypeID: String, itemImage : UIImage?, type : String, waitingTime : String, price : String, discountAmount : String, validFrom : String, validTO : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void ){
        
        print("mealTypeID",mealTypeID)
        var extraItemString = ""
        let jsonEncoder = JSONEncoder()
        do{
            let jsonData = try jsonEncoder.encode(self.arrSubMenuItems)
            extraItemString = String(data: jsonData, encoding: .utf8)!//String(data: jsonData, encoding: String.Encoding.utf8)
            print(extraItemString)
            
        }catch{
            
        }
        
        
        var extraItemOtionString = ""
        do{
            let jsonData = try jsonEncoder.encode(self.arrAllOptionItems)
            extraItemOtionString = String(data: jsonData, encoding: .utf8)!//String(data: jsonData, encoding: String.Encoding.utf8)
            print(extraItemOtionString)
            
        }catch{
            
        }
        
        if self.discountType != ""{
            if self.discountType != "NONE"{
                if validFrom.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                    if validTO.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                        if discountAmount.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                            
                            if itemImage != nil{
                                if itemName.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                    if itemCategoryID.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                        if mealTypeID.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                            if type.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                                if price.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                                    if waitingTime.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                                        print("mealTypeID",mealTypeID)
                                                        let param : [String: Any] = ["mealTypeId" : mealTypeID,
                                                                                     "categoryId" : itemCategoryID,
                                                                                     "itemName" : itemName,
                                                                                     "type" : type,
                                                                                     "waitingTime" : waitingTime,
                                                                                     "menuImage" : itemImage!,
                                                                                     "price" : price,
                                                                                     "itemExtra"  : extraItemString,
                                                                                     "itemOption" : extraItemOtionString,
                                                                                     "discountType" : self.discountType,
                                                                                     "discountAmount" : discountAmount,
                                                                                     "customerId"     : UserDefaultValues.userID!,
                                                                                     "validityFrom" : validFrom,
                                                                                     "validityTo" : validTO]
                                                        WebServiceManager.shared.requestMultipartAPIForTwoImages(url: WebServiceConstants.addMenuItem, parameter: param, httpMethodType: .post, isAuthorized: false, authToken: UserDefaultValues.authToken) { (data, err) in
                                                            if let responseData = data{
                                                                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                                                                    if resData != nil{
                                                                        if resData?.statuscode! == ResponseCode.success.rawValue{
                                                                            
                                                                            completion(true,(resData?.message!)!)
                                                                        }else{
                                                                            completion(false,(resData?.message!)!)
                                                                        }
                                                                    }else{
                                                                        completion(false, err!.localizedDescription)
                                                                    }
                                                                }
                                                            }else{
                                                                completion(false, "Request Timeout.")
                                                            }
                                                        }
                                                        
                                                    }else{
                                                        completion(false, "Please add Estimated Preperation time in minutes.")
                                                    }
                                                    
                                                    
                                                }else{
                                                    completion(false, "Please add item price")
                                                    
                                                }
                                            }else{
                                                completion(false, "Please choose item type")
                                                
                                            }
                                        }else{
                                            completion(false, "Please choose Meal type")
                                        }
                                        
                                    }else{
                                        completion(false, "Please choose item Category")
                                        
                                    }
                                }else{
                                    completion(false, "Please choose item Name")
                                    
                                }
                            }else{
                                completion(false, "Please add Food item image.")
                            }
                        }else{
                            completion(false, "Please add discount amount or discount percentage")
                        }
                    }else{
                        completion(false, "Please add date of discount valid to.")
                    }
                    
                }else{
                        completion(false, "Please add date of discount valid from.")
                    }
            }else{
                if itemImage != nil{
                    if itemName.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                        if itemCategoryID.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                            if type.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                if price.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                    if waitingTime.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                        let param : [String: Any] = ["mealTypeId" : mealTypeID,
                                                                     "categoryId" : itemCategoryID,
                                                                     "itemName" : itemName,
                                                                     "type" : type,
                                                                     "waitingTime" : waitingTime,
                                                                     "menuImage" : itemImage!,
                                                                     "price" : price,
                                                                     "itemExtra"  : extraItemString,
                                                                     "itemOption" : extraItemOtionString,
                                                                     "discountType" : self.discountType,
                                                                     "discountAmount" : discountAmount,
                                                                     "customerId"     : UserDefaultValues.userID!,
                                                                     "validityFrom" : validFrom,
                                                                     "validityTo" : validTO]
                                        WebServiceManager.shared.requestMultipartAPIForTwoImages(url: WebServiceConstants.addMenuItem, parameter: param, httpMethodType: .post, isAuthorized: false, authToken: UserDefaultValues.authToken) { (data, err) in
                                            if let responseData = data{
                                                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                                                    if resData != nil{
                                                        if resData?.statuscode! == ResponseCode.success.rawValue{
                                                            
                                                            completion(true,(resData?.message!)!)
                                                        }else{
                                                            completion(false,(resData?.message!)!)
                                                        }
                                                    }else{
                                                        completion(false, err!.localizedDescription)
                                                    }
                                                }
                                            }else{
                                                completion(false, "Request Timeout.")
                                            }
                                        }
                                        
                                    }else{
                                        completion(false, "Please add Estimated Preperation time in minutes.")
                                    }
                                    
                                    
                                }else{
                                    completion(false, "Please add item price")
                                    
                                }
                            }else{
                                completion(false, "Please choose item type")
                                
                            }
                        }else{
                            completion(false, "Please choose item Category")
                            
                        }
                    }else{
                        completion(false, "Please choose item Name")
                        
                    }
                }else{
                    completion(false, "Please add Food item image.")
                }

            }
        }else{
            completion(false, "Please choose discount Type")

        }
        

    }
    
    
    func editMenuItem(itemId : String, itemName : String, itemCategoryID : String, itemImage : UIImage?, type : String, waitingTime : String, price : String, discountAmount : String, validFrom : String, validTO : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void ){
        var extraItemString = ""
        let jsonEncoder = JSONEncoder()
        do{
            let jsonData = try jsonEncoder.encode(self.arrSubMenuItems)
            extraItemString = String(data: jsonData, encoding: .utf8)!//String(data: jsonData, encoding: String.Encoding.utf8)
            print(extraItemString)
            
        }catch{
            
        }
        
        
        var extraItemOtionString = ""
        do{
            let jsonData = try jsonEncoder.encode(self.arrAllOptionItems)
            extraItemOtionString = String(data: jsonData, encoding: .utf8)!//String(data: jsonData, encoding: String.Encoding.utf8)
            print(extraItemOtionString)
            
        }catch{
            
        }
        
        
        if self.discountType != ""{
            if self.discountType != "NONE"{
                if validFrom.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                    if validTO.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                        if discountAmount.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                            
                            if itemImage != nil{
                                if itemName.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                    if itemCategoryID.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                        if type.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                            if price.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                                if waitingTime.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                                    let param : [String: Any] = ["mealTypeId" : UserDefaultValues.vendorID,
                                                                                 "categoryId" : itemCategoryID,
                                                                                 "itemName" : itemName,
                                                                                 "type" : type,
                                                                                 "waitingTime" : waitingTime,
                                                                                 "menuImage" : itemImage!,
                                                                                 "price" : price,
                                                                                 "itemExtra"  : extraItemString,
                                                                                 "itemOption" : extraItemOtionString,
                                                                                 "discountType" : self.discountType,
                                                                                 "discountAmount" : discountAmount,
                                                                                 "customerId"     : UserDefaultValues.userID!,
                                                                                 "validityFrom" : validFrom,
                                                                                 "validityTo" : validTO,
                                                                                 "itemId" : itemId]
                                                    WebServiceManager.shared.requestMultipartAPIForTwoImages(url: WebServiceConstants.editMenuItem, parameter: param, httpMethodType: .post, isAuthorized: false, authToken: UserDefaultValues.authToken) { (data, err) in
                                                        if let responseData = data{
                                                            let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                                                                if resData != nil{
                                                                    if resData?.statuscode! == ResponseCode.success.rawValue{
                                                                        
                                                                        completion(true,(resData?.message!)!)
                                                                    }else{
                                                                        completion(false,(resData?.message!)!)
                                                                    }
                                                                }else{
                                                                    completion(false, err!.localizedDescription)
                                                                }
                                                            }
                                                        }else{
                                                            completion(false, "Request Timeout.")
                                                        }
                                                    }
                                                    
                                                }else{
                                                    completion(false, "Please add Estimated Preperation time in minutes.")
                                                }
                                                
                                                
                                            }else{
                                                completion(false, "Please add item price")
                                                
                                            }
                                        }else{
                                            completion(false, "Please choose item type")
                                            
                                        }
                                    }else{
                                        completion(false, "Please choose item Category")
                                        
                                    }
                                }else{
                                    completion(false, "Please choose item Name")
                                    
                                }
                            }else{
                                completion(false, "Please add Food item image.")
                            }
                        }else{
                            completion(false, "Please add discount amount or discount percentage")
                        }
                    }else{
                        completion(false, "Please add date of discount valid to.")
                    }
                    
                }else{
                        completion(false, "Please add date of discount valid from.")
                    }
            }else{
                if itemImage != nil{
                    if itemName.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                        if itemCategoryID.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                            if type.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                if price.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                    if waitingTime.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                                        let param : [String: Any] = ["mealTypeId" : UserDefaultValues.vendorID,
                                                                     "categoryId" : itemCategoryID,
                                                                     "itemName" : itemName,
                                                                     "type" : type,
                                                                     "waitingTime" : waitingTime,
                                                                     "menuImage" : itemImage!,
                                                                     "price" : price,
                                                                     "itemExtra"  : extraItemString,
                                                                     "itemOption" : extraItemOtionString,
                                                                     "discountType" : self.discountType,
                                                                     "discountAmount" : discountAmount,
                                                                     "customerId"     : UserDefaultValues.userID!,
                                                                     "validityFrom" : validFrom,
                                                                     "validityTo" : validTO,
                                                                     "itemId" : itemId]
                                        WebServiceManager.shared.requestMultipartAPIForTwoImages(url: WebServiceConstants.editMenuItem, parameter: param, httpMethodType: .post, isAuthorized: false, authToken: UserDefaultValues.authToken) { (data, err) in
                                            if let responseData = data{
                                                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                                                    if resData != nil{
                                                        if resData?.statuscode! == ResponseCode.success.rawValue{
                                                            
                                                            completion(true,(resData?.message!)!)
                                                        }else{
                                                            completion(false,(resData?.message!)!)
                                                        }
                                                    }else{
                                                        completion(false, err!.localizedDescription)
                                                    }
                                                }
                                            }else{
                                                completion(false, "Request Timeout.")
                                            }
                                        }
                                        
                                    }else{
                                        completion(false, "Please add Estimated Preperation time in minutes.")
                                    }
                                    
                                    
                                }else{
                                    completion(false, "Please add item price")
                                    
                                }
                            }else{
                                completion(false, "Please choose item type")
                                
                            }
                        }else{
                            completion(false, "Please choose item Category")
                            
                        }
                    }else{
                        completion(false, "Please choose item Name")
                        
                    }
                }else{
                    completion(false, "Please add Food item image.")
                }

            }
        }else{
            completion(false, "Please choose discount Type")

        }
    }
    
    func getMenuItemDetails(itemID : String, completion : @escaping(_ success : Bool ,_ message : String, _ itemDetail : GetMenuDetailResponseData?) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!, "vendorId" : UserDefaultValues.vendorID, "itemId" : itemID]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getMenuItem, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetMenuDetailsModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            completion(true,(resData?.message!)!, resData?.responseData)
                        }else{
                            completion(false,(resData?.message!)!, nil)
                        }
                    }else{
                        completion(false, err!.localizedDescription, nil)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue, nil)
                
            }
        }
    }
    
    func changeMenuItemStatus(itemID : String, status : Bool, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId" : UserDefaultValues.vendorID,
                                     "itemId" : itemID,
                                     "isActive" : status == true ? "true" : "false"]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.changeMenuItemStatus, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
                
            }
        }
    }

    
}
