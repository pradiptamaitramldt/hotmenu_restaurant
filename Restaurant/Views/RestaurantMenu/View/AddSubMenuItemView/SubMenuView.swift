//
//  SubMenuView.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
protocol SubMenuDelegate{
    func updateView()
    func showExtraAlert(message : String)
    func showOptionDropDownExtra(indexPath : IndexPath)

}

class SubMenuView: UIView {
    @IBOutlet weak var tableViewheight: NSLayoutConstraint!
    @IBOutlet weak var mainViewheight: NSLayoutConstraint!
    @IBOutlet weak var buttonSubmit: NormalBoldButton!
    @IBOutlet weak var tableViewSubMenu: UITableView!
    
    var presenter : RestaurentMenuPresenter?
    var delegate : SubMenuDelegate?
    override init(frame: CGRect) {
       super.init(frame: frame)
        loadViewFromNib ()
     }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }

    func loadViewFromNib() {
        
        let view = UINib(nibName: "SubMenuView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        
        view.frame = bounds
        
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
        self.buttonSubmit.setRoundedCornered(height: self.buttonSubmit.frame.size.height)
        self.tableViewSubMenu.dataSource = self
        self.tableViewSubMenu.delegate = self
        self.tableViewSubMenu.register(UINib(nibName: "AddSubMenuCell", bundle: nil), forCellReuseIdentifier: "addSubMenuCell")
    }
    
    func setupView(){
        self.tableViewheight.constant = CGFloat(Double((self.presenter?.arrSubMenuItems.count)!) * 50.0)
        self.mainViewheight.constant = self.tableViewheight.constant + 125.0

    }

    @IBAction func addnewItemClick(_ sender: Any) {
        for (index, _) in self.presenter!.arrSubMenuItems.enumerated(){
            let cell = self.tableViewSubMenu.cellForRow(at: IndexPath(row: index, section: 0)) as! AddSubMenuCell
            self.presenter!.arrSubMenuItems[index].name = cell.textFieldName.text!
            self.presenter!.arrSubMenuItems[index].price = cell.textFieldPrice.text!
        }
        self.presenter?.arrSubMenuItems.append(SubMenuItemToAdd(name: "", price: "", isActive : false))
        self.tableViewSubMenu.reloadData()
        self.tableViewheight.constant = CGFloat(Double((self.presenter?.arrSubMenuItems.count)!) * 50.0)
        self.mainViewheight.constant = self.tableViewheight.constant + 125.0

    }
    
    @IBAction func buttonCrossDidClick(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func button_SubmitDidClick(_ sender: Any) {
        
        var flag = true
        for (index, _) in self.presenter!.arrSubMenuItems.enumerated(){
            let cell = self.tableViewSubMenu.cellForRow(at: IndexPath(row: index, section: 0)) as! AddSubMenuCell
            if cell.button_checkBox.isSelected && cell.textFieldName.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "" && cell.textFieldPrice.text!.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                self.presenter!.arrSubMenuItems[index].price = cell.textFieldPrice.text!
                self.presenter!.arrSubMenuItems[index].name = cell.textFieldName.text!
                flag = true
            }else if cell.button_checkBox.isSelected && cell.textFieldName.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" && cell.textFieldPrice.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                self.delegate?.showExtraAlert(message: "Extra Item name and price should be entered or the Options should be Unchecked.")
                flag = false
                break
            }else if !cell.button_checkBox.isSelected &&  cell.textFieldName.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" && cell.textFieldPrice.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                flag = true
            }
            
            if cell.textFieldName.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "" && cell.textFieldPrice.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                self.delegate?.showExtraAlert(message: "Plese add item price")
                flag = false
                break
            }else if cell.textFieldName.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" && cell.textFieldPrice.text!.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                self.delegate?.showExtraAlert(message: "Plese add item Name")
                flag = false
                break
            }else if cell.textFieldName.text!.trimmingCharacters(in: .whitespacesAndNewlines) != "" && cell.textFieldPrice.text!.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                self.presenter!.arrSubMenuItems[index].price = cell.textFieldPrice.text!
                self.presenter!.arrSubMenuItems[index].name = cell.textFieldName.text!
                flag = true
            }
        }
        if flag{
            self.presenter!.arrSubMenuItems = self.presenter!.arrSubMenuItems.filter { ($0.name != "") }
            self.removeFromSuperview()
            self.delegate?.updateView()
            
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension SubMenuView : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.presenter?.arrSubMenuItems.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addSubMenuCell") as! AddSubMenuCell
        cell.indexPath = indexPath
        cell.presenter = presenter
        cell.objSubMenuItem = (self.presenter?.arrSubMenuItems[indexPath.row])!
        cell.delegate = self
        cell.setUpCellView()
        return cell
    }
    
}

extension SubMenuView : SubmenuCellDelegate{
    func showDropDown(indexPath: IndexPath) {
        self.delegate?.showOptionDropDownExtra(indexPath: indexPath)
        
    }
    
    func removeItem(indexPath: IndexPath) {
        if (self.presenter?.arrSubMenuItems.count)! > 0{
            self.presenter?.arrSubMenuItems.remove(at: indexPath.row)
            self.tableViewSubMenu.reloadData()
            self.tableViewheight.constant = CGFloat(Double((self.presenter?.arrSubMenuItems.count)!) * 50.0)
            self.mainViewheight.constant = self.tableViewheight.constant + 125.0
        }
        
    }
    
    func addNewItem() {
        
    }
    
    
}
