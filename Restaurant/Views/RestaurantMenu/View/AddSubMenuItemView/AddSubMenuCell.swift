//
//  AddSubMenuCell.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
protocol SubmenuCellDelegate{
    func removeItem(indexPath : IndexPath)
    func addNewItem()
    func showDropDown(indexPath : IndexPath)

}

class AddSubMenuCell: UITableViewCell {
    @IBOutlet weak var button_checkBox: UIButton!
    @IBOutlet weak var textFieldName: CustomTextField!
    @IBOutlet weak var textFieldPrice: CustomTextField!
    
    @IBOutlet weak var buttonCross: UIButton!
    
    var indexPath : IndexPath?
    var objSubMenuItem = SubMenuItemToAdd()
    var presenter : RestaurentMenuPresenter?
    var delegate : SubmenuCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUpCellView(){
//        if indexPath?.row == (self.presenter?.arrSubMenuItems.count)! - 1 {
//            self.buttonCross.isHidden = false
//        }else{
//            self.buttonCross.isHidden = true
//
//        }
//        if (self.presenter?.arrSubMenuItems.count)! == 1{
//            self.buttonCross.isHidden = false
//        }
        
        self.button_checkBox.isSelected = (self.presenter?.arrSubMenuItems[indexPath!.row].isActive)!
        self.textFieldPrice.text = (self.presenter?.arrSubMenuItems[indexPath!.row].price)!
        self.textFieldName.text = (self.presenter?.arrSubMenuItems[indexPath!.row].name)!

    }
    @IBAction func buttonShowDropDownClick(_ sender: Any) {
        self.delegate?.showDropDown(indexPath: self.indexPath!)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func button_RemoveClick(_ sender: Any) {
        self.delegate?.removeItem(indexPath: self.indexPath!)
    }
    
    @IBAction func btnCheckBoxDidClick(_ sender: Any) {
        self.button_checkBox.isSelected = !self.button_checkBox.isSelected
        self.presenter?.arrSubMenuItems[indexPath!.row].isActive = self.button_checkBox.isSelected
    }
    
}
