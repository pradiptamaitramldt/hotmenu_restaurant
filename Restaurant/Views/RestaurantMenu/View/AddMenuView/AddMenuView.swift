//
//  AddMenuView.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

protocol AddMenuDelegate{
    func showImagePicker()
    func addmenuItem(itemName : String, itemCategoryID : String, mealTypeID: String, itemImage : UIImage?, type : String, waitingTime : String, price : String, discountAmount : String, validFrom : String, validTo : String)
    func showAddMessageAlert(message : String)
    func showAddHeaderAlert()
    func showAddOptionDropDown(indexPath : IndexPath)
    func deleteOptionTypeForAddMenu(index : Int)
    
}
/*
textFieldMenuName
 textFieldCategory
 textFieldMealType
   imageViewMenuItem
 buttonSubmit
 textFieldPrice
 textFieldTime
 textFieldChooseDiscountType
 labelNaira
 labelPercent
 textFieldValidFrom
 labelMenuItemName
 */
class AddMenuView: UIView {
    @IBOutlet weak var viewMenuName: UIView!
    @IBOutlet weak var viewChooseCategory: UIView!
    @IBOutlet weak var viewChooseMealType: UIView!
    @IBOutlet weak var textFieldMenuName: CustomTextField!
    @IBOutlet weak var textFieldCategory: CustomTextField!
    @IBOutlet weak var textFieldMealType: CustomTextField!
    @IBOutlet weak var viewUploadImage: UIView!
    @IBOutlet weak var imageViewMenuItem: UIImageView!
    @IBOutlet weak var buttonSubmit: NormalBoldButton!
    @IBOutlet weak var view_Price: UIView!
    @IBOutlet weak var textFieldPrice: CustomTextField!
    @IBOutlet weak var view_Time: UIView!
    @IBOutlet weak var textFieldTime: CustomTextField!
    @IBOutlet weak var viewChooseDiscountType: UIView!
    @IBOutlet weak var textFieldChooseDiscountType: CustomTextField!
    @IBOutlet weak var viewDiscountAmout: UIView!
    @IBOutlet weak var textFieldDiscountAmount: CustomTextField!
    @IBOutlet weak var labelNaira: UILabel!
    @IBOutlet weak var labelPercent: UILabel!
    @IBOutlet weak var viewMenuExtras: UIView!
    @IBOutlet weak var tableViewMenuExtras: UITableView!
    @IBOutlet weak var viewMenuOptions: UIView!
    @IBOutlet weak var tableViewMenuOptions: UITableView!
    @IBOutlet weak var viewValidFrom: UIView!
    @IBOutlet weak var textFieldValidFrom: CustomTextField!
    @IBOutlet weak var viewValidTo: UIView!
    @IBOutlet weak var tectFieldValidTo: CustomTextField!
    @IBOutlet weak var tableViewExtraheightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewOptionsHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var viewDIscountSection: UIView!
    @IBOutlet weak var discountSectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelMenuItemName: CustomBoldFontLabel!
    
    var pickerValidFrom : UIDatePicker?
    var pickerValidTo: UIDatePicker?
    
    var validFromDate: Date?
    var validToDate: Date?
    
    var categoryTypePicker : UIPickerView?
    var mealTypePicker : UIPickerView?
    var presenter : RestaurentMenuPresenter?
    var submenu : SubMenuView?
    var addOption : AddOptions?
    
    var delegate : AddMenuDelegate?
    var arrDiscountOptins = ["Flat Discount", "Discount in %", "Do Not Apply Discount"]
    var discountOptinsPicker : UIPickerView?
    var validFrom = ""
    var validTo = ""
    var defaultDIscountSectionHeight : CGFloat = 0.0
    var optionsPicker : UIPickerView?
    var extrasPicker : UIPickerView?
    
    var textfieldIndexPath : IndexPath?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadViewFromNib ()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadViewFromNib ()
        
    }
    
    func loadViewFromNib() {
        self.presenter?.selectedItemType = "NON VEG"
        let view = UINib(nibName: "AddMenuView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        
        view.frame = bounds
        
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
        let delayTime = DispatchTime.now() + 0.1
        print("one")
        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
            self.loadView()
            
        })
    }
    
    func loadView(){
        self.viewMenuName.setRoundedCornered(height: self.viewMenuName.frame.size.height)
        self.viewChooseCategory.setRoundedCornered(height: self.viewChooseCategory.frame.size.height)
        self.viewChooseMealType.setRoundedCornered(height: self.viewChooseMealType.frame.size.height)
        self.viewUploadImage.setRoundedCornered(height: self.viewUploadImage.frame.size.height)
        self.buttonSubmit.setRoundedCornered(height: self.buttonSubmit.frame.size.height)
        self.view_Time.setRoundedCornered(height: self.view_Time.frame.size.height)
        self.view_Price.setRoundedCornered(height: self.view_Price.frame.size.height)
        self.viewDiscountAmout.setRoundedCornered(height: self.viewDiscountAmout.frame.size.height)
        self.viewChooseDiscountType.setRoundedCornered(height: self.viewChooseDiscountType.frame.size.height)
        self.viewMenuExtras.setRoundedCornered(height: self.viewChooseDiscountType.frame.size.height)
        self.viewMenuOptions.setRoundedCornered(height: self.viewChooseDiscountType.frame.size.height)
        self.viewValidFrom.setRoundedCornered(height: self.viewChooseDiscountType.frame.size.height)
        self.viewValidTo.setRoundedCornered(height: self.viewChooseDiscountType.frame.size.height)
        self.presenter?.discountType = "NONE"
        self.createRestaurantTypePicker()
        self.createMealTypePicker()
        self.createDiscountTypePicker()
        pickerValidFrom = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 216))
        pickerValidFrom?.minimumDate = Date()
        
        pickerValidTo = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 216))
        
        self.createDatePicker(textField: textFieldValidFrom, picker: pickerValidFrom!)
        self.createDatePicker(textField: tectFieldValidTo, picker: pickerValidTo!)
        self.showAllDetails()
        self.tableViewMenuExtras.register(UINib(nibName: "MenuExtraItemCell", bundle: nil), forCellReuseIdentifier: "menuExtraItemCell")
        self.tableViewMenuExtras.dataSource = self
        self.tableViewMenuExtras.delegate = self
        self.tableViewMenuOptions.register(UINib(nibName: "MenuOptionsItemCell", bundle: nil), forCellReuseIdentifier: "menuOptionsItemCell")
        self.tableViewMenuOptions.dataSource = self
        self.tableViewMenuOptions.delegate = self
        self.defaultDIscountSectionHeight = CGFloat((self.viewChooseDiscountType.frame.size.height * 4) + 50.0)
        self.discountSectionHeightConstraint.constant = self.defaultDIscountSectionHeight
        
    }
    
    @IBAction func btnCrossDidClick(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func button_UploadImageClick(_ sender: Any) {
        self.delegate?.showImagePicker()
        
    }
    
    
    @IBAction func buttonAddExtraClick(_ sender: Any) {
        self.showAddSubMenuView()
    }
    @IBAction func addOptionsDidClick(_ sender: Any) {
        self.showAddOptionsView()
    }
    
    @IBAction func button_SubmitCLick(_ sender: Any) {
        self.delegate?.addmenuItem(itemName: self.textFieldMenuName.text!, itemCategoryID: self.presenter!.selectedCategoryID, mealTypeID: self.presenter?.selectedMealTyeID ?? "", itemImage: self.imageViewMenuItem.image, type: self.presenter!.selectedItemType, waitingTime: self.textFieldTime.text!, price: self.textFieldPrice.text!, discountAmount: self.textFieldDiscountAmount.text! == "" ? "0" : self.textFieldDiscountAmount.text!, validFrom: self.validFrom, validTo: self.validTo)
    }
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    
    func showAllDetails(){
        self.presenter?.arrAllOptionItems.removeAll()
        self.presenter?.arrSubMenuItems.removeAll()
        self.tableViewMenuOptions.reloadData()
        self.tableViewMenuExtras.reloadData()
        var totalItemCount = 0
        for item in self.presenter!.arrAllOptionItems{
            totalItemCount += item.arrOptions.count
        }
        let headerHeight = (Double(self.presenter!.arrAllOptionItems.count) * 40.0)
        self.tableViewOptionsHeightContraint.constant = CGFloat(headerHeight + ((Double(totalItemCount) * 30.0)))
        self.tableViewExtraheightConstraint.constant = CGFloat(Double((self.presenter?.arrSubMenuItems.count)!) * 30.0)
        
    }
    
    func showAddSubMenuView(){
        submenu = SubMenuView(frame: self.bounds)
        submenu?.presenter = self.presenter
        submenu?.setupView()
        submenu?.delegate = self
        self.addSubview(submenu!)
        self.bringSubviewToFront(submenu!)
        
    }
    
    func showAddOptionsView(){
        addOption = AddOptions(frame: self.bounds)
        addOption?.presenter = self.presenter
        addOption?.setupView()
        addOption?.delegate = self
        self.addSubview(addOption!)
        self.bringSubviewToFront(addOption!)
        
    }
    
    
    func createRestaurantTypePicker()
    {
        
        categoryTypePicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 216))
        categoryTypePicker?.dataSource = self
        categoryTypePicker?.delegate = self
        
        textFieldCategory.inputView = categoryTypePicker
        
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.frame.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionDone), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionCancel), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 44))
        view_Accessory.backgroundColor = UIColor(named: "TextColorDark")
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        textFieldCategory.inputAccessoryView = view_Accessory
        
    }
    
    func createMealTypePicker() {
        
        mealTypePicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 216))
        mealTypePicker?.dataSource = self
        mealTypePicker?.delegate = self
        
        textFieldMealType.inputView = mealTypePicker
        
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.frame.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionDoneMeal), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionCancelMeal), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 44))
        view_Accessory.backgroundColor = UIColor(named: "TextColorDark")
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        textFieldMealType.inputAccessoryView = view_Accessory
        
    }
    
    
    @objc func selectionDone(_ sender: UIButton ) {
        
        if textFieldCategory.isFirstResponder
        {
            textFieldCategory.text = String(describing: self.presenter!.arrAllCategories[(categoryTypePicker?.selectedRow(inComponent: 0))!].name!)
            self.presenter?.selectedCategoryID = String(describing: self.presenter!.arrAllCategories[(categoryTypePicker?.selectedRow(inComponent: 0))!].id!)
            
            textFieldCategory?.resignFirstResponder()
        }
    }
    /*
        {
            "_id" = 5f3b9d456978f831e4050bf1;
            type = Breakfast;
        },
        {
            "_id" = 5f3b9d456978f831e4050bf2;
            type = Lunch;
        },
        {
            "_id" = 5f3b9d456978f831e4050bf3;
            type = Dinner;
        },
        {
            "_id" = 5f3b9d456978f831e4050bf4;
            type = Snacks;
        }
        */
    @objc func selectionDoneMeal(_ sender: UIButton ) {
        
        if textFieldMealType.isFirstResponder
        {
            print("id",self.presenter!.arrMealTypes[(mealTypePicker?.selectedRow(inComponent: 0))!].id!)
            textFieldMealType.text = String(describing: self.presenter!.arrMealTypes[(mealTypePicker?.selectedRow(inComponent: 0))!].type!)
            self.presenter?.selectedMealTyeID = String(describing: self.presenter!.arrMealTypes[(mealTypePicker?.selectedRow(inComponent: 0))!].id!)
            
            textFieldMealType?.resignFirstResponder()
        }
    }
    
    @objc func selectionCancel(_ sender: UIButton ) {
        textFieldCategory.text = ""
        textFieldCategory.resignFirstResponder()
    }
    
    @objc func selectionCancelMeal(_ sender: UIButton ) {
        textFieldMealType.text = ""
        textFieldMealType.resignFirstResponder()
    }
    
    func createDiscountTypePicker()
    {
        
        discountOptinsPicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 216))
        discountOptinsPicker?.dataSource = self
        discountOptinsPicker?.delegate = self
        
        textFieldChooseDiscountType.inputView = discountOptinsPicker
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.frame.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionDone2), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionCancel2), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 44))
        view_Accessory.backgroundColor = UIColor(named: "TextColorDark")
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        textFieldChooseDiscountType.inputAccessoryView = view_Accessory
        
    }
    
    
    
    
    @objc func selectionDone2(_ sender: UIButton ) {
        
        if textFieldChooseDiscountType.isFirstResponder
        {
            textFieldChooseDiscountType.text = String(describing: self.arrDiscountOptins[(discountOptinsPicker?.selectedRow(inComponent: 0))!])
            textFieldChooseDiscountType?.resignFirstResponder()
            if (discountOptinsPicker?.selectedRow(inComponent: 0))! == 0{
                labelNaira.isHidden = false
                labelPercent.isHidden = true
                self.presenter?.discountType = "FLAT"
                self.discountSectionHeightConstraint.constant = self.defaultDIscountSectionHeight
            }else if(discountOptinsPicker?.selectedRow(inComponent: 0))! == 1{
                labelNaira.isHidden = true
                labelPercent.isHidden = false
                self.presenter?.discountType = "PERCENTAGE"
                self.discountSectionHeightConstraint.constant = self.defaultDIscountSectionHeight
            }else if (discountOptinsPicker?.selectedRow(inComponent: 0))! == 2{
                self.labelNaira.isHidden = true
                self.labelPercent.isHidden = false
                self.presenter?.discountType = "NONE"
                self.discountSectionHeightConstraint.constant = self.viewChooseDiscountType.frame.size.height
            }
        }
    }
    
    @objc func selectionCancel2(_ sender: UIButton ) {
        textFieldChooseDiscountType.text = ""
        textFieldChooseDiscountType.resignFirstResponder()
    }
    
    func createDatePicker(textField : UITextField, picker : UIDatePicker) {
        
        //toolbar
        //Toolbar for "Cancel" and "Done"
        
        textField.inputView = picker
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.frame.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionDateDone), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionDateCancel), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 44))
        view_Accessory.backgroundColor = UIColor(named: "TextColorDark")
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        textField.inputAccessoryView = view_Accessory
        textField.inputView = picker
        
        //format picker for date
        picker.datePickerMode = .date
        
    }
    
    
    @objc func selectionDateDone(_ sender: UIButton ) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        if textFieldValidFrom.isFirstResponder {
            textFieldValidFrom.text = formatter.string(from: pickerValidFrom!.date)
            self.validFrom = formatter.string(from: pickerValidFrom!.date)
            tectFieldValidTo?.becomeFirstResponder()
            self.validFromDate = pickerValidFrom!.date
            if self.validToDate != nil {
                if self.validFromDate! >= self.validToDate! {
                    self.validToDate = nil
                    self.tectFieldValidTo.text = ""
                    self.validTo = ""
                }
            }
            self.pickerValidTo?.minimumDate = self.validFromDate
        } else {
            tectFieldValidTo.text = formatter.string(from: pickerValidTo!.date)
            self.validTo = formatter.string(from: pickerValidTo!.date)
            tectFieldValidTo?.resignFirstResponder()
            self.validToDate = pickerValidTo!.date
            if self.validFromDate != nil {
                if self.validFromDate! >= self.validToDate! {
                    self.validFromDate = nil
                    self.textFieldValidFrom.text = ""
                    self.validFrom = ""
                }
            }
            self.pickerValidFrom?.maximumDate = self.validToDate
        }
    }
    
    
    @objc func selectionDateCancel(_ sender: UIButton ) {
        if textFieldValidFrom.isFirstResponder
        {
            textFieldValidFrom?.resignFirstResponder()
        }else if tectFieldValidTo.isFirstResponder{
            tectFieldValidTo.resignFirstResponder()
        }
    }
    
    
    func createOptionsPicker(textField : UITextField)
    {
        
        optionsPicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 216))
        optionsPicker?.dataSource = self
        optionsPicker?.delegate = self
        
        textField.inputView = optionsPicker
        
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.frame.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionDoneOption), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionCancelOption), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 44))
        view_Accessory.backgroundColor = UIColor(named: "TextColorDark")
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        textField.inputAccessoryView = view_Accessory
        
    }
    
    @objc func selectionDoneOption(_ sender: UIButton) {
        if self.textfieldIndexPath != nil{
            guard let cell = self.addOption?.tableViewSubMenu.cellForRow(at: self.textfieldIndexPath!) as! AddOptionsCell? else {
                return
            }
            cell.textFieldName.text = String(describing: (self.presenter?.arrOptionNames[(optionsPicker?.selectedRow(inComponent: 0))!].name)!)
            self.presenter?.arrAllOptionItems[self.textfieldIndexPath!.section].arrOptions[self.textfieldIndexPath!.row].name = String(describing: (self.presenter?.arrOptionNames[(optionsPicker?.selectedRow(inComponent: 0))!].name)!)
            cell.textFieldName.inputView = nil
            cell.textFieldName.inputAccessoryView = nil
            cell.textFieldName.resignFirstResponder()
        }
    }
    
    @objc func selectionCancelOption(_ sender: UIButton ) {
        guard let cell = self.addOption?.tableViewSubMenu.cellForRow(at: self.textfieldIndexPath!) as! AddOptionsCell? else {
            return
        }
        cell.textFieldName.inputView = nil
        cell.textFieldName.inputAccessoryView = nil
        cell.textFieldName.resignFirstResponder()
    }
    
    func createExtrasPicker(textField : UITextField)
    {
        
        extrasPicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 216))
        extrasPicker?.dataSource = self
        extrasPicker?.delegate = self
        
        textField.inputView = extrasPicker
        
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.frame.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionDoneExtra), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionCancelExtra), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 44))
        view_Accessory.backgroundColor = UIColor(named: "TextColorDark")
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        textField.inputAccessoryView = view_Accessory
        
    }
    
    
    
    
    @objc func selectionDoneExtra(_ sender: UIButton) {
        if self.textfieldIndexPath != nil{
            guard let cell = self.submenu?.tableViewSubMenu.cellForRow(at: self.textfieldIndexPath!) as! AddSubMenuCell? else {
                return
            }
            cell.textFieldName.text = String(describing: (self.presenter?.arrExtrasNames[(extrasPicker?.selectedRow(inComponent: 0))!].name)!)
            self.presenter?.arrSubMenuItems[self.textfieldIndexPath!.row].name = String(describing: (self.presenter?.arrExtrasNames[(extrasPicker?.selectedRow(inComponent: 0))!].name)!)
            cell.textFieldName.resignFirstResponder()
        }
    }
    
    @objc func selectionCancelExtra(_ sender: UIButton ) {
        guard let cell = self.submenu?.tableViewSubMenu.cellForRow(at: self.textfieldIndexPath!) as! AddSubMenuCell? else {
            return
        }
        cell.textFieldName.resignFirstResponder()
    }
    
    
    
    
    
    
}


extension AddMenuView : SubMenuDelegate{
    func showOptionDropDownExtra(indexPath: IndexPath) {
        let cell = self.submenu?.tableViewSubMenu.cellForRow(at: indexPath) as! AddSubMenuCell
        self.createExtrasPicker(textField: cell.textFieldName)
        self.textfieldIndexPath = indexPath
        cell.textFieldName.resignFirstResponder()
        cell.textFieldName.becomeFirstResponder()
        
    }
    
    func showExtraAlert(message: String) {
        self.delegate?.showAddMessageAlert(message: message)
        
    }
    
    func updateView() {
        self.tableViewMenuExtras.reloadData()
        self.tableViewExtraheightConstraint.constant = CGFloat(Double((self.presenter?.arrSubMenuItems.count)!) * 30.0)
    }
}

extension AddMenuView : AddoptionsDelegate{
    func deleteOptionType(index: Int) {
        self.delegate?.deleteOptionTypeForAddMenu(index : index)
    }
    
    func showOptionDropDown(indexPath: IndexPath) {
        //        self.delegate?.showAddOptionDropDown(indexPath: indexPath)
        let cell = self.addOption?.tableViewSubMenu.cellForRow(at: indexPath) as! AddOptionsCell
        self.createOptionsPicker(textField: cell.textFieldName)
        self.textfieldIndexPath = indexPath
        cell.textFieldName.resignFirstResponder()
        
        cell.textFieldName.becomeFirstResponder()
        
    }
    
    func addOptionHeaderTitle() {
        if self.addOption?.textFieldOptionType.text != ""{
//            self.presenter!.arrAllOptionItems.append(Option(optionTitle: (self.addOption?.textFieldOptionType.text!)!, arrOptions: []))
            self.presenter!.arrAllOptionItems.append(ItemOption(arrOptions: [], optionTitle: self.addOption?.textFieldOptionType.text, isActive: nil))
            self.addOption?.textFieldOptionType.text = ""
            self.addOption?.setupView()
        }else{
            DispatchQueue.main.async {
                self.showOptionAlert(message: "Option Name Can not be blank")
            }
        }
        
        //        self.delegate?.showAddHeaderAlert()
    }
    
    func showOptionAlert(message: String) {
        self.delegate?.showAddMessageAlert(message: message)
        
    }
    
    func updateOptionsView()
    {
        self.tableViewMenuOptions.reloadData()
        self.tableViewMenuExtras.reloadData()
        var totalItemCount = 0
        for item in self.presenter!.arrAllOptionItems{
            totalItemCount += item.arrOptions.count
        }
        let headerHeight = (Double(self.presenter!.arrAllOptionItems.count) * 40.0)
        self.tableViewOptionsHeightContraint.constant = CGFloat(headerHeight + ((Double(totalItemCount) * 30.0)))
        self.tableViewExtraheightConstraint.constant = CGFloat(Double((self.presenter?.arrSubMenuItems.count)!) * 30.0)
    }
}

extension AddMenuView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == discountOptinsPicker{
            return self.arrDiscountOptins.count
        }else if pickerView == categoryTypePicker{
            return self.presenter!.arrAllCategories.count
        }else if pickerView == optionsPicker{
            return (self.presenter?.arrOptionNames.count)!
        }else if pickerView == mealTypePicker{
            return (self.presenter?.arrMealTypes.count)!
        }else {
            return (self.presenter?.arrExtrasNames.count)!
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == discountOptinsPicker{
            return String(describing: self.arrDiscountOptins[row])
        }else if pickerView == categoryTypePicker{
            return String(describing: self.presenter!.arrAllCategories[row].name ?? "")
        }else if pickerView == optionsPicker{
            return String(describing: (self.presenter?.arrOptionNames[row].name)!)
        }else if pickerView == mealTypePicker{
            return String(describing: (self.presenter?.arrMealTypes[row].type)!)
        }else{
            return String(describing: (self.presenter?.arrExtrasNames[row].name)!)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //        self.textFieldState.text = String(describing: self.presenter.arrStates[row])
        //        self.presenter.objJobProfile.visibleTattoo = String(describing: arrVisibleTatoo[row]) == "Yes" ? 1 : 0
        
    }
    
}




extension AddMenuView : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tableViewMenuExtras{
            return (self.presenter?.arrSubMenuItems.count)!
        }else{
            return (self.presenter?.arrAllOptionItems.count)!
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewMenuExtras{
            return (self.presenter?.arrSubMenuItems.count)!
        }else{
            return (self.presenter?.arrAllOptionItems[section].arrOptions.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tableViewMenuExtras{
            return nil
        }else{
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
            view.backgroundColor = #colorLiteral(red: 0.9113200307, green: 0.9047325253, blue: 0.9163637757, alpha: 1)
            let label = UILabel(frame: CGRect(x: 20, y: 0, width: 200, height: 40))
            guard let option = self.presenter?.arrAllOptionItems[section] else{
                return view
            }
            label.font = ceraProBoldNormalFontSize
            label.text = "\(option.optionTitle)"
            label.textColor = UIColor.darkGray
            view.addSubview(label)
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tableViewMenuExtras{
            return 0.0
        }else{
            return 40.0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewMenuExtras{
            let cell = tableView.dequeueReusableCell(withIdentifier: "menuExtraItemCell") as! MenuExtraItemCell
            guard let item = self.presenter?.arrSubMenuItems[indexPath.row] else{
                return cell
            }
            cell.labelItemName.text = item.name
            let price = Float(item.price)
            cell.label_ItemPrice.text = String(describing: "\(currencySign) \(String(describing: String(format: "%.2f", price!)))")
            cell.imageCheckBox.isHidden = !item.isActive
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "menuOptionsItemCell") as! MenuOptionsItemCell
            guard let item = self.presenter?.arrAllOptionItems[indexPath.section].arrOptions[indexPath.row] else{
                return cell
            }
            cell.label_OptionName.text = item.name
            cell.imageCheckBox.isHidden = !(item.isActive ?? false)
            return cell
        }
    }
    
}
