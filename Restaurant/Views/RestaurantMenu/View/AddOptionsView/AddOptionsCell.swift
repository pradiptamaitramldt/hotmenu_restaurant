//
//  AddSubMenuCell.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
protocol AddOptionsCellDelegate{
    func removeItem(indexPath : IndexPath)
    func showDropDown(indexPath : IndexPath)
    
}

class AddOptionsCell: UITableViewCell {
    @IBOutlet weak var button_checkBox: UIButton!
    @IBOutlet weak var textFieldName: CustomTextField!
    
    @IBOutlet weak var buttonCross: UIButton!
    @IBOutlet weak var buttonDropDown: UIButton!
    
    var indexPath : IndexPath?
    var objItem = ArrOption()
    var presenter : RestaurentMenuPresenter?
    var delegate : AddOptionsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUpCellView(){
        
        self.button_checkBox.isSelected = (self.presenter?.arrAllOptionItems[indexPath!.section].arrOptions[indexPath!.row].isActive)!
        self.textFieldName.text = (self.presenter?.arrAllOptionItems[indexPath!.section].arrOptions[indexPath!.row].name)!

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func button_RemoveClick(_ sender: Any) {
        self.delegate?.removeItem(indexPath: self.indexPath!)
    }
    
    @IBAction func buttonDropDownClick(_ sender: Any) {
        self.delegate?.showDropDown(indexPath: self.indexPath!)
    }
    
    @IBAction func btnCheckBoxDidClick(_ sender: Any) {
        self.button_checkBox.isSelected = !self.button_checkBox.isSelected
        self.presenter?.arrAllOptionItems[indexPath!.section].arrOptions[indexPath!.row].isActive = self.button_checkBox.isSelected
    }
    
}
