//
//  SubMenuView.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

protocol AddoptionsDelegate{
    func updateOptionsView()
    func showOptionAlert(message : String)
    func addOptionHeaderTitle()
    func showOptionDropDown(indexPath : IndexPath)
    func deleteOptionType(index : Int)

}


class AddOptions: UIView {
    @IBOutlet weak var tableViewheight: NSLayoutConstraint!
    @IBOutlet weak var mainViewheight: NSLayoutConstraint!
    @IBOutlet weak var buttonSubmit: NormalBoldButton!
    @IBOutlet weak var tableViewSubMenu: UITableView!
    @IBOutlet weak var textFieldOptionType: CustomTextField!
    
    var presenter : RestaurentMenuPresenter?
    var delegate : AddoptionsDelegate?
    override init(frame: CGRect) {
       super.init(frame: frame)
        loadViewFromNib ()
     }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }

    func loadViewFromNib() {
        
        let view = UINib(nibName: "AddOptions", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        
        view.frame = bounds
        
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
        self.buttonSubmit.setRoundedCornered(height: self.buttonSubmit.frame.size.height)
        self.tableViewSubMenu.dataSource = self
        self.tableViewSubMenu.delegate = self
        self.tableViewSubMenu.register(UINib(nibName: "AddOptionsCell", bundle: nil), forCellReuseIdentifier: "addOptionsCell")

    }
    
    func setupView(){
        var totalItemCount = 0
        for item in self.presenter!.arrAllOptionItems{
            totalItemCount += item.arrOptions.count
        }
        let headerHeight = (Double(self.presenter!.arrAllOptionItems.count) * 40.0)
        self.tableViewheight.constant = CGFloat(headerHeight + ((Double(totalItemCount) * 50.0)))
        self.mainViewheight.constant = self.tableViewheight.constant + 150.0
        self.tableViewSubMenu.reloadData()

    }
    
    @IBAction func addHeaderLabelDidClick(_ sender: Any) {
        for (indexHeader, option) in self.presenter!.arrAllOptionItems.enumerated(){
            for (indexitem, _) in option.arrOptions.enumerated(){
                let cell = self.tableViewSubMenu.cellForRow(at: IndexPath(row: indexitem, section: indexHeader)) as! AddOptionsCell
                self.presenter!.arrAllOptionItems[indexHeader].arrOptions[indexitem].name = cell.textFieldName.text!
            }
        }

        self.delegate?.addOptionHeaderTitle()
    }
    
    
    @IBAction func buttonCrossDidClick(_ sender: Any) {
        self.removeFromSuperview()
        
    }
    
    @IBAction func button_SubmitDidClick(_ sender: Any) {
        var flag = true
        for (indexHeader, option) in self.presenter!.arrAllOptionItems.enumerated(){
            for (indexitem, _) in option.arrOptions.enumerated(){
                let cell = self.tableViewSubMenu.cellForRow(at: IndexPath(row: indexitem, section: indexHeader)) as! AddOptionsCell
                if cell.textFieldName.text!.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                    self.presenter!.arrAllOptionItems[indexHeader].arrOptions[indexitem].name = cell.textFieldName.text!
                    flag = true

                }else if cell.textFieldName.text!.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
                    if cell.button_checkBox.isSelected{
                        self.delegate?.showOptionAlert(message: "Option Item name should be entered or the Options should be Unchecked.")
                        flag = false
                        break

                    }else{
                        flag = true
                    }
                }
            }
        }
        if flag{
            for (indexHeader, _) in self.presenter!.arrAllOptionItems.enumerated(){
                self.presenter!.arrAllOptionItems[indexHeader].arrOptions = self.presenter!.arrAllOptionItems[indexHeader].arrOptions.filter { ($0.name != "") }
            }

            self.removeFromSuperview()
            self.delegate?.updateOptionsView()

        }
    }
    
    @objc func addnewOptionItemClick(_ sender: CustomButton) {
        for (indexHeader, option) in self.presenter!.arrAllOptionItems.enumerated(){
            for (indexitem, _) in option.arrOptions.enumerated(){
                let cell = self.tableViewSubMenu.cellForRow(at: IndexPath(row: indexitem, section: indexHeader)) as! AddOptionsCell
                self.presenter!.arrAllOptionItems[indexHeader].arrOptions[indexitem].name = cell.textFieldName.text!
            }
        }
        self.presenter?.arrAllOptionItems[sender.params["section"] as! Int].arrOptions.append(ArrOption(name: "", isActive: false))
        self.tableViewSubMenu.reloadData()
        var totalItemCount = 0
        for item in self.presenter!.arrAllOptionItems{
            totalItemCount += item.arrOptions.count
        }
        let headerHeight = (Double(self.presenter!.arrAllOptionItems.count) * 40.0)
        self.tableViewheight.constant = CGFloat(headerHeight + ((Double(totalItemCount) * 50.0)))
        self.mainViewheight.constant = self.tableViewheight.constant + 150.0

    }
    
    
    @objc func deleteOptionTypeClick(_ sender: CustomButton) {
        self.delegate?.deleteOptionType(index: sender.params["section"] as! Int)
    }

}

extension AddOptions : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return (self.presenter?.arrAllOptionItems.count)!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.presenter?.arrAllOptionItems[section].arrOptions.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
        
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        view.backgroundColor = #colorLiteral(red: 0.9113200307, green: 0.9047325253, blue: 0.9163637757, alpha: 1)

        let label = UILabel(frame: CGRect(x: 30.0, y: 0, width: 200, height: 40))
        guard let option = self.presenter?.arrAllOptionItems[section] else{
           return view
        }
        label.font = ceraProBoldNormalFontSize
        label.text = "\(option.optionTitle)"
        label.textColor = UIColor.darkGray
        view.addSubview(label)
        let button = CustomButton(frame: CGRect(x: tableView.frame.size.width - 140.0, y: 3.0, width: 100.0, height: 34))
        button.setTitle("Add Item", for: .normal)
        button.layer.borderWidth = 1.0
        button.layer.borderColor = themeDarkColor?.cgColor
        button.layer.cornerRadius = button.frame.size.height/2
        button.setTitleColor(themeDarkColor, for: .normal)
        button.titleLabel?.font = ceraProBoldNormalFontSize
        button.params["section"] = section
        button.addTarget(self, action: #selector(addnewOptionItemClick(_:)), for: .touchUpInside)
        
        
        let buttonDelete = CustomButton(frame: CGRect(x: tableView.frame.size.width - 40.0, y: 0.0, width: 40.0, height: 40))
        buttonDelete.params["section"] = section
        buttonDelete.setImage(#imageLiteral(resourceName: "delete_red"), for: .normal)
        buttonDelete.addTarget(self, action: #selector(deleteOptionTypeClick(_:)), for: .touchUpInside)
        view.addSubview(buttonDelete)

        view.addSubview(button)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addOptionsCell") as! AddOptionsCell
        cell.indexPath = indexPath
        cell.presenter = presenter
        cell.objItem = (self.presenter?.arrAllOptionItems[indexPath.section].arrOptions[indexPath.row])!
        cell.delegate = self
        cell.setUpCellView()
        return cell
    }
    
}
extension AddOptions : AddOptionsCellDelegate{
    func showDropDown(indexPath: IndexPath) {
        self.delegate?.showOptionDropDown(indexPath : indexPath)
    }
    
    func removeItem(indexPath: IndexPath) {
        if (self.presenter?.arrAllOptionItems[indexPath.section].arrOptions.count)! > 0{
            self.presenter?.arrAllOptionItems[indexPath.section].arrOptions.remove(at: indexPath.row)
            self.tableViewSubMenu.reloadData()
            var totalItemCount = 0
            for item in self.presenter!.arrAllOptionItems{
                totalItemCount += item.arrOptions.count
            }
            let headerHeight = (Double(self.presenter!.arrAllOptionItems.count) * 40.0)
            self.tableViewheight.constant = CGFloat(headerHeight + ((Double(totalItemCount) * 50.0)))
            self.mainViewheight.constant = self.tableViewheight.constant + 150.0
        }
        
    }
    
    
    
}
