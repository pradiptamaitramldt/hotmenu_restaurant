//
//  MenuExtraItemCell.swift
//  Restaurant
//
//  Created by Souvik on 09/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class MenuExtraItemCell: UITableViewCell {
    @IBOutlet weak var labelItemName: CustomNormalLabel!
    @IBOutlet weak var label_ItemPrice: CustomNormalLabel!
    
    @IBOutlet weak var imageCheckBox: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
