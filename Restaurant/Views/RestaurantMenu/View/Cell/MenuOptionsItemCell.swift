//
//  MenuOptionsItemCell.swift
//  Restaurant
//
//  Created by Souvik on 09/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class MenuOptionsItemCell: UITableViewCell {
    @IBOutlet weak var imageCheckBox: UIImageView!
    @IBOutlet weak var label_OptionName: CustomNormalLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
