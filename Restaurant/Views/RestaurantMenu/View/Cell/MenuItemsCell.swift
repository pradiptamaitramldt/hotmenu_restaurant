//
//  MenuItemsCell.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
protocol MenuItemsCellDelegate{
    func btnEditClicked(itmID : String)
    func btnOnOffClicked(itemID : String,state : Bool)
    func deleteMenuItem(itemID : String)
}

class MenuItemsCell: UITableViewCell {
    @IBOutlet weak var label_ItemName: CustomBoldFontLabel!
    @IBOutlet weak var label_Price: CustomBoldFontLabel!
    @IBOutlet weak var switvhOnOff: UISwitch!
    @IBOutlet weak var label_mealType: CustomBoldFontLabel!
    
    @IBOutlet weak var labelSwitch: LabelSwitch!
    
    var delegate : MenuItemsCellDelegate?
    var itemID : String = ""
    var isActive : Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func prepareCellForDisplay(){
        let ls2 = LabelSwitchConfig(text: "On",
                              textColor: .white,
                                   font: ceraProBoldNormalFontSize,
                                   gradientColors: [UIColor(named: "NavigationBarColor")!.cgColor, UIColor(named: "ThemeColor")!.cgColor], startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1, y: 0.5))
        
        let rs2 = LabelSwitchConfig(text: "Off",
                              textColor: .white,
                                   font: ceraProBoldNormalFontSize,
                                   gradientColors: [UIColor(named: "ThemeColor")!.cgColor, UIColor(named: "NavigationBarColor")!.cgColor], startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1, y: 0.5))
        
        let gradientLabelSwitch = LabelSwitch(center: CGPoint(x: screenWidth - 135 , y: contentView.center.y), leftConfig: ls2, rightConfig: rs2, defaultState: .L)
        gradientLabelSwitch.delegate = self
        gradientLabelSwitch.curState = isActive ? .R : .L
        gradientLabelSwitch.circleShadow = false
        gradientLabelSwitch.fullSizeTapEnabled = true

        self.contentView.addSubview(gradientLabelSwitch)

    }
    
    @IBAction func button_EdidClick(_ sender: Any) {
        self.delegate?.btnEditClicked(itmID: self.itemID)
    }
    
    @IBAction func buttonDeleteClick(_ sender: Any) {
        self.delegate?.deleteMenuItem(itemID: self.itemID)
        
    }
    
}

extension MenuItemsCell: LabelSwitchDelegate {
    func switchChangToState(sender: LabelSwitch) {
        switch sender.curState {
            case .L: print("left state")
            self.delegate?.btnOnOffClicked(itemID: self.itemID, state: false)
            case .R: print("right state")
            self.delegate?.btnOnOffClicked(itemID: self.itemID, state: true)

        }
    }
}
