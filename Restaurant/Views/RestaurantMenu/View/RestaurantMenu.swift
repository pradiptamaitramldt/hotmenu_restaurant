//
//  RestaurantMenu.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class RestaurantMenu: BaseViewController, SideMenuItemContent, Storyboardable  {

    @IBOutlet weak var collectionViewItems: UICollectionView!
    @IBOutlet weak var tableViewmenuItems: UITableView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var notificaionBell: UIBarButtonItem!
    
    var bottom_menu = BottomMenu()
    var banner : AddMenuView?
    var editMenu : EditMenuView?
    let presenter = RestaurentMenuPresenter()
    var picker1 = UIImagePickerController()
    var picker2 = UIImagePickerController()
    var isAllItems = true
    var didGetValue = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = String(describing: "\(UserDefaultValues.userName)")
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.viewBottom.bounds.size.width,height:self.viewBottom.bounds.size.height)
        self.viewBottom.addSubview(bottom_menu)
        bottom_menu.searchSeleted()
        bottom_menu.delegate = self
        self.tableViewmenuItems.dataSource = self
        self.tableViewmenuItems.delegate = self
        self.tableViewmenuItems.register(UINib(nibName: "MenuItemsCell", bundle: nil), forCellReuseIdentifier: "menuItemsCell")
        self.collectionViewItems.dataSource = self
        self.collectionViewItems.delegate = self

        AMShimmer.start(for: self.tableViewmenuItems)
        AMShimmer.start(for: self.collectionViewItems)
        self.presenter.getAllFoodCategories { (status, message) in
                        
            self.presenter.getAllMealTypes { (status, message) in
                self.presenter.getAllPreOptions { (status, message) in
                    self.presenter.getAllPreExtras { (status, message) in
                       
                    }
                }
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(moveToDetails(_:)), name: .gotPush, object: nil)

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.notificaionBell.addBadge(number: 0)
        if UserDefaultValues.userID != nil{

        self.callMenuListAPI()
        }
    }
    
    @IBAction func notificationBellDidClick(_ sender: Any) {
        let storybrd = UIStoryboard(name: "NotificationSettingsView", bundle: nil)
        let vc = storybrd.instantiateViewController(identifier: "notificationsListView") as! NotificationsListView
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func button_menuDidClick(_ sender: Any) {
        showSideMenu()

    }
    
    @IBAction func addMenuDidClick(_ sender: Any) {
        self.showAddMenuView()
    }
    
    @objc func moveToDetails(_ notificationData: Notification){
        self.notificaionBell.updateBadge(number: UserDefaultValues.badgeCount)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RestaurantMenu {
    //MARK: - Self Mothods -
     func callMenuListAPI(){
        if UserDefaultValues.userID != nil{
             DispatchQueue.main.async {
                AMShimmer.start(for: self.tableViewmenuItems)
            }
            self.presenter.getAllAddedMenuItems { (staus, message) in
                DispatchQueue.main.async {
                    AMShimmer.stop(for: self.tableViewmenuItems)
                    AMShimmer.stop(for: self.collectionViewItems)
                    if staus {
                        self.didGetValue = true
                    }
                    else {
                        self.didGetValue = false
                    }
                    self.tableViewmenuItems.reloadData()
                    self.collectionViewItems.reloadData()
                    self.collectionViewItems.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)

                }
            }

        }

    }
    
    func showAddMenuView(){
        
        self.presenter.arrSubMenuItems.removeAll()
        self.presenter.arrAllOptionItems.removeAll()
        banner = AddMenuView(frame: self.view.bounds)
        banner?.presenter = self.presenter
        banner?.delegate = self
        self.view.addSubview(banner!)
        self.view.bringSubviewToFront(banner!)


    }
    
    func showEditMenuView(){
        editMenu = EditMenuView(frame: self.view.bounds, presenter: self.presenter)
        editMenu?.presenter = self.presenter
        editMenu?.delegate = self
        self.view.addSubview(editMenu!)
        self.view.bringSubviewToFront(editMenu!)


    }

    
    func showImagePicker(picker : UIImagePickerController){
        

        let alertMessage = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        let titleAttributes = [NSAttributedString.Key.font: ceraProLargeFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let titleString = NSAttributedString(string: "HotMenu", attributes: titleAttributes as [NSAttributedString.Key : Any])
        let messageAttributes = [NSAttributedString.Key.font: ceraProNormalFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let messageString = NSAttributedString(string: "Please select Image for this menu item.", attributes: messageAttributes as [NSAttributedString.Key : Any])
        alertMessage.setValue(titleString, forKey: "attributedTitle")
        alertMessage.setValue(messageString, forKey: "attributedMessage")

        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                picker.delegate = self
                picker.sourceType = .camera;
                picker.allowsEditing = false
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        alertMessage.addAction(cameraAction)
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                picker.delegate = self
                picker.sourceType = .photoLibrary;
                picker.allowsEditing = false
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        alertMessage.addAction(galleryAction)
        let action = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alertMessage .addAction(action)
        alertMessage.view.tintColor = themeDarkColor
        
        self.present(alertMessage, animated: true, completion: nil)
    }
}

extension RestaurantMenu : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.presenter.objAllMenuItemsHandler.categoryList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "orderTypeCollectionViewCell", for: indexPath) as! OrderTypeCollectionViewCell
        guard let category = self.presenter.objAllMenuItemsHandler.categoryList?[indexPath.item] else {
            return cell
        }
        cell.label_Name.text = "\(category.categoryName!)(\(category.inItemCount!))"
        if cell.isSelected{
            cell.contentView.backgroundColor = UIColor.black
            cell.label_Name.textColor = .white
        }else{
            cell.contentView.backgroundColor = UIColor.white
            cell.label_Name.textColor = .black
        }
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 2, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        guard let category = self.presenter.objAllMenuItemsHandler.categoryList?[indexPath.item] else {
            return CGSize(width: 0, height: 0)
        }

        let teStStr = "\(category.categoryName!)(\(category.inItemCount!))"
        let size : CGSize = (teStStr.size(withAttributes:
            [NSAttributedString.Key.font: ceraProNormalFontSize as Any]))
        return CGSize(width:size.width+16,height:30.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10.0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! OrderTypeCollectionViewCell
        cell.contentView.backgroundColor = UIColor.black
        cell.label_Name.textColor = .white
        collectionView.selectItem(at: indexPath, animated: false, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
        if indexPath.item == 0{
            self.callMenuListAPI()
        }else{
          
            AMShimmer.start(for: tableViewmenuItems)
            self.presenter.getAllAddedMenuItems { (staus, message) in
                DispatchQueue.main.async {
                    AMShimmer.stop(for: self.tableViewmenuItems)
                    self.presenter.objAllMenuItemsHandler.itemlist = self.presenter.objAllMenuItemsHandler.itemlist?.filter({$0.category?.id == self.presenter.objAllMenuItemsHandler.categoryList![indexPath.item].id!})
                    self.tableViewmenuItems.reloadData()

                }
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as! OrderTypeCollectionViewCell? else {
            return
        }
        cell.contentView.backgroundColor = UIColor.white
        cell.label_Name.textColor = .black
    }


}

//MARK: Addmenu delegate
extension RestaurantMenu: AddMenuDelegate{
    func deleteOptionTypeForAddMenu(index: Int) {
        DispatchQueue.main.async {
            self.showAlertWithOkAndCancel(message: "Do you really want to delete this Option Type?", okAction: {
                self.presenter.arrAllOptionItems.remove(at: index)
                self.banner?.addOption!.setupView()

            }) {
                
            }
        }

    }
    
    func showAddOptionDropDown(indexPath: IndexPath) {
        
    }
    
    func showAddHeaderAlert() {
        let alertController = UIAlertController(title: "Add New Option Header Title", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Header Title"
        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
//            self.presenter.arrAllOptionItems.append(Option(optionTitle: firstTextField.text!, arrOptions: []))
            self.presenter.arrAllOptionItems.append(ItemOption(arrOptions: [], optionTitle: firstTextField.text, isActive: nil))
            self.banner?.addOption!.setupView()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    
    func showAddMessageAlert(message: String) {
        DispatchQueue.main.async {
            self.showAlertWith(message: message) {
            }
        }

    }
    
    func addmenuItem(itemName: String, itemCategoryID: String, mealTypeID: String, itemImage: UIImage?, type: String, waitingTime: String, price: String, discountAmount: String, validFrom: String, validTo: String) {
          //  self.startLoaderAnimation()
        AMShimmer.start(for: self.banner!.buttonSubmit)
        AMShimmer.start(for: self.banner!.textFieldMenuName)
        AMShimmer.start(for: self.banner!.textFieldCategory)
        AMShimmer.start(for: self.banner!.textFieldMealType)
        AMShimmer.start(for: self.banner!.imageViewMenuItem)
        AMShimmer.start(for: self.banner!.textFieldPrice)
        AMShimmer.start(for: self.banner!.textFieldTime)
        AMShimmer.start(for: self.banner!.textFieldChooseDiscountType)
        AMShimmer.start(for: self.banner!.labelNaira)
        AMShimmer.start(for: self.banner!.textFieldValidFrom)
       // AMShimmer.start(for: self.banner!.labelMenuItemName)
        print("mealTypeID",mealTypeID)
        self.presenter.addMenuItem(itemName: itemName, itemCategoryID: itemCategoryID, mealTypeID: mealTypeID, itemImage: itemImage, type: type, waitingTime: waitingTime, price: price, discountAmount: discountAmount, validFrom: validFrom, validTO: validTo, completion: { (status, message) in
                
                DispatchQueue.main.async {
                  
                    AMShimmer.stop(for: self.banner!.buttonSubmit)
                    AMShimmer.stop(for: self.banner!.textFieldMenuName)
                    AMShimmer.stop(for: self.banner!.textFieldCategory)
                    AMShimmer.stop(for: self.banner!.textFieldMealType)
                    AMShimmer.stop(for: self.banner!.imageViewMenuItem)
                    AMShimmer.stop(for: self.banner!.textFieldPrice)
                    AMShimmer.stop(for: self.banner!.textFieldTime)
                    AMShimmer.stop(for: self.banner!.textFieldChooseDiscountType)
                    AMShimmer.stop(for: self.banner!.labelNaira)
                    AMShimmer.stop(for: self.banner!.textFieldValidFrom)
                  //  AMShimmer.stop(for: self.banner!.labelMenuItemName)
                    self.showAlertWith(message: message) {
                        if status{
                            //success
    //                        self.presenter.arrAllOptionItems.removeAll()
    //                        self.presenter.arrSubMenuItems.removeAll()
                            self.banner?.removeFromSuperview()
                        
                            AMShimmer.start(for: self.tableViewmenuItems)
                            self.presenter.getAllAddedMenuItems { (staus, message) in
                               
                                DispatchQueue.main.async {
                                    AMShimmer.stop(for: self.tableViewmenuItems)
                                    self.tableViewmenuItems.reloadData()
                                    self.collectionViewItems.reloadData()
                                }
                            }
                        }
                    }
                }
            })

        }
    
    
    func showImagePicker() {
        self.showImagePicker(picker: picker1)
    }
    
}

//MARK: Addmenu delegate
extension RestaurantMenu: EditMenuDelegate{
    func deleteOptionTypeForEdit(index: Int) {
        DispatchQueue.main.async {
            self.showAlertWithOkAndCancel(message: "Do you really want to delete this Option Type?", okAction: {
                self.presenter.arrAllOptionItems.remove(at: index)
                self.editMenu?.addOption!.setupView()

            }) {
                
            }
        }

    }
    
    func showEditOptionDropDown(indexPath: IndexPath) {
        
    }
    
    func showAddheaderAlert() {
        let alertController = UIAlertController(title: "Add New Option Header Title", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Header Title"
            textField.autocapitalizationType = UITextAutocapitalizationType.words
        }
        let saveAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
//            self.presenter.arrAllOptionItems.append(Option(optionTitle: firstTextField.text!, arrOptions: []))
            self.presenter.arrAllOptionItems.append(ItemOption(arrOptions: [], optionTitle: firstTextField.text, isActive: nil))
            self.editMenu?.addOption?.setupView()

        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })

        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)

    }
    
    func showMessageAlert(message: String) {
        DispatchQueue.main.async {
            self.showAlertWith(message: message) {
            }
        }

    }
    
    func editMenuItem(id: String, itemName: String, itemCategoryID: String, itemImage: UIImage?, type: String, waitingTime: String, price: String, discountAmount: String, validFrom: String, validTo: String) {
    //    self.startLoaderAnimation()
//        self.presenter.
        self.presenter.editMenuItem(itemId: id, itemName: itemName, itemCategoryID: itemCategoryID, itemImage: itemImage, type: type, waitingTime: waitingTime, price: price, discountAmount: discountAmount, validFrom: validFrom, validTO: validTo, completion: { (status, message) in
         //   self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.showAlertWith(message: message) {
                    if status{
                        //success
                        self.editMenu?.removeFromSuperview()
                     //   self.startLoaderAnimation()
                        self.presenter.getAllAddedMenuItems { (staus, message) in
                         //   self.stopLoaderAnimation()
                            DispatchQueue.main.async {
                                self.tableViewmenuItems.reloadData()
                                self.collectionViewItems.reloadData()
                            }
                        }
                    }
                }
            }
        })

    }
    
    func showEditImagePicker() {
        self.showImagePicker(picker: picker2)
    }
    

    
}


//MARK: bottomMenu delegate
extension RestaurantMenu: bottomMenuDelegate{
    func selectedButton(selectedItem:NSInteger){
        
        if selectedItem == 1 {
            //home
            //            let st = UIStoryboard.init(name: "DashBoardView", bundle: nil)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 0
            UIApplication.shared.windows[0].rootViewController = viewController
        }else if selectedItem == 2{
            //menu
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 1
            UIApplication.shared.windows[0].rootViewController = viewController

        }
        else if selectedItem == 3{
            //OrderList
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 6
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }else{
            //userprofile
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 5
            UIApplication.shared.windows[0].rootViewController = viewController
        }
    }
    
}


extension RestaurantMenu : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    //MARK: - Image Picker -
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if picker == picker1{//Logo
            if let pickedImage = info[.originalImage] as? UIImage {
                self.banner?.imageViewMenuItem.image = pickedImage
            }
            picker.dismiss(animated: true, completion: nil)

        }else{
            if let pickedImage = info[.originalImage] as? UIImage {
                self.editMenu?.imageViewMenuItem.image = pickedImage
            }
            picker.dismiss(animated: true, completion: nil)

        }
    }
}


extension RestaurantMenu : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        if didGetValue {
           return self.presenter.objAllMenuItemsHandler.itemlist?.count ?? 0
        }
        else {
            return 3
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if didGetValue {
             return self.presenter.objAllMenuItemsHandler.itemlist?[section].items?.count ?? 0
        }
        else {
             return 3
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: 300, height: 40))
        guard let category = self.presenter.objAllMenuItemsHandler.itemlist?[section] else{
           return view
        }
        label.font = ceraProBoldNormalFontSize
        label.text = "\(category.category?.categoryName ?? "")"
        label.textColor = UIColor.darkGray
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuItemsCell") as! MenuItemsCell
        if didGetValue {
            guard let item = self.presenter.objAllMenuItemsHandler.itemlist?[indexPath.section].items?[indexPath.row] else{
                return cell
            }
            cell.label_ItemName.text = item.itemName ?? ""
            let price = Float(item.price!)
            cell.label_Price.text = "\(currencySign) \(String(describing: String(format: "%.2f", price)))"
            cell.itemID = item.id!
            cell.label_mealType.text = "Meal Type: \(item.mealTypeID?.type ?? "")"
            cell.isActive = item.isActive!
            cell.delegate = self
            cell.prepareCellForDisplay()
        }
        
        return cell
    }
    
}

extension RestaurantMenu : MenuItemsCellDelegate{
    func deleteMenuItem(itemID: String) {
        self.showAlertWithOkAndCancel(message: "Are you sure you want to delete?", okAction: {
          
            self.presenter.deleteMenuItem(itemId: itemID) { (status, message) in
             
                self.callMenuListAPI()
            }
        }) {
            
        }
    }
    
    func btnEditClicked(itmID: String) {
        self.startLoaderAnimation()
        self.presenter.getMenuItemDetails(itemID: itmID) { (status, message, itemDetail) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                guard let item = itemDetail else{
                    return
                }
                self.presenter.objSelectedMenuForEdit = item
                self.showEditMenuView()

            }
        }
    }
    
    func btnOnOffClicked(itemID: String, state: Bool) {
      //  self.startLoaderAnimation()
        self.presenter.changeMenuItemStatus(itemID: itemID, status: state) { (status, message) in
        //    self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.showAlert(message: message)

            }

        }

    }
        

    
}



