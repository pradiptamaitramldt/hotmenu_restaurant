//
//  CreateAccountView.swift
//  Restaurant
//
//  Created by Bit Mini on 10/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class CreateAccountView: UITableViewController {
    @IBOutlet weak var viewFName: UIView!
    @IBOutlet weak var textFieldFName: CustomTextField!
    @IBOutlet weak var viewManagerName: UIView!
    @IBOutlet weak var textFieldManagerName: CustomTextField!
    @IBOutlet weak var viewRestaurantType: UIView!
    @IBOutlet weak var textFieldRestaurantType: CustomTextField!
    
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var textFieldEmail: CustomTextField!
    @IBOutlet weak var viewMobile: UIView!
    @IBOutlet weak var textFieldCountryCode: CustomTextField!
    @IBOutlet weak var textFieldMobile: CustomTextField!
    @IBOutlet weak var imageCountryFlag: UIImageView!
    
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var textFieldLocation: CustomTextField!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewConfirmPassword: UIView!
    @IBOutlet weak var textPassword: CustomTextField!
    @IBOutlet weak var textConfirmPassword: CustomTextField!
    @IBOutlet weak var btn_Confirm: NormalBoldButton!
    @IBOutlet weak var viewUploadLogo: UIView!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var viewUploadBanner: UIView!
    @IBOutlet weak var imageBanner: UIImageView!
    @IBOutlet weak var collectionViewTypes: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    var presenter = CreateAccountPresenter()
    var restaurantTypePicker : UIPickerView?
    var cityPicker : UIPickerView?
    var picker1 = UIImagePickerController()
    var picker2 = UIImagePickerController()
    var selectedLocation = CLLocationCoordinate2D()
    let categoryListPresenter = CategoriesListPresenter()
    var arrRestaurantTypes = ["VEG", "NON VEG", "BOTH"]
    var arrSelectesTypes: [VendorType] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionViewTypes.dataSource = self
        self.collectionViewTypes.delegate = self
        
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            if let objCountry = CountryPickerViewController.getCountryDetailsBy(countryCode: countryCode){
                self.imageCountryFlag.image = UIImage(named: objCountry.flag!)
                self.textFieldCountryCode.text = objCountry.dialCode!
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.textFieldCountryCode.delegate = self
        self.textFieldLocation.delegate = self
        self.textFieldRestaurantType.delegate = self
        self.viewFName.setRoundedCornered(height: viewFName.frame.size.height)
        self.viewManagerName.setRoundedCornered(height: viewManagerName.frame.size.height)
        self.viewEmail.setRoundedCornered(height: viewEmail.frame.size.height)
        self.viewMobile.setRoundedCornered(height: viewMobile.frame.size.height)
        self.viewLocation.setRoundedCornered(height: viewLocation.frame.size.height)
        self.viewPassword.setRoundedCornered(height: viewPassword.frame.size.height)
        self.viewConfirmPassword.setRoundedCornered(height: viewConfirmPassword.frame.size.height)
        self.viewRestaurantType.setRoundedCornered(height: self.viewRestaurantType.frame.size.height)
        self.viewUploadLogo.setRoundedCornered(height: self.viewUploadLogo.frame.size.height)
        self.viewUploadBanner.setRoundedCornered(height: self.viewUploadBanner.frame.size.height)
        self.btn_Confirm.setRoundedCornered(height: btn_Confirm.frame.size.height)
        self.textFieldMobile.delegate = self
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.createRestaurantTypePicker()
    }
    
    @IBAction func btn_backDidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_SignInAgainDidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btn_ConfirmDidClick(_ sender: Any) {
        var strRestaurantTypes = ""
        for item in self.arrSelectesTypes{
            strRestaurantTypes += "\(item.name!),"
        }
        if strRestaurantTypes.count > 0{
            strRestaurantTypes.removeLast()
        }
        self.startLoaderAnimation()
        self.presenter.createAccount(restaurantName: self.textFieldFName.text!, managerName: self.textFieldManagerName.text!, restaurantType: strRestaurantTypes, email: self.textFieldEmail.text!.lowercased(), countryCode: self.textFieldCountryCode.text!, phone: self.textFieldMobile.text!, location: self.textFieldLocation.text!, latitude: String(describing: selectedLocation.latitude), longitude: String(describing: selectedLocation.longitude), password: self.textPassword.text!, confirmPassword: self.textConfirmPassword.text!, isLogo: self.imageLogo.image != nil ? true : false, logoData: self.imageLogo.image, isBanner: self.imageBanner.image != nil ? true : false, bannerData: self.imageBanner.image) { (status, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.showAlertWith(message: message) {
                    if status{
                        //success
                        //Otp Verify
//                        let vc = self.storyboard?.instantiateViewController(identifier: "addTimingView") as! AddTimingView
//                        vc.restaurantID = self.presenter.restaurantID
//                        self.navigationController?.pushViewController(vc, animated: true)
                        
                        let vc = self.storyboard?.instantiateViewController(identifier: "verifyOTPView") as! VerifyOTPView
                        vc.userID = self.presenter.restaurantID
                        vc.viewShowFor = "verifyRegiter"
                        vc.isLogin = false
                        vc.email = self.textFieldEmail.text!.lowercased()
                        self.navigationController?.pushViewController(vc, animated: true)

                    }
                }
            }

        }
    }
    
    @IBAction func button_AddLogoClick(_ sender: Any) {
        self.showImagePicker(picker: picker1)
    }
    
    @IBAction func button_AddBannerDidClick(_ sender: Any) {
        self.showBannerPicker(picker: picker2)
    }
    
    @objc func removeItemFromList(_ sender : UIButton){
        if self.arrSelectesTypes.count >= sender.tag{
            self.arrSelectesTypes.remove(at: sender.tag)
            self.collectionViewTypes.reloadData()
        }
    }    
}
extension CreateAccountView{
    
//    func createRestaurantTypePicker()
//    {
//
//        restaurantTypePicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
//        restaurantTypePicker?.dataSource = self
//        restaurantTypePicker?.delegate = self
//
//        textFieldRestaurantType.inputView = restaurantTypePicker
//
//        let button_Done = UIButton()
//        button_Done.setTitle("Done", for: .normal)
//        button_Done.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
//
//        button_Done.setTitleColor(.white, for: .normal)
//        button_Done.sizeToFit()
//        button_Done.frame.origin = CGPoint(x: self.view.frame.width - button_Done.frame.width - 8, y: 5)
//        button_Done.addTarget(self, action: #selector(selectionDone), for: .touchUpInside)
//
//        let button_Cancel = UIButton()
//        button_Cancel.setTitle("Cancel", for: .normal)
//        button_Cancel.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
//        button_Cancel.setTitleColor(.white, for: .normal)
//        button_Cancel.sizeToFit()
//        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
//        button_Cancel.addTarget(self, action: #selector(selectionCancel), for: .touchUpInside)
//
//        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
//        view_Accessory.backgroundColor = UIColor(named: "TextColorDark")
//        view_Accessory.addSubview(button_Done)
//        view_Accessory.addSubview(button_Cancel)
//        textFieldRestaurantType.inputAccessoryView = view_Accessory
//
//    }
//
    
    
//
//    @objc func selectionDone(_ sender: UIButton ) {
//
//        if textFieldRestaurantType.isFirstResponder
//        {
////            textFieldRestaurantType.text = String(describing: self.arrRestaurantTypes[(restaurantTypePicker?.selectedRow(inComponent: 0))!])
////            self.presenter.selectedStateID = String(describing: self.presenter.arrStates[(statePicker?.selectedRow(inComponent: 0))!].id!)
//            self.arrSelectesTypes.append(String(describing: self.presenter.arrRestaurantTypes[(restaurantTypePicker?.selectedRow(inComponent: 0))!].name!))
//            self.collectionViewTypes.reloadData()
//            textFieldRestaurantType?.resignFirstResponder()
//        }
//    }
//
//    @objc func selectionCancel(_ sender: UIButton ) {
//        textFieldRestaurantType.text = ""
//        textFieldRestaurantType.resignFirstResponder()
//    }
    
    func showImagePicker(picker : UIImagePickerController){
        

        let alertMessage = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        let titleAttributes = [NSAttributedString.Key.font: ceraProLargeFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let titleString = NSAttributedString(string: "HotMenu", attributes: titleAttributes as [NSAttributedString.Key : Any])
        let messageAttributes = [NSAttributedString.Key.font: ceraProNormalFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let messageString = NSAttributedString(string: "Please select Image for your Restaurant Logo", attributes: messageAttributes as [NSAttributedString.Key : Any])
        alertMessage.setValue(titleString, forKey: "attributedTitle")
        alertMessage.setValue(messageString, forKey: "attributedMessage")

        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                picker.delegate = self
                picker.sourceType = .camera;
                picker.allowsEditing = true
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        alertMessage.addAction(cameraAction)
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                picker.delegate = self
                picker.sourceType = .photoLibrary;
                picker.allowsEditing = true
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        alertMessage.addAction(galleryAction)
        let action = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alertMessage .addAction(action)
        alertMessage.view.tintColor = themeDarkColor
        
        self.present(alertMessage, animated: true, completion: nil)

    }

    func showBannerPicker(picker : UIImagePickerController){
        

        let alertMessage = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        let titleAttributes = [NSAttributedString.Key.font: ceraProLargeFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let titleString = NSAttributedString(string: "HotMenu", attributes: titleAttributes as [NSAttributedString.Key : Any])
        let messageAttributes = [NSAttributedString.Key.font: ceraProNormalFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let messageString = NSAttributedString(string: "Please select Image for your Restaurant Banner", attributes: messageAttributes as [NSAttributedString.Key : Any])
        alertMessage.setValue(titleString, forKey: "attributedTitle")
        alertMessage.setValue(messageString, forKey: "attributedMessage")

        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                picker.delegate = self
                picker.sourceType = .camera;
                picker.allowsEditing = false
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        alertMessage.addAction(cameraAction)
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                picker.delegate = self
                picker.sourceType = .photoLibrary;
                picker.allowsEditing = false
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        alertMessage.addAction(galleryAction)
        let action = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alertMessage .addAction(action)
        alertMessage.view.tintColor = themeDarkColor
        
        self.present(alertMessage, animated: true, completion: nil)

    }

    
}

extension CreateAccountView : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    //MARK: - Image Picker -
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if picker == picker1{//Logo
            if let pickedImage = info[.editedImage] as? UIImage {
                self.imageLogo.image = pickedImage
            }
            picker.dismiss(animated: true, completion: nil)

        }else{//Banner
            if let pickedImage = info[.originalImage] as? UIImage {
                self.imageBanner.image = pickedImage
            }
            picker.dismiss(animated: true, completion: nil)

        }
    }
}


extension CreateAccountView : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFieldCountryCode{
            let countryPickerStoryBoard = UIStoryboard.init(name: "CountryPicker", bundle: nil)
            let countryPickerVC = countryPickerStoryBoard.instantiateViewController(withIdentifier: "countryPickerViewController") as! CountryPickerViewController
            countryPickerVC.delegate = self
            self.present(countryPickerVC, animated: true, completion: nil)
        }else if textField == textFieldLocation{
            let vc = self.storyboard?.instantiateViewController(identifier: "chooseLocationView") as! ChooseLocationView
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }else if textField == textFieldRestaurantType{
            if self.categoryListPresenter.arrRestaurantTypes.count == 0{
                self.startLoaderAnimation()
                self.categoryListPresenter.getAllPossibleResturantTypes { (status, message) in
                    self.stopLoaderAnimation()
                    DispatchQueue.main.async {
                        let vc = self.storyboard?.instantiateViewController(identifier: "chooseCategoryView") as! ChooseCategoryView
                        vc.delegate = self
                        vc.presenter = self.categoryListPresenter
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }else{
                let vc = self.storyboard?.instantiateViewController(identifier: "chooseCategoryView") as! ChooseCategoryView
                vc.delegate = self
                vc.presenter = self.categoryListPresenter
                self.navigationController?.pushViewController(vc, animated: true)

            }

        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldMobile{
        let maxLength = 11
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        }else{
            return true
        }
        
    }
}

extension CreateAccountView : ChooseCategoryDelegate{
    func gotChoosencategories() {
        self.arrSelectesTypes = self.categoryListPresenter.arrRestaurantTypes.filter{($0.isSelected == true)}
        self.collectionViewTypes.reloadData()
    }
}

extension CreateAccountView : ChooseLocationDelegate{
    func gotAddress(address: String, location: CLLocationCoordinate2D) {
        self.textFieldLocation.text = address
        self.selectedLocation = location
    }
}

extension CreateAccountView : CountryPickerViewControllerDelegate{
    func didSelectCountry(country: Country) {
        self.textFieldCountryCode.text = country.dialCode!
        self.imageCountryFlag.image = UIImage(named: country.flag!)
    }
}

extension CreateAccountView: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.presenter.arrRestaurantTypes.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(describing: self.presenter.arrRestaurantTypes[row].name!)
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        self.textFieldState.text = String(describing: self.presenter.arrStates[row])
//        self.presenter.objJobProfile.visibleTattoo = String(describing: arrVisibleTatoo[row]) == "Yes" ? 1 : 0
        
    }

}

extension CreateAccountView {
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 40, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 3:
            return 117.0
        case 4:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 5:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 6:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 7:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 8:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 9:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 10:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 78, currentScrenHeight: self.view.bounds.height)
        case 11:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 73, currentScrenHeight: self.view.bounds.height)
        case 12:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 69, currentScrenHeight: self.view.bounds.height)
        default:
            return 0.0
        }
    }

}


extension CreateAccountView : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSelectesTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "orderTypeCollectionViewCell", for: indexPath) as! OrderTypeCollectionViewCell
        cell.label_Name.text = "\(self.arrSelectesTypes[indexPath.item].name!)      "
        cell.contentView.backgroundColor = UIColor.black
        cell.label_Name.textColor = .white
        cell.buttonCancel.tag = indexPath.item
        cell.buttonCancel.addTarget(self, action: #selector(self.removeItemFromList(_:)), for: .touchUpInside)

        return cell
    }
    
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let category = self.arrSelectesTypes[indexPath.item].name!

        let teStStr = "\(category)      "
        let size : CGSize = (teStStr.size(withAttributes:
            [NSAttributedString.Key.font: ceraProNormalFontSize as Any]))
        return CGSize(width:size.width+16,height:26.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10.0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10.0
    }
    
    

}
