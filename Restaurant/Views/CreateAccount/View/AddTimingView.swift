//
//  AddTimingView.swift
//  Restaurant
//
//  Created by Souvik on 18/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class AddTimingView: BaseViewController {
    
    @IBOutlet weak var button_Moday: NormalButton!
    @IBOutlet weak var textFieldMondayOpeningTime: UITextField!
    @IBOutlet weak var textFieldMondayClosingTime: UITextField!
    
    @IBOutlet weak var buttonTuesday: NormalButton!
    @IBOutlet weak var textFieldTuesdayOpeningTime: UITextField!
    @IBOutlet weak var textFieldTuesdayClosingTime: UITextField!
    
    @IBOutlet weak var buttonWednesday: NormalButton!
    @IBOutlet weak var textFieldSednesdayOpeningTime: UITextField!
    @IBOutlet weak var textFieldWednesdayClosingTime: UITextField!
    
    @IBOutlet weak var buttonThursday: NormalButton!
    @IBOutlet weak var textFieldThursdayOpeningTime: UITextField!
    @IBOutlet weak var textFieldThursdayClosingTime: UITextField!
    
    @IBOutlet weak var buttonFriday: NormalButton!
    @IBOutlet weak var textFieldFridayOpeningTime: UITextField!
    @IBOutlet weak var textFieldFridayClosingTime: UITextField!
    
    @IBOutlet weak var buttonSatuday: NormalButton!
    @IBOutlet weak var textFieldSaturdayOpeningTime: UITextField!
    @IBOutlet weak var textFieldSaturdayClosingTime: UITextField!
    
    @IBOutlet weak var buttonSunday: NormalButton!
    @IBOutlet weak var textFieldSundayOpeningTime: UITextField!
    @IBOutlet weak var textFieldSundayClosingTime: UITextField!
    
    @IBOutlet weak var view_License: UIView!
    @IBOutlet weak var imageViewLicense: UIImageView!
    @IBOutlet weak var buttonSubmit: NormalBoldButton!
    
    var pickerMonday : UIDatePicker?
    var pickerTuesday : UIDatePicker?
    var pickerWednesday : UIDatePicker?
    var pickerThursday : UIDatePicker?
    var pickerFriday : UIDatePicker?
    var pickerSaturday : UIDatePicker?
    var pickerSunday : UIDatePicker?
    let presenter = CreateAccountPresenter()
    var picker1 = UIImagePickerController()
    var restaurantID : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        pickerMonday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldMondayOpeningTime, picker: pickerMonday!)
        self.createDatePicker(textField: textFieldMondayClosingTime, picker: pickerMonday!)

        pickerTuesday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldTuesdayOpeningTime, picker: pickerTuesday!)
        self.createDatePicker(textField: textFieldTuesdayClosingTime, picker: pickerTuesday!)

        pickerWednesday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldSednesdayOpeningTime, picker: pickerWednesday!)
        self.createDatePicker(textField: textFieldWednesdayClosingTime, picker: pickerWednesday!)

        pickerThursday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldThursdayOpeningTime, picker: pickerThursday!)
        self.createDatePicker(textField: textFieldThursdayClosingTime, picker: pickerThursday!)

        pickerFriday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldFridayOpeningTime, picker: pickerFriday!)
        self.createDatePicker(textField: textFieldFridayClosingTime, picker: pickerFriday!)

        pickerSaturday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldSaturdayOpeningTime, picker: pickerSaturday!)
        self.createDatePicker(textField: textFieldSaturdayClosingTime, picker: pickerSaturday!)

        pickerSunday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldSundayOpeningTime, picker: pickerSunday!)
        self.createDatePicker(textField: textFieldSundayClosingTime, picker: pickerSunday!)



        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view_License.setRoundedCornered(height: self.view_License.frame.size.height)
        self.buttonSubmit.setRoundedCornered(height: self.buttonSubmit.frame.size.height)
    }

    @IBAction func button_MondayDidCilck(_ sender: Any) {
        self.button_Moday.isSelected = !self.button_Moday.isSelected
        if button_Moday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Monday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Monday", startTime: self.textFieldMondayOpeningTime.text!, endTime: self.textFieldMondayClosingTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Monday", startTime: self.textFieldMondayOpeningTime.text!, endTime: self.textFieldMondayClosingTime.text!))
            }

        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Monday" }
            
        }
    }
    @IBAction func buttonTuesdayDidClick(_ sender: Any) {
        self.buttonTuesday.isSelected = !self.buttonTuesday.isSelected
        if buttonTuesday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Tuesday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Tuesday", startTime: self.textFieldTuesdayOpeningTime.text!, endTime: self.textFieldTuesdayClosingTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Tuesday", startTime: self.textFieldTuesdayOpeningTime.text!, endTime: self.textFieldTuesdayOpeningTime.text!))
            }

        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Tuesday" }
            
        }

    }
    @IBAction func buttonWednesdayDidClick(_ sender: Any) {
        self.buttonWednesday.isSelected = !self.buttonWednesday.isSelected
        if buttonWednesday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Wednesday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Wednesday", startTime: self.textFieldSednesdayOpeningTime.text!, endTime: self.textFieldWednesdayClosingTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Wednesday", startTime: self.textFieldSednesdayOpeningTime.text!, endTime: self.textFieldWednesdayClosingTime.text!))
            }

        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Wednesday" }
            
        }

    }
    @IBAction func buttonThursdayDidClick(_ sender: Any) {
        self.buttonThursday.isSelected = !self.buttonThursday.isSelected
        if buttonThursday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Thursday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Thursday", startTime: self.textFieldThursdayOpeningTime.text!, endTime: self.textFieldThursdayClosingTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Thursday", startTime: self.textFieldThursdayClosingTime.text!, endTime: self.textFieldThursdayClosingTime.text!))
            }

        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Thursday" }
            
        }

    }
    @IBAction func buttonFridayDidClick(_ sender: Any) {
        self.buttonFriday.isSelected = !self.buttonFriday.isSelected
        if buttonFriday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Friday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Friday", startTime: self.textFieldFridayOpeningTime.text!, endTime: self.textFieldFridayClosingTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Friday", startTime: self.textFieldFridayOpeningTime.text!, endTime: self.textFieldFridayClosingTime.text!))
            }

        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Friday" }
            
        }

    }
    @IBAction func buttonSaturdayDidClick(_ sender: Any) {
        self.buttonSatuday.isSelected = !self.buttonSatuday.isSelected
        if buttonSatuday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Saturday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Saturday", startTime: self.textFieldSaturdayOpeningTime.text!, endTime: self.textFieldSaturdayClosingTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Saturday", startTime: self.textFieldSaturdayOpeningTime.text!, endTime: self.textFieldSaturdayClosingTime.text!))
            }

        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Saturday" }
            
        }

    }
    @IBAction func buttonSundayDidClick(_ sender: Any) {
        self.buttonSunday.isSelected = !self.buttonSunday.isSelected
        if buttonSunday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Sunday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Sunday", startTime: self.textFieldSundayOpeningTime.text!, endTime: self.textFieldSundayOpeningTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Sunday", startTime: self.textFieldSundayOpeningTime.text!, endTime: self.textFieldSundayClosingTime.text!))
            }
        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Sunday" }
            
        }

    }
    
    @IBAction func buttonUploadLicenseDidClick(_ sender: Any) {
        self.showImagePicker(picker: picker1)

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnbackDidClick(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnSubmitDidClick(_ sender: Any) {
        self.startLoaderAnimation()
        self.presenter.addTiming(restaurantId: self.restaurantID, licenseData: self.imageViewLicense.image) { (status, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.showAlertWith(message: message) {
                    if status{
//                        success
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
            }
            
        }
    }
    
}

extension AddTimingView {
    
    //MARK: - Self Methods -
    
    
    func showImagePicker(picker : UIImagePickerController){
        

        let alertMessage = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        let titleAttributes = [NSAttributedString.Key.font: ceraProLargeFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let titleString = NSAttributedString(string: "HotMenu", attributes: titleAttributes as [NSAttributedString.Key : Any])
        let messageAttributes = [NSAttributedString.Key.font: ceraProNormalFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let messageString = NSAttributedString(string: "Please select Image for your Restaurant Logo", attributes: messageAttributes as [NSAttributedString.Key : Any])
        alertMessage.setValue(titleString, forKey: "attributedTitle")
        alertMessage.setValue(messageString, forKey: "attributedMessage")

        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                picker.delegate = self
                picker.sourceType = .camera;
                picker.allowsEditing = false
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        alertMessage.addAction(cameraAction)
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                picker.delegate = self
                picker.sourceType = .photoLibrary;
                picker.allowsEditing = false
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        alertMessage.addAction(galleryAction)
        let action = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alertMessage .addAction(action)
        alertMessage.view.tintColor = themeDarkColor
        
        self.present(alertMessage, animated: true, completion: nil)

    }

    func createDatePicker(textField : UITextField, picker : UIDatePicker) {
        
        //toolbar
        //Toolbar for "Cancel" and "Done"
        
        textField.inputView = picker
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.view.frame.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionDone), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionCancel), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
        view_Accessory.backgroundColor = UIColor(named: "TextColorDark")
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        textField.inputAccessoryView = view_Accessory
        textField.inputView = picker
        
        //format picker for date
        picker.datePickerMode = .time
        
    }

    
        @objc func selectionDone(_ sender: UIButton ) {
            
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
//            let time = formatter.string(from: sender.date)
//
            if textFieldMondayOpeningTime.isFirstResponder
            {
                textFieldMondayOpeningTime.text = formatter.string(from: pickerMonday!.date)
                self.button_Moday.isSelected = true
                let time = Times(day: "Monday", startTime: formatter.string(from: pickerMonday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Monday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Monday", startTime: formatter.string(from: pickerMonday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }
                textFieldMondayClosingTime?.becomeFirstResponder()
            }else if textFieldMondayClosingTime.isFirstResponder{
                textFieldMondayClosingTime.text = formatter.string(from: pickerMonday!.date)
                let time = Times(day: "Monday", startTime: "", endTime: formatter.string(from: pickerMonday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Monday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Monday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerMonday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldMondayClosingTime.resignFirstResponder()
            }
            
            if textFieldTuesdayOpeningTime.isFirstResponder
            {
                textFieldTuesdayOpeningTime.text = formatter.string(from: pickerTuesday!.date)
                self.buttonTuesday.isSelected = true
                let time = Times(day: "Tuesday", startTime: formatter.string(from: pickerTuesday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Tuesday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Tuesday", startTime: formatter.string(from: pickerTuesday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldTuesdayClosingTime?.becomeFirstResponder()
            }else if textFieldTuesdayClosingTime.isFirstResponder{
                textFieldTuesdayClosingTime.text = formatter.string(from: pickerTuesday!.date)
                let time = Times(day: "Tuesday", startTime: "", endTime: formatter.string(from: pickerTuesday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Tuesday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Tuesday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerTuesday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldTuesdayClosingTime.resignFirstResponder()
            }
            
            if textFieldSednesdayOpeningTime.isFirstResponder
            {
                textFieldSednesdayOpeningTime.text = formatter.string(from: pickerWednesday!.date)
                self.buttonWednesday.isSelected = true
                let time = Times(day: "Wednesday", startTime: formatter.string(from: pickerWednesday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Wednesday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Wednesday", startTime: formatter.string(from: pickerWednesday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldWednesdayClosingTime?.becomeFirstResponder()
            }else if textFieldWednesdayClosingTime.isFirstResponder{
                textFieldWednesdayClosingTime.text = formatter.string(from: pickerMonday!.date)
                let time = Times(day: "Wednesday", startTime: "", endTime: formatter.string(from: pickerWednesday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Wednesday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Wednesday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerWednesday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldWednesdayClosingTime.resignFirstResponder()
            }

            if textFieldThursdayOpeningTime.isFirstResponder
            {
                textFieldThursdayOpeningTime.text = formatter.string(from: pickerThursday!.date)
                self.buttonThursday.isSelected = true
                let time = Times(day: "Thursday", startTime: formatter.string(from: pickerThursday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Thursday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Thursday", startTime: formatter.string(from: pickerThursday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldThursdayClosingTime?.becomeFirstResponder()
            }else if textFieldThursdayClosingTime.isFirstResponder{
                textFieldThursdayClosingTime.text = formatter.string(from: pickerThursday!.date)
                let time = Times(day: "Thursday", startTime: "", endTime: formatter.string(from: pickerThursday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Thursday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Thursday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerThursday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldThursdayClosingTime.resignFirstResponder()
            }

            if textFieldFridayOpeningTime.isFirstResponder
            {
                textFieldFridayOpeningTime.text = formatter.string(from: pickerFriday!.date)
                self.buttonFriday.isSelected = true
                let time = Times(day: "Friday", startTime: formatter.string(from: pickerFriday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Friday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Friday", startTime: formatter.string(from: pickerFriday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldFridayClosingTime?.becomeFirstResponder()
            }else if textFieldFridayClosingTime.isFirstResponder{
                textFieldFridayClosingTime.text = formatter.string(from: pickerFriday!.date)
                let time = Times(day: "Friday", startTime: "", endTime: formatter.string(from: pickerFriday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Friday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Friday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerFriday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldFridayClosingTime.resignFirstResponder()
            }
            
            if textFieldSaturdayOpeningTime.isFirstResponder
            {
                textFieldSaturdayOpeningTime.text = formatter.string(from: pickerSaturday!.date)
                self.buttonSatuday.isSelected = true
                let time = Times(day: "Saturday", startTime: formatter.string(from: pickerSaturday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Saturday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Saturday", startTime: formatter.string(from: pickerSaturday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldSaturdayClosingTime?.becomeFirstResponder()
            }else if textFieldSaturdayClosingTime.isFirstResponder{
                textFieldSaturdayClosingTime.text = formatter.string(from: pickerSaturday!.date)
                let time = Times(day: "Saturday", startTime: "", endTime: formatter.string(from: pickerSaturday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Saturday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Saturday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerSaturday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldSaturdayClosingTime.resignFirstResponder()
            }
            
            if textFieldSundayOpeningTime.isFirstResponder
            {
                textFieldSundayOpeningTime.text = formatter.string(from: pickerSunday!.date)
                self.buttonSunday.isSelected = true
                let time = Times(day: "Sunday", startTime: formatter.string(from: pickerSunday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Sunday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Sunday", startTime: formatter.string(from: pickerSunday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldSundayClosingTime?.becomeFirstResponder()
            }else if textFieldSundayClosingTime.isFirstResponder{
                textFieldSundayClosingTime.text = formatter.string(from: pickerSunday!.date)
                let time = Times(day: "Sunday", startTime: "", endTime: formatter.string(from: pickerSunday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Sunday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Sunday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerSunday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldSundayClosingTime.resignFirstResponder()
            }



            
        }
        
        @objc func selectionCancel(_ sender: UIButton ) {
            if textFieldMondayOpeningTime.isFirstResponder
            {
                textFieldFridayClosingTime?.resignFirstResponder()
            }else if textFieldMondayClosingTime.isFirstResponder{
                textFieldMondayClosingTime.resignFirstResponder()
            }
            
            if textFieldTuesdayOpeningTime.isFirstResponder
            {
                textFieldTuesdayOpeningTime?.resignFirstResponder()
            }else if textFieldTuesdayClosingTime.isFirstResponder{
                textFieldTuesdayClosingTime.resignFirstResponder()
            }
            
            if textFieldSednesdayOpeningTime.isFirstResponder
            {
                textFieldSednesdayOpeningTime?.resignFirstResponder()
            }else if textFieldWednesdayClosingTime.isFirstResponder{
                textFieldWednesdayClosingTime.resignFirstResponder()
            }

            if textFieldThursdayOpeningTime.isFirstResponder
            {
                textFieldThursdayOpeningTime?.resignFirstResponder()
            }else if textFieldThursdayClosingTime.isFirstResponder{
                textFieldThursdayClosingTime.resignFirstResponder()
            }

            if textFieldFridayOpeningTime.isFirstResponder
            {
                textFieldFridayOpeningTime?.resignFirstResponder()
            }else if textFieldFridayClosingTime.isFirstResponder{
                textFieldFridayClosingTime.resignFirstResponder()
            }
            
            if textFieldSaturdayOpeningTime.isFirstResponder
            {
                textFieldSaturdayOpeningTime?.resignFirstResponder()
            }else if textFieldSaturdayClosingTime.isFirstResponder{
                textFieldSaturdayClosingTime.resignFirstResponder()
            }
            
            if textFieldSundayOpeningTime.isFirstResponder
            {
                textFieldSaturdayOpeningTime?.resignFirstResponder()
            }else if textFieldSundayClosingTime.isFirstResponder{
                textFieldSundayOpeningTime.resignFirstResponder()
            }


        }

}

extension AddTimingView : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    //MARK: - Image Picker -
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if picker == picker1{//Logo
            if let pickedImage = info[.originalImage] as? UIImage {
                self.imageViewLicense.image = pickedImage
            }
            picker.dismiss(animated: true, completion: nil)

        }
    }
}
