//
//  ChooseLocationView.swift
//  Restaurant
//
//  Created by Souvik on 04/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
protocol ChooseLocationDelegate {
    func gotAddress(address : String, location : CLLocationCoordinate2D)
}

class ChooseLocationView: BaseViewController {
    @IBOutlet weak var textFieldLocation: CustomTextField!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var tableViewAddress: UITableView!
    
    var autocompleteResults :[GApiResponse.Autocomplete] = []
    var delegate : ChooseLocationDelegate?
    var selectedAddress = ""
    var selectedLocation = CLLocationCoordinate2D()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewAddress.dataSource = self
        self.tableViewAddress.delegate = self

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewLocation.setRoundedCornered(height: viewLocation.frame.size.height)
    }

    
    @IBAction func btnBackDidCLick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDoneDidClick(_ sender: Any) {
        self.delegate?.gotAddress(address: self.selectedAddress, location: self.selectedLocation)
        self.navigationController?.popViewController(animated: true)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ChooseLocationView{
    func showResults(string:String){
        var input = GInput()
        input.keyword = string
        GoogleApi.shared.callApi(input: input) { (response) in
            if response.isValidFor(.autocomplete) {
                DispatchQueue.main.async {
                    self.autocompleteResults = response.data as! [GApiResponse.Autocomplete]
                    self.tableViewAddress.reloadData()
                }
            } else { print(response.error ?? "ERROR") }
        }
    }
    func hideResults(){
        autocompleteResults.removeAll()
        tableViewAddress.reloadData()
    }

}


extension ChooseLocationView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        hideResults() ; return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text! as NSString
        let fullText = text.replacingCharacters(in: range, with: string)
        if fullText.count > 2 {
            showResults(string:fullText)
        }else{
            hideResults()
        }
        return true
    }
    
}


extension ChooseLocationView : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return autocompleteResults.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchResultCell")
        let label = cell?.viewWithTag(1) as! UILabel
        label.text = autocompleteResults[indexPath.row].formattedAddress
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        textFieldLocation.text = autocompleteResults[indexPath.row].formattedAddress
        selectedAddress = autocompleteResults[indexPath.row].formattedAddress
        textFieldLocation.resignFirstResponder()
        var input = GInput()
        input.keyword = autocompleteResults[indexPath.row].placeId
        GoogleApi.shared.callApi(.placeInformation,input: input) { (response) in
            if let place =  response.data as? GApiResponse.PlaceInfo, response.isValidFor(.placeInformation) {
                DispatchQueue.main.async {
                    if let lat = place.latitude, let lng = place.longitude{
                        let center  = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                        print(center)
                        self.selectedLocation = center
                        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                        print(region)
                    }
                    self.autocompleteResults.removeAll()
                    self.tableViewAddress.reloadData()
                }
            } else { print(response.error ?? "ERROR") }
        }
    }
}
