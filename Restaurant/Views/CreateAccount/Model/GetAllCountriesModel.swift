//
//  GetAllCountriesModel.swift
//  Restaurant
//
//  Created by Bit Mini on 12/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

class GetAllCountriesModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: [CountriesModel]?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: [CountriesModel]?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseDatum
class CountriesModel: Codable {
    let name, id: String?

    enum CodingKeys: String, CodingKey {
        case name
        case id = "_id"
    }

    init(name: String?, id: String?) {
        self.name = name
        self.id = id
    }
}
