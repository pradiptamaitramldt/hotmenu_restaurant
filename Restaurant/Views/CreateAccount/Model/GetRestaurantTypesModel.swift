//
//  GetRestaurantTypesModel.swift
//  Restaurant
//
//  Created by Souvik on 23/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation
import Foundation

// MARK: - GetRestaurantTypesModel
class GetRestaurantTypesModel: Codable {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: GetRestaurantTypesResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: GetRestaurantTypesResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class GetRestaurantTypesResponseData: Codable {
    var vendorTypes: [VendorType]?

    init(vendorTypes: [VendorType]?) {
        self.vendorTypes = vendorTypes
    }
}

// MARK: - VendorType
class VendorType: Codable {
    var isActive: Bool?
    var id, name: String?
    var v: Int?
    var createdAt, updatedAt: String?
    var isSelected = false
    enum CodingKeys: String, CodingKey {
        case isActive
        case id = "_id"
        case name
        case v = "__v"
        case createdAt, updatedAt
    }

    init(isActive: Bool?, id: String?, name: String?, v: Int?, createdAt: String?, updatedAt: String?) {
        self.isActive = isActive
        self.id = id
        self.name = name
        self.v = v
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}
