//
//  RegistraionResponseModel.swift
//  Restaurant
//
//  Created by Bit Mini on 13/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//


import Foundation

// MARK: - RestaurantDetailsAPI
class RegistraionResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: RegistraionModel?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: RegistraionModel?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class RegistraionModel: Codable {
    let userDetails: UserDetails?
    let authToken: String?
    let restaurantName: String?

    init(userDetails: UserDetails?, authToken: String?, restaurantName: String?) {
        self.userDetails = userDetails
        self.authToken = authToken
        self.restaurantName = restaurantName
    }
}

// MARK: - UserDetails
class UserDetails: Codable {
    let firstName, lastName, vendorID, email: String?
    let phone, location, id, loginID: String?
    let profileImage, userType, loginType: String?

    enum CodingKeys: String, CodingKey {
        case firstName, lastName
        case vendorID = "vendorId"
        case email, phone, location, id
        case loginID = "loginId"
        case profileImage, userType, loginType
    }

    init(firstName: String?, lastName: String?, vendorID: String?, email: String?, phone: String?, location: String?, id: String?, loginID: String?, profileImage: String?, userType: String?, loginType: String?) {
        self.firstName = firstName
        self.lastName = lastName
        self.vendorID = vendorID
        self.email = email
        self.phone = phone
        self.location = location
        self.id = id
        self.loginID = loginID
        self.profileImage = profileImage
        self.userType = userType
        self.loginType = loginType
    }
}

