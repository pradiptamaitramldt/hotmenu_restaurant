//
//  AddTimingRequestDataModel.swift
//  Restaurant
//
//  Created by Souvik on 18/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class AddTimingRequestDataModel : Codable {
    var restaurantTime : [Times] = []
    init() {
        
    }
    
    init(restaurantTime : [Times]) {
        self.restaurantTime = restaurantTime
    }
}

class Times  : Codable{
    var day : String = ""
    var startTime : String = ""
    var endTime : String = ""
    
    init() {
        
    }

    init(day : String, startTime : String, endTime : String) {
        self.day = day
        self.startTime = startTime
        self.endTime = endTime
    }

}
