//
//  CreateAccountPresenter.swift
//  Restaurant
//
//  Created by Bit Mini on 12/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit


class CreateAccountPresenter: NSObject {
    let objAddTiming = AddTimingRequestDataModel()
    var restaurantID = ""
    var arrRestaurantTypes : [VendorType] = []
    
    func createAccount(restaurantName : String, managerName : String, restaurantType : String, email : String, countryCode : String, phone : String,  location : String, latitude : String, longitude : String, password : String,confirmPassword : String, isLogo : Bool, logoData : UIImage?, isBanner : Bool, bannerData : UIImage?, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        if restaurantName.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            if restaurantType.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                if Utility.isValidEmail(email){
                    if Utility.isValidMobileNumber(phone){
                        if countryCode.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                            if Utility.isValidPassword(password){
                                if password == confirmPassword{
                                    if location.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
//                                        if bannerData != nil{
                                            let param : [String: Any] = ["restaurantName" : restaurantName,
                                                                         "managerName"  : managerName,
                                                                         "restaurantType"     : restaurantType,
                                                                         "restaurantEmail"     : email,
                                                                         "countryCode": countryCode,
                                                                         "restaurantPhone"     : phone,
                                                                         "location"   : location,
                                                                         "password"   : password,
                                                                         "confirmPassword": confirmPassword,
                                                                         "latitude"  : latitude,
                                                                         "longitude"  : longitude,
                                                                         "deviceToken" : UserDefaultValues.deviceToken,
                                                                         "appType" : "IOS",
                                                                         "pushMode" : UserDefaultValues.pushMode,
                                                                         "logo" : logoData,
                                                                         "banner" : bannerData]
                                            WebServiceManager.shared.requestMultipartAPIForTwoImages(url: WebServiceConstants.registerUser, parameter: param, httpMethodType: .post, isAuthorized: false, authToken: "") { (data, err) in
                                                if let responseData = data{
                                                    let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: RegistrationNewResponseModel.self) { (resData, err) in
                                                        if resData != nil{
                                                            if resData?.statuscode! == ResponseCode.success.rawValue{
                                                                self.restaurantID = (resData?.responseData?.vendorID!)!
                                                                completion(true,(resData?.message!)!)
                                                            }else{
                                                                completion(false,(resData?.message!)!)
                                                            }
                                                        }else{
                                                            completion(false, err!.localizedDescription)
                                                        }
                                                    }
                                                }else{
                                                    completion(false, "Request Timeout.")
                                                    
                                                }
                                            }
                                            
//                                        }else{
//                                            completion(false, "Please add restaurant banner image.")
//                                        }
                                    }else{
                                        completion(false, "Please enter Restaurant Location")
                                    }
                                }else{
                                    completion(false, "Confirm Password does'nt match.")
                                }
                            }else{
                                completion(false, "Please add a suitable password (Password must be more than 8 characters, with atleast one capital, One small letter and atleast one numeric character).")
                            }
                        }else{
                            completion(false, "Please add your Country Code.")
                        }
                    }else{
                        completion(false, "Please enter your Mobile No. (Mobile number should be contain 10-11 digits)")
                    }
                }else{
                    completion(false, "Please enter Your Email.")
                }
                
            }else{
                completion(false , "Please select Restaurant type")
            }
        }else{
            completion(false, "Please enter your Resturant name.")
        }
        
    }
    
    
    
    func addTiming(restaurantId : String, licenseData : UIImage?, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let jsonEncoder = JSONEncoder()
        do{
            let jsonData = try jsonEncoder.encode(self.objAddTiming.restaurantTime)
            let json = String(data: jsonData, encoding: .utf8)//String(data: jsonData, encoding: String.Encoding.utf8)
            print(json!)
//            if licenseData != nil{
                let param : [String: Any] = ["restaurantTime" : json!,
                                             "vendorId"  : restaurantId,
                                             "licenceImage"     : licenseData]
                WebServiceManager.shared.requestMultipartAPIForTwoImages(url: WebServiceConstants.addTiming, parameter: param, httpMethodType: .post, isAuthorized: false, authToken: "") { (data, err) in
                    if let responseData = data{
                        let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                            if resData != nil{
                                if resData?.statuscode! == ResponseCode.success.rawValue{
                                    
                                    completion(true,(resData?.message!)!)
                                }else{
                                    completion(false,(resData?.message!)!)
                                }
                            }else{
                                completion(false, err!.localizedDescription)
                            }
                        }
                    }else{
                        completion(false, "Request Timeout.")
                    }
                }
//            }else{
//                completion(false, "Please acc CAC Certificate image")
//            }

        }catch{
            
        }

        
    }
    
    func getAllPossibleResturantTypes(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getRestaurantTypes, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: nil, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetRestaurantTypesModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.arrRestaurantTypes = (resData?.responseData?.vendorTypes)!
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
            }
        }
    }
    
    
}
