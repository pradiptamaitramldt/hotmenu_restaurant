//
//  DeliveryBoyListTableCell.swift
//  Restaurant
//
//  Created by Pradipta Maitra on 14/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class DeliveryBoyListTableCell: UITableViewCell {

    @IBOutlet weak var imageView_profile: UIImageView!
    @IBOutlet weak var label_title: UILabel!
    @IBOutlet weak var label_email: UILabel!
    @IBOutlet weak var label_phone: UILabel!
    @IBOutlet weak var label_numberPlate: UILabel!
    @IBOutlet weak var label_vehicle: UILabel!
    @IBOutlet weak var image_check: UIImageView!
    
    private let imgSelected = UIImage(named: "check")
    private let imgNotSelected = UIImage(named: "uncheck")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.image_check.image = nil
        imageView_profile?.setRoundedCornered(height: self.imageView_profile?.frame.width ?? 58 / 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        self.image_check.image = selected ? imgSelected : imgNotSelected
    }

}
