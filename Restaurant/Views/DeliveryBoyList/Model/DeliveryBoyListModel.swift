//
//  DeliveryBoyListModel.swift
//  Restaurant
//
//  Created by Pradipta Maitra on 14/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation

// MARK: - DeliveryBoyResponseModel
struct DeliveryBoyResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: [DeliveryBoyData]?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseDatum
struct DeliveryBoyData: Codable {
    let countryCode, location: String?
    let profileImage: String?
    let isActive: Bool?
    let deviceToken, numberPlate, driverLicense, vehicle: String?
    let id, firstName, lastName, email: String?
    let phone: Int?
    let password, loginType, createdAt, updatedAt: String?
    let v: Int?
    let appType: String?
    let  isAvailable : Bool?
    enum CodingKeys: String, CodingKey {
        case countryCode, location, profileImage, isActive, deviceToken, numberPlate, driverLicense, vehicle,isAvailable
        case id = "_id"
        case firstName, lastName, email, phone, password, loginType, createdAt, updatedAt
        case v = "__v"
        case appType
    }
}
//5fae4f7d0faff267338ed976
