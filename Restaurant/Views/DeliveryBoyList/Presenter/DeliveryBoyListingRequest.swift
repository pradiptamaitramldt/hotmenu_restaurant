//
//  DeliveryBoyListingRequest.swift
//  Restaurant
//
//  Created by BrainiumSSD on 16/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit
import Alamofire

class DeliveryBoyListingRequest: NSObject {

    //"id":"5f6c86c5a56f04095b322e17"
    // vendorId : 5f219079af26827ae127bb2e
    // loginId : 5fb25a031c1ecc43c0914d99
    // auth : iVDnlp95hSlovhfX8p76ipNRGOK_9a1RJvbz3_Y3hvQ
    var utility = Utility()
    private func printJSON(apiName: String, data: Data) {
        print("API name: ", apiName)
        print(String(data: data, encoding: String.Encoding.utf8) ?? "")
    }
    func presentAlertWithTitle(title: String, message : String, vc: UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in print("Youve pressed OK Button")
        }
        alertController.addAction(OKAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    func fetchDeliveryBoyData<T : APIResponseParentModel>(vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
        
        let fetchAllUserUrl =  WebServiceConstants.getDeliveryBoyList
       let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
       "vendorId" : UserDefaultValues.vendorID]
        print("params",param)
        
        print("Auth",WebServiceHeaderGenerator.generateHeaderWithPublicKey())
        print("API",fetchAllUserUrl)
        Alamofire.request(fetchAllUserUrl, method: .post, parameters: param, encoding: URLEncoding.default, headers: WebServiceHeaderGenerator.generateHeaderWithPublicKey()).responseData { response in

            switch(response.result){
            case .success(_):
                self.printJSON(apiName: fetchAllUserUrl, data: response.result.value!)
                print(response.result.description)
                if response.result.value != nil {
                    completion(response.result.value, "", true)
                }
                //self.presentAlertWithTitle(title: "Alert", message: (response.result.message)!, vc: vc)
                break
            case .failure(_):
                print(response.result.error ?? "Fail")
                self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                break
            }
        }
    }
    func assignDeliveryBoyData<T : APIResponseParentModel>(vc: UIViewController,deliveryBoyId : String,orderId:String, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
        
        let fetchAllUserUrl =  WebServiceConstants.assignDeliveryBoy
       let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
       "vendorId" : UserDefaultValues.vendorID,
       "deliveryBoyId" : deliveryBoyId,
       "orderId":orderId]
        print("params",param)
        
        print("Auth",WebServiceHeaderGenerator.generateHeaderWithPublicKey())
        print("API",fetchAllUserUrl)
        Alamofire.request(fetchAllUserUrl, method: .post, parameters: param, encoding: URLEncoding.default, headers: WebServiceHeaderGenerator.generateHeaderWithPublicKey()).responseData { response in

            switch(response.result){
            case .success(_):
                self.printJSON(apiName: fetchAllUserUrl, data: response.result.value!)
                print(response.result.description)
                if response.result.value != nil {
                    completion(response.result.value, "", true)
                }
                //self.presentAlertWithTitle(title: "Alert", message: (response.result.message)!, vc: vc)
                break
            case .failure(_):
                print(response.result.error ?? "Fail")
                self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                break
            }
        }
    }
}
