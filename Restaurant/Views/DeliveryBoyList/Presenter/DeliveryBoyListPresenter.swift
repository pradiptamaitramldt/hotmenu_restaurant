//
//  DeliveryBoyListPresenter.swift
//  Restaurant
//
//  Created by Pradipta Maitra on 14/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class DeliveryBoyListPresenter: NSObject {
    var deliveryListArray: [DeliveryBoyData] = []
    func deliveryBoyList(completion : @escaping(_ success : Bool ,_ message : String, _ response: [DeliveryBoyData]) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId" : UserDefaultValues.vendorID]
      //  let param : [String: Any] = ["customerId" : "5f6b1ce95588532877a0b4c0",
     //   "vendorId" : "5f16a8d3f80ec1769b23fcf9"]
        
        self.callPost(url: URL(string: WebServiceConstants.getDeliveryBoyList)!, params: param, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKeyy()) { (message, data) in
            let str = String(decoding: data!, as: UTF8.self)
            print("response:\(str)")
            JSONResponseDecoder.decodeFrom(data!, returningModelType: DeliveryBoyResponseModel.self, completion: { (successUploadProfilePicture, parsingError) in
                if parsingError == nil {
                    guard let successResponse = successUploadProfilePicture, let successMessage = successResponse.message else { return }
                    completion(true,(successMessage), successResponse.responseData ?? [])
                } else {
                    completion(false, "error happens", [])
                }
            })
        }
        
        
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getDeliveryBoyList, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .get) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: DeliveryBoyResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.deliveryListArray = resData?.responseData ?? []
                            completion(true,(resData?.message!)!, self.deliveryListArray)
                        }else{
                            completion(false,(resData?.message!)!, [])
                        }
                    }else{
                        print("Error",err)
                        completion(false, err!.localizedDescription, [])
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue, [])
                
            }
        }
    }
    
    func assignDeliveryBoy(deliveryBoyID: String, orderID: String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId" : UserDefaultValues.vendorID,
                                     "deliveryBoyId" : deliveryBoyID,
                                     "orderId" : orderID]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.assignDeliveryBoy, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: DashboardResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
                
            }
        }
    }
    
    func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }

    func callPost(url:URL, params:[String:Any], httpHeader: [String: String], finish: @escaping ((message:String, data:Data?)) -> Void)
    {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = httpHeader
        
        Utility.log("URL: \(url)")
        Utility.log("Header: \(httpHeader)")
        Utility.log("Parameter: ")
        Utility.log(params)
        
        let postString = self.getPostString(params: params)
        request.httpBody = postString.data(using: .utf8)

        var result:(message:String, data:Data?) = (message: "Fail", data: nil)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in

            if(error != nil)
            {
                result.message = "Fail Error not null : \(error.debugDescription)"
            }
            else
            {
                result.message = "Success"
                result.data = data
            }

            finish(result)
        }
        task.resume()
    }
}
