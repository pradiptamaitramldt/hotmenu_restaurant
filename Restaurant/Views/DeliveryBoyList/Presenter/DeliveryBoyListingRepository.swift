//
//  DeliveryBoyListingRepository.swift
//  Restaurant
//
//  Created by BrainiumSSD on 16/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit

class DeliveryBoyListingRepository: NSObject {
    func fetchDeliveryBoyData(vc: UIViewController,completion: @escaping (DeliveryBoyResponseModel, Bool, NSError?) -> Void) {
       
        let request = DeliveryBoyListingRequest()
        request.fetchDeliveryBoyData(vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(DeliveryBoyResponseModel.self, from: response! as! Data)
                   // let message = objResponse.message
                 //   print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
                print("objResponse\(response)")
            }
        }
    }
    func assignDeliveryBoyData(vc: UIViewController,deliveryBoyId : String,orderId:String,completion: @escaping (DeliveryBoyResponseModel, Bool, NSError?) -> Void) {
       
        let request = DeliveryBoyListingRequest()
        request.assignDeliveryBoyData(vc: vc,deliveryBoyId : deliveryBoyId,orderId:orderId, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(DeliveryBoyResponseModel.self, from: response! as! Data)
                   // let message = objResponse.message
                 //   print("message...\(message ?? "NA")")
                    completion(objResponse, success, nil)
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
                print("objResponse\(response)")
            }
        }
    }
}
