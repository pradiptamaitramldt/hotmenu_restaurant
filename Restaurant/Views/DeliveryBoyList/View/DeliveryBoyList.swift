//
//  DeliveryBoyList.swift
//  Restaurant
//
//  Created by Pradipta Maitra on 14/11/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit
import Alamofire
import AMShimmer

class DeliveryBoyList: UIViewController {

    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var bottom_view: UIView!
    @IBOutlet weak var buttonDone: UIBarButtonItem!
    @IBOutlet var allDeliveryButton: CustomButton!
    @IBOutlet var activeButton: CustomButton!
    @IBOutlet var statusLabel: CustomBoldFontLabel!
    
    // var presenter = DeliveryBoyListPresenter()
    var orderID: String = ""
    var bottom_menu = BottomMenu()
    var deliveryListArray: [DeliveryBoyData] = []
    var activeDeliveryBoyData : [DeliveryBoyData] = []
    var selectedTab = "Allll"
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.bottom_view.bounds.size.width,height:self.bottom_view.bounds.size.height)
        self.bottom_view.addSubview(bottom_menu)
        bottom_menu.notesSeleted()
        bottom_menu.delegate = self
        
        self.table_view.delegate = self
        self.table_view.dataSource = self
   
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
        fetchDeliveryBoyFetch()
    }
    func fetchDeliveryBoyFetch() {
        let fetchAllUserRepository = DeliveryBoyListingRepository()
        AMShimmer.start(for: self.table_view)
       fetchAllUserRepository.fetchDeliveryBoyData(vc:  self) { (response, success, error) in
        DispatchQueue.main.async {
            AMShimmer.stop(for: self.table_view)
        }
                  if (error != nil) {
                     
                  }
                  if (success) {
                      if response.statuscode == 200 {
                        self.selectedTab = "All"
                        self.deliveryListArray = response.responseData ?? []
                        if (response.responseData?.count ?? 0) > 0 {
                            for index in 0...((response.responseData?.count ?? 1)  - 1) {

                                if response.responseData?[index].isAvailable == true {
                                    self.activeDeliveryBoyData.append((response.responseData?[index])!)
                                }
                            }
                        }
                        if self.deliveryListArray.count == 0 {
                            self.statusLabel.text = "No Delivery Boy Found"
                        }
                        else {
                            self.statusLabel.text = ""
                        }
                    self.table_view.reloadData()
                      }else {
                          
                      }
                  }else{
                     
                  }
    }
}
    func assignDeliveryBoy(deliveryBoyID: String,orderId : String) {
        
      let fetchAllUserRepository = DeliveryBoyListingRepository()
        fetchAllUserRepository.assignDeliveryBoyData(vc:  self,deliveryBoyId: deliveryBoyID,orderId: orderId) { (response, success, error) in
                   
                   if (error != nil) {
                      
                   }
                   if (success) {
                       if response.statuscode == 200 {
                         self.deliveryListArray = response.responseData ?? []
                         for index in 0...((response.responseData?.count ?? 1)  - 1) {

                             if response.responseData?[index].isActive == true {
                                 self.activeDeliveryBoyData.append((response.responseData?[index])!)
                             }
                         }
                         
                         self.table_view.reloadData()
                       }else {
                           
                       }
                   }else{
                      
                   }
//        self.presenter.assignDeliveryBoy(deliveryBoyID: deliveryBoyID, orderID: self.orderID) { (success, message) in
//            self.stopLoaderAnimation()
//            let refreshAlert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
//
//            refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
//              print("Handle Ok logic here")
//                self.navigationController?.popViewController(animated: true)
//              }))
//
//            self.present(refreshAlert, animated: true, completion: nil)
//        }
        }
    }
    @IBAction func allDeliveryBoyButtonAction(_ sender: Any) {
        selectedTab = "All"
        activeButton.backgroundColor = .clear
        activeButton.setNormalTitleColor(.black)
        allDeliveryButton.backgroundColor = #colorLiteral(red: 0.1333333333, green: 0.1411764706, blue: 0.3333333333, alpha: 1)
        allDeliveryButton.setNormalTitleColor(.white)
        if self.deliveryListArray.count == 0 {
            self.statusLabel.text = "No Delivery Boy Found"
        }
        else {
            self.statusLabel.text = ""
        }
        self.table_view.reloadData()
    }
    @IBAction func activeDeliveryBoyAction(_ sender: Any) {
       selectedTab = "Active"
        allDeliveryButton.backgroundColor = .clear
        allDeliveryButton.setNormalTitleColor(.black)
        activeButton.backgroundColor = #colorLiteral(red: 0.1333333333, green: 0.1411764706, blue: 0.3333333333, alpha: 1)
        activeButton.setNormalTitleColor(.white)
        if self.activeDeliveryBoyData.count == 0 {
            self.statusLabel.text = "No Active Delivery Boy Found"
        }
        else {
            self.statusLabel.text = ""
        }
        self.table_view.reloadData()
    }
    
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonDoneAction(_ sender: Any) {
        
        
    }
    
}
extension DeliveryBoyList : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedTab == "All" {
           return  self.deliveryListArray.count
        }
        else if selectedTab == "Active" {
           return self.activeDeliveryBoyData.count
        }
        else {
            return 3
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "deliveryBoyListTableCell") as! DeliveryBoyListTableCell
        if selectedTab == "All" {
            let profileImage = self.deliveryListArray[indexPath.row].profileImage ?? ""
            if profileImage != "" {
                let url = URL(string: profileImage)
                cell.imageView_profile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "User_icon"))
            }else{
                cell.imageView_profile.image = #imageLiteral(resourceName: "User_icon")
            }
            cell.label_title.text = "\(self.deliveryListArray[indexPath.row].firstName ?? "NA") \(self.deliveryListArray[indexPath.row].lastName ?? "")"
            cell.label_email.text = self.deliveryListArray[indexPath.row].email
            cell.label_phone.text = "\(self.deliveryListArray[indexPath.row].phone ?? 0)"
            cell.label_numberPlate.text = self.deliveryListArray[indexPath.row].numberPlate
            cell.label_vehicle.text = self.deliveryListArray[indexPath.row].vehicle
        }
        else if selectedTab == "Active" {
            let profileImage = self.activeDeliveryBoyData[indexPath.row].profileImage ?? ""
            if profileImage != "" {
                let url = URL(string: profileImage)
                cell.imageView_profile.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "User_icon"))
            }else{
                cell.imageView_profile.image = #imageLiteral(resourceName: "User_icon")
            }
            cell.label_title.text = "\(self.activeDeliveryBoyData[indexPath.row].firstName ?? "NA") \(self.activeDeliveryBoyData[indexPath.row].lastName ?? "")"
            cell.label_email.text = self.activeDeliveryBoyData[indexPath.row].email
            cell.label_phone.text = "\(self.activeDeliveryBoyData[indexPath.row].phone ?? 0)"
            cell.label_numberPlate.text = self.activeDeliveryBoyData[indexPath.row].numberPlate
            cell.label_vehicle.text = self.activeDeliveryBoyData[indexPath.row].vehicle
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedTab == "Active" {
            if let indexPath = tableView.indexPathForSelectedRow{
                let data = self.activeDeliveryBoyData[indexPath.row]
                print(data.firstName ?? "")
                
                let refreshAlert = UIAlertController(title: "", message: "Do you want to assign \(data.firstName ?? "NA")", preferredStyle: UIAlertController.Style.alert)

                refreshAlert.addAction(UIAlertAction(title: "ASSIGN", style: .default, handler: { (action: UIAlertAction!) in
                  print("Handle Ok logic here")
                    self.assignDeliveryBoy(deliveryBoyID: data.id ?? "", orderId: self.orderID)
                  }))

                refreshAlert.addAction(UIAlertAction(title: "CANCEL", style: .cancel, handler: { (action: UIAlertAction!) in
                  print("Handle Cancel Logic here")
                  }))

                present(refreshAlert, animated: true, completion: nil)
            }
        }

    }
}
//MARK: bottomMenu delegate
extension DeliveryBoyList: bottomMenuDelegate{
    func selectedButton(selectedItem:NSInteger){
        
        if selectedItem == 1 {
            //home
            //            let st = UIStoryboard.init(name: "DashBoardView", bundle: nil)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 0
            UIApplication.shared.windows[0].rootViewController = viewController
        }else if selectedItem == 2{
            //menu
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 1
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }
        else if selectedItem == 3{
            //OrderList
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 6
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }else{
            //userprofile
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 5
            UIApplication.shared.windows[0].rootViewController = viewController
        }
    }
    
}
