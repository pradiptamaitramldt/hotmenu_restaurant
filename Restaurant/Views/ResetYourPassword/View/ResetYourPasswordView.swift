//
//  ResetYourPasswordView.swift
//  Restaurant
//
//  Created by Bit Mini on 11/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class ResetYourPasswordView: BaseTableViewController {
    @IBOutlet weak var view_NewPassword: UIView!
    @IBOutlet weak var textFieldNewPassword: CustomTextField!
    @IBOutlet weak var view_ConfirmPassword: UIView!
    @IBOutlet weak var textFieldConfirmPassword: CustomTextField!
    @IBOutlet weak var btn_Submit: NormalBoldButton!
    var presenter = ResetPasswordPresenter()
    var email : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view_NewPassword.setRoundedCornered(height: view_NewPassword.frame.size.height)
        self.view_ConfirmPassword.setRoundedCornered(height: view_ConfirmPassword.frame.size.height)
        self.btn_Submit.setRoundedCornered(height: btn_Submit.frame.size.height)
        
    }
    
    @IBAction func btn_BackdidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_SubmitDidClick(_ sender: Any) {
       
        AMShimmer.start(for: self.btn_Submit)
        AMShimmer.start(for: self.textFieldConfirmPassword)
        AMShimmer.start(for: self.textFieldNewPassword)
        self.presenter.resetPassword(email: email, password: self.textFieldNewPassword.text!, confirmPassword: self.textFieldConfirmPassword.text!) { (status, message) in
            
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.btn_Submit)
                AMShimmer.stop(for: self.textFieldConfirmPassword)
                AMShimmer.stop(for: self.textFieldNewPassword)
                if status{
                    //success login after reset
                    //                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
                    //                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
                    //                    viewController.viewControllerIndex = 2
                    //                    UIApplication.shared.windows[0].rootViewController = viewController
                    
                    self.navigationController?.popToRootViewController(animated: true)
                }else{
                    
                    self.showAlertWith(message: message) {
                    }
                }
            }
        }
    }
}
// MARK: - Table view data source

extension ResetYourPasswordView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 43, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        case 3:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 148, currentScrenHeight: self.view.bounds.height)
        default:
            return 0.0
        }
    }
}
