//
//  ResetPasswordPresenter.swift
//  Restaurant
//
//  Created by Bit Mini on 13/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class ResetPasswordPresenter: NSObject {
    
    func resetPassword(email : String, password : String, confirmPassword : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        
        if Utility.isValidEmail(email){
            if Utility.isValidPassword(password){
                if password == confirmPassword{
                    let param : [String: Any] = ["email" : email,
                                                 "password" : password,
                                                 "confirmPassword" : confirmPassword,
                                                 "userType" : UserType.vendor.rawValue]
                    WebServiceManager.shared.requestAPI(url: WebServiceConstants.resetPassword, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: param, httpMethodType: .post) { (data, err) in
                        if let responseData = data{
                            let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: LoginResponseModel.self) { (resData, err) in
                                if resData != nil{
                                    if resData?.statuscode! == ResponseCode.success.rawValue{
                                        completion(true,(resData?.message!)!)
                                    }else{
                                        completion(false,(resData?.message!)!)
                                    }
                                }else{
                                    completion(false, err!.localizedDescription)
                                }
                            }
                        }
                    }
                }else{
                    completion(false, "Confirm password does'nt match.")
                }
            }else{
                completion(false, "Please add a suitable password (Password must be more than 8 characters, with atleast one capital, One small letter and atleast one numeric character).")
            }
        }else{
            completion(false, "Please enter Your Email.")
        }
        
        
    }
    
}
