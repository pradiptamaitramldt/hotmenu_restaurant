//
//  GetUserDetailsResponseModel.swift
//  Restaurant
//
//  Created by Bit Mini on 24/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation
// MARK: - GetUserDetailsResponseModel
class GetUserDetailsResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: UserDetailsData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: UserDetailsData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class UserDetailsData: Codable {
    let email, password, socialID, countryCode: String
    let countryCodeID: String
    let phone: Int
    let gender: String
    let profileImage: String
    let verifyOtp: String
    let lastLogin: String?
    let appType, deviceToken, id, fullName: String
    let status, userType, loginType, createdAt: String
    let updatedAt: String
    let v: Int

    enum CodingKeys: String, CodingKey {
        case email, password
        case socialID = "socialId"
        case countryCode
        case countryCodeID = "countryCodeId"
        case phone, gender, profileImage, verifyOtp, lastLogin, appType, deviceToken
        case id = "_id"
        case fullName, status, userType, loginType, createdAt, updatedAt
        case v = "__v"
    }
}
