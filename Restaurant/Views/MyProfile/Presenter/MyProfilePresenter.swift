//
//  MyProfilePresenter.swift
//  Restaurant
//
//  Created by Bit Mini on 24/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class MyProfilePresenter: NSObject {
    func getUserProfileDetail(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getUserDetails, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetUserDetailsResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            UserDefaultValues.profileImage = resData?.responseData?.profileImage ?? ""
                            UserDefaultValues.email = resData?.responseData?.email ?? ""
                            UserDefaultValues.phone = String(describing:(resData?.responseData?.phone)!)
                            UserDefaultValues.countryCode = resData?.responseData?.countryCode ?? ""
                            UserDefaultValues.userName = resData?.responseData?.fullName ?? ""
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }
        }
        
    }

}
