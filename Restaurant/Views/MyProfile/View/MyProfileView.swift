//
//  MyProfileView.swift
//  Restaurant
//
//  Created by Bit Mini on 24/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class MyProfileView: BaseTableViewController {

    @IBOutlet weak var view_ProfilePic: UIView!
    @IBOutlet weak var imageProfilePic: UIImageView!
    @IBOutlet weak var view_CameraButton: UIView!
    @IBOutlet weak var btn_EditProfile: NormalBoldButton!
    @IBOutlet weak var label_UserName: CustomNormalLabel!
    @IBOutlet weak var label_Email: CustomNormalLabel!
    @IBOutlet weak var label_Mobile: CustomNormalLabel!
    
    var bottom_menu = BottomMenu()
    var viewBottom = UIView()
    var presenter = MyProfilePresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if UserDefaultValues.userID != nil{

       
            AMShimmer.start(for: self.imageProfilePic)
            AMShimmer.start(for: self.label_Email)
            AMShimmer.start(for: self.label_Email)
            AMShimmer.start(for: self.label_UserName)
            AMShimmer.start(for: self.btn_EditProfile)
        self.presenter.getUserProfileDetail{ (status, message) in
          
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.imageProfilePic)
                AMShimmer.stop(for: self.label_Email)
                AMShimmer.stop(for: self.label_Email)
                AMShimmer.stop(for: self.label_UserName)
                AMShimmer.stop(for: self.btn_EditProfile)
                if status{
                    //success
                    self.label_UserName.text = "\(UserDefaultValues.userName)"
                    self.label_Email.text = "Email: \(UserDefaultValues.email)"
                    self.label_Mobile.text = "Phone: \(UserDefaultValues.countryCode)\(UserDefaultValues.phone)"
                    if UserDefaultValues.profileImage != ""{
                        let url = URL(string: UserDefaultValues.profileImage)
                        self.imageProfilePic.sd_setImage(with: url, placeholderImage: nil)
//                            let data = try? Data(contentsOf: url!)
//                            if let imageData = data {
//                                self.imageProfilePic.image = UIImage(data: imageData)
//
//                            }
                    }
                }else{

                    self.showAlertWith(message: message) {
                    }
                }
            }

        }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.showBottomMenu()
        self.imageProfilePic.setRoundedCornered(height: imageProfilePic.frame.size.height)
        self.view_CameraButton.setRoundedCornered(height: view_CameraButton.frame.size.height)
        self.btn_EditProfile.setRoundedCornered(height: btn_EditProfile.frame.size.height)
    }

    
    @IBAction func btnEdit_ProfileDidClick(_ sender: Any) {
    }
}

extension MyProfileView {
    func showBottomMenu(){
        let bottomSafeArea: CGFloat
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows[0]
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            //            topSafeArea = safeFrame.minY
            bottomSafeArea = window.frame.maxY - safeFrame.maxY
        } else {
            //            topSafeArea = topLayoutGuide.length
            bottomSafeArea = bottomLayoutGuide.length
        }
        self.viewBottom = UIView(frame: CGRect(x: 0.0, y: self.view.bounds.height - (bottomSafeArea) - 80.0, width: self.view.bounds.width, height: 80.0))
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:viewBottom.bounds.size.width,height:viewBottom.bounds.size.height)
        viewBottom.addSubview(bottom_menu)
        bottom_menu.userSeleted()
        bottom_menu.delegate = self
        if let window = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first {
            window.addSubview(viewBottom)
        }
        
    }

}

//MARK: bottomMenu delegate
extension MyProfileView: bottomMenuDelegate{
    func selectedButton(selectedItem:NSInteger){
        
        if selectedItem == 1 {
            //home
            //            let st = UIStoryboard.init(name: "DashBoardView", bundle: nil)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 0
            UIApplication.shared.windows[0].rootViewController = viewController
        }else if selectedItem == 2{
            //menu
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 1
            UIApplication.shared.windows[0].rootViewController = viewController

        }
        else if selectedItem == 3{
            //OrderList
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 6
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }else{
            //userprofile
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 5
            UIApplication.shared.windows[0].rootViewController = viewController
        }
    }
    
}

// MARK: - Table view data source

extension MyProfileView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 195.0//getTableViewRowHeightRespectToScreenHeight(givenheight: 201, currentScrenHeight: self.view.bounds.height)
        case 1:
            return 161.0 //getTableViewRowHeightRespectToScreenHeight(givenheight: 161, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        default:
            return 0.0
        }
    }
}

