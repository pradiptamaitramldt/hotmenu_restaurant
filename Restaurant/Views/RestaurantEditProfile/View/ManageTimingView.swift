//
//  RestaurantEditTimingView.swift
//  Restaurant
//
//  Created by Souvik on 16/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class ManageTimingView: BaseViewController, SideMenuItemContent, Storyboardable   {
    @IBOutlet weak var button_Moday: NormalButton!
    @IBOutlet weak var textFieldMondayOpeningTime: UITextField!
    @IBOutlet weak var textFieldMondayClosingTime: UITextField!
    
    @IBOutlet weak var buttonTuesday: NormalButton!
    @IBOutlet weak var textFieldTuesdayOpeningTime: UITextField!
    @IBOutlet weak var textFieldTuesdayClosingTime: UITextField!
    
    @IBOutlet weak var buttonWednesday: NormalButton!
    @IBOutlet weak var textFieldSednesdayOpeningTime: UITextField!
    @IBOutlet weak var textFieldWednesdayClosingTime: UITextField!
    
    @IBOutlet weak var buttonThursday: NormalButton!
    @IBOutlet weak var textFieldThursdayOpeningTime: UITextField!
    @IBOutlet weak var textFieldThursdayClosingTime: UITextField!
    
    @IBOutlet weak var buttonFriday: NormalButton!
    @IBOutlet weak var textFieldFridayOpeningTime: UITextField!
    @IBOutlet weak var textFieldFridayClosingTime: UITextField!
    
    @IBOutlet weak var buttonSatuday: NormalButton!
    @IBOutlet weak var textFieldSaturdayOpeningTime: UITextField!
    @IBOutlet weak var textFieldSaturdayClosingTime: UITextField!
    
    @IBOutlet weak var buttonSunday: NormalButton!
    @IBOutlet weak var textFieldSundayOpeningTime: UITextField!
    @IBOutlet weak var textFieldSundayClosingTime: UITextField!
    
    @IBOutlet weak var view_Address: UIView!
    @IBOutlet weak var textFieldAddress: CustomTextField!
    
    @IBOutlet weak var buttonSubmit: NormalBoldButton!
    @IBOutlet weak var label_AllowPreOrder: CustomNormalLabel!
    @IBOutlet weak var buttonRestaurantClosed: NormalButton!
    
    var pickerMonday : UIDatePicker?
    var pickerTuesday : UIDatePicker?
    var pickerWednesday : UIDatePicker?
    var pickerThursday : UIDatePicker?
    var pickerFriday : UIDatePicker?
    var pickerSaturday : UIDatePicker?
    var pickerSunday : UIDatePicker?
    let presenter = RestaurantProfilePresenter()
    var status : Bool = true
    var gradientLabelSwitch1 : LabelSwitch?
    var isRestaurantisClosed = "false"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerMonday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldMondayOpeningTime, picker: pickerMonday!)
        self.createDatePicker(textField: textFieldMondayClosingTime, picker: pickerMonday!)

        pickerTuesday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldTuesdayOpeningTime, picker: pickerTuesday!)
        self.createDatePicker(textField: textFieldTuesdayClosingTime, picker: pickerTuesday!)

        pickerWednesday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldSednesdayOpeningTime, picker: pickerWednesday!)
        self.createDatePicker(textField: textFieldWednesdayClosingTime, picker: pickerWednesday!)

        pickerThursday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldThursdayOpeningTime, picker: pickerThursday!)
        self.createDatePicker(textField: textFieldThursdayClosingTime, picker: pickerThursday!)

        pickerFriday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldFridayOpeningTime, picker: pickerFriday!)
        self.createDatePicker(textField: textFieldFridayClosingTime, picker: pickerFriday!)

        pickerSaturday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldSaturdayOpeningTime, picker: pickerSaturday!)
        self.createDatePicker(textField: textFieldSaturdayClosingTime, picker: pickerSaturday!)

        pickerSunday = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
        self.createDatePicker(textField: textFieldSundayOpeningTime, picker: pickerSunday!)
        self.createDatePicker(textField: textFieldSundayClosingTime, picker: pickerSunday!)

        let ls1 = LabelSwitchConfig(text: "On",
                              textColor: .white,
                                   font: ceraProBoldNormalFontSize,
                                   gradientColors: [UIColor(named: "NavigationBarColor")!.cgColor, UIColor(named: "ThemeColor")!.cgColor], startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1, y: 0.5))
        
        let rs1 = LabelSwitchConfig(text: "Off",
                              textColor: .white,
                                   font: ceraProBoldNormalFontSize,
                                   gradientColors: [UIColor(named: "ThemeColor")!.cgColor, UIColor(named: "NavigationBarColor")!.cgColor], startPoint: CGPoint(x: 0.0, y: 0.5), endPoint: CGPoint(x: 1, y: 0.5))
        
        gradientLabelSwitch1 = LabelSwitch(center: CGPoint(x: view.frame.size.width - 50, y: self.label_AllowPreOrder.center.y), leftConfig: ls1, rightConfig: rs1, defaultState: .L)
        gradientLabelSwitch1?.tag = 1
        gradientLabelSwitch1?.delegate = self
        gradientLabelSwitch1?.circleShadow = false
        gradientLabelSwitch1?.fullSizeTapEnabled = true
//        self.view.addSubview(gradientLabelSwitch1!)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startLoaderAnimation()
        self.presenter.getRestaurantProfileDetails { (status, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.showRestaurantTimings()
            }
        }

    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.buttonSubmit.setRoundedCornered(height: self.buttonSubmit.frame.size.height)
        self.view_Address.setRoundedCornered(height: self.view_Address.frame.size.height)

    }

    @IBAction func buttonBackDidClick(_ sender: Any) {
            showSideMenu()
        
    }

    
    @IBAction func buttonRestaurantClosedDidClick(_ sender: Any) {
        self.buttonRestaurantClosed.isSelected = !self.buttonRestaurantClosed.isSelected
        if self.buttonRestaurantClosed.isSelected{
            self.isRestaurantisClosed = "true"
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Monday" }
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Tuesday" }
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Wednesday" }
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Thursday" }
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Friday" }
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Saturday" }
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Sunday" }
            self.button_Moday.isSelected = false
            self.buttonTuesday.isSelected = false
            self.buttonWednesday.isSelected = false
            self.buttonThursday.isSelected = false
            self.buttonFriday.isSelected = false
            self.buttonSatuday.isSelected = false
            self.buttonSunday.isSelected = false
        }else{
            self.isRestaurantisClosed = "false"
        }
    }
    
    @IBAction func button_MondayDidCilck(_ sender: Any) {
        self.button_Moday.isSelected = !self.button_Moday.isSelected
        self.buttonRestaurantClosed.isSelected = false
        if button_Moday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Monday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Monday", startTime: self.textFieldMondayOpeningTime.text!, endTime: self.textFieldMondayClosingTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Monday", startTime: self.textFieldMondayOpeningTime.text!, endTime: self.textFieldMondayClosingTime.text!))
            }

        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Monday" }
            
        }
    }
    @IBAction func buttonTuesdayDidClick(_ sender: Any) {
        self.buttonTuesday.isSelected = !self.buttonTuesday.isSelected
        self.buttonRestaurantClosed.isSelected = false

        if buttonTuesday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Tuesday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Tuesday", startTime: self.textFieldTuesdayOpeningTime.text!, endTime: self.textFieldTuesdayClosingTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Tuesday", startTime: self.textFieldTuesdayOpeningTime.text!, endTime: self.textFieldTuesdayOpeningTime.text!))
            }

        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Tuesday" }
            
        }

    }
    @IBAction func buttonWednesdayDidClick(_ sender: Any) {
        self.buttonWednesday.isSelected = !self.buttonWednesday.isSelected
        self.buttonRestaurantClosed.isSelected = false

        if buttonWednesday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Wednesday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Wednesday", startTime: self.textFieldSednesdayOpeningTime.text!, endTime: self.textFieldWednesdayClosingTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Wednesday", startTime: self.textFieldSednesdayOpeningTime.text!, endTime: self.textFieldWednesdayClosingTime.text!))
            }

        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Wednesday" }
            
        }

    }
    @IBAction func buttonThursdayDidClick(_ sender: Any) {
        self.buttonThursday.isSelected = !self.buttonThursday.isSelected
        self.buttonRestaurantClosed.isSelected = false

        if buttonThursday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Thursday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Thursday", startTime: self.textFieldThursdayOpeningTime.text!, endTime: self.textFieldThursdayClosingTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Thursday", startTime: self.textFieldThursdayClosingTime.text!, endTime: self.textFieldThursdayClosingTime.text!))
            }

        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Thursday" }
            
        }

    }
    @IBAction func buttonFridayDidClick(_ sender: Any) {
        self.buttonFriday.isSelected = !self.buttonFriday.isSelected
        self.buttonRestaurantClosed.isSelected = false

        if buttonFriday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Friday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Friday", startTime: self.textFieldFridayOpeningTime.text!, endTime: self.textFieldFridayClosingTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Friday", startTime: self.textFieldFridayOpeningTime.text!, endTime: self.textFieldFridayClosingTime.text!))
            }

        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Friday" }
            
        }

    }
    @IBAction func buttonSaturdayDidClick(_ sender: Any) {
        self.buttonSatuday.isSelected = !self.buttonSatuday.isSelected
        self.buttonRestaurantClosed.isSelected = false

        if buttonSatuday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Saturday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Saturday", startTime: self.textFieldSaturdayOpeningTime.text!, endTime: self.textFieldSaturdayClosingTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Saturday", startTime: self.textFieldSaturdayOpeningTime.text!, endTime: self.textFieldSaturdayClosingTime.text!))
            }

        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Saturday" }
            
        }

    }
    @IBAction func buttonSundayDidClick(_ sender: Any) {
        self.buttonSunday.isSelected = !self.buttonSunday.isSelected
        self.buttonRestaurantClosed.isSelected = false

        if buttonSunday.isSelected{
            if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Sunday"}) {
                self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Sunday", startTime: self.textFieldSundayOpeningTime.text!, endTime: self.textFieldSundayOpeningTime.text!)
            }else{
                self.presenter.objAddTiming.restaurantTime.append(Times(day: "Sunday", startTime: self.textFieldSundayOpeningTime.text!, endTime: self.textFieldSundayClosingTime.text!))
            }
        }else{
            self.presenter.objAddTiming.restaurantTime = self.presenter.objAddTiming.restaurantTime.filter { $0.day != "Sunday" }
            
        }

    }

    @IBAction func buttonSubmitDidClick(_ sender: Any) {
        if self.buttonRestaurantClosed.isSelected{
          self.isRestaurantisClosed = "true"
        }else{
            self.isRestaurantisClosed = "false"
        }
        

        self.startLoaderAnimation()
        self.presenter.editTiming(isRestaurantClosed: self.isRestaurantisClosed, preOrder: self.status ? "ON" : "OFF", address: self.textFieldAddress.text!, latitude: String(describing: self.presenter.objRestaurantDetail.vendorLatlong!.vendorLat!), longitude: String(describing : self.presenter.objRestaurantDetail.vendorLatlong!.vendorLong!)) { (status, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
//                for controller in self.navigationController!.viewControllers as Array {
//                    if controller.isKind(of: RestaurentProfileView.self) {
//                        self.navigationController!.popToViewController(controller, animated: true)
//                        break
//                    }
//                }

//                let vc = self.storyboard?.instantiateViewController(identifier: "restaurentProfileView") as! RestaurentProfileView
//                self.navigationController?.popToViewController(vc, animated: true)

                self.showAlertWith(message: message) {
                    
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ManageTimingView: LabelSwitchDelegate {
    func switchChangToState(sender: LabelSwitch) {
        switch sender.curState {
        case .L: print("left state")
        status = false
        case .R: print("right state")
        status = true
        }
    }
}


extension ManageTimingView {
    
    //MARK: - Self Methods -
    func showRestaurantTimings(){
        let obj = self.presenter.objRestaurantDetail
        self.gradientLabelSwitch1?.curState = obj.vendor?.preOrder == "ON" ? .R : .L
        self.textFieldAddress.text = obj.vendor?.address!
        if obj.vendor != nil{
            for item in obj.vendorTime ?? []{
                switch item.day{
                case "Monday":
                    self.button_Moday.isSelected = true
                    self.textFieldMondayOpeningTime.text = item.openTime!
                    self.textFieldMondayClosingTime.text = item.closeTime!
                case "Tuesday":
                    self.buttonTuesday.isSelected = true
                    self.textFieldTuesdayOpeningTime.text = item.openTime!
                    self.textFieldTuesdayClosingTime.text = item.closeTime!
                case "Wednesday":
                    self.buttonWednesday.isSelected = true
                    self.textFieldSednesdayOpeningTime.text = item.openTime!
                    self.textFieldWednesdayClosingTime.text = item.closeTime!
                case "Thursday":
                    self.buttonThursday.isSelected = true
                    self.textFieldThursdayOpeningTime.text = item.openTime!
                    self.textFieldThursdayClosingTime.text = item.closeTime!
                case "Friday":
                    self.buttonFriday.isSelected = true
                    self.textFieldFridayOpeningTime.text = item.openTime!
                    self.textFieldFridayClosingTime.text = item.closeTime!
                case "Saturday":
                    self.buttonSatuday.isSelected = true
                    self.textFieldSaturdayOpeningTime.text = item.openTime!
                    self.textFieldSaturdayClosingTime.text = item.closeTime!
                case "Sunday":
                    self.buttonSunday.isSelected = true
                    self.textFieldSundayOpeningTime.text = item.openTime!
                    self.textFieldSundayClosingTime.text = item.closeTime!
                    
                default:
                    print("")
                }
                self.presenter.objAddTiming.restaurantTime.append(Times(day: item.day!, startTime: item.openTime!, endTime: item.closeTime!))
                
            }

        }

    }
    
    
    
    func createDatePicker(textField : UITextField, picker : UIDatePicker) {
        
        //toolbar
        //Toolbar for "Cancel" and "Done"
        
        textField.inputView = picker
        let button_Done = UIButton()
        button_Done.setTitle("Done", for: .normal)
        button_Done.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        
        button_Done.setTitleColor(.white, for: .normal)
        button_Done.sizeToFit()
        button_Done.frame.origin = CGPoint(x: self.view.frame.width - button_Done.frame.width - 8, y: 5)
        button_Done.addTarget(self, action: #selector(selectionDone), for: .touchUpInside)
        
        let button_Cancel = UIButton()
        button_Cancel.setTitle("Cancel", for: .normal)
        button_Cancel.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
        button_Cancel.setTitleColor(.white, for: .normal)
        button_Cancel.sizeToFit()
        button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
        button_Cancel.addTarget(self, action: #selector(selectionCancel), for: .touchUpInside)
        
        let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
        view_Accessory.backgroundColor = UIColor(named: "TextColorDark")
        view_Accessory.addSubview(button_Done)
        view_Accessory.addSubview(button_Cancel)
        textField.inputAccessoryView = view_Accessory
        textField.inputView = picker
        
        //format picker for date
        picker.datePickerMode = .time
        
    }

    
        @objc func selectionDone(_ sender: UIButton ) {
            
            let formatter = DateFormatter()
            formatter.dateFormat = "hh:mm a"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
//            let time = formatter.string(from: sender.date)
//
            if textFieldMondayOpeningTime.isFirstResponder
            {
                textFieldMondayOpeningTime.text = formatter.string(from: pickerMonday!.date)
                self.button_Moday.isSelected = true
                let time = Times(day: "Monday", startTime: formatter.string(from: pickerMonday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Monday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Monday", startTime: formatter.string(from: pickerMonday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }
                textFieldMondayClosingTime?.becomeFirstResponder()
            }else if textFieldMondayClosingTime.isFirstResponder{
                textFieldMondayClosingTime.text = formatter.string(from: pickerMonday!.date)
                let time = Times(day: "Monday", startTime: "", endTime: formatter.string(from: pickerMonday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Monday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Monday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerMonday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldMondayClosingTime.resignFirstResponder()
            }
            
            if textFieldTuesdayOpeningTime.isFirstResponder
            {
                textFieldTuesdayOpeningTime.text = formatter.string(from: pickerTuesday!.date)
                self.buttonTuesday.isSelected = true
                let time = Times(day: "Tuesday", startTime: formatter.string(from: pickerTuesday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Tuesday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Tuesday", startTime: formatter.string(from: pickerTuesday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldTuesdayClosingTime?.becomeFirstResponder()
            }else if textFieldTuesdayClosingTime.isFirstResponder{
                textFieldTuesdayClosingTime.text = formatter.string(from: pickerTuesday!.date)
                let time = Times(day: "Tuesday", startTime: "", endTime: formatter.string(from: pickerTuesday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Tuesday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Tuesday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerTuesday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldTuesdayClosingTime.resignFirstResponder()
            }
            
            if textFieldSednesdayOpeningTime.isFirstResponder
            {
                textFieldSednesdayOpeningTime.text = formatter.string(from: pickerWednesday!.date)
                self.buttonWednesday.isSelected = true
                let time = Times(day: "Wednesday", startTime: formatter.string(from: pickerWednesday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Wednesday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Wednesday", startTime: formatter.string(from: pickerWednesday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldWednesdayClosingTime?.becomeFirstResponder()
            }else if textFieldWednesdayClosingTime.isFirstResponder{
                textFieldWednesdayClosingTime.text = formatter.string(from: pickerMonday!.date)
                let time = Times(day: "Wednesday", startTime: "", endTime: formatter.string(from: pickerWednesday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Wednesday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Wednesday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerWednesday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldWednesdayClosingTime.resignFirstResponder()
            }

            if textFieldThursdayOpeningTime.isFirstResponder
            {
                textFieldThursdayOpeningTime.text = formatter.string(from: pickerThursday!.date)
                self.buttonThursday.isSelected = true
                let time = Times(day: "Thursday", startTime: formatter.string(from: pickerThursday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Thursday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Thursday", startTime: formatter.string(from: pickerThursday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldThursdayClosingTime?.becomeFirstResponder()
            }else if textFieldThursdayClosingTime.isFirstResponder{
                textFieldThursdayClosingTime.text = formatter.string(from: pickerThursday!.date)
                let time = Times(day: "Thursday", startTime: "", endTime: formatter.string(from: pickerThursday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Thursday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Thursday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerThursday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldThursdayClosingTime.resignFirstResponder()
            }

            if textFieldFridayOpeningTime.isFirstResponder
            {
                textFieldFridayOpeningTime.text = formatter.string(from: pickerFriday!.date)
                self.buttonFriday.isSelected = true
                let time = Times(day: "Friday", startTime: formatter.string(from: pickerFriday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Friday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Friday", startTime: formatter.string(from: pickerFriday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldFridayClosingTime?.becomeFirstResponder()
            }else if textFieldFridayClosingTime.isFirstResponder{
                textFieldFridayClosingTime.text = formatter.string(from: pickerFriday!.date)
                let time = Times(day: "Friday", startTime: "", endTime: formatter.string(from: pickerFriday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Friday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Friday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerFriday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldFridayClosingTime.resignFirstResponder()
            }
            
            if textFieldSaturdayOpeningTime.isFirstResponder
            {
                textFieldSaturdayOpeningTime.text = formatter.string(from: pickerSaturday!.date)
                self.buttonSatuday.isSelected = true
                let time = Times(day: "Saturday", startTime: formatter.string(from: pickerSaturday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Saturday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Saturday", startTime: formatter.string(from: pickerSaturday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldSaturdayClosingTime?.becomeFirstResponder()
            }else if textFieldSaturdayClosingTime.isFirstResponder{
                textFieldSaturdayClosingTime.text = formatter.string(from: pickerSaturday!.date)
                let time = Times(day: "Saturday", startTime: "", endTime: formatter.string(from: pickerSaturday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Saturday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Saturday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerSaturday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldSaturdayClosingTime.resignFirstResponder()
            }
            
            if textFieldSundayOpeningTime.isFirstResponder
            {
                textFieldSundayOpeningTime.text = formatter.string(from: pickerSunday!.date)
                self.buttonSunday.isSelected = true
                let time = Times(day: "Sunday", startTime: formatter.string(from: pickerSunday!.date), endTime: "")
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Sunday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Sunday", startTime: formatter.string(from: pickerSunday!.date), endTime: self.presenter.objAddTiming.restaurantTime[row].endTime)
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldSundayClosingTime?.becomeFirstResponder()
            }else if textFieldSundayClosingTime.isFirstResponder{
                textFieldSundayClosingTime.text = formatter.string(from: pickerSunday!.date)
                let time = Times(day: "Sunday", startTime: "", endTime: formatter.string(from: pickerSunday!.date))
                if let row = self.presenter.objAddTiming.restaurantTime.firstIndex(where: {$0.day == "Sunday"}) {
                    self.presenter.objAddTiming.restaurantTime[row] = Times(day: "Sunday", startTime: self.presenter.objAddTiming.restaurantTime[row].startTime, endTime: formatter.string(from: pickerSunday!.date))
                }else{
                    self.presenter.objAddTiming.restaurantTime.append(time)
                }

                textFieldSundayClosingTime.resignFirstResponder()
            }



            
        }
        
        @objc func selectionCancel(_ sender: UIButton ) {
            if textFieldMondayOpeningTime.isFirstResponder
            {
                textFieldFridayClosingTime?.resignFirstResponder()
            }else if textFieldMondayClosingTime.isFirstResponder{
                textFieldMondayClosingTime.resignFirstResponder()
            }
            
            if textFieldTuesdayOpeningTime.isFirstResponder
            {
                textFieldTuesdayOpeningTime?.resignFirstResponder()
            }else if textFieldTuesdayClosingTime.isFirstResponder{
                textFieldTuesdayClosingTime.resignFirstResponder()
            }
            
            if textFieldSednesdayOpeningTime.isFirstResponder
            {
                textFieldSednesdayOpeningTime?.resignFirstResponder()
            }else if textFieldWednesdayClosingTime.isFirstResponder{
                textFieldWednesdayClosingTime.resignFirstResponder()
            }

            if textFieldThursdayOpeningTime.isFirstResponder
            {
                textFieldThursdayOpeningTime?.resignFirstResponder()
            }else if textFieldThursdayClosingTime.isFirstResponder{
                textFieldThursdayClosingTime.resignFirstResponder()
            }

            if textFieldFridayOpeningTime.isFirstResponder
            {
                textFieldFridayOpeningTime?.resignFirstResponder()
            }else if textFieldFridayClosingTime.isFirstResponder{
                textFieldFridayClosingTime.resignFirstResponder()
            }
            
            if textFieldSaturdayOpeningTime.isFirstResponder
            {
                textFieldSaturdayOpeningTime?.resignFirstResponder()
            }else if textFieldSaturdayClosingTime.isFirstResponder{
                textFieldSaturdayClosingTime.resignFirstResponder()
            }
            
            if textFieldSundayOpeningTime.isFirstResponder
            {
                textFieldSaturdayOpeningTime?.resignFirstResponder()
            }else if textFieldSundayClosingTime.isFirstResponder{
                textFieldSundayOpeningTime.resignFirstResponder()
            }


        }

}

