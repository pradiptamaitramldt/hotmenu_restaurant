//
//  RestaurantEditProfileView.swift
//  Restaurant
//
//  Created by Souvik on 11/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class RestaurantEditProfileView: UITableViewController {
    @IBOutlet weak var imageBanner: UIImageView!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var viewRestaurantName: UIView!
    @IBOutlet weak var textFieldRestaurantName: CustomTextField!
    @IBOutlet weak var viewManagerName: UIView!
    @IBOutlet weak var textFieldManagerName: CustomTextField!
    @IBOutlet weak var viewRestaurantType: UIView!
    @IBOutlet weak var textFieldRestaurantType: CustomTextField!
    @IBOutlet weak var viewRestaurantEmail: UIView!
    @IBOutlet weak var testFieldRestaurantEmail: CustomTextField!
    @IBOutlet weak var viewPhoneNumber: UIView!
    @IBOutlet weak var imageCountryFlag: UIImageView!
    @IBOutlet weak var textFieldCountryCode: CustomTextField!
    @IBOutlet weak var textFieldMobileNumber: CustomTextField!
    @IBOutlet weak var collectionViewTypes: UICollectionView!
    //@IBOutlet weak var imageCACCertificateImage: UIImageView!
    //@IBOutlet weak var viewCACCert: UIView!
    
    let presenter = RestaurantProfilePresenter()
    var restaurantTypePicker : UIPickerView?
    var picker1 = UIImagePickerController()
    var picker2 = UIImagePickerController()
    var picker3 = UIImagePickerController()

    var arrSelectedTypes: [VendorType] = []
    let categoryListPresenter = CategoriesListPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.textFieldCountryCode.delegate = self
        self.textFieldMobileNumber.delegate = self
        self.textFieldCountryCode.delegate = self
        self.collectionViewTypes.dataSource = self
        self.collectionViewTypes.delegate = self
//        self.presenter.getAllPossibleResturantTypes { (status, message) in
//
//        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.textFieldRestaurantType.delegate = self
        self.viewRestaurantName.setRoundedCornered(height: viewRestaurantName.frame.size.height)
        self.viewManagerName.setRoundedCornered(height: viewManagerName.frame.size.height)
        self.viewRestaurantType.setRoundedCornered(height: viewRestaurantType.frame.size.height)
        self.viewRestaurantEmail.setRoundedCornered(height: viewRestaurantEmail.frame.size.height)
        self.viewPhoneNumber.setRoundedCornered(height: viewPhoneNumber.frame.size.height)
        //self.viewCACCert.setRoundedCornered(height: viewCACCert.frame.size.height)



    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startLoaderAnimation()
        self.presenter.getRestaurantProfileDetails { (status, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.showRestaurantDetails()
            }
        }

    }
    @IBAction func btnChangeLogo(_ sender: Any) {
        showImagePicker(picker: picker1)
    }
    
    @IBAction func btnChangeBannerDidClick(_ sender: Any) {
        showBannerPicker(picker: picker2)
    }
    
    @IBAction func btnAddCACCetificateDidClick(_ sender: Any) {
        self.showCACPicker(picker: picker3)
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonNextDidClick(_ sender: Any) {
        
        var strRestaurantTypes = ""
        for item in self.arrSelectedTypes{
            strRestaurantTypes += "\(item.name!),"
        }
        if strRestaurantTypes.count > 0{
            strRestaurantTypes.removeLast()
            
        }
        self.startLoaderAnimation()
        self.presenter.editRestaurantInfo(email: self.testFieldRestaurantEmail.text!, countryCode: self.textFieldCountryCode.text!, phone: self.textFieldMobileNumber.text!, name: textFieldRestaurantName.text!, type: strRestaurantTypes, manager: textFieldManagerName.text!) { (status, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.showAlertWith(message: message) {
                    let vc = self.storyboard?.instantiateViewController(identifier: "restaurantEditTimingView") as! RestaurantEditTimingView
                    self.navigationController?.pushViewController(vc, animated: true)

                }
            }
            
        }


    }
    
    //MARK: - self Methods -
    
    @objc func removeItemFromList(_ sender : UIButton){
        if self.arrSelectedTypes.count >= sender.tag{
            self.arrSelectedTypes.remove(at: sender.tag)
            self.collectionViewTypes.reloadData()
//            var strRestaurantTypes = ""
//            for item in self.arrSelectedTypes{
//                strRestaurantTypes += "\(item.name!),"
//            }
//            if strRestaurantTypes.count > 0{
//                strRestaurantTypes.removeLast()
//
//            }
//
//            self.presenter.editRestaurantInfo(name: textFieldRestaurantName.text!, type: strRestaurantTypes, manager: textFieldManagerName.text!) { (status, message) in
//                self.stopLoaderAnimation()
//                DispatchQueue.main.async {
//                    self.showAlert(message: message)
//                }
//
//            }

        }
    }
    
    func showRestaurantDetails(){
        let obj = self.presenter.objRestaurantDetail
        if obj.vendor != nil{
            let arrSelectedTypesStrings = (self.presenter.objRestaurantDetail.vendor?.restaurantType!.split{$0 == ","}.map(String.init))!
            if self.categoryListPresenter.arrRestaurantTypes.count == 0{
                self.startLoaderAnimation()
                self.categoryListPresenter.getAllPossibleResturantTypes { (status, message) in
                    self.stopLoaderAnimation()
                    DispatchQueue.main.async {
                        for (index, item) in self.categoryListPresenter.arrRestaurantTypes.enumerated(){
                            for name in arrSelectedTypesStrings{
                                if item.name == name{
                                    self.categoryListPresenter.arrRestaurantTypes[index].isSelected = true
                                }
                            }
                        }
                        self.arrSelectedTypes = self.categoryListPresenter.arrRestaurantTypes.filter{($0.isSelected == true)}

                        self.collectionViewTypes.reloadData()
                    }
                }
            }else{
                DispatchQueue.main.async {
                    for (index, item) in self.categoryListPresenter.arrRestaurantTypes.enumerated(){
                        for name in arrSelectedTypesStrings{
                            if item.name == name{
                                self.categoryListPresenter.arrRestaurantTypes[index].isSelected = true
                            }
                        }
                    }
                    self.arrSelectedTypes = self.categoryListPresenter.arrRestaurantTypes.filter{($0.isSelected == true)}

                    self.collectionViewTypes.reloadData()
                }

            }
            self.imageBanner.sd_setImage(with: URL(string: obj.imageURL! + (obj.vendor?.banner!)!))
            self.imageLogo.sd_setImage(with: URL(string: obj.imageURL! + (obj.vendor?.logo!)!))
//            self.textFieldRestaurantName.text = String(describing: "\(obj.vendor?.restaurantName! ?? "")")
            self.textFieldManagerName.text = String(describing: "\(obj.vendor?.managerName! ?? "")")
//            self.textFieldRestaurantType.text = String(describing: "\(obj.vendor?.restaurantType! ?? "")")
            self.testFieldRestaurantEmail.text = String(describing: "\(obj.vendor?.contactEmail! ?? "")")
            self.textFieldCountryCode.text = String(describing: "\(obj.vendor?.countryCode! ?? "")")
            self.textFieldMobileNumber.text = String(describing: "\(obj.vendor?.contactPhone! ?? 0)")

            if let objCountry = CountryPickerViewController.getCountryDetailsBy(dialCode: String(describing: "\(obj.vendor?.countryCode! ?? "")")){
                self.imageCountryFlag.image = UIImage(named: objCountry.flag!)
                self.textFieldCountryCode.text = objCountry.dialCode!
            }


        }
//        self.createRestaurantTypePicker()

    }
    
    
//        func createRestaurantTypePicker()
//        {
//
//            restaurantTypePicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 216))
//            restaurantTypePicker?.dataSource = self
//            restaurantTypePicker?.delegate = self
//
//            textFieldRestaurantType.inputView = restaurantTypePicker
//
//            let button_Done = UIButton()
//            button_Done.setTitle("Done", for: .normal)
//            button_Done.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
//
//            button_Done.setTitleColor(.white, for: .normal)
//            button_Done.sizeToFit()
//            button_Done.frame.origin = CGPoint(x: self.view.frame.width - button_Done.frame.width - 8, y: 5)
//            button_Done.addTarget(self, action: #selector(selectionDone), for: .touchUpInside)
//
//            let button_Cancel = UIButton()
//            button_Cancel.setTitle("Cancel", for: .normal)
//            button_Cancel.titleLabel?.font = UIFont(name: "Gilroy-Bold", size: 16)
//            button_Cancel.setTitleColor(.white, for: .normal)
//            button_Cancel.sizeToFit()
//            button_Cancel.frame.origin = CGPoint(x: 8, y: 5)
//            button_Cancel.addTarget(self, action: #selector(selectionCancel), for: .touchUpInside)
//
//            let view_Accessory = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
//            view_Accessory.backgroundColor = UIColor(named: "TextColorDark")
//            view_Accessory.addSubview(button_Done)
//            view_Accessory.addSubview(button_Cancel)
//            textFieldRestaurantType.inputAccessoryView = view_Accessory
//
//        }
//
//
//
//
//        @objc func selectionDone(_ sender: UIButton ) {
//
//            if textFieldRestaurantType.isFirstResponder
//            {
//                self.arrSelectedTypes.append(String(describing: self.presenter.arrRestaurantTypes[(restaurantTypePicker?.selectedRow(inComponent: 0))!].name!))
//                self.collectionViewTypes.reloadData()
//                textFieldRestaurantType?.resignFirstResponder()
//            }
//        }
//
//        @objc func selectionCancel(_ sender: UIButton ) {
//            textFieldRestaurantType.text = ""
//            textFieldRestaurantType.resignFirstResponder()
//        }
        
        func showImagePicker(picker : UIImagePickerController){
            

            let alertMessage = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
            let titleAttributes = [NSAttributedString.Key.font: ceraProLargeFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
            let titleString = NSAttributedString(string: "HotMenu", attributes: titleAttributes as [NSAttributedString.Key : Any])
            let messageAttributes = [NSAttributedString.Key.font: ceraProNormalFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
            let messageString = NSAttributedString(string: "Please select Image for your Restaurant Logo", attributes: messageAttributes as [NSAttributedString.Key : Any])
            alertMessage.setValue(titleString, forKey: "attributedTitle")
            alertMessage.setValue(messageString, forKey: "attributedMessage")

            let cameraAction = UIAlertAction(title: "Camera", style: .default) { (act) in
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    picker.delegate = self
                    picker.sourceType = .camera;
                    picker.allowsEditing = true
                    self.present(picker, animated: true, completion: nil)
                }
            }
            
            alertMessage.addAction(cameraAction)
            let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (act) in
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                    picker.delegate = self
                    picker.sourceType = .photoLibrary;
                    picker.allowsEditing = true
                    self.present(picker, animated: true, completion: nil)
                }
            }
            
            alertMessage.addAction(galleryAction)
            let action = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            alertMessage .addAction(action)
            alertMessage.view.tintColor = themeDarkColor
            
            self.present(alertMessage, animated: true, completion: nil)

        }

    
    func showCACPicker(picker : UIImagePickerController){
        

        let alertMessage = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        let titleAttributes = [NSAttributedString.Key.font: ceraProLargeFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let titleString = NSAttributedString(string: "HotMenu", attributes: titleAttributes as [NSAttributedString.Key : Any])
        let messageAttributes = [NSAttributedString.Key.font: ceraProNormalFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let messageString = NSAttributedString(string: "Please select Restaurant CAC certificate Image", attributes: messageAttributes as [NSAttributedString.Key : Any])
        alertMessage.setValue(titleString, forKey: "attributedTitle")
        alertMessage.setValue(messageString, forKey: "attributedMessage")

        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                picker.delegate = self
                picker.sourceType = .camera;
                picker.allowsEditing = false
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        alertMessage.addAction(cameraAction)
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                picker.delegate = self
                picker.sourceType = .photoLibrary;
                picker.allowsEditing = false
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        alertMessage.addAction(galleryAction)
        let action = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alertMessage .addAction(action)
        alertMessage.view.tintColor = themeDarkColor
        
        self.present(alertMessage, animated: true, completion: nil)

    }
        func showBannerPicker(picker : UIImagePickerController){
            

            let alertMessage = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
            let titleAttributes = [NSAttributedString.Key.font: ceraProLargeFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
            let titleString = NSAttributedString(string: "HotMenu", attributes: titleAttributes as [NSAttributedString.Key : Any])
            let messageAttributes = [NSAttributedString.Key.font: ceraProNormalFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
            let messageString = NSAttributedString(string: "Please select Image for your Restaurant Banner", attributes: messageAttributes as [NSAttributedString.Key : Any])
            alertMessage.setValue(titleString, forKey: "attributedTitle")
            alertMessage.setValue(messageString, forKey: "attributedMessage")

            let cameraAction = UIAlertAction(title: "Camera", style: .default) { (act) in
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    picker.delegate = self
                    picker.sourceType = .camera;
                    picker.allowsEditing = false
                    self.present(picker, animated: true, completion: nil)
                }
            }
            
            alertMessage.addAction(cameraAction)
            let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (act) in
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                    picker.delegate = self
                    picker.sourceType = .photoLibrary;
                    picker.allowsEditing = false
                    self.present(picker, animated: true, completion: nil)
                }
            }
            
            alertMessage.addAction(galleryAction)
            let action = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            alertMessage .addAction(action)
            alertMessage.view.tintColor = themeDarkColor
            
            self.present(alertMessage, animated: true, completion: nil)

        }

    
    

}


//MARK: - Image Picker -
extension RestaurantEditProfileView : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if picker == picker1{//Logo
            if let pickedImage = info[.editedImage] as? UIImage {
                self.imageLogo.image = pickedImage
                //Api call
                self.startLoaderAnimation()
                self.presenter.chageLogoImage(image: pickedImage) { (status, message) in
                    self.stopLoaderAnimation()
                    DispatchQueue.main.async {
                        self.showAlert(message: message)
                    }
                    
                }
            }
            picker.dismiss(animated: true, completion: nil)

        } else if picker == picker3{//Cerificate
            if let pickedImage = info[.originalImage] as? UIImage {
                //self.imageCACCertificateImage.image = pickedImage
                //Api call
                self.startLoaderAnimation()
                self.presenter.chageCACImage(image: pickedImage) { (status, message) in
                    self.stopLoaderAnimation()
                    DispatchQueue.main.async {
                        self.showAlertWith(message: message) {
                            self.presenter.objRestaurantDetail.cacLicense = true
                            self.tableView.reloadData()

                        }
                    }
                    
                }
            }
            picker.dismiss(animated: true, completion: nil)

        }else{//Banner
            if let pickedImage = info[.originalImage] as? UIImage {
                self.imageBanner.image = pickedImage
                self.startLoaderAnimation()
                self.presenter.chageBannerImage(image: pickedImage) { (status, message) in
                    self.stopLoaderAnimation()
                    DispatchQueue.main.async {
                        self.showAlert(message: message)
                    }
                    
                }

            }
            picker.dismiss(animated: true, completion: nil)

        }
    }
}


extension RestaurantEditProfileView : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFieldCountryCode{
            let countryPickerStoryBoard = UIStoryboard.init(name: "CountryPicker", bundle: nil)
            let countryPickerVC = countryPickerStoryBoard.instantiateViewController(withIdentifier: "countryPickerViewController") as! CountryPickerViewController
            countryPickerVC.delegate = self
            self.present(countryPickerVC, animated: true, completion: nil)
        }else if textField == textFieldRestaurantType{
            if self.categoryListPresenter.arrRestaurantTypes.count == 0{
                self.startLoaderAnimation()
                self.categoryListPresenter.getAllPossibleResturantTypes { (status, message) in
                    self.stopLoaderAnimation()
                    DispatchQueue.main.async {
                        let vc = self.storyboard?.instantiateViewController(identifier: "chooseCategoryView") as! ChooseCategoryView
                        vc.delegate = self
                        vc.presenter = self.categoryListPresenter
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "chooseCategoryView") as! ChooseCategoryView
                vc.delegate = self
                vc.presenter = self.categoryListPresenter
                self.navigationController?.pushViewController(vc, animated: true)

            }

        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
//        var strRestaurantTypes = ""
//        for item in self.arrSelectedTypes{
//            strRestaurantTypes += "\(item.name!),"
//        }
//        if strRestaurantTypes.count > 0{
//            strRestaurantTypes.removeLast()
//
//        }
//
//        if textField == textFieldRestaurantName || textField == textFieldManagerName{
//            self.startLoaderAnimation()
//            self.presenter.editRestaurantInfo(name: textFieldRestaurantName.text!, type: strRestaurantTypes, manager: textFieldManagerName.text!) { (status, message) in
//                self.stopLoaderAnimation()
//                DispatchQueue.main.async {
//                    self.showAlert(message: message)
//                }
//
//            }
//        }else if textField == testFieldRestaurantEmail{
//            self.startLoaderAnimation()
//            self.presenter.editRestaurantEmail(email: testFieldRestaurantEmail.text!.lowercased()) { (status, message) in
//                self.stopLoaderAnimation()
//                DispatchQueue.main.async {
//                    self.showAlert(message: message)
//                }
//
//            }
//        }else if textField == textFieldMobileNumber || textField == textFieldCountryCode {
//            self.startLoaderAnimation()
//            self.presenter.editRestaurantPhoneInfo(countryCode: textFieldCountryCode.text!, phone: textFieldMobileNumber.text!) { (status, message) in
//                self.stopLoaderAnimation()
//                DispatchQueue.main.async {
//                    self.showAlert(message: message)
//                }
//
//            }
//        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldMobileNumber{
        let maxLength = 11
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        }else{
            return true
        }
        
    }
}

extension RestaurantEditProfileView : CountryPickerViewControllerDelegate{
    func didSelectCountry(country: Country) {
        self.textFieldCountryCode.text = country.dialCode!
        self.imageCountryFlag.image = UIImage(named: country.flag!)
    }
}


extension RestaurantEditProfileView : ChooseCategoryDelegate{
    func gotChoosencategories() {
        self.arrSelectedTypes = self.categoryListPresenter.arrRestaurantTypes.filter{($0.isSelected == true)}
        self.collectionViewTypes.reloadData()
        self.startLoaderAnimation()
//        var strRestaurantTypes = ""
//        for item in self.arrSelectedTypes{
//            strRestaurantTypes += "\(item.name!),"
//        }
//        if strRestaurantTypes.count > 0{
//            strRestaurantTypes.removeLast()
//
//        }
//
//        self.presenter.editRestaurantInfo(name: textFieldRestaurantName.text!, type: strRestaurantTypes, manager: textFieldManagerName.text!) { (status, message) in
//            self.stopLoaderAnimation()
//            DispatchQueue.main.async {
//                self.showAlert(message: message)
//            }
//            
//        }

    }
}

extension RestaurantEditProfileView: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.presenter.arrRestaurantTypes.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(describing: self.presenter.arrRestaurantTypes[row].name!)
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        self.textFieldState.text = String(describing: self.presenter.arrStates[row])
//        self.presenter.objJobProfile.visibleTattoo = String(describing: arrVisibleTatoo[row]) == "Yes" ? 1 : 0
        
    }

}


extension RestaurantEditProfileView : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrSelectedTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "orderTypeCollectionViewCell", for: indexPath) as! OrderTypeCollectionViewCell
        cell.label_Name.text = "\(self.arrSelectedTypes[indexPath.item].name!)      "
        cell.contentView.backgroundColor = UIColor.black
        cell.label_Name.textColor = .white
        cell.buttonCancel.tag = indexPath.item
        cell.buttonCancel.addTarget(self, action: #selector(self.removeItemFromList(_:)), for: .touchUpInside)
        return cell
    }
    
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let category = self.arrSelectedTypes[indexPath.item].name!

        let teStStr = "\(category)      "
        let size : CGSize = (teStStr.size(withAttributes:
            [NSAttributedString.Key.font: ceraProNormalFontSize as Any]))
        return CGSize(width:size.width+16,height:26.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 10.0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }

}

extension RestaurantEditProfileView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 282.0
        } else if indexPath.row == 1 {
            return 87.0
        } else if indexPath.row == 2 {
            return 87.0
        } else if indexPath.row == 3 {
            return 153.0
        } else if indexPath.row == 4 {
            return 87.0
        } else if indexPath.row == 5 {
            return 87.0
        } /* else if indexPath.row == 6 {
            if !(self.presenter.objRestaurantDetail.cacLicense ?? true){
                return 93.0
            } else {
                return 0.0
            }
        } */ else if indexPath.row == 6 {
            return 80.0
        } else {
            return 80.0
        }
    }
}
