//
//  AllNotificationsListModel.swift
//  Restaurant
//
//  Created by Souvik on 31/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation
import Foundation

// MARK: - AllNotificationsListModel
class AllNotificationsListModel: Codable {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: NotificationsListResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: NotificationsListResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class NotificationsListResponseData: Codable {
    var notifications: [PushNotificationModel]?

    init(notifications: [PushNotificationModel]?) {
        self.notifications = notifications
    }
}

// MARK: - Notification
class PushNotificationModel: Codable {
    var orderID, id, userID, userType: String?
    var title, type, content, isRead: String?
    var createdAt, updatedAt: String?
    var v: Int?

    enum CodingKeys: String, CodingKey {
        case orderID = "orderId"
        case id = "_id"
        case userID = "userId"
        case userType, title, type, content, isRead, createdAt, updatedAt
        case v = "__v"
    }

    init(orderID: String?, id: String?, userID: String?, userType: String?, title: String?, type: String?, content: String?, isRead: String?, createdAt: String?, updatedAt: String?, v: Int?) {
        self.orderID = orderID
        self.id = id
        self.userID = userID
        self.userType = userType
        self.title = title
        self.type = type
        self.content = content
        self.isRead = isRead
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.v = v
    }}
