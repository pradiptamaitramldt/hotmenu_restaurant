//
//  NotificationsCell.swift
//  Restaurant
//
//  Created by Souvik on 31/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class NotificationsCell: UITableViewCell {

    @IBOutlet weak var labelTitle: CustomBoldFontLabel!
    @IBOutlet weak var labelDesc: CustomNormalLabel!
    @IBOutlet weak var labelTime: CustomNormalLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
