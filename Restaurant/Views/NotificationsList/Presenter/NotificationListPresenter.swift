//
//  NotificationListPresenter.swift
//  Restaurant
//
//  Created by Souvik on 31/07/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class NotificationListPresenter: NSObject {

    var arrNotifications : [PushNotificationModel] = []
    
    func getAllNotifications(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!, "vendorId" : UserDefaultValues.vendorID]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getNotificationList, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: AllNotificationsListModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.arrNotifications = resData?.responseData?.notifications ?? []
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
            }
        }
    }

    
    
}
