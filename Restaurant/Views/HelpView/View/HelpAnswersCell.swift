//
//  HelpAnswersCell.swift
//  Restaurant
//
//  Created by Souvik on 06/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class HelpAnswersCell: UITableViewCell {
    @IBOutlet weak var labelAnswer: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
