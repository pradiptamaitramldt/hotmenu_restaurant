//
//  HelpView.swift
//  Restaurant
//
//  Created by Souvik on 06/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class HelpView: BaseViewController, SideMenuItemContent, Storyboardable{
    @IBOutlet weak var tableViewFAQs: UITableView!
    var presenter = PrivacyPolicyPresenter()
    var isExapnded = false
    var selectedIndex : IndexPath = IndexPath(row: -1, section: -1)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewFAQs.dataSource = self
        self.tableViewFAQs.delegate = self

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startLoaderAnimation()
        self.presenter.getFAQContents(pageName: "VendorFaq") { (status, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.tableViewFAQs.reloadData()
            }
        }
    }
    
    @IBAction func btnSideMenuDidClick(_ sender: Any) {
        self.showSideMenu()
    }

    @objc func selectHeader(_ button : CustomButton){
        self.isExapnded = !self.isExapnded
        if isExapnded{
            self.selectedIndex = IndexPath(row: 0, section: button.params["section"] as! Int)

        }else{
            self.selectedIndex = IndexPath(row: -1, section: -1)
        }

        self.tableViewFAQs.reloadData()

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension HelpView : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.arrQuesttions.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isExapnded{
            switch section {
            case selectedIndex.section:
                return self.presenter.arrQuesttions[section].answer!.count
            default:
                return 0
            }
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let superview = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60))
        superview.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        let view = UIView(frame: CGRect(x: 5, y: 2, width: tableView.frame.size.width-10, height: 58))
        view.backgroundColor = #colorLiteral(red: 0.9113200307, green: 0.9047325253, blue: 0.9163637757, alpha: 1)
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.size.width - 30, height: 58))
        label.numberOfLines = 0
        let option = self.presenter.arrQuesttions[section]
        label.font = ceraProBoldNormalFontSize
        label.text = "\(option.question ?? "")"
        label.sizeToFit()
        label.textColor = UIColor.darkGray
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.cornerRadius = 6.0
        view.addSubview(label)
        let image = UIImageView()
        if section == selectedIndex.section{
            image.image = #imageLiteral(resourceName: "down_Arrow")
            image.frame = CGRect(x: view.frame.size.width - 24, y: 20, width: 20, height: 12)

        }else{
            image.image = #imageLiteral(resourceName: "next_Arrow")
            image.frame = CGRect(x: view.frame.size.width - 20, y: 20, width: 12, height: 20)

        }
        view.addSubview(image)
        let btn = CustomButton(frame: view.bounds)
        btn.addTarget(self, action: #selector(selectHeader(_:)), for: .touchUpInside)
        btn.params["section"] = section
        view.addSubview(btn)
        superview.addSubview(view)
        return superview
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0

    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "helpAnswersCell") as! HelpAnswersCell
        let item = self.presenter.arrQuesttions[indexPath.section].answer![indexPath.row]
        cell.labelAnswer.numberOfLines = 0
        cell.labelAnswer.sizeToFit()
        cell.labelAnswer.text = item
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.isExapnded = !self.isExapnded
        self.selectedIndex = indexPath

        self.tableViewFAQs.reloadData()
    }
}
