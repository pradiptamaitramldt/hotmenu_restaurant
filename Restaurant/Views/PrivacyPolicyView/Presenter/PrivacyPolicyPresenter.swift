//
//  PrivacyPolicyPresenter.swift
//  Restaurant
//
//  Created by Souvik on 08/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class PrivacyPolicyPresenter: NSObject {

    var objPrivacyContent = ""
    var arrQuesttions : [ContentElement] = []
    func getCRMContents(pageName :  String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getCRMContent, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: ["vendorId": UserDefaultValues.vendorID, "customerId" : UserDefaultValues.userID!, "pageName" : pageName], httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetPrivacyContents.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.objPrivacyContent = (resData?.responseData?.content?.content!)!
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
            }
        }
    }

    
    func getFAQContents(pageName :  String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getCRMContent, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: ["vendorId": UserDefaultValues.vendorID, "customerId" : UserDefaultValues.userID!, "pageName" : pageName], httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetFAQContents.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.arrQuesttions = (resData?.responseData?.content?.content)!
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
            }
        }
    }
}
