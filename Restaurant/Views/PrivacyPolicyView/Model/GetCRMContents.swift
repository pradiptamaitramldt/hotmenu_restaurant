//
//  GetCRMContents.swift
//  Restaurant
//
//  Created by Souvik on 08/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

import Foundation

// MARK: - GetCRMContents
class GetFAQContents: Codable {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: FAQContentsResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: FAQContentsResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class FAQContentsResponseData: Codable {
    var content: ResponseDataContent?

    init(content: ResponseDataContent?) {
        self.content = content
    }
}

// MARK: - ResponseDataContent
class ResponseDataContent: Codable {
    var pageName: String?
    var isActive: Bool?
    var id: String?
    var content: [ContentElement]?
    var createdAt, updatedAt: String?
    var v: Int?

    enum CodingKeys: String, CodingKey {
        case pageName, isActive
        case id = "_id"
        case content, createdAt, updatedAt
        case v = "__v"
    }

    init(pageName: String?, isActive: Bool?, id: String?, content: [ContentElement]?, createdAt: String?, updatedAt: String?, v: Int?) {
        self.pageName = pageName
        self.isActive = isActive
        self.id = id
        self.content = content
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.v = v
    }
}

// MARK: - ContentElement
class ContentElement: Codable {
    var question: String?
    var answer: [String]?

    init(question: String?, answer: [String]?) {
        self.question = question
        self.answer = answer
    }
}


import Foundation

// MARK: - GetPrivacyContents
class GetPrivacyContents: Codable {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: PrivacyContentsResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: PrivacyContentsResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class PrivacyContentsResponseData: Codable {
    var content: Content?

    init(content: Content?) {
        self.content = content
    }
}

// MARK: - Content
class Content: Codable {
    var pageName: String?
    var isActive: Bool?
    var id, content, createdAt, updatedAt: String?
    var v: Int?

    enum CodingKeys: String, CodingKey {
        case pageName, isActive
        case id = "_id"
        case content, createdAt, updatedAt
        case v = "__v"
    }

    init(pageName: String?, isActive: Bool?, id: String?, content: String?, createdAt: String?, updatedAt: String?, v: Int?) {
        self.pageName = pageName
        self.isActive = isActive
        self.id = id
        self.content = content
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.v = v
    }
}
