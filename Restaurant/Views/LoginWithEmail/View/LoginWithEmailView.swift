//
//  LoginWithEmailView.swift
//  Restaurant
//
//  Created by Bit Mini on 07/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class LoginWithEmailView: BaseTableViewController {
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var textFieldEmail: CustomTextField!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var textFieldPassword: CustomTextField!
    @IBOutlet weak var button_Login: NormalBoldButton!
    var presenter = LoginWithEmailPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewEmail.setRoundedCornered(height: viewEmail.frame.size.height)
        self.viewPassword.setRoundedCornered(height: viewPassword.frame.size.height)
        self.button_Login.setRoundedCornered(height: button_Login.frame.size.height)
    }
    
    @IBAction func btn_LoginDidClick(_ sender: Any) {
        AMShimmer.start(for: self.textFieldEmail)
        AMShimmer.start(for: self.textFieldPassword)
        AMShimmer.start(for: self.button_Login)
        self.presenter.loginUser(email: self.textFieldEmail.text!.lowercased(), password: self.textFieldPassword.text!, userType: .vendor, loginType: .others) { (status, message,id,code) in
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.textFieldEmail)
                AMShimmer.stop(for: self.textFieldPassword)
                AMShimmer.stop(for: self.button_Login)
                if status{
                    //success
                    if code == 200{
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
                        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
                        viewController.viewControllerIndex = 2
                        UIApplication.shared.windows[0].rootViewController = viewController

                    }else if code == 410{
                        let vc = self.storyboard?.instantiateViewController(identifier: "verifyOTPView") as! VerifyOTPView
                        vc.userID = id
                        vc.viewShowFor = "verifyRegiter"
                        vc.isLogin = true
                        vc.email = self.textFieldEmail.text!.lowercased()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else{
                    self.showAlertWith(message: message) {
                    }
                }
            }
        }

    }
    
    @IBAction func btn_BackdidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension LoginWithEmailView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 100, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 90, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 29, currentScrenHeight: self.view.bounds.height)
        case 3:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 90, currentScrenHeight: self.view.bounds.height)
        case 4:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 73, currentScrenHeight: self.view.bounds.height)
        case 5:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 55, currentScrenHeight: self.view.bounds.height)
            case 6:
                return getTableViewRowHeightRespectToScreenHeight(givenheight: 120, currentScrenHeight: self.view.bounds.height)

        default:
            return 0.0
        }
    }
}
