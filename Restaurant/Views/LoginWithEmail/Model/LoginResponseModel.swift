//
//  LoginResponseModel.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let loginResponseModel = try? newJSONDecoder().decode(LoginResponseModel.self, from: jsonData)

import Foundation

// MARK: - LoginResponseModel

class LoginResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: LoginResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: LoginResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class LoginResponseData: Codable {
    let userDetails: LoginUserDetails?
    let authToken: String?

    init(userDetails: LoginUserDetails?, authToken: String?) {
        self.userDetails = userDetails
        self.authToken = authToken
    }
}

// MARK: - UserDetails
class LoginUserDetails: Codable {
    let vendorID, email, phone, location: String?
    let id, loginID, profileImage, userType: String?
    let loginType, fullName: String?

    enum CodingKeys: String, CodingKey {
        case vendorID = "vendorId"
        case email, phone, location, id
        case loginID = "loginId"
        case fullName = "fullName"
        case profileImage, userType, loginType
    }

    init(vendorID: String? = "", email: String?, phone: String?, location: String?, id: String?, loginID: String?, profileImage: String?, userType: String?, loginType: String?, fullName: String?) {
        self.vendorID = vendorID
        self.email = email
        self.phone = phone
        self.location = location
        self.id = id
        self.loginID = loginID
        self.profileImage = profileImage
        self.userType = userType
        self.loginType = loginType
        self.fullName = fullName
    }
}

/*
// MARK: - LoginResponseModel
struct LoginResponseModel: Codable {
    let success: Bool
    let statuscode: Int
    let message: String
    let responseData: LoginResponseData

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct LoginResponseData: Codable {
    let userDetails: LoginUserDetails
    let authToken: String
}

// MARK: - UserDetails
struct LoginUserDetails: Codable {
    let fullName, email, countryCode, countryCodeID: String
    let phone, socialID, id, loginID: String
    let profileImage: String
    let userType, loginType: String

    enum CodingKeys: String, CodingKey {
        case fullName, email, countryCode
        case countryCodeID = "countryCodeId"
        case phone
        case socialID = "socialId"
        case id
        case loginID = "loginId"
        case profileImage, userType, loginType
    }
}
*/
