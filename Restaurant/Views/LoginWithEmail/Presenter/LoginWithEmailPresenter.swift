//
//  LoginWithEmailPresenter.swift
//  Restaurant
//
//  Created by Bit Mini on 13/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class LoginWithEmailPresenter: NSObject {
    
    func loginUser(email : String, password : String,userType : UserType, loginType : LoginType, completion : @escaping(_ success : Bool ,_ message : String, _ id : String, _ code : Int) -> Void){
        
        var emailOrPhone = ""
        //        if email.isNumeric{
        //            let last10 = email.suffix(10)
        //            if Utility.isValidMobileNumber(String(last10)){
        //                emailOrPhone = String(last10)
        //            }else{
        //                completion(false, "Please enter valid mobile number", "", 0)
        //            }
        //        }else{
        //
        //            if Utility.isValidEmail(email){
        //                emailOrPhone = email
        //
        //            }else{
        //                completion(false, "Please enter Your valid Email.", "", 0)
        //            }
        //        }
        
        if Utility.isValidEmail(email){
            emailOrPhone = email
            
        }else{
            completion(false, "Please enter Your valid Email.", "", 0)
        }
                    let param : [String: Any] =
                        ["email" : emailOrPhone,
                        "password"  : password,
                        "loginType" : "GENERAL",
                        "deviceToken" : UserDefaultValues.deviceToken,
                        "appType" : "IOS",
                        "countryCode" : SingleToneClass.shared.defaultPhoneCountry?.dialCode ?? "",
                        "pushMode" : UserDefaultValues.pushMode]
                    
                    print("param",param)
                    WebServiceManager.shared.requestAPI(url: WebServiceConstants.loginUser, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: param, httpMethodType: .post) { (data, err) in
                        if let responseData = data{
                                                    let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: LoginResponseModel.self) { (resData, err) in
                                                        if resData != nil{
                                                            if resData?.statuscode! == ResponseCode.success.rawValue{
                                                                UserDefaultValues.authToken = (resData?.responseData?.authToken!)!
                                                                UserDefaultValues.userID = (resData?.responseData?.userDetails?.id!)!
                                                                UserDefaultValues.loginID = resData?.responseData?.userDetails?.loginID
                                                                UserDefaultValues.profileImage = (resData?.responseData?.userDetails?.profileImage!)!
                                                                UserDefaultValues.LoginType = resData?.responseData?.userDetails?.loginType ?? ""
                                                                UserDefaultValues.CustomerLoginType = resData?.responseData?.userDetails?.userType ?? ""
                                                                UserDefaultValues.vendorID = (resData?.responseData?.userDetails?.vendorID!)!
                                                                UserDefaultValues.userName = resData?.responseData?.userDetails?.fullName ?? ""
                                                                completion(true,resData?.message ?? "", resData?.responseData?.userDetails?.id ?? "", resData?.statuscode ?? 0)
                                                            }else{
                                                                completion(false,(resData?.message!)!, "", 0)
                                                            }
                                                        }else{
                                                            completion(false, err!.localizedDescription, "", 0)
                                                        }
                                                    }
                        }
        }
                            
        //                    let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
        //                        if resData != nil{
        //                            if resData?.statuscode! == 410{
        //                                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: RegistrationNewResponseModel.self) { (resData, err) in
        //                                    if resData != nil{
        //                                        completion(true,(resData?.message!)!, (resData?.responseData?.vendorID!)!, 410)
        //
        //                                    }else{
        //                                        completion(false, err!.localizedDescription, "", 0)
        //                                    }
        //                                }
        //
        //                            }else if resData?.statuscode! == ResponseCode.success.rawValue{
        //                                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: RegistraionResponseModel.self) { (resData, err) in
        //                                    if resData != nil{
        //                                        if resData?.statuscode! == ResponseCode.success.rawValue{
        //                                            UserDefaultValues.authToken = (resData?.responseData?.authToken!)!
        //                                            UserDefaultValues.userID = (resData?.responseData?.userDetails?.id!)!
        //                                          //  UserDefaultValues.RestaurantName = (resData?.responseData?.restaurantName!)!
        //                                            UserDefaultValues.profileImage = (resData?.responseData?.userDetails?.profileImage!)!
        //                                            UserDefaultValues.LoginType = resData?.responseData?.userDetails?.loginType ?? ""
        //                                            UserDefaultValues.CustomerLoginType = "RESTAURANT"
        //                                   //         UserDefaultValues.vendorID = (resData?.responseData?.userDetails?.vendorID!)!
        //                                   //         UserDefaultValues.vendorName = (resData?.responseData?.restaurantName!)!
        //
        //                                            completion(true,"", "", 200)
        //                                        }else{
        //                                            completion(false,(resData?.message!)!, "", 0)
        //                                        }
        //                                    }else{
        //                                        completion(false, err!.localizedDescription, "", 0)
        //                                    }
        //                                }
        //                            }else{
        //                                completion(false,(resData?.message!)!, "", 0)
        //
        //                            }
        //                        }else{
        //                            completion(false, err!.localizedDescription, "", 0)
        //                        }
        //                    }
//                        }
//        else{
//                            completion(false, "Network Error.", "", 0)
//                        }

        //loginType.rawValue
       // if !Utility.isValidNewPassword(password){
         //   }/
      //  }
    //    else{
//            completion(false, "Please check the password (Password must be 8 characters, all numeric character).", "", 0)
//        }
        
    }
    
}
