//
//  ReviewView.swift
//  Restaurant
//
//  Created by Souvik on 16/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class ReviewView: BaseViewController {
    @IBOutlet weak var tableViewReviews: UITableView!
    var expandedRow = 0
    let presenter = ReviewsPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewReviews.dataSource = self
        self.tableViewReviews.delegate = self
        self.tableViewReviews.estimatedRowHeight = UITableView.automaticDimension
        self.tableViewReviews.rowHeight = 150.0

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.startLoaderAnimation()
        self.presenter.getRestaurantReviews { (status, message) in
            self.stopLoaderAnimation()
            DispatchQueue.main.async {
                self.tableViewReviews.reloadData()

            }
        }
    }

    @IBAction func buttonBackDidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @objc func replyClicked(_ sender : UIButton){
        guard let cell = self.tableViewReviews.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! ReviewsCell? else{
            return
        }
        
        
        cell.buttonReply.isSelected = !cell.buttonReply.isSelected
        
        if cell.buttonReply.isSelected {
            
            expandedRow = sender.tag
            cell.textViewHeight.constant = 100
            self.tableViewReviews.reloadData()
            cell.buttonReply.setTitle("Submit", for: .normal)
        }
            
        else if !cell.buttonReply.isSelected {
            self.startLoaderAnimation()
            self.presenter.replyReview(message: cell.textViewReview.text!, reviewId: self.presenter.arrReviews[sender.tag].reviewID!) { (status, message) in
                self.stopLoaderAnimation()
                DispatchQueue.main.async {
                    self.expandedRow = sender.tag
                    cell.textViewHeight.constant = 0
                    self.tableViewReviews.reloadData()
                    cell.buttonReply.setTitle("Reply", for: .normal)
                }
            }

        }
    }

}

extension ReviewView : UITableViewDataSource , UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.arrReviews.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewsCell") as! ReviewsCell
        let review = self.presenter.arrReviews[indexPath.row]
        cell.labelUserName.text = review.customerName!
        cell.viewrating.rating = Double(review.customerRating!)!
        cell.buttonReply.tag = indexPath.row
        cell.buttonReply.addTarget(self, action: #selector(replyClicked(_:)), for: .touchUpInside)
        cell.labelReview.text = review.comment?.customer!
        cell.labelReview.sizeToFit()
        return cell
    }
    
}
