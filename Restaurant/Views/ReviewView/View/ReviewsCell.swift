//
//  ReviewsCell.swift
//  Restaurant
//
//  Created by Souvik on 16/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class ReviewsCell: UITableViewCell {
    @IBOutlet weak var labelUserName: CustomBoldFontLabel!
    @IBOutlet weak var viewrating: CosmosView!
    @IBOutlet weak var textViewReview: UITextView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonReply: NormalBoldButton!
    @IBOutlet weak var labelReview: CustomBoldFontLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
