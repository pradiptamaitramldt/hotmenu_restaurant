//
//  ReviewsPresenter.swift
//  Restaurant
//
//  Created by Souvik on 16/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class ReviewsPresenter: NSObject {

    var arrReviews : [Review] = []
    func getRestaurantReviews(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!, "vendorId" : UserDefaultValues.vendorID]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getRestaurantReviews, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: GetReviewsListModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            guard let data = resData?.responseData else{
                                completion(false,(resData?.message!)!)
                                return
                            }
                            self.arrReviews = data.reviews!
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)

            }
        }
    }
    
    func replyReview(message : String,reviewId : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!, "vendorId" : UserDefaultValues.vendorID, "reviewId" : reviewId, "reply" : message]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.replyRestaurantReviews, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)

            }
        }
    }
}
