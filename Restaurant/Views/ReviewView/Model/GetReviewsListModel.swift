//
//  GetReviewsListModel.swift
//  Restaurant
//
//  Created by Souvik on 16/06/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation
import Foundation

// MARK: - GetReviewsListModel
class GetReviewsListModel: Codable {
    var success: Bool?
    var statuscode: Int?
    var message: String?
    var responseData: GetReviewsListResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }

    init(success: Bool?, statuscode: Int?, message: String?, responseData: GetReviewsListResponseData?) {
        self.success = success
        self.statuscode = statuscode
        self.message = message
        self.responseData = responseData
    }
}

// MARK: - ResponseData
class GetReviewsListResponseData: Codable {
    var reviews: [Review]?

    init(reviews: [Review]?) {
        self.reviews = reviews
    }
}

// MARK: - Review
class Review: Codable {
    var customerName: String?
    var comment: Comment?
    var customerRating, reviewID: String?

    enum CodingKeys: String, CodingKey {
        case customerName, comment, customerRating
        case reviewID = "reviewId"
    }

    init(customerName: String?, comment: Comment?, customerRating: String?, reviewID: String?) {
        self.customerName = customerName
        self.comment = comment
        self.customerRating = customerRating
        self.reviewID = reviewID
    }
}

// MARK: - Comment
class Comment: Codable {
    var customer, restaurant: String?

    init(customer: String?, restaurant: String?) {
        self.customer = customer
        self.restaurant = restaurant
    }
}
