//
//  BottomMenu.swift
//  Yacht Now 
//
//  Copyright © 2019 Yacht Now. All rights reserved.
//

import UIKit

protocol bottomMenuDelegate: class {
    func selectedButton(selectedItem:NSInteger)
}

class BottomMenu: UIView {
    
    @IBOutlet weak var view_Home: UIView!
    @IBOutlet var button_home: UIButton!
    @IBOutlet weak var labelMenu: CustomNormalLabel!
    @IBOutlet weak var labelHome: CustomNormalLabel!
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet var button_Search: UIButton!
    
    @IBOutlet weak var viewNotes: UIView!
    @IBOutlet var button_Notes: UIButton!
    @IBOutlet weak var labelOrders: CustomNormalLabel!
    
    @IBOutlet weak var viewUser: UIView!
    @IBOutlet var button_user: UIButton!
    @IBOutlet weak var labelProfile: CustomNormalLabel!
    
    
    var delegate :bottomMenuDelegate?
    
    
    
    class func instanceFromNib() -> BottomMenu {
        return UINib(nibName: "BottomMenu", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BottomMenu
    }
    
    
    
    
    //Home selected
    @IBAction func home_buttonClick(_ sender: UIButton) {
        self.homeSelected()
        self.delegate?.selectedButton(selectedItem: 1)
    }
    //Inventory selected
    @IBAction func searchButtonClick(_ sender: UIButton) {
        self.searchSeleted()
        self.delegate?.selectedButton(selectedItem: 2)
    }
    
    //Service selected
    @IBAction func notesButtonClick(_ sender: UIButton) {
        self.notesSeleted()
        self.delegate?.selectedButton(selectedItem: 3)
    }
    
    //Products selected
    @IBAction func userButtonClick(_ sender: UIButton) {
       self.userSeleted()
       self.delegate?.selectedButton(selectedItem: 4)
    }
    
    
    func selectedItem(index: NSInteger) {
        if index == 1 {
        
           self.homeSelected()

        }else if index == 2 {
            
           self.searchSeleted()
            
        }else if index == 3 {
            
           self.notesSeleted()
        }else{
           self.userSeleted()
        }
    }
    
    
    
    func homeSelected() {
//        self.view_Home.backgroundColor = UIColor(named: "MenuSelectedBGColor")
//        self.viewSearch.backgroundColor = UIColor.clear
//        self.viewNotes.backgroundColor = UIColor.clear
//        self.viewUser.backgroundColor = UIColor.clear
        self.button_home.setImage(#imageLiteral(resourceName: "Home_icon_green"), for: .normal)
        self.button_Search.setImage(#imageLiteral(resourceName: "plate"), for: .normal)
        self.button_Notes.setImage(#imageLiteral(resourceName: "notes_icon"), for: .normal)
        self.button_user.setImage(#imageLiteral(resourceName: "User_icon"), for: .normal)

        self.labelHome.textColor = themeDarkColor
        self.labelMenu.textColor = menuDefaultColor
        self.labelOrders.textColor = menuDefaultColor
        self.labelProfile.textColor = menuDefaultColor
        
    }
    
    func searchSeleted() {
//        self.view_Home.backgroundColor = UIColor.clear
//        self.viewSearch.backgroundColor = UIColor(named: "MenuSelectedBGColor")
//        self.viewNotes.backgroundColor = UIColor.clear
//        self.viewUser.backgroundColor = UIColor.clear
        self.button_home.setImage(#imageLiteral(resourceName: "Home_icon"), for: .normal)
        self.button_Search.setImage(#imageLiteral(resourceName: "plate_green"), for: .normal)
        self.button_Notes.setImage(#imageLiteral(resourceName: "notes_icon"), for: .normal)
        self.button_user.setImage(#imageLiteral(resourceName: "User_icon"), for: .normal)

        self.labelHome.textColor = menuDefaultColor
        self.labelMenu.textColor = themeDarkColor
        self.labelOrders.textColor = menuDefaultColor
        self.labelProfile.textColor = menuDefaultColor
    }
    
    
    func notesSeleted() {
//        self.view_Home.backgroundColor = UIColor.clear
//        self.viewSearch.backgroundColor = UIColor.clear
//        self.viewNotes.backgroundColor = UIColor(named: "MenuSelectedBGColor")
//        self.viewUser.backgroundColor = UIColor.clear
        self.button_home.setImage(#imageLiteral(resourceName: "Home_icon"), for: .normal)
        self.button_Search.setImage(#imageLiteral(resourceName: "plate"), for: .normal)
        self.button_Notes.setImage(#imageLiteral(resourceName: "notes_icon_green"), for: .normal)
        self.button_user.setImage(#imageLiteral(resourceName: "User_icon"), for: .normal)

        self.labelHome.textColor = menuDefaultColor
        self.labelMenu.textColor = menuDefaultColor
        self.labelOrders.textColor = themeDarkColor
        self.labelProfile.textColor = menuDefaultColor
    }
    
    func userSeleted() {
//        self.view_Home.backgroundColor = UIColor.clear
//        self.viewSearch.backgroundColor = UIColor.clear
//        self.viewNotes.backgroundColor = UIColor.clear
//        self.viewUser.backgroundColor = UIColor(named: "MenuSelectedBGColor")
        self.button_home.setImage(#imageLiteral(resourceName: "Home_icon"), for: .normal)
        self.button_Search.setImage(#imageLiteral(resourceName: "plate"), for: .normal)
        self.button_Notes.setImage(#imageLiteral(resourceName: "notes_icon"), for: .normal)
        self.button_user.setImage(#imageLiteral(resourceName: "User_icon_green"), for: .normal)

        self.labelHome.textColor = menuDefaultColor
        self.labelMenu.textColor = menuDefaultColor
        self.labelOrders.textColor = menuDefaultColor
        self.labelProfile.textColor = themeDarkColor
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}


extension UIColor{
    
}
