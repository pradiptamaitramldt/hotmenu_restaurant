//
//  ForgotPasswordView.swift
//  Restaurant
//
//  Created by Bit Mini on 11/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class ForgotPasswordView: BaseTableViewController {
    @IBOutlet weak var view_Email: UIView!
    @IBOutlet weak var textField_Email: CustomTextField!
    @IBOutlet weak var btn_forgotEmail: NormalButton!
    @IBOutlet weak var btn_Submit: NormalBoldButton!
    var presenter = ForgotPasswordPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view_Email.setRoundedCornered(height: view_Email.frame.size.height)
        self.btn_Submit.setRoundedCornered(height: btn_Submit.frame.size.height)
    }
    
    @IBAction func btn_SubmitClick(_ sender: Any) {
        AMShimmer.start(for: self.textField_Email)
        AMShimmer.start(for: self.btn_Submit)
        self.presenter.forgotpassword(email: self.textField_Email.text!.lowercased()) { (status, message) in
            
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.textField_Email)
                AMShimmer.stop(for: self.btn_Submit)
                self.showAlertWith(message: message) {
                    if status{
                        let vc = self.storyboard?.instantiateViewController(identifier: "verifyOTPView") as! VerifyOTPView
                        vc.email = self.textField_Email.text!.lowercased()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
          
        }
    }
    
    @IBAction func btn_ForgotEmailDidClick(_ sender: Any) {
    }

    @IBAction func btn_BackdidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    


}

// MARK: - Table view data source

extension ForgotPasswordView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        case 3:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 40, currentScrenHeight: self.view.bounds.height)
        case 4:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 73, currentScrenHeight: self.view.bounds.height)
        case 5:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
        default:
            return 0.0
        }
    }
}
