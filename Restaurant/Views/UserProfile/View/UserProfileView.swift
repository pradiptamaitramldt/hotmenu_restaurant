//
//  UserProfileView.swift
//  Restaurant
//
//  Created by Bit Mini on 21/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class UserProfileView: BaseTableViewController, SideMenuItemContent, Storyboardable   {
    var bottom_menu = BottomMenu()
    var viewBottom = UIView()
    @IBOutlet weak var btn_ViewProfile: NormalBoldButton!
    @IBOutlet weak var btnRestaurantProfile: NormalBoldButton!
    //    @IBOutlet weak var btn_MyFavourites: NormalBoldButton!
//    @IBOutlet weak var btn_MyOrders: NormalBoldButton!
//    @IBOutlet weak var btn_MyPayments: NormalBoldButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let bottomSafeArea: CGFloat
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows[0]
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            //            topSafeArea = safeFrame.minY
            bottomSafeArea = window.frame.maxY - safeFrame.maxY
        } else {
            //            topSafeArea = topLayoutGuide.length
            bottomSafeArea = bottomLayoutGuide.length
        }
        self.viewBottom = UIView(frame: CGRect(x: 0.0, y: self.view.bounds.height - (bottomSafeArea) - 80.0, width: self.view.bounds.width, height: 80.0))
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:viewBottom.bounds.size.width,height:viewBottom.bounds.size.height)
        viewBottom.addSubview(bottom_menu)
        bottom_menu.userSeleted()
        bottom_menu.delegate = self
        if let window = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first {
            window.addSubview(viewBottom)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.btn_ViewProfile.setRoundedCornered(height: btn_ViewProfile.frame.size.height)
        self.btnRestaurantProfile.setRoundedCornered(height: btnRestaurantProfile.frame.size.height)

//        self.btn_MyFavourites.setRoundedCornered(height: btn_MyFavourites.frame.size.height)
//        self.btn_MyOrders.setRoundedCornered(height: btn_MyOrders.frame.size.height)
//        self.btn_MyPayments.setRoundedCornered(height: btn_MyPayments.frame.size.height)

    }

    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewBottom.removeFromSuperview()
    }
    

    @IBAction func btn_MenuDidClick(_ sender: Any) {
        showSideMenu()
    }

    @IBAction func btn_ViewProfileDidClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "myProfileView") as! MyProfileView
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func restaurantProfileDidClick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "restaurentProfileView") as! RestaurentProfileView
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btn_MyFavouritesDidClick(_ sender: Any) {
    }
    @IBAction func btn_MyOrders(_ sender: Any) {
    }
    @IBAction func btn_MyPayments(_ sender: Any) {
    }
    
}

//MARK: bottomMenu delegate
extension UserProfileView: bottomMenuDelegate{
    func selectedButton(selectedItem:NSInteger){
        
        if selectedItem == 1 {
            //home
            //            let st = UIStoryboard.init(name: "DashBoardView", bundle: nil)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 0
            UIApplication.shared.windows[0].rootViewController = viewController
        }else if selectedItem == 2{
            //menu
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 1
            UIApplication.shared.windows[0].rootViewController = viewController

        }
        else if selectedItem == 3{
            //OrderList
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 6
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }else{
            //userprofile
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 5
            UIApplication.shared.windows[0].rootViewController = viewController
        }
    }
    
}

// MARK: - Table view data source

extension UserProfileView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 170, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 80, currentScrenHeight: self.view.bounds.height)
            case 2:
                return getTableViewRowHeightRespectToScreenHeight(givenheight: 80, currentScrenHeight: self.view.bounds.height)

        default:
            return 0.0
        }
    }
}
