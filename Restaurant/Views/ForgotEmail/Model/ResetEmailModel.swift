//
//  ResetEmailModel.swift
//  Restaurant
//
//  Created by Pradipta Maitra on 30/09/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import Foundation

// MARK: - ResetEmailResponseModel
struct ResetEmailResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: ResetEmailResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
}

// MARK: - ResponseData
struct ResetEmailResponseData: Codable {
    let email, changeEmailOtp: String?
}
