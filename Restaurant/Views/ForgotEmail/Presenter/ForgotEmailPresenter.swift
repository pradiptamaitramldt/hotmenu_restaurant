//
//  ForgotEmailPresenter.swift
//  Restaurant
//
//  Created by Souvik on 17/08/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class ForgotEmailPresenter: NSObject {

    
    func forgotEmail(mobile : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        
        if Utility.isValidMobileNumber(mobile){
            let param : [String: Any] = ["phone" : mobile]
                WebServiceManager.shared.requestAPI(url: WebServiceConstants.forgotEmail, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: param, httpMethodType: .post) { (data, err) in
                    if let responseData = data{
                        let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: ResetEmailResponseModel.self) { (resData, err) in
                            if resData != nil{
                                if resData?.statuscode! == ResponseCode.success.rawValue{
                                    completion(true,(resData?.message!)!)
                                }else{
                                    completion(false,(resData?.message!)!)
                                }
                            }else{
                                completion(false, err!.localizedDescription)
                            }
                        }
                    }else{
                        completion(false, "Request Timeout")
                    }
                }
        }else{
            completion(false, "Please enter a valid mobile number.")
        }
    }
}
