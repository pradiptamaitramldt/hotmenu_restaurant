//
//  ForgotEmailView.swift
//  Restaurant
//
//  Created by Bit Mini on 11/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class ForgotEmailView: BaseTableViewController {
    @IBOutlet weak var view_Mobile: UIView!
    @IBOutlet weak var textField_CountryCode: CustomTextField!
    @IBOutlet weak var textField_MobileNumber: CustomTextField!
    @IBOutlet weak var btn_Submit: NormalBoldButton!
    @IBOutlet weak var imgCountryFlag: UIImageView!
    
    let presenter = ForgotEmailPresenter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view_Mobile.setRoundedCornered(height: view_Mobile.frame.size.height)
        self.btn_Submit.setRoundedCornered(height: btn_Submit.frame.size.height)
    }
    
    @IBAction func btn_BackdidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSUbmitDidClick(_ sender: Any) {
        AMShimmer.start(for: self.btn_Submit)
        AMShimmer.start(for: self.textField_MobileNumber)
        self.presenter.forgotEmail(mobile: self.textField_MobileNumber.text!) { (status, message) in
           AMShimmer.stop(for: self.btn_Submit)
           AMShimmer.stop(for: self.textField_MobileNumber)
            DispatchQueue.main.async {
                self.showAlertWith(message: message) {
                    if status{
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
}
// MARK: - Table view data source

extension ForgotEmailView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 160, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        case 3:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 73, currentScrenHeight: self.view.bounds.height)
        case 4:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
        default:
            return 0.0
        }
    }
}


extension ForgotEmailView : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textField_CountryCode{
            let countryPickerStoryBoard = UIStoryboard.init(name: "CountryPicker", bundle: nil)
            let countryPickerVC = countryPickerStoryBoard.instantiateViewController(withIdentifier: "countryPickerViewController") as! CountryPickerViewController
            countryPickerVC.delegate = self
            self.present(countryPickerVC, animated: true, completion: nil)
        }
    }
}


extension ForgotEmailView : CountryPickerViewControllerDelegate{
    func didSelectCountry(country: Country) {
        self.textField_CountryCode.text = country.dialCode!
        self.imgCountryFlag.image = UIImage(named: country.flag!)
    }
}
