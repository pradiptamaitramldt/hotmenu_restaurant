//
//  EditProfileView.swift
//  Restaurant
//
//  Created by Bit Mini on 25/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class EditProfileView: BaseTableViewController {
    @IBOutlet weak var imageView_ProfilePic: UIImageView!
    @IBOutlet weak var view_ImageUploader: UIView!
    @IBOutlet weak var view_FName: UIView!
    @IBOutlet weak var textField_FName: CustomTextField!
    @IBOutlet weak var view_LName: UIView!
    @IBOutlet weak var textField_LName: CustomTextField!
    @IBOutlet weak var view_Email: UIView!
    @IBOutlet weak var textField_Email: CustomTextField!
    @IBOutlet weak var view_Phone: UIView!
    @IBOutlet weak var imageView_CountryFlag: UIImageView!
    @IBOutlet weak var textField_CountryCode: CustomTextField!
    @IBOutlet weak var textField_Phone: CustomTextField!
    @IBOutlet weak var button_Save: NormalBoldButton!
    
    var bottom_menu = BottomMenu()
    var viewBottom = UIView()
    var countries: [Country] = []
    var picker1 = UIImagePickerController()
    var presenter = EditProfileManager()

    override func viewDidLoad(){
        super.viewDidLoad()
        
        let user_name = UserDefaultValues.userName
        let array = user_name.components(separatedBy: " ")
        var first_name = ""
        var last_name = ""
        if array.count>0 {
            first_name = array[0]
            last_name = array[1]
        }
        self.textField_FName.text = first_name
        self.textField_LName.text = last_name
        self.textField_Email.text = UserDefaultValues.email
        self.textField_Phone.text = UserDefaultValues.phone
        self.textField_CountryCode.text = UserDefaultValues.countryCode
        self.textField_CountryCode.delegate = self
//        DispatchQueue.main.async {
            self.loadCountriesJson()
            
//        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if UserDefaultValues.profileImage != ""{
            let url = URL(string: UserDefaultValues.profileImage)
            self.imageView_ProfilePic.sd_setImage(with: url, placeholderImage: nil)
//            let data = try? Data(contentsOf: url!)
//            if let imageData = data {
//                self.imageView_ProfilePic.image = UIImage(data: imageData)
//                self.imageView_ProfilePic.sd_setImage(with: url, placeholderImage: nil)
//            }
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.showBottomMenu()
        self.imageView_ProfilePic.setRoundedCornered(height: imageView_ProfilePic.frame.size.height)
        self.view_ImageUploader.setRoundedCornered(height: view_ImageUploader.frame.size.height)
        self.view_FName.setRoundedCornered(height: view_FName.frame.size.height)
        self.view_LName.setRoundedCornered(height: view_LName.frame.size.height)
        self.view_Email.setRoundedCornered(height: view_Email.frame.size.height)
        self.view_Phone.setRoundedCornered(height: view_Phone.frame.size.height)
        self.button_Save.setRoundedCornered(height: button_Save.frame.size.height)
        
    }

    
    @IBAction func btn_BackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_SaveClick(_ sender: Any) {
       
        AMShimmer.start(for: self.textField_FName)
        AMShimmer.start(for: self.textField_LName)
        AMShimmer.start(for: self.textField_Email)
        AMShimmer.start(for: self.textField_Phone)
        AMShimmer.start(for: self.textField_CountryCode)
        AMShimmer.start(for: self.imageView_CountryFlag)
        AMShimmer.start(for: self.imageView_ProfilePic)
        AMShimmer.start(for: self.button_Save)
        self.presenter.editProfile(firstName: self.textField_FName.text!, lastName: self.textField_LName.text!, email: self.textField_Email.text!.lowercased(), countryCode: self.textField_CountryCode.text!, phone: self.textField_Phone.text!, loginType: UserDefaultValues.LoginType) { (status, message) in
        
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.textField_FName)
                AMShimmer.stop(for: self.textField_LName)
                AMShimmer.stop(for: self.textField_Email)
                AMShimmer.stop(for: self.textField_Phone)
                AMShimmer.stop(for: self.textField_CountryCode)
                AMShimmer.stop(for: self.imageView_CountryFlag)
                AMShimmer.stop(for: self.imageView_ProfilePic)
                AMShimmer.stop(for: self.button_Save)
                self.showAlertWith(message: message) {
                }
            }

        }
    }
    
    @IBAction func button_SelectImageClick(_ sender: Any) {
        self.showImagePicker(picker: picker1)
    }
    
}


extension EditProfileView {
    
    func showBottomMenu(){
        let bottomSafeArea: CGFloat
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows[0]
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            //            topSafeArea = safeFrame.minY
            bottomSafeArea = window.frame.maxY - safeFrame.maxY
        } else {
            //            topSafeArea = topLayoutGuide.length
            bottomSafeArea = bottomLayoutGuide.length
        }
        self.viewBottom = UIView(frame: CGRect(x: 0.0, y: self.view.bounds.height - (bottomSafeArea) - 80.0, width: self.view.bounds.width, height: 80.0))
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:viewBottom.bounds.size.width,height:viewBottom.bounds.size.height)
        viewBottom.addSubview(bottom_menu)
        bottom_menu.userSeleted()
        bottom_menu.delegate = self
        if let window = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first {
            window.addSubview(viewBottom)
        }
    }

    func loadCountriesJson() {
        if let url = Bundle.main.url(forResource: "countryCodes", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                self.countries = try decoder.decode([Country].self, from: data)
                for country in self.countries{
                    if country.dialCode == UserDefaultValues.countryCode{
                        self.imageView_CountryFlag.image = UIImage(named: country.flag!)
                    }
                }
            } catch {
                print("error:\(error)")
            }
        }
    }
    
    func showImagePicker(picker : UIImagePickerController){

        let alertMessage = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        let titleAttributes = [NSAttributedString.Key.font: ceraProLargeFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let titleString = NSAttributedString(string: "HotMenu", attributes: titleAttributes as [NSAttributedString.Key : Any])
        let messageAttributes = [NSAttributedString.Key.font: ceraProNormalFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let messageString = NSAttributedString(string: "Please select Image for your Profile picture", attributes: messageAttributes as [NSAttributedString.Key : Any])
        alertMessage.setValue(titleString, forKey: "attributedTitle")
        alertMessage.setValue(messageString, forKey: "attributedMessage")

        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                picker.delegate = self
                picker.sourceType = .camera;
                picker.allowsEditing = true
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        alertMessage.addAction(cameraAction)
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (act) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                picker.delegate = self
                picker.sourceType = .photoLibrary;
                picker.allowsEditing = true
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        alertMessage.addAction(galleryAction)
        let action = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alertMessage .addAction(action)
        alertMessage.view.tintColor = themeDarkColor
        
        self.present(alertMessage, animated: true, completion: nil)

    }

    

}

extension EditProfileView : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK: - Image Picker -
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.editedImage] as? UIImage {
            self.imageView_ProfilePic.image = pickedImage
            AMShimmer.start(for: self.imageView_ProfilePic)
            self.presenter.editProfileImage(image: pickedImage) { (status, message) in
                DispatchQueue.main.async {
                    AMShimmer.stop(for: self.imageView_ProfilePic)
                    self.showAlertWith(message: message) {
                    }
                }
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

extension EditProfileView : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textField_CountryCode{
            let countryPickerStoryBoard = UIStoryboard.init(name: "CountryPicker", bundle: nil)
            let countryPickerVC = countryPickerStoryBoard.instantiateViewController(withIdentifier: "countryPickerViewController") as! CountryPickerViewController
            countryPickerVC.delegate = self
            self.present(countryPickerVC, animated: true, completion: nil)
        }
    }
}

extension EditProfileView : CountryPickerViewControllerDelegate{
    func didSelectCountry(country: Country) {
        self.textField_CountryCode.text = country.dialCode!
        self.imageView_CountryFlag.image = UIImage(named: country.flag!)
    }
}



//MARK: bottomMenu delegate
extension EditProfileView: bottomMenuDelegate{
    func selectedButton(selectedItem:NSInteger){
        
        if selectedItem == 1 {
            //home
            //            let st = UIStoryboard.init(name: "DashBoardView", bundle: nil)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 0
            UIApplication.shared.windows[0].rootViewController = viewController
        }else if selectedItem == 2{
            //menu
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 1
            UIApplication.shared.windows[0].rootViewController = viewController

        }
        else if selectedItem == 3{
            //OrderList
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 6
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }else{
            //userprofile
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 5
            UIApplication.shared.windows[0].rootViewController = viewController
        }
    }
    
}

// MARK: - Table view data source

extension EditProfileView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 40, currentScrenHeight: self.view.bounds.height)
        case 1:
            return 195.0 //getTableViewRowHeightRespectToScreenHeight(givenheight: 161, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        case 3:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        case 4:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        case 5:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        case 6:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 55, currentScrenHeight: self.view.bounds.height)
        case 7:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 55, currentScrenHeight: self.view.bounds.height)
            case 8:
                return getTableViewRowHeightRespectToScreenHeight(givenheight: 182, currentScrenHeight: self.view.bounds.height)

            
        default:
            return 0.0
        }
    }
}
