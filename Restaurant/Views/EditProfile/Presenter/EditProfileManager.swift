//
//  EditProfileManager.swift
//  Restaurant
//
//  Created by Bit Mini on 25/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class EditProfileManager: NSObject {
    func editProfile(firstName : String, lastName : String, email : String, countryCode : String, phone : String,loginType : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        if firstName.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            if lastName.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                if Utility.isValidEmail(email){
                    if Utility.isValidMobileNumber(phone){
                        if countryCode.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                            let fullName = "\(firstName) \(lastName)"
                            let phoneNumber = "\(phone)"
                            let param : [String: Any] = ["fullName" : fullName,
                                                         "phone"     : phoneNumber,
                                                         "customerId" : UserDefaultValues.userID!,"countryCode" : countryCode]
                            WebServiceManager.shared.requestAPI(url: WebServiceConstants.editUserProfile, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
                                if let responseData = data{
                                    let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                                        if resData != nil{
                                            if resData?.statuscode! == ResponseCode.success.rawValue{
                                                completion(true,"Profile Updated.")
                                            }else{
                                                completion(false,(resData?.message!)!)
                                            }
                                        }else{
                                            completion(false, err!.localizedDescription)
                                        }
                                    }
                                }
                            }
                        }else{
                            completion(false, "Please add your Country Code.")
                        }
                    }else{
                        completion(false, "Please enter valid Mobile No.")
                    }
                }else{
                    completion(false, "Please enter valid Email.")
                }
            }else{
                completion(false, "Please enter Your Last Name.")
            }
        }else{
            completion(false, "Please enter Your First Name.")
        }

    }
    
    func editProfileImage(image : UIImage,completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        WebServiceManager.shared.requestMultipartAPI(url: WebServiceConstants.editUserImage, parameter: ["customerId" : UserDefaultValues.userID!, "loginId" : UserDefaultValues.loginID ?? ""], httpMethodType: .post, multipartData: image.pngData(), isAuthorized: true, authToken: "Bearer \(UserDefaultValues.authToken)") { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            completion(true,"Profile Image Updated.")
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }
        }
    }


}
