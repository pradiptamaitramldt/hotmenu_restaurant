//
//  VerifyOTPView.swift
//  Restaurant
//
//  Created by Bit Mini on 11/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class VerifyOTPView: BaseTableViewController {
    @IBOutlet weak var view_OTP: UIView!
    @IBOutlet weak var textFieldOTP: CustomTextField!
    @IBOutlet weak var btn_ResendCode: NormalBoldButton!
    @IBOutlet weak var btn_SubmitCode: NormalBoldButton!
    var email : String = ""
    var presenter = VerifyOTPPresenter()
    var userID : String = ""
    var viewShowFor = ""
    var isLogin = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view_OTP.setRoundedCornered(height: view_OTP.frame.size.height)
        self.btn_ResendCode.setRoundedCornered(height: btn_ResendCode.frame.size.height)
        self.btn_SubmitCode.setRoundedCornered(height: btn_SubmitCode.frame.size.height)

    }

    @IBAction func btn_BackdidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_ResendCodeClick(_ sender: Any) {
       
        AMShimmer.start(for: self.btn_ResendCode)
        self.presenter.resendOTP(email: self.email) { (statsu, message) in
           
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.btn_ResendCode)
                self.showAlertWith(message: message) {
                    
                }
            }
        }
        
    }
    
    @IBAction func btn_SubmitDidClick(_ sender: Any) {
        if self.viewShowFor == "verifyRegiter"{
            if textFieldOTP.text! != ""{
                AMShimmer.start(for: self.textFieldOTP)
                AMShimmer.start(for: self.btn_SubmitCode)
                self.presenter.verifyOTP(userId: self.userID, code: textFieldOTP.text!, isLogin: self.isLogin) { (status, message) in
                    
                    DispatchQueue.main.async {
                        AMShimmer.stop(for: self.textFieldOTP)
                        AMShimmer.stop(for: self.btn_SubmitCode)
                        if status{
                            self.showAlertWith(message: "Your email is verifed now. You can login with your email and password.") {
                                
                                UserDefaultValues.LoginType = UserType.vendor.rawValue
                                if self.isLogin{
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
                                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
                                    UIApplication.shared.windows[0].rootViewController = viewController
                                }else{
                                    let vc = self.storyboard?.instantiateViewController(identifier: "addTimingView") as! AddTimingView
                                    vc.restaurantID = self.userID
                                    self.navigationController?.pushViewController(vc, animated: true)
                                    //                                    self.navigationController?.popToRootViewController(animated: true)
                                }
                            }
                        }else{
                            self.showAlertWith(message: message) {
                            }
                        }
                    }
                }
            }else{
                showAlertWith(message: "Please enter the code you have got in your mail.") {
                }

            }
        }else{
            if textFieldOTP.text! == UserDefaultValues.OTP{
                let vc = self.storyboard?.instantiateViewController(identifier: "resetYourPasswordView") as! ResetYourPasswordView
                vc.email = self.email
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                showAlertWith(message: "Code does'nt match.") {
                    self.textFieldOTP.text = ""
                }
            }
        }
    }
    

}
// MARK: - Table view data source

extension VerifyOTPView {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
        case 1:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
        case 2:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
        case 3:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 73, currentScrenHeight: self.view.bounds.height)
        case 4:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 24, currentScrenHeight: self.view.bounds.height)
        case 5:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 113, currentScrenHeight: self.view.bounds.height)
        case 6:
            return getTableViewRowHeightRespectToScreenHeight(givenheight: 59, currentScrenHeight: self.view.bounds.height)
            
        default:
            return 0.0
        }
    }
}
