//
//  VerifyOTPPresenter.swift
//  Restaurant
//
//  Created by Bit Mini on 13/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

class VerifyOTPPresenter : NSObject{
    func resendOTP(email : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["email" : email, "userType" : UserType.vendor.rawValue]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.resendOTP, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: ForgotPasswordResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            UserDefaultValues.OTP = (resData?.responseData?.forgotPassOtp!)!
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }
        }
    }
    
    func verifyOTP(userId : String,code : String,isLogin : Bool, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        let param : [String: Any] = ["email" : userId,
                                     "otp" : code]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.verifyRegitrationOTP, httpHeader: WebServiceHeaderGenerator.generateHeader(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: LoginResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            if isLogin{
                                UserDefaultValues.authToken = (resData?.responseData?.authToken!)!
                                UserDefaultValues.userID = (resData?.responseData?.userDetails?.id!)!
                              //  UserDefaultValues.RestaurantName = (resData?.responseData?.restaurantName!)!
                                UserDefaultValues.profileImage = (resData?.responseData?.userDetails?.profileImage!)!
                                UserDefaultValues.LoginType = UserType.vendor.rawValue
                                UserDefaultValues.CustomerLoginType = "RESTAURANT"
                              //  UserDefaultValues.vendorID = (resData?.responseData?.userDetails?.vendorID!)!
                              //  UserDefaultValues.vendorName = (resData?.responseData?.restaurantName!)!
                            }
                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)
            }
        }
    }
}
