 //
//  ChangePasswordView.swift
//  Restaurant
//
//  Created by Bit Mini on 25/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer
 
class ChangePasswordView: BaseTableViewController {
    @IBOutlet weak var view_OldPassword: UIView!
    @IBOutlet weak var textFieldOldPassword: CustomTextField!
    @IBOutlet weak var view_NewPassword: UIView!
    @IBOutlet weak var textFieldNewPassword: CustomTextField!
    @IBOutlet weak var view_ConfirmPassword: UIView!
    @IBOutlet weak var textFieldConfirmPassword: CustomTextField!
    @IBOutlet weak var btn_Submit: NormalBoldButton!
    
    var presenter = ChangePasswordPresenter()
    var bottom_menu = BottomMenu()
    var viewBottom = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.showBottomMenu()
        self.view_OldPassword.setRoundedCornered(height: view_OldPassword.frame.size.height)
        self.view_NewPassword.setRoundedCornered(height: view_NewPassword.frame.size.height)
        self.view_ConfirmPassword.setRoundedCornered(height: view_ConfirmPassword.frame.size.height)
        self.btn_Submit.setRoundedCornered(height: btn_Submit.frame.size.height)
        
    }

    
    @IBAction func btn_backDidClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_SubmitClick(_ sender: Any) {
       
        AMShimmer.start(for: self.textFieldOldPassword)
        AMShimmer.start(for: self.textFieldNewPassword)
        AMShimmer.start(for: self.textFieldConfirmPassword)
        AMShimmer.start(for: self.btn_Submit)
        self.presenter.changePassword(Oldpassword: self.textFieldOldPassword.text!, newPassword: self.textFieldNewPassword.text!, confirmPassword: self.textFieldConfirmPassword.text!) { (status, message) in
            DispatchQueue.main.async {
                AMShimmer.stop(for: self.textFieldOldPassword)
                       AMShimmer.stop(for: self.textFieldNewPassword)
                       AMShimmer.stop(for: self.textFieldConfirmPassword)
                       AMShimmer.stop(for: self.btn_Submit)
                self.showAlertWith(message: message) {
                }
            }

        }
    }
    
}
 
 extension ChangePasswordView {
    
    func showBottomMenu(){
        let bottomSafeArea: CGFloat
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.windows[0]
            let safeFrame = window.safeAreaLayoutGuide.layoutFrame
            //            topSafeArea = safeFrame.minY
            bottomSafeArea = window.frame.maxY - safeFrame.maxY
        } else {
            //            topSafeArea = topLayoutGuide.length
            bottomSafeArea = bottomLayoutGuide.length
        }
        self.viewBottom = UIView(frame: CGRect(x: 0.0, y: self.view.bounds.height - (bottomSafeArea) - 80.0, width: self.view.bounds.width, height: 80.0))
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:viewBottom.bounds.size.width,height:viewBottom.bounds.size.height)
        viewBottom.addSubview(bottom_menu)
        bottom_menu.userSeleted()
        bottom_menu.delegate = self
        if let window = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first {
            window.addSubview(viewBottom)
        }
        
    }
 }
 

 //MARK: bottomMenu delegate
 extension ChangePasswordView: bottomMenuDelegate{
    func selectedButton(selectedItem:NSInteger){
        
        if selectedItem == 1 {
            //home
            //            let st = UIStoryboard.init(name: "DashBoardView", bundle: nil)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 0
            UIApplication.shared.windows[0].rootViewController = viewController
        }else if selectedItem == 2{
            //menu
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 1
            UIApplication.shared.windows[0].rootViewController = viewController

        }
        else if selectedItem == 3{
            //OrderList
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 6
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }else{
            //userprofile
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 5
            UIApplication.shared.windows[0].rootViewController = viewController
        }
    }
     
 }

 // MARK: - Table view data source

 extension ChangePasswordView {
     
     override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         switch indexPath.row {
         case 0:
             return getTableViewRowHeightRespectToScreenHeight(givenheight: 175, currentScrenHeight: self.view.bounds.height)
         case 1:
             return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
         case 2:
             return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
         case 3:
             return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
         case 4:
             return getTableViewRowHeightRespectToScreenHeight(givenheight: 75, currentScrenHeight: self.view.bounds.height)
         case 5:
             return getTableViewRowHeightRespectToScreenHeight(givenheight: 182, currentScrenHeight: self.view.bounds.height)
         default:
             return 0.0
         }
     }
 }

