//
//  ChangePasswordPresenter.swift
//  Restaurant
//
//  Created by Bit Mini on 25/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class ChangePasswordPresenter: NSObject {
    func changePassword(Oldpassword : String, newPassword : String, confirmPassword : String, completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        
        if Utility.isValidPassword(Oldpassword){
            if Utility.isValidPassword(newPassword){
                if Utility.isValidPassword(confirmPassword){
                    if newPassword == confirmPassword {
                        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                                     "oldPassword": Oldpassword,
                                                     "newPassword": newPassword,
                                                     "confirmPassword":confirmPassword]
                        WebServiceManager.shared.requestAPI(url: WebServiceConstants.changePassword, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
                            if let responseData = data{
                                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: CommonResponseModel.self) { (resData, err) in
                                    if resData != nil{
                                        if resData?.statuscode! == ResponseCode.success.rawValue{
                                            completion(true,(resData?.message!)!)
                                        }else{
                                            completion(false,(resData?.message!)!)
                                        }
                                    }else{
                                        completion(false, err!.localizedDescription)
                                    }
                                }
                            }
                        }
                    }else{
                        completion(false, "New password and confirm password doesn't match.")
                    }
                    
                }else{
                    completion(false, "Please enter valid Password.")
                    
                }
                
            }else{
                completion(false, "Please enter valid new Password.")
                
            }
            
        }else{
            completion(false, "Please enter valid old Password.")

        }
        
        
    }

}
