//
//  DashBoardPresenter.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class DashBoardPresenter: NSObject {
    var cancelled: Int = 0
    var delivered :  Int = 0
    var new : Int = 0
    var ready: Int = 0
    var pickup: Int = 0
    func getAllOrdersCount(completion : @escaping(_ success : Bool ,_ message : String) -> Void){
        
        let param : [String: Any] = ["customerId" : UserDefaultValues.userID!,
                                     "vendorId" : UserDefaultValues.vendorID]
        WebServiceManager.shared.requestAPI(url: WebServiceConstants.getDashBoardItems, httpHeader: WebServiceHeaderGenerator.generateHeaderWithAuthKey(),parameter: param, httpMethodType: .post) { (data, err) in
            if let responseData = data{
                let _ = JSONResponseDecoder.decodeFrom(responseData, returningModelType: DashboardResponseModel.self) { (resData, err) in
                    if resData != nil{
                        if resData?.statuscode! == ResponseCode.success.rawValue{
                            self.cancelled = (resData?.responseData?.cancelledOrder)!
                            self.delivered = (resData?.responseData?.delivered)!
                            self.new = (resData?.responseData?.new)!
                            self.pickup = (resData?.responseData?.pickedUp)!
                            self.ready = (resData?.responseData?.ready)!

                            completion(true,(resData?.message!)!)
                        }else{
                            completion(false,(resData?.message!)!)
                        }
                    }else{
                        completion(false, err!.localizedDescription)
                    }
                }
            }else{
                completion(false, ErrorInfo.netWorkError.rawValue)

            }
        }
    }
}
