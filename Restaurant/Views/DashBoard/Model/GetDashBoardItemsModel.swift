//
//  GetDashBoardItemsModel.swift
//  Restaurant
//
//  Created by Souvik on 19/05/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import Foundation

// MARK: - DashboardResponseModel
struct DashboardResponseModel: Codable {
    let success: Bool?
    let statuscode: Int?
    let message: String?
    let responseData: DashboardResponseData?

    enum CodingKeys: String, CodingKey {
        case success
        case statuscode = "STATUSCODE"
        case message
        case responseData = "response_data"
    }
    
}

// MARK: - ResponseData
struct DashboardResponseData: Codable {
    let new, ready, pickedUp, delivered: Int?
    let cancelledOrder: Int?
}
