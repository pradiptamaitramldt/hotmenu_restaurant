//
//  DashBoardView.swift
//  Restaurant
//
//  Created by Bit Mini on 11/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit
import AMShimmer

class DashBoardView: BaseViewController, SideMenuItemContent, Storyboardable    {
    
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var labelPreparingOrders: CustomNormalLabel!
    @IBOutlet weak var labelDelivered: CustomNormalLabel!
    @IBOutlet weak var labelDelayedOrder: CustomNormalLabel!
    @IBOutlet weak var labelNewOrders: CustomNormalLabel!
    @IBOutlet weak var labelPreOrders: CustomNormalLabel!
    @IBOutlet weak var labelReadyForDelivered: CustomNormalLabel!
    
    @IBOutlet weak var imageOrderPreparing: UIImageView!
    @IBOutlet weak var imageDelivered: UIImageView!
    @IBOutlet weak var imageNewOrder: UIImageView!
    @IBOutlet weak var imageDelayed: UIImageView!
    @IBOutlet weak var imagePreOrder: UIImageView!
    @IBOutlet weak var imageReadyToDeliver: UIImageView!
    @IBOutlet weak var notificationBell: UIBarButtonItem!
    
    var bottom_menu = BottomMenu()
    var presenter = DashBoardPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //  self.title = String(describing: "\(UserDefaultValues.vendorName)")
        
        bottom_menu = BottomMenu.instanceFromNib()
        bottom_menu.frame = CGRect(x:0,y:0,width:self.viewBottom.bounds.size.width,height:self.viewBottom.bounds.size.height)
        self.viewBottom.addSubview(bottom_menu)
        bottom_menu.homeSelected()
        bottom_menu.delegate = self
        self.updateCounts()
        NotificationCenter.default.addObserver(self, selector: #selector(moveToDetails(_:)), name: .gotPush, object: nil)
        
        // Do any additional setup after loading the view.
    }
    // self.startLoaderAnimation()
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.notificationBell.addBadge(number: 0)
        
        if UserDefaultValues.userID != nil{
            AMShimmer.start(for: self.imageNewOrder)
            AMShimmer.start(for: self.imageDelivered)
            AMShimmer.start(for: self.imageOrderPreparing)
            AMShimmer.start(for: self.imageDelayed)
          //  AMShimmer.start(for: self.imagePreOrder)
            AMShimmer.start(for: self.imageReadyToDeliver)
            
            AMShimmer.start(for: self.labelPreparingOrders)
            AMShimmer.start(for: self.labelDelivered)
            AMShimmer.start(for: self.labelDelayedOrder)
            AMShimmer.start(for: self.labelNewOrders)
         //   AMShimmer.start(for: self.labelPreOrders)
            AMShimmer.start(for: self.labelReadyForDelivered)
            self.presenter.getAllOrdersCount { (status, message) in
               
                DispatchQueue.main.async {
                    AMShimmer.stop(for: self.imageNewOrder)
                    AMShimmer.stop(for: self.imageDelivered)
                    AMShimmer.stop(for: self.imageOrderPreparing)
                    AMShimmer.stop(for: self.imageDelayed)
                  //  AMShimmer.stop(for: self.imagePreOrder)
                    AMShimmer.stop(for: self.imageReadyToDeliver)
                    AMShimmer.stop(for: self.labelPreparingOrders)
                    AMShimmer.stop(for: self.labelDelivered)
                    AMShimmer.stop(for: self.labelDelayedOrder)
                    AMShimmer.stop(for: self.labelNewOrders)
                  //  AMShimmer.stop(for: self.labelPreOrders)
                    AMShimmer.stop(for: self.labelReadyForDelivered)
                    self.updateCounts()
                }
            }
        }
    }
    
    @IBAction func btn_MenuDidClick(_ sender: Any) {
        showSideMenu()
    }
    
    @IBAction func btnOrdersPreparingDidClick(_ sender: Any) {
        //OrderList
        //        if self.presenter.prepared > 0{
        UserDefaultValues.selectedType = OrderStatus.pickedup.rawValue
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
        viewController.viewControllerIndex = 6
        UIApplication.shared.windows[0].rootViewController = viewController
//                }
        
    }
    
    @IBAction func notificationBellDidClick(_ sender: Any) {
        let storybrd = UIStoryboard(name: "NotificationSettingsView", bundle: nil)
        let vc = storybrd.instantiateViewController(identifier: "notificationsListView") as! NotificationsListView
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnOrdersDeliveredDidClick(_ sender: Any) {
        //OrderList
        //        if self.presenter.delivered > 0{
        UserDefaultValues.selectedType = OrderStatus.delivered.rawValue
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
        viewController.viewControllerIndex = 6
        UIApplication.shared.windows[0].rootViewController = viewController
        //        }
        
    }
    
    @IBAction func btnOrdersDelaydDidClick(_ sender: Any) {
        //OrderList
        //        if self.presenter.delay > 0{
        UserDefaultValues.selectedType = OrderStatus.cancelled.rawValue
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
        viewController.viewControllerIndex = 6
        UIApplication.shared.windows[0].rootViewController = viewController
        //        }
        
    }
    
    @IBAction func btnNewOrdersDidCllck(_ sender: Any) {
        //OrderList
        //        if self.presenter.new > 0{
        
        UserDefaultValues.selectedType = OrderStatus.new.rawValue
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
        viewController.viewControllerIndex = 6
        UIApplication.shared.windows[0].rootViewController = viewController
        //        }
    }
    
    @IBAction func btnPreOrdersDidClick(_ sender: Any) {
        //OrderList
        //        if self.presenter.pre > 0{
//        UserDefaultValues.selectedType = OrderStatus.pre.rawValue
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
//        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
//        viewController.viewControllerIndex = 6
//        UIApplication.shared.windows[0].rootViewController = viewController
        //        }
        
    }
    
    @IBAction func btnOrdersReadyToDeliveryDidClick(_ sender: Any) {
        //OrderList
        //        if self.presenter.ready > 0{
        UserDefaultValues.selectedType = OrderStatus.ready.rawValue
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
        viewController.viewControllerIndex = 6
        UIApplication.shared.windows[0].rootViewController = viewController
        //        }
        
    }
    
    @objc func moveToDetails(_ notificationData: Notification){
        self.notificationBell.updateBadge(number: UserDefaultValues.badgeCount)
    }
    
    func updateCounts(){
        self.labelNewOrders.text = "\(self.presenter.new) New Orders"
        if self.presenter.new > 0{
            self.labelNewOrders.textColor = themeDarkColor
            //            self.imageNewOrder.image = #imageLiteral(resourceName: "newOrder")
        }else{
            self.labelNewOrders.textColor = .black
            //            self.imageNewOrder.image = #imageLiteral(resourceName: "newOrder_black")
        }
        self.labelReadyForDelivered.text = "\(self.presenter.ready) Orders ready for Delivery"
        if self.presenter.ready > 0{
            self.labelReadyForDelivered.textColor = themeDarkColor
            //              self.imageReadyToDeliver.image = #imageLiteral(resourceName: "readyToDeliveru_green")
        }else{
            self.labelReadyForDelivered.textColor = .black
            //              self.imageReadyToDeliver.image = #imageLiteral(resourceName: "readyToDeliveru")
        }
        self.labelPreparingOrders.text = "\(self.presenter.pickup) Orders being Picked Up"
        if self.presenter.pickup > 0{
            self.labelPreparingOrders.textColor = themeDarkColor
            //            self.imageOrderPreparing.image = #imageLiteral(resourceName: "orderPreparing_green")
        }else{
            self.labelPreparingOrders.textColor = .black
            //            self.imageOrderPreparing.image = #imageLiteral(resourceName: "orderPreparing")
        }
        self.labelDelivered.text = "\(self.presenter.delivered) Orders Delivered"
        if self.presenter.delivered > 0{
            self.labelDelivered.textColor = themeDarkColor
            //            self.imageDelivered.image = #imageLiteral(resourceName: "Delivered_green")
        }else{
            self.labelDelivered.textColor = .black
            //            self.imageDelivered.image = #imageLiteral(resourceName: "Delivered")
        }
        self.labelDelayedOrder.text = "\(self.presenter.cancelled) Orders Cancelled"
        if self.presenter.cancelled > 0{
            self.labelDelayedOrder.textColor = themeDarkColor
            //            self.imageDelayed.image = #imageLiteral(resourceName: "delayed_green")
        }else{
            self.labelDelayedOrder.textColor = .black
            //            self.imageDelayed.image = #imageLiteral(resourceName: "delayed")
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//MARK: bottomMenu delegate
extension DashBoardView: bottomMenuDelegate{
    func selectedButton(selectedItem:NSInteger){
        
        if selectedItem == 1 {
            //home
            //            let st = UIStoryboard.init(name: "DashBoardView", bundle: nil)
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 0
            UIApplication.shared.windows[0].rootViewController = viewController
        }else if selectedItem == 2{
            //menu
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 1
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }
        else if selectedItem == 3{
            //OrderList
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 6
            UIApplication.shared.windows[0].rootViewController = viewController
            
        }else{
            //userprofile
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "HostViewController", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "hostViewController") as! HostViewController
            viewController.viewControllerIndex = 5
            UIApplication.shared.windows[0].rootViewController = viewController
        }
    }
    
}
