    //
    //  UIClasses.swift
    //  Restaurant
    //
    //  Created by Bit Mini on 06/02/20.
    //  Copyright © 2020 Bit Mini. All rights reserved.
    //
    
    import Foundation
    import UIKit
    
    var ceraProNormalFontSize : UIFont{
        return UIFont(name: "CeraPro-Medium", size: 17.0)!
    }
    
    var ceraProMediumFontSize : UIFont{
        return UIFont(name: "CeraPro-Medium", size: 20.0)!
    }
    
    var ceraProLargeFontSize : UIFont{
        return UIFont(name: "CeraPro-Medium", size: 22.0)!
    }
    
    
    var ceraProBoldNormalFontSize : UIFont{
        return UIFont(name: "CeraPro-Bold", size: 17.0)!
    }
    
    
    var themeDarkColor = UIColor.init(named: "TextColorDark")
    
    var menuDefaultColor = UIColor.init(named: "MenuDefaultColor")
    
    
    
    class CustomButton: UIButton {
        var params: Dictionary<String, Any>
        override init(frame: CGRect) {
            self.params = [:]
            super.init(frame: frame)
        }
        
        required init?(coder aDecoder: NSCoder) {
            self.params = [:]
            super.init(coder: aDecoder)
        }
        
        override func awakeFromNib() {
            
        }
        
        public func setTitleAlignment(_ alignment: UIControl.ContentHorizontalAlignment) {
            self.contentHorizontalAlignment = alignment
        }
        
        public func setTitleFont(fontWeight: FontWeight, ofSize: CGFloat = 17) {
            switch fontWeight {
            case .bold:
                self.titleLabel?.font = UIFont(name: "CeraPro-Bold", size: ofSize)
            case .medium:
                self.titleLabel?.font = UIFont(name: "CeraPro-Medium", size: ofSize)
            case .light:
                self.titleLabel?.font = UIFont(name: "CeraPro-Light", size: ofSize)
            case .thin:
                self.titleLabel?.font = UIFont(name: "CeraPro-Thin", size: ofSize)
            default:
                self.titleLabel?.font = UIFont(name: "CeraPro-Regular", size: ofSize)
            }
        }
        
        public func setNormalTitle(_ title: String) {
            self.setTitle(title, for: .normal)
        }
        
        public func setNormalTitleColor(_ color: UIColor?) {
            self.setTitleColor(color ?? UIColor.black, for: .normal)
        }
        
        public func setRoundedCorner() {
            self.layer.cornerRadius = self.bounds.size.height / 2
            self.layer.masksToBounds = true
        }
        
        func customizeButtonFont(fullText: String, mainText: String, creditsText: String) {
            
            let fontBig = UIFont(name:"CeraPro-Light", size: 17.0)
            let fontSmall = UIFont(name:"CeraPro-Regular", size: 19.0)
            let attributedString = NSMutableAttributedString(string: fullText, attributes: nil)
            
            let bigRange = (attributedString.string as NSString).range(of: mainText)
            let creditsRange = (attributedString.string as NSString).range(of: creditsText)
            
            attributedString.setAttributes([ NSAttributedString.Key.font: fontBig!, NSAttributedString.Key.foregroundColor: UIColor.white], range: bigRange)
            
            attributedString.setAttributes([NSAttributedString.Key.font: fontSmall!, NSAttributedString.Key.foregroundColor: UIColor.init(named: "YellowColor")!], range: creditsRange)
            
            self.setAttributedTitle(attributedString, for: .normal)
        }
    }
    
    class RoundedButton: UIButton {
        
        override func awakeFromNib() {
            super.awakeFromNib()
            layer.cornerRadius = frame.size.height / 2
            clipsToBounds = true
            imageView?.contentMode = .scaleAspectFit
        }
        
        @IBInspectable var borderwidth: CGFloat = 0 {
            didSet {
                layer.borderWidth = borderWidth
            }
        }
        
        
        @IBInspectable var fontSize: CGFloat = 20.0 {
            didSet {
                titleLabel?.font = UIFont(name: "CeraPro-Medium", size: fontSize)
            }
        }
        
        @IBInspectable var bordercolor: UIColor? {
            didSet {
                layer.borderColor = borderColor?.cgColor
            }
        }
        
        @IBInspectable var bgColor: UIColor? {
            didSet {
                backgroundColor = bgColor
            }
        }
        
        override var isHighlighted: Bool {
            didSet {
                if isHighlighted{
                    backgroundColor = backgroundColor?.withAlphaComponent(0.6)
                }else{
                    backgroundColor = backgroundColor?.withAlphaComponent(1.0)
                }
            }
        }
    }
    
    
    class CustomNormalLabel: UILabel {
        
        override func awakeFromNib() {
            super.awakeFromNib()
            font = UIFont(name: "CeraPro-Medium", size: fontSize)
            
        }
        
        @IBInspectable var fontSize: CGFloat = 17.0 {
            didSet {
                font = UIFont(name: "CeraPro-Medium", size: fontSize)
            }
        }
        
    }
    
    class CustomBoldLabel: UILabel {
        
        override func awakeFromNib() {
            super.awakeFromNib()
            font = UIFont(name: "CeraPro-Medium", size: fontSize)
        }
        
        @IBInspectable var fontSize: CGFloat = 17.0 {
            didSet {
                font = UIFont(name: "CeraPro-Medium", size: fontSize)
            }
        }
        
    }
    
    class CustomBoldFontLabel: UILabel {
        
        override func awakeFromNib() {
            super.awakeFromNib()
            font = ceraProBoldNormalFontSize
        }
        
        @IBInspectable var fontSize: CGFloat = 17.0 {
            didSet {
                font = UIFont(name: "CeraPro-Bold", size: fontSize)
            }
        }
        
    }
    
    
    class NormalButton: UIButton {
        
        override func awakeFromNib() {
            super.awakeFromNib()
            imageView?.contentMode = .scaleAspectFit
            titleLabel?.font = UIFont(name: "CeraPro-Medium", size: fontSize)
            
        }
        
        
        
        @IBInspectable var fontSize: CGFloat = 17.0 {
            didSet {
                titleLabel?.font = UIFont(name: "CeraPro-Medium", size: fontSize)
            }
        }
        
        
        @IBInspectable var bgColor: UIColor? {
            didSet {
                backgroundColor = bgColor
            }
        }
        
        override var isHighlighted: Bool {
            didSet {
                if isHighlighted{
                    backgroundColor = backgroundColor?.withAlphaComponent(0.6)
                }else{
                    backgroundColor = backgroundColor?.withAlphaComponent(1.0)
                }
            }
        }
    }
    
    
    class NormalBoldButton: UIButton {
        
        override func awakeFromNib() {
            super.awakeFromNib()
            imageView?.contentMode = .scaleAspectFit
            titleLabel?.font = UIFont(name: "CeraPro-Medium", size: fontSize)
        }
        
        @IBInspectable var fontSize: CGFloat = 17.0 {
            didSet {
                titleLabel?.font = UIFont(name: "CeraPro-Medium", size: fontSize)
            }
        }
        
        
        @IBInspectable var bgColor: UIColor? {
            didSet {
                backgroundColor = bgColor
            }
        }
        
        override var isHighlighted: Bool {
            didSet {
                if isHighlighted{
                    backgroundColor = backgroundColor?.withAlphaComponent(0.6)
                }else{
                    backgroundColor = backgroundColor?.withAlphaComponent(1.0)
                }
            }
        }
    }
    
    enum Direction {
        case Left
        case Right
    }
    
    class CustomTextField: UITextField {
        override func awakeFromNib() {
            super.awakeFromNib()
            font = UIFont(name: "CeraPro-Medium", size: 15.0)
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        }
        // add image to textfield
        func withImage(direction: Direction, image: UIImage , placeholderText : String){
            self.backgroundColor = UIColor.init(named: "TextFieldBgColor")
            self.textColor = UIColor.init(named: "BlackColor")
            self.attributedPlaceholder = NSAttributedString(string: placeholderText,
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
            
        
            let imageView = UIImageView(frame: CGRect(x: 25.0, y: 8.0, width: 24.0, height: 24.0))
            //let image = UIImage(named: image)
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            

            let view = UIView(frame: CGRect(x: 0, y: 0, width:60, height: 40))
            view.addSubview(imageView)
            view.backgroundColor = .clear
            
            if(Direction.Left == direction){
                self.leftViewMode = .always
                self.leftView = view
            }else{
                self.rightViewMode = .always
                self.rightView = view
            }
        }
        
        func withImage(direction: Direction, image: UIImage?, placeholder: String) {
            self.attributedPlaceholder = NSAttributedString(string: placeholder,
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
            let imageView = UIImageView(frame: CGRect(x: 5.0, y: 5.0, width: 25.0, height: 25.0))
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
            view.addSubview(imageView)
            view.backgroundColor = .clear
            if (Direction.Left == direction) {
                self.leftViewMode = .always
                self.leftView = view
            } else {
                self.rightViewMode = .always
                self.rightView = view
            }
        }
        
        func setFont(fontWeight: FontWeight, size: CGFloat) {
            switch fontWeight {
            case .bold:
                self.font = UIFont(name: "CeraPro-Bold", size: size)
            case .medium:
                self.font = UIFont(name: "CeraPro-Medium", size: size)
            case .light:
                self.font = UIFont(name: "CeraPro-Light", size: size)
            case .semibold:
                self.font = UIFont(name: "CeraPro-Bold", size: size)
            default:
                self.font = UIFont(name: "CeraPro-Regular", size: size)
            }
        }
        
        func withImageForMobile(direction: Direction, image: UIImage , placeholderText : String){
            self.backgroundColor = UIColor.init(named: "TextFieldBgColor")
            self.textColor = UIColor.init(named: "BlackColor")
            self.attributedPlaceholder = NSAttributedString(string: placeholderText,
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
            
        
            let imageView = UIImageView(frame: CGRect(x: 10.0, y: 8.0, width: 24.0, height: 24.0))
            //let image = UIImage(named: image)
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            

            let view = UIView(frame: CGRect(x: 0, y: 0, width:40, height: 40))
            view.addSubview(imageView)
            view.backgroundColor = .clear
            
            if(Direction.Left == direction){
                self.leftViewMode = .always
                self.leftView = view
            }else{
                self.rightViewMode = .always
                self.rightView = view
            }
        }
         open var fontSize : CGFloat = CGFloat(17) {
               didSet {
                   self.font = UIFont(name: "Muli-Regular", size: fontSize)
               }
           }
        
        func setDefaultTextFieldColor(){
            self.textColor = UIColor.init(named: "BlackColor")
        }
        
        public func addDropDownArrow() {
            self.rightViewMode = .always
            let rightView = UIView(frame: CGRect(x: self.bounds.width - 30, y: 0, width: 30, height: self.bounds.height))
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: self.bounds.height))
            imageView.image = UIImage(named: "IconDropDownArrow")
            imageView.contentMode = .scaleAspectFit
            rightView.addSubview(imageView)
            self.rightView = rightView
        }
    }
