//  SingleToneClass.swift
//  Accenture Tech
//  Created by Generic Generic on 23/05/19.
//  Copyright © 2019 Generic Generic. All rights reserved.

import UIKit

class SingleToneClass {
    
    private init() {}
    
    static let shared = SingleToneClass()
     var localTimeZoneAbbreviation: String { return TimeZone.current.abbreviation() ?? "" }
     var defaultPhoneCountry : Country?
     var currencySymbol = String()
}
