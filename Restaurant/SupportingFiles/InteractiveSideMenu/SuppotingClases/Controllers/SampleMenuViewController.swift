//
// SampleMenuViewController.swift
//
// Copyright 2017 Handsome LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit
//import InteractiveSideMenu

/**
 Menu controller is responsible for creating its content and showing/hiding menu using 'menuContainerViewController' property.
 */
class SampleMenuViewController: MenuViewController, Storyboardable {

    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var avatarImageView: UIImageView!
    @IBOutlet fileprivate weak var avatarImageViewCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var label_UserName: CustomBoldLabel!
    @IBOutlet weak var viewImageContainer: UIView!
    
    private var gradientLayer = CAGradientLayer()
    //private var arrayMenuItemNames : [String] = ["Favourite Restaurants", "Notification Settings", "About Us", "Help", "Logout"]
    //private var arrayMenuItemNames : [String] = ["Manage Profile", "Manage Opening Time", "Manage Payments", "Manage Notification", "About Us", "Help", "Logout"]
//    private var arrayMenuItemNames : [String] = ["Manage Profile", "Manage Notification", "About Us", "FAQ", "Logout"]
    private var arrayMenuItemNames : [String] = ["Manage Profile", "About Us", "FAQ", "Logout"]
    //var arrSubMenus = ["My Payment Details", "View Transaction History", "View Available Balance"]
    //var arrSubHelps = ["FAQ", "Live Chat", "Social Media"]
    var arrAboutUs = ["Who we are", "Rate App", "Privacy Policy"]

    var isSubMenuShowing = false
    var isSubHelpShowing = false
    var isAboutUsShowing = false

    private var gradientApplied: Bool = false
    private var isAboutUsSelected : Bool = false

    override var prefersStatusBarHidden: Bool {
        return false
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.label_UserName.text = "\(UserDefaultValues.userName)"

        // Select the initial row
//        tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: UITableView.ScrollPosition.none)
        viewImageContainer.layer.shadowOpacity = 0.4
        viewImageContainer.layer.shadowOffset = CGSize(width: 0, height: 6)
        viewImageContainer.layer.shadowColor = UIColor.black.cgColor
        viewImageContainer.layer.shadowRadius = 3.0
        viewImageContainer.layer.cornerRadius = viewImageContainer.frame.size.width/2
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width/2
        avatarImageView.maskToBound = true
        avatarImageView.layer.borderWidth = 5.0
        avatarImageView.layer.borderColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        self.avatarImageView.sd_setImage(with: URL(string: UserDefaultValues.profileImage))

    
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
            if UserDefaultValues.profileImage != ""{
                let url = URL(string: UserDefaultValues.profileImage)
                    let data = try? Data(contentsOf: url!)
                    if let imageData = data {
                        self.avatarImageView.image = UIImage(data: imageData)
                    }

            }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        avatarImageViewCenterXConstraint.constant = -50.0// -((menuContainerViewController?.transitionOptions.visibleContentWidth ?? 0.0)/10) * 9

//        if gradientLayer.superlayer != nil {
//            gradientLayer.removeFromSuperlayer()
//        }
//        let topColor = UIColor(red: 200.0/255.0, green: 12.0/255.0, blue: 54.0/255.0, alpha: 1.0)
//        let bottomColor = UIColor(red: 57.0/255.0, green: 33.0/255.0, blue: 61.0/255.0, alpha: 1.0)
//        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
//        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
//        gradientLayer.frame = view.bounds
//        view.layer.insertSublayer(gradientLayer, at: 0)
        self.view.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
    }
    
    @IBAction func btnProfileDidClick(_ sender: Any) {
        
        guard let menuContainerViewController = self.menuContainerViewController else {
            return
        }
        menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[5])
        menuContainerViewController.hideSideMenu()
    }
    

    deinit{
        print()
    }
}

extension SampleMenuViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrayMenuItemNames.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let menuTitle = self.arrayMenuItemNames[section]
        if menuTitle == "Manage Payments"{
            if isSubMenuShowing{
                return 4
            }else{
                return 1
            }
        }else if menuTitle == "Help"{
            if isSubHelpShowing{
                return 4
            }else{
                return 1
            }
        }else if menuTitle == "About Us"{
            if isAboutUsShowing{
                return 4
            }else{
                return 1
            }
        }else{
            return 1
        }
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let menuTitle = self.arrayMenuItemNames[indexPath.section]
        if menuTitle == "Manage Payments"{
            if isSubMenuShowing{
                switch indexPath.row {
                case 0:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "SampleTableDownCell", for: indexPath) as? SampleTableCell else {
                        preconditionFailure("Unregistered table view cell")
                    }
                    cell.titleLabel.text = self.arrayMenuItemNames[indexPath.section]
                    //                    cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
                    cell.titleLabel.textColor = UIColor(named: "TextColorDark") ?? UIColor.black
                    return cell
                    
                default:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "SampleSubTableCell") as? SampleTableCell else{
                        preconditionFailure("Unregistered table view cell")
                        
                    }
                    //cell.titleLabel.text = self.arrSubMenus[indexPath.row-1]
                    cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
                    cell.titleLabel.textColor = UIColor(named: "TextColorDark") ?? UIColor.black
                    return cell
                }
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SampleTableCell.self), for: indexPath) as? SampleTableCell else {
                    preconditionFailure("Unregistered table view cell")
                }
                cell.titleLabel.text = self.arrayMenuItemNames[indexPath.section]
                cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
                cell.imageLeftArrow.isHidden = false
                cell.titleLabel.textColor = UIColor(named: "TextColorDark") ?? UIColor.black
                return cell
                
            }
        }else if menuTitle == "Help"{
            if isSubHelpShowing{
                switch indexPath.row {
                case 0:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "SampleTableDownCell", for: indexPath) as? SampleTableCell else {
                        preconditionFailure("Unregistered table view cell")
                    }
                    cell.titleLabel.text = self.arrayMenuItemNames[indexPath.section]
                    //                    cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
                    cell.titleLabel.textColor = UIColor(named: "TextColorDark") ?? UIColor.black
                    return cell
                    
                default:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "SampleSubTableCell") as? SampleTableCell else{
                        preconditionFailure("Unregistered table view cell")
                        
                    }
                    //cell.titleLabel.text = self.arrSubHelps[indexPath.row-1]
                    cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
                    cell.titleLabel.textColor = UIColor(named: "TextColorDark") ?? UIColor.black
                    return cell
                }
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SampleTableCell.self), for: indexPath) as? SampleTableCell else {
                    preconditionFailure("Unregistered table view cell")
                }
                cell.titleLabel.text = self.arrayMenuItemNames[indexPath.section]
                cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
                cell.imageLeftArrow.isHidden = false
                cell.titleLabel.textColor = UIColor(named: "TextColorDark") ?? UIColor.black
                return cell
                
            }
        }
        else if menuTitle == "About Us"{
            if isAboutUsShowing{
                switch indexPath.row {
                case 0:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "SampleTableDownCell", for: indexPath) as? SampleTableCell else {
                        preconditionFailure("Unregistered table view cell")
                    }
                    cell.titleLabel.text = self.arrayMenuItemNames[indexPath.section]
                    //                    cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
                    cell.titleLabel.textColor = UIColor(named: "TextColorDark") ?? UIColor.black
                    return cell
                    
                default:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "SampleSubTableCell") as? SampleTableCell else{
                        preconditionFailure("Unregistered table view cell")
                        
                    }
                    cell.titleLabel.text = self.arrAboutUs[indexPath.row-1]
                    cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
                    cell.titleLabel.textColor = UIColor(named: "TextColorDark") ?? UIColor.black
                    return cell
                }
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SampleTableCell.self), for: indexPath) as? SampleTableCell else {
                    preconditionFailure("Unregistered table view cell")
                }
                cell.titleLabel.text = self.arrayMenuItemNames[indexPath.section]
                cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
                cell.imageLeftArrow.isHidden = false
                cell.titleLabel.textColor = UIColor(named: "TextColorDark") ?? UIColor.black
                return cell
                
            }
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SampleTableCell.self), for: indexPath) as? SampleTableCell else {
                preconditionFailure("Unregistered table view cell")
            }
            cell.titleLabel.text = self.arrayMenuItemNames[indexPath.section]
            //menuContainerViewController?.contentViewControllers[indexPath.row].title ?? "A Controller"
            if indexPath.section == self.arrayMenuItemNames.count - 1{
                if UserDefaultValues.CustomerLoginType != "GUEST"{
                    cell.imageLeftArrow.image = UIImage(named: "next_red_Arrow")
                    cell.titleLabel.textColor = UIColor.red
                }else{
                    cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
                    cell.titleLabel.textColor = UIColor(named: "TextColorDark") ?? UIColor.black
                }
            }else{
                cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
                cell.titleLabel.textColor = UIColor(named: "TextColorDark") ?? UIColor.black
            }
            cell.imageLeftArrow.isHidden = true
            return cell
            
        }
        //        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SampleTableCell.self), for: indexPath) as? SampleTableCell else {
        //            preconditionFailure("Unregistered table view cell")
        //        }
        //        cell.titleLabel.text = self.arrayMenuItemNames[indexPath.row]
        //        //menuContainerViewController?.contentViewControllers[indexPath.row].title ?? "A Controller"
        //        if indexPath.row == self.arrayMenuItemNames.count - 1{
        //            cell.imageLeftArrow.image = UIImage(named: "next_red_Arrow")
        //            cell.titleLabel.textColor = UIColor.red
        //        }else{
        //            cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
        //            cell.titleLabel.textColor = UIColor.black
        //        }
        //        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let menuTitle = self.arrayMenuItemNames[indexPath.section]
        switch menuTitle {
        case "Manage Profile":
            self.isSubMenuShowing = false
            self.isSubHelpShowing = false
            self.isAboutUsShowing = false
            self.tableView.reloadData()
            
            guard let menuContainerViewController = self.menuContainerViewController else {
                return
            }
            menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[5])
            menuContainerViewController.hideSideMenu()
            
        case "Manage Opening Time":
            self.isSubMenuShowing = false
            self.isSubHelpShowing = false
            self.isAboutUsShowing = false
            self.tableView.reloadData()
            
            guard let menuContainerViewController = self.menuContainerViewController else {
                return
            }
            menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[8])
            menuContainerViewController.hideSideMenu()
            
        case "Manage Payments":
            self.isSubHelpShowing = false
            self.isAboutUsShowing = false
            self.tableView.reloadData()
            
            if !isSubMenuShowing{
                self.isSubMenuShowing = true
                self.tableView.reloadData()
                
            }else{
                
                switch indexPath.row {
                case 0:
                    self.isSubMenuShowing = false
                    self.tableView.reloadData()
                case 1:
                    print("My Payment Details")
                    guard let menuContainerViewController = self.menuContainerViewController else {
                        return
                    }
                    menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[9])
                    menuContainerViewController.hideSideMenu()
                case 2:
                    print("View Transaction History")
                case 3:
                    print("View Available Balance")
                default:
                    print("Default")
                }
                
            }
        case "Manage Notification":
            self.isSubMenuShowing = false
            self.isSubHelpShowing = false
            self.isAboutUsShowing = false

            self.tableView.reloadData()
            
            guard let menuContainerViewController = self.menuContainerViewController else {
                return
            }
            menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[7])
            menuContainerViewController.hideSideMenu()
        case "About Us":
            self.isSubHelpShowing = false
            self.isSubMenuShowing = false

            self.tableView.reloadData()
            
            if !isAboutUsShowing{
                self.isAboutUsShowing = true
                self.tableView.reloadData()
                
            }else{
                
                switch indexPath.row {
                case 0:
                    self.isAboutUsShowing = false
                    self.tableView.reloadData()
                case 1:
                    print("Who we are")
                    /*
                    guard let menuContainerViewController = self.menuContainerViewController else {
                        return
                    }
                    menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[12])
                    menuContainerViewController.hideSideMenu()
                    */
                case 2:
                    print("Rate app")
                case 3:
                    print("privacy policy")
                    /*
                    guard let menuContainerViewController = self.menuContainerViewController else {
                        return
                    }
                    menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[10])
                    menuContainerViewController.hideSideMenu()
                    */
                default:
                    print("Default")
                }
                
            }
        case "Help":
            print("Help")
            self.isSubMenuShowing = false
            self.isAboutUsShowing = false
            self.tableView.reloadData()
            
            if !isSubHelpShowing{
                self.isSubHelpShowing = true
                self.tableView.reloadData()
                
            }else{
                
                switch indexPath.row {
                case 0:
                    self.isSubHelpShowing = false
                    self.tableView.reloadData()
                case 1:
                    print("FAQ")
                    /*
                    guard let menuContainerViewController = self.menuContainerViewController else {
                        return
                    }
                    menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[11])
                    menuContainerViewController.hideSideMenu()
                    */
                case 2:
                    print("Live Chat")
                case 3:
                    print("Social Media")
                default:
                    print("Default")
                }
                
            }
            
            
        case "Logout":
            resetUserDefaults()
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "landingScreenbeforeLogin") as! LandingScreenbeforeLogin
            UIApplication.shared.windows[0].rootViewController = viewController
            UIApplication.shared.windows.first?.rootViewController = viewController
            UIApplication.shared.windows.first?.makeKeyAndVisible()
            
        default:
            print("")
        }



    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = UIColor.white
        return v
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5
    }
}

/*

 extension SampleMenuViewController: UITableViewDelegate, UITableViewDataSource {

     func numberOfSections(in tableView: UITableView) -> Int {
         return 1
     }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return menuContainerViewController?.contentViewControllers.count ?? 0
     }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         switch indexPath.row {
         case 2:
             return isAboutUsSelected ? 154.0 : 50.0
         default:
             return 50.0
         }
     }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         switch indexPath.row {
         case 2:
             let cell = tableView.dequeueReusableCell(withIdentifier: "AboutUsCell") as! IncreaseableHeightCell
             return cell
         default:
             guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SampleTableCell.self), for: indexPath) as? SampleTableCell else {
                 preconditionFailure("Unregistered table view cell")
             }
             cell.titleLabel.text = self.arrayMenuItemNames[indexPath.row]
             //menuContainerViewController?.contentViewControllers[indexPath.row].title ?? "A Controller"
             if indexPath.row == self.arrayMenuItemNames.count - 1{
                 cell.imageLeftArrow.image = UIImage(named: "next_red_Arrow")
                 cell.titleLabel.textColor = UIColor.red
             }else{
                 cell.imageLeftArrow.image = UIImage(named: "next_Arrow")
                 cell.titleLabel.textColor = UIColor.black
             }
             return cell
             
         }
     }

     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         switch indexPath.row {
         case 2:
             self.isAboutUsSelected = true
         default:
             self.isAboutUsSelected = false
             guard let menuContainerViewController = self.menuContainerViewController else {
                 return
             }
             menuContainerViewController.selectContentViewController(menuContainerViewController.contentViewControllers[indexPath.row])
             menuContainerViewController.hideSideMenu()

         }
         

     }

     func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
         let v = UIView()
         v.backgroundColor = UIColor.white
         return v
     }

     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
         return 0.5
     }
 }
 
 */
