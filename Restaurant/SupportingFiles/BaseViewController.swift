//
//  BaseViewController.swift
//  Restaurant
//
//  Created by Bit Mini on 07/02/20.
//  Copyright © 2020 Bit Mini. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarTitleFont()
        // Do any additional setup after loading the view.
    }
    private func setNavigationBarTitleFont(){
        let attributes = [NSAttributedString.Key.font: UIFont(name: "CeraPro-Medium", size: 25)!,
                          NSAttributedString.Key.foregroundColor:UIColor.white]
        UINavigationBar.appearance().titleTextAttributes = attributes
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
