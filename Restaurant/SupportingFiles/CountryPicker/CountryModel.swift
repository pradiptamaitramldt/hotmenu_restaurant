//
//  CountryModel.swift
//  Global
//
//  Created by Mukesh Singh on 14/01/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation

class Country: NSObject, Codable {
    public var code: String?
    public var name: String?
    public var dialCode: String?
    public var flag: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case name = "name"
        case dialCode = "dial_code"
        
        
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(String.self, forKey: .code) ?? ""
        name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        dialCode = try values.decodeIfPresent(String.self, forKey: .dialCode) ?? ""
        flag = code?.lowercased()
    }
    
}
