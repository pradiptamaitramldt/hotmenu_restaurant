//
//  GlobalMethods.swift
//  PurchasePal
//
//  Created by Bit Mini on 26/11/19.
//  Copyright © 2019 Bit Mini. All rights reserved.
//

import Foundation
import CoreGraphics
import LocalAuthentication
import SystemConfiguration

class UserDefaultValues {
    static var deviceToken : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "deviceToken")
        }
        get{
            return UserDefaults.standard.value(forKey: "deviceToken") as! String
        }
    }

    static var pushMode : String{
          set{
              UserDefaults.standard.setValue(newValue, forKey: "pushMode")
          }
          get{
              return UserDefaults.standard.value(forKey: "pushMode") as! String
          }
      }

    static var userID : String?{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "userID")
        }
        get{
            return UserDefaults.standard.value(forKey: "userID") as? String
        }
    }
    
    static var loginID : String?{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "loginID")
        }
        get{
            return UserDefaults.standard.value(forKey: "loginID") as? String
        }
    }
    
    static var authToken : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "token")
        }
        get{
            return UserDefaults.standard.value(forKey: "token") as! String
        }
    }
    
    static var userName : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "userName")
        }
        get{
            return UserDefaults.standard.value(forKey: "userName") as! String
        }
    }
    
    static var FirstName : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "FName")
        }
        get{
            return UserDefaults.standard.value(forKey: "FName") as! String
        }
    }
    
    static var LastName : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "LName")
        }
        get{
            return UserDefaults.standard.value(forKey: "LName") as! String
        }
    }
    
    
    static var RestaurantName : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "RestaurantName")
        }
        get{
            return UserDefaults.standard.value(forKey: "RestaurantName") as! String
        }
    }

    static var profileImage : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "profileImage")
        }
        get{
            return String(describing: UserDefaults.standard.value(forKey: "profileImage") ?? "")
        }
    }


    
    static var email : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "email")
        }
        get{
            return UserDefaults.standard.value(forKey: "email") as! String
        }
    }
    
    static var phone : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "phone")
        }
        get{
            return UserDefaults.standard.value(forKey: "phone") as! String
        }
    }
    static var countryCode : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "countryCode")
        }
        get{
            return UserDefaults.standard.value(forKey: "countryCode") as! String
        }
    }

    
    static var OTP : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "otp")
        }
        get{
            return UserDefaults.standard.value(forKey: "otp") as! String
        }
    }
    
    static var LoginType : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "loginType")
        }
        get{
            return UserDefaults.standard.value(forKey: "loginType") as! String
        }
    }
    
    static var CustomerLoginType : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "customerLoginType")
        }
        get{
            return UserDefaults.standard.value(forKey: "customerLoginType") as! String
        }
    }
    
    static var customerSelectedOrderItems : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "customerLoginType")
        }
        get{
            return UserDefaults.standard.value(forKey: "customerLoginType") as! String
        }
    }
    
    static var vendorID : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "vendorID")
        }
        get{
            return UserDefaults.standard.value(forKey: "vendorID") as! String
        }
    }
    
    
//    static var vendorName : String{
//        set{
//            UserDefaults.standard.setValue(newValue, forKey: "vendorName")
//        }
//        get{
//            return UserDefaults.standard.value(forKey: "vendorName") as! String
//        }
//    }

    
    static var selectedType : String{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "selectedType")
        }
        get{
            return String(describing: UserDefaults.standard.value(forKey: "selectedType") ?? OrderStatus.all.rawValue)
        }
    }

    static var badgeCount : Int{
        set{
            UserDefaults.standard.setValue(newValue, forKey: "badgeCount")
        }
        get{
            return UserDefaults.standard.value(forKey: "badgeCount") as? Int ?? 0
        }
    }


}


func resetUserDefaults() {
    let deviceToken = UserDefaultValues.deviceToken
    let pushMode = UserDefaultValues.pushMode
    let defaults = UserDefaults.standard
    let dictionary = defaults.dictionaryRepresentation()
    dictionary.keys.forEach { key in
        defaults.removeObject(forKey: key)
    }
    UserDefaultValues.deviceToken = deviceToken
    UserDefaultValues.pushMode = pushMode
}

var designedScreenheight : CGFloat = 896
let currencySign = "₹"
let screenWidth = UIApplication.shared.windows[0].screen.bounds.width
var GoogleClientID = "486927194464-6o592u3rd6ba72rlha7hf77nvnocnhfv.apps.googleusercontent.com"
var GOOGLE_API_KEY = "AIzaSyCsKNjZeoA-mQg1Sgmwvmr8xrO1F1EUgV0"
let GooglePlacesAPIkey = "AIzaSyAV71oo1DUmdgPCcOJxHmoJsj_u9hchQtY"



func returnInCurrencyFormat(amount: Float) -> String{
    let finalAmount = String(format: "%.2f", amount)
    return "\(currencySign) \(finalAmount)"
}


func returnInCurrencyFormat(amount: String) -> String{
    let finalAmount = String(format: "%.2f", amount)
    return "\(currencySign) \(finalAmount)"
}


func getTableViewRowHeightRespectToScreenHeight(givenheight : CGFloat, currentScrenHeight : CGFloat) -> CGFloat {
    return (givenheight / designedScreenheight) * currentScrenHeight
    
}


struct Utility {
     let prefs = UserDefaults.standard
    public static func log(_ items: Any?) {
        print(items ?? "Nil value")
    }
//    static func isNetworkReachable() -> Bool {
//        return (Reachability().connection != Reachability.Connection.none)
//    }
    static func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    //Validate Email
    public static func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    //validate Mobile no
    
    public static func isValidMobileNumber(_ mobile: String) -> Bool {
        let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        let isValidPhone = phoneTest.evaluate(with: mobile)
        return isValidPhone
    }
    
    //validate Password
    public static func isValidPassword(_ password : String) -> Bool {
        do {//^(.{0,7}|[^0-9]*|[^A-Z]*|[a-zA-Z0-9]*)$//
            
            //"^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$"
            //^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z].*[a-z]).{8}$

            let regex = try NSRegularExpression(pattern: "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,16}$", options: .caseInsensitive)
            if(regex.firstMatch(in: password, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, password.count)) != nil){

                if(password.count>=8 && password.count<=20){
                    return true
                }else{
                    return false
                }
            }else{
                return false
            }
        } catch {
            return false
        }
    }
    public static func isValidNewPassword(_ password : String) -> Bool {
        do {//^(.{0,7}|[^0-9]*|[^A-Z]*|[a-zA-Z0-9]*)$//
            
            //"^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$"
            //^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z].*[a-z]).{8}$

            let regex = try NSRegularExpression(pattern: "[0-9]", options: .caseInsensitive)
            if(regex.firstMatch(in: password, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, password.count)) != nil){

                if(password.count == 8){
                    return true
                }else{
                    return false
                }
            }else{
                return false
            }
        } catch {
            return false
        }
    }
    
    public static func showAlert(message : String){
        
    }
    
    
    
    
    
}

