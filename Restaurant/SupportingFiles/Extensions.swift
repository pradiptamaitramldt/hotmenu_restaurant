//
//  Extensions.swift
//  ImCelebrity
//
//  Created by Amstech on 8/1/17.
//  Copyright © 2017 Brainium InfoTech. All rights reserved.
//

import Foundation
import UIKit
import GameplayKit
import AMShimmer

enum ViewSide {
    case Left, Right, Top, Bottom
}
enum VerticalLocation: String {
    case bottom
    case top
}

extension UIView {
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 2)
        layer.shadowRadius = 2
        
    }
    
    func addShadow(location: VerticalLocation, color: UIColor = .black, opacity: Float = 0.5, radius: CGFloat = 5.0) {
        switch location {
        case .bottom:
            addShadow(offset: CGSize(width: 0, height:10), color: color, opacity: opacity, radius: radius)
        case .top:
            addShadow(offset: CGSize(width: 0, height: -10), color: color, opacity: opacity, radius: radius)
        }
    }
    
    func addShadow(offset: CGSize, color: UIColor = .black, opacity: Float = 0.5, radius: CGFloat = 5.0) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
    }
    
    func addBorder(toSide side: ViewSide, withColor color: CGColor, andThickness thickness: CGFloat) {
        
        let border = CALayer()
        border.backgroundColor = color
        
        switch side {
        case .Left: border.frame = CGRect(x: frame.minX, y: frame.minY, width: thickness, height: frame.height); break
        case .Right: border.frame = CGRect(x: frame.maxX, y: frame.minY, width: thickness, height: frame.height); break
        case .Top: border.frame = CGRect(x: frame.minX, y: frame.minY, width: frame.width, height: thickness); break
        case .Bottom: border.frame = CGRect(x: frame.minX, y: frame.maxY, width: frame.width, height: thickness); break
        }
        
        self.layer.addSublayer(border)
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadow_Color: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    @IBInspectable var maskToBound: Bool {
        get {
            return layer.masksToBounds
        }
        set {
            layer.masksToBounds = newValue
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    class func initFromNib<T: UIView>() -> T {
            return Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)?[0] as! T
        }
    
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 10.0),
                   shadowOpacity: Float = 0.8,
                   shadowRadius: CGFloat = 5.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    func setRoundedCornered(height : CGFloat){
        self.layer.cornerRadius = height/2
    }


}

extension UITableView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Handlee-Regular", size: 20.0)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

extension UITextField {
    
    @IBInspectable var placeholderColor: UIColor? {
        get {
            return nil
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : newValue ?? .lightGray])
        }
    }
    
}

extension UIImage {
    
    func resizeImage( targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / self.size.width
        let heightRatio = targetSize.height / self.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return self.jpegData(compressionQuality: quality.rawValue)
    }
    
    
    
    
    func imageWithColor(color: UIColor) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    
    func alpha(_ value:CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    
    
    //To change Color
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
    public class func gifImageWithData(data: NSData) -> UIImage? {
        guard let source = CGImageSourceCreateWithData(data, nil) else {
            print("image doesn't exist")
            return nil
        }
        
        return UIImage.animatedImageWithSource(source: source)
    }
    
    public class func gifImageWithURL(gifUrl:String) -> UIImage? {
        guard let bundleURL = NSURL(string: gifUrl)
            else {
                print("image named \"\(gifUrl)\" doesn't exist")
                return nil
        }
        guard let imageData = NSData(contentsOf: bundleURL as URL) else {
            print("image named \"\(gifUrl)\" into NSData")
            return nil
        }
        
        return gifImageWithData(data: imageData)
    }
    
    public class func gifImageWithName(name: String) -> UIImage? {
        guard let bundleURL = Bundle.main
            .url(forResource: name, withExtension: "gif") else {
                print("SwiftGif: This image named \"\(name)\" does not exist")
                return nil
        }
        
        guard let imageData = NSData(contentsOf: bundleURL) else {
            print("SwiftGif: Cannot turn image named \"\(name)\" into NSData")
            return nil
        }
        
        return gifImageWithData(data: imageData)
    }
    
    class func delayForImageAtIndex(index: Int, source: CGImageSource!) -> Double {
        var delay = 0.1
        
        let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
        let gifProperties: CFDictionary = unsafeBitCast(CFDictionaryGetValue(cfProperties, Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque()), to: CFDictionary.self)
        
        var delayObject: AnyObject = unsafeBitCast(CFDictionaryGetValue(gifProperties, Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()), to: AnyObject.self)
        
        if delayObject.doubleValue == 0 {
            delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties, Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
        }
        
        delay = delayObject as! Double
        
        if delay < 0.1 {
            delay = 0.1
        }
        
        return delay
    }
    
    class func gcdForPair(a: Int?, _ b: Int?) -> Int {
        var a = a
        var b = b
        if b == nil || a == nil {
            if b != nil {
                return b!
            } else if a != nil {
                return a!
            } else {
                return 0
            }
        }
        
        if a! < b! {
            let c = a!
            a = b!
            b = c
        }
        
        var rest: Int
        while true {
            rest = a! % b!
            
            if rest == 0 {
                return b!
            } else {
                a = b!
                b = rest
            }
        }
    }
    
    class func gcdForArray(array: Array<Int>) -> Int {
        if array.isEmpty {
            return 1
        }
        
        var gcd = array[0]
        
        for val in array {
            gcd = UIImage.gcdForPair(a: val, gcd)
        }
        
        return gcd
    }
    
    class func animatedImageWithSource(source: CGImageSource) -> UIImage? {
        let count = CGImageSourceGetCount(source)
        var images = [CGImage]()
        var delays = [Int]()
        
        for i in 0..<count {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(image)
            }
            
            let delaySeconds = UIImage.delayForImageAtIndex(index: Int(i), source: source)
            delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
        }
        
        let duration: Int = {
            var sum = 0
            
            for val: Int in delays {
                sum += val
            }
            
            return sum
        }()
        
        let gcd = gcdForArray(array: delays)
        var frames = [UIImage]()
        
        var frame: UIImage
        var frameCount: Int
        for i in 0..<count {
            frame = UIImage(cgImage: images[Int(i)])
            frameCount = Int(delays[Int(i)] / gcd)
            
            for _ in 0..<frameCount {
                frames.append(frame)
            }
        }
        
        let animation = UIImage.animatedImage(with: frames, duration: Double(duration) / 1000.0)
        
        return animation
    }
    
    
}
extension Notification.Name {
    static let initial = Notification.Name("Initial")
    static let gotPush = Notification.Name("gotPush")
}


extension String {
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }


    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
    
    func findMentionText() -> [String] {
        var arr_hasStrings:[String] = []
        let regex = try? NSRegularExpression(pattern: "(#[a-zA-Z0-9_\\p{Arabic}\\p{N}]*)", options: [])
        if let matches = regex?.matches(in: self, options:[], range:NSMakeRange(0, self.count)) {
            for match in matches {
                let str = NSString(string: self).substring(with: NSRange(location:match.range.location, length: match.range.length ))
                arr_hasStrings.append(str)
            }
        }
        return arr_hasStrings
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func startIndexOfSubString(substring: String) -> Int? {
            if let range = self.lowercased().range(of: substring.lowercased()) {
                let startPos = self.distance(from: self.startIndex, to: range.lowerBound)
                return startPos
            } else {
                return nil
            }
        }
        
        func indexDistance(of character: Character) -> Int? {
            guard let index = firstIndex(of: character) else { return nil }
            return distance(from: startIndex, to: index)
        }
        
        func capitalizingFirstLetter() -> String {
            return prefix(1).uppercased() + dropFirst()
        }
        
        mutating func capitalizeFirstLetter() {
            self = self.capitalizingFirstLetter()
        }
        
    //    func sha1() -> String {
    //        let data = Data(self.utf8)
    //        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
    //        data.withUnsafeBytes {
    //            _ = CC_SHA1($0.baseAddress, CC_LONG(data.count), &digest)
    //        }
    //        let hexBytes = digest.map { String(format: "%02hhx", $0) }
    //        return hexBytes.joined()
    //    }
        
        func changeDateTimeStringFormat(inputTimeZone: TimeZone, outputTimeZone: TimeZone, inputFormat: String, outputFormat: String ) -> String {
            let inputDateFormatter = DateFormatter()
            inputDateFormatter.timeZone = inputTimeZone
            inputDateFormatter.dateFormat = inputFormat
            let inputDateTime = inputDateFormatter.date(from: self)
            if inputDateTime == nil {
                return ""
            }
            let outputDateFormatter = DateFormatter()
            outputDateFormatter.timeZone = outputTimeZone
            outputDateFormatter.dateFormat = outputFormat
            let outputDateTimeString = outputDateFormatter.string(from: inputDateTime!)
            return outputDateTimeString
        }
        
        //-----the following methods are for validation purpose-----
        func isValidEmail() -> Bool {
            let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
        }
        
        func isValidPassword() -> Bool {
            //Should have at least eight characters, including one uppercase character, one lowercase character, one number and one character that isn’t a letter or digit.
            // let passwordRegex = "^(?=\\P{Ll}*\\p{Ll})(?=\\P{Lu}*\\p{Lu})(?=\\P{N}*\\p{N})(?=[\\p{L}\\p{N}]*[^\\p{L}\\p{N}])[\\s\\S]{8,}$"
            // return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
            
            //Should have six to eight characters, including one uppercase character, one lowercase character, one number and one character that isn’t a letter or digit.
            //"^(?=\\P{Ll}*\\p{Ll})(?=\\P{Lu}*\\p{Lu})(?=\\P{N}*\\p{N})(?=[\\p{L}\\p{N}]*[^\\p{L}\\p{N}])[\\s\\S]{6,8}$"
            //let passwordRegex = "^(?=\\P{Ll}*\\p{Ll})(?=\\P{Lu}*\\p{Lu})(?=[\\p{L}\\p{N}]*[^\\p{L}\\p{N}])[\\s\\S]{6,8}$"
            //let passwordRegex = "^(?=\\P{Ll}*\\p{Ll})(?=\\P{Lu}*\\p{Lu})(?=[\\p{L}\\p{N}]*[^\\p{L}\\p{N}])[\\s\\S]{8,}$"
            //return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
            //Password should be minimum 6 characters long
            
            let passwordRegex = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$" //"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$"//"^[a-zA-Z0-9\\s\\S]{8,}$"
            return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
        }
        
        func isValidName() -> Bool {
            if self.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                //string blank
                return false
            }
            if self.rangeOfCharacter(from: .whitespaces) != nil {
                //string contains white spaces
                return false
            }
            let charset = CharacterSet(charactersIn: "1234567890~`!@#$%^&*()_=+[{}]|:;<,>?/")
            if self.rangeOfCharacter(from: charset) != nil {
                return false
            }
            return true
        }
        
        func isValidPhoneNumber() -> Bool {
            let phoneNumberRegex = "^[0-9]{6,12}$"
            //let phoneNumberRegex = "^((\\+)|(00))[0-9]{6,14}$"
            return NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex).evaluate(with: self)
        }
        
        func isValidCardNumber() -> Bool {
            let cardNumberRegex = "^[0-9]{16}$"
            return NSPredicate(format: "SELF MATCHES %@", cardNumberRegex).evaluate(with: self)
        }
        
        func isValidCardVerificationCode() -> Bool {
            let cardVerificationCodeRegex = "^[0-9]{3}$"
            return NSPredicate(format: "SELF MATCHES %@", cardVerificationCodeRegex).evaluate(with: self)
        }
        
        func isValidOTP() -> Bool {
            let otpRegex = "^[0-9]{4,}$"//4 characters OTP with only digits
            return NSPredicate(format: "SELF MATCHES %@", otpRegex).evaluate(with: self)
        }
        
        func toFloat() -> Float {
            let floatNumber = Float(self)
            return floatNumber ?? 0.0
        }
        
        func toInt() -> Int {
            let intNumber = Int(self)
            return intNumber ?? 0
        }
        
        func isValidWalletAmount() -> Bool {
            let num = Float(self)
            if num != nil {
                if num! >= 1 {
                    return true
                } else {
                    return false
                }
            }
            else {
                return false
            }
        }
        
        func isValidInteger() -> Bool {
            let num = Int(self)
            if num != nil {
                if num! >= 1 {
                    return true
                } else {
                    return false
                }
            }
            else {
                return false
            }
        }
        
        func isValidString() -> Bool {
            return self.trimmingCharacters(in: .whitespacesAndNewlines) != ""
        }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


extension CAShapeLayer {
    func drawCircleAtLocation(location: CGPoint, withRadius radius: CGFloat, andColor color: UIColor, filled: Bool) {
        fillColor = filled ? color.cgColor : UIColor.white.cgColor
        strokeColor = color.cgColor
        let origin = CGPoint(x: location.x - radius, y: location.y - radius)
        path = UIBezierPath(ovalIn: CGRect(origin: origin, size: CGSize(width: radius * 2, height: radius * 2))).cgPath
    }
}

private var handle: UInt8 = 0

extension UIBarButtonItem {
    private var badgeLayer: CAShapeLayer? {
        if let b: AnyObject = objc_getAssociatedObject(self, &handle) as AnyObject? {
            return b as? CAShapeLayer
        } else {
            return nil
        }
    }

    func addBadge(number: Int, withOffset offset: CGPoint = CGPoint.zero, andColor color: UIColor = UIColor.red, andFilled filled: Bool = true) {
        guard let view = self.value(forKey: "view") as? UIView else { return }

        badgeLayer?.removeFromSuperlayer()

        // Initialize Badge
        let badge = CAShapeLayer()
        let radius = CGFloat(7)
        let location = CGPoint(x: view.frame.width - (radius + offset.x), y: (radius + offset.y))
        badge.drawCircleAtLocation(location: location, withRadius: radius, andColor: color, filled: filled)
        view.layer.addSublayer(badge)

        // Initialiaze Badge's label
        let label = CATextLayer()
        label.string = "\(number)"
        label.alignmentMode = CATextLayerAlignmentMode.center
        label.fontSize = 11
        label.frame = CGRect(origin: CGPoint(x: location.x - 4, y: offset.y), size: CGSize(width: 8, height: 16))
        label.foregroundColor = filled ? UIColor.white.cgColor : color.cgColor
        label.backgroundColor = UIColor.clear.cgColor
        label.contentsScale = UIScreen.main.scale
        badge.addSublayer(label)

        // Save Badge as UIBarButtonItem property
        objc_setAssociatedObject(self, &handle, badge, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }

    func updateBadge(number: Int) {
        if let text = badgeLayer?.sublayers?.filter({ $0 is CATextLayer }).first as? CATextLayer {
            text.string = "\(number)"
        }
    }

    func removeBadge() {
        badgeLayer?.removeFromSuperlayer()
    }
}

extension Date {
    
    func setMaximumAge(age: Int) -> Date {
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!;
        let currentDate: Date = Date()
        let components: NSDateComponents = NSDateComponents()
        components.year = -age
        let maxAge: Date = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))! as Date
        return maxAge
    }
    
    func setMinimumAge(age: Int) -> Date {
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!;
        let currentDate: Date = Date()
        let components: NSDateComponents = NSDateComponents()
        components.year = -age
        let minAge: Date = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))! as Date
        return minAge
    }
    
        func timeAgoDisplay() -> String {
            
            let calendar = Calendar.current
            let minuteAgo = calendar.date(byAdding: .minute, value: -1, to: Date())!
            let hourAgo = calendar.date(byAdding: .hour, value: -1, to: Date())!
            let dayAgo = calendar.date(byAdding: .day, value: -1, to: Date())!
            let weekAgo = calendar.date(byAdding: .day, value: -7, to: Date())!
            
            if minuteAgo < self {
                let diff = Calendar.current.dateComponents([.second], from: self, to: Date()).second ?? 0
                return "\(diff) sec ago"
            } else if hourAgo < self {
                let diff = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute ?? 0
                return "\(diff) min ago"
            } else if dayAgo < self {
                let diff = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour ?? 0
                return "\(diff) hrs ago"
            } else if weekAgo < self {
                let diff = Calendar.current.dateComponents([.day], from: self, to: Date()).day ?? 0
                return "\(diff) days ago"
            }
            let diff = Calendar.current.dateComponents([.weekOfYear], from: self, to: Date()).weekOfYear ?? 0
            return "\(diff) weeks ago"
        }
}

extension UIViewController : NVActivityIndicatorViewable{
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    // We are willing to become first responder to get shake motion
    override open var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    // Enable detection of shake motion
    override open func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            print("Why are you shaking me?")
        }
    }
    
    func showAlert(message : String){
        
        let alertMessage = UIAlertController(title: "HotMenu Alert!", message: "", preferredStyle: .actionSheet)
        let titleAttributes = [NSAttributedString.Key.font: ceraProLargeFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let titleString = NSAttributedString(string: "HotMenu Alert!", attributes: titleAttributes as [NSAttributedString.Key : Any])
        let messageAttributes = [NSAttributedString.Key.font: ceraProNormalFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let messageString = NSAttributedString(string: message, attributes: messageAttributes as [NSAttributedString.Key : Any])
        alertMessage.setValue(titleString, forKey: "attributedTitle")
        alertMessage.setValue(messageString, forKey: "attributedMessage")
        let okAction = UIAlertAction(title: "Ok", style: .destructive) { (alt) in
        }
        alertMessage.view.tintColor = themeDarkColor
        alertMessage.addAction(okAction)
        
        self.present(alertMessage, animated: true, completion: nil)
    }
    
    func showAlertWith(message : String, okAction : @escaping () -> Void){
        let alertMessage = UIAlertController(title: "HotMenu Alert!", message: "", preferredStyle: .actionSheet)
        let titleAttributes = [NSAttributedString.Key.font: ceraProLargeFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let titleString = NSAttributedString(string: "HotMenu Alert!", attributes: titleAttributes as [NSAttributedString.Key : Any])
        let messageAttributes = [NSAttributedString.Key.font: ceraProNormalFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let messageString = NSAttributedString(string: message, attributes: messageAttributes as [NSAttributedString.Key : Any])
        alertMessage.setValue(titleString, forKey: "attributedTitle")
        alertMessage.setValue(messageString, forKey: "attributedMessage")
        let ok = UIAlertAction(title: "Ok", style: .destructive) { (alt) in
            okAction()
        }
        alertMessage.view.tintColor = themeDarkColor
        
        alertMessage.addAction(ok)
        self.present(alertMessage, animated: true, completion: nil)
    }
    
    func showAlertWithOkAndCancel(message : String, okAction : @escaping () -> Void, cancelAction : @escaping () -> Void){
        let alertMessage = UIAlertController(title: "HotMenu Alert!", message: "", preferredStyle: .actionSheet)
        let titleAttributes = [NSAttributedString.Key.font: ceraProLargeFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let titleString = NSAttributedString(string: "HotMenu Alert!", attributes: titleAttributes as [NSAttributedString.Key : Any])
        let messageAttributes = [NSAttributedString.Key.font: ceraProNormalFontSize, NSAttributedString.Key.foregroundColor: themeDarkColor]
        let messageString = NSAttributedString(string: message, attributes: messageAttributes as [NSAttributedString.Key : Any])
        alertMessage.setValue(titleString, forKey: "attributedTitle")
        alertMessage.setValue(messageString, forKey: "attributedMessage")
        let ok = UIAlertAction(title: "Ok", style: .default) { (alt) in
            okAction()
        }
        alertMessage.view.tintColor = themeDarkColor
        
        alertMessage.addAction(ok)
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive) { (alt) in
            cancelAction()
        }
        alertMessage.addAction(cancel)
        
        self.present(alertMessage, animated: true, completion: nil)
    }
    
    func startLoaderAnimation(){
        DispatchQueue.main.async {
          // self.startAnimating()
        }
    }
    
    func stopLoaderAnimation(){
        DispatchQueue.main.async {
           // self.stopAnimating()
        }
    }
}

extension Int {
    static func random(min: Int, max: Int) -> Int {
        precondition(min <= max)
        let randomizer = GKRandomSource.sharedRandom()
        return min + randomizer.nextInt(upperBound: max - min + 1)
    }
}
