//
//  WebServiceManager.swift
//  CellularBackupContacts
//
//  Created by Mukesh Singh on 17/10/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation
import UIKit

public enum ErrorInfo: String, Error {
    case notConnectedToInternet = "No network Connection"
    case netWorkError = "Network Error"
}

public enum ResponseCode: Int {
    case success = 200
    case userCreateSuccess = 201
    case OTPNotVerified = 403
    case internalDBError = 5005
    case dataAlreadyExist = 2008
    case insufficientInformationProvided = 5002
}

public enum UserType: String {
    case customer = "customer"
    case vendor = "RESTAURANT"
    case deliveryBoy = "deliveryboy"
}

public enum LoginType: String {
    case google = "GOOGLE"
    case facebook = "FACEBOOK"
    case others = "GENERAL"
}

public struct WebServiceConstants {
    static private let baseURL = "https://nodeserver.mydevfactory.com:3480/api/"
    //"http://35.181.146.152:3481/api/""https://nodeserver.brainiuminfotech.com:3481/api/"
    //https://nodeserver.mydevfactory.com:3480/api/restaurant/login
    // old base URL : static private let baseURL = "https://nodeserver.mydevfactory.com:3481/api/"
    static var getAllStates: String {
        return self.baseURL + "getAllStates"
    }
    
    static var getAllCities: String {
        return self.baseURL + "getAllCities"
    }
    
    static var registerUser: String {
        return self.baseURL + "vendor/registerVendor"
    }
    
    static var verifyRegitrationOTP: String {
        return self.baseURL + "restaurant/verifyOTP"
    }
    
    static var addTiming: String {
        return self.baseURL + "vendor/registerVendorTime"
    }
    static var loginUser: String {
        return self.baseURL + "restaurant/login"
    }
    
    static var forgotPassword: String {
        return self.baseURL + "restaurant/forgotPassword"
    }
    
    static var resetPassword: String {
        return self.baseURL + "restaurant/resetPassword"
    }
    
    static var resendOTP: String {
        return self.baseURL + "customer/resendForgotPassOtp"
    }
    
    
    static var getUserDetails: String {
        return self.baseURL + "restaurant/profile"
    }
    
    static var editUserProfile: String {
        return self.baseURL + "restaurant/editProfile"
    }
    
    static var editUserImage: String {
        return self.baseURL + "restaurant/profileImageUpload"
    }
    static var changePassword: String {
        return self.baseURL + "restaurant/changePassword"
    }
    
    static var dashBoardAPI: String {
        return self.baseURL + "customer/dashboard"
    }
    
    static var forgotEmail: String {
        return self.baseURL + "restaurant/updateVendorEmail"
    }
    
    static var restaurantDetailsAPI: String {
        return self.baseURL + "customer/vendorDetails"
    }
    
    static var getRestaurantOrderStausListAPI: String {
        return self.baseURL + "restaurant/orderStatus"
    }
    
    static var getOrderListAPI: String {
        return self.baseURL + "restaurant/orderList"
    }
    
    static var chanegeOrderStatusAPI: String {
        return self.baseURL + "restaurant/orderAccept"
    }
    
    static var cancelOrderAPI: String {
        return self.baseURL + "restaurant/orderCancelled"
    }
    
    static var getAllCategories: String {
        return self.baseURL + "restaurant/allCategories"
    }
    
    static var addMenuItem: String {
        return self.baseURL + "restaurant/addItem"
    }
    
    static var editMenuItem: String {
        return self.baseURL + "restaurant/updateItem"
    }
    
    static var getMenuItem: String {
        return self.baseURL + "restaurant/getItem"
    }
    
    static var getAllMenuItem: String {
        return self.baseURL + "restaurant/itemList"
    }
    
    static var getDashBoardItems: String {
        return self.baseURL + "restaurant/dashboard"
    }
    
    static var getDeliveryBoyList: String {
        return self.baseURL + "restaurant/assignDeliveryBoyList"
    }
    
    static var assignDeliveryBoy: String {
        return self.baseURL + "restaurant/assignOrderToDeliveryBoy"
    }
    
    static var getRestaurantProfile: String {
        return self.baseURL + "vendor/getVendorDetails"
    }
    
    static var changeLogo: String {
        return self.baseURL + "vendor/logoUpload"
    }
    
    static var licenceUpload: String {
        return self.baseURL + "vendor/licenceUpload"
    }
    
    static var changeBanner: String {
        return self.baseURL + "vendor/bannerUpload"
    }
    
    static var changeRestaurantInfo: String {
        return self.baseURL + "vendor/updateVendorDetails"
    }
    
    static var changeRestaurantEmailInfo: String {
        return self.baseURL + "vendor/updateVendorEmail"
    }
    
    static var changeRestaurantPhoneInfo: String {
        return self.baseURL + "vendor/updateVendorPhone"
    }
    
    static var editRestaurantTiming: String {
        return self.baseURL + "vendor/updateVendorTime"
    }
    
    static var changeMenuItemStatus: String {
        return self.baseURL + "restaurant/updateItemStatus"
    }
    
    static var changeNotificationSetting: String {
        return self.baseURL + "vendor/updateNotificationData"
    }
    
    static var getNotificationSetting: String {
        return self.baseURL + "vendor/getNotificationData"
    }
    
    static var getPreOptions: String {
        return self.baseURL + "restaurant/getItemOptions"
    }
    
    static var getAllMealTypes: String {
        return self.baseURL + "restaurant/allMealTypes"
    }
    
    static var getRestaurantReviews: String {
        return self.baseURL + "vendor/getVendorReviews"
    }
    
    static var replyRestaurantReviews: String {
        return self.baseURL + "vendor/updateVendorReviews"
    }
    
    static var getPreExtras: String {
        return self.baseURL + "restaurant/getItemExtraName"
    }
    
    static var getRestaurantTypes: String {
        return self.baseURL + "vendor/vendorTypes"
    }
    
    static var deleteMenuItem: String {
        return self.baseURL + "restaurant/deleteItem"
    }
    
    static var addPaymentInfo: String {
        return self.baseURL + "vendor/subaccountCreate"
    }
    
    static var getBankList: String {
        return self.baseURL + "vendor/getSubAccount"
    }
    
    static var getBankDetails: String {
        return self.baseURL + "vendor/getSubAccount"
    }
    
    static var getNotificationList: String {
        return self.baseURL + "vendor/notificationList"
    }
    
    static var getCRMContent: String {
        return self.baseURL + "vendor/getVendorContent"
    }
    
    static var updateBadgeCount: String {
        return self.baseURL + "vendor/updateBadgeCount"
    }
    
}


public enum HTTPMethodType: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
}

/// Responsible for generating common headers for requests.
struct WebServiceHeaderGenerator {
    /// Generates common headers specific to APIs. Can also accept additional headers if demanded by specific APIs.
    ///
    /// - Returns: A configured header JSON dictionary which includes both common and additional params.
    public static func generateHeader() -> [String: String] {
        var headerDict = [String: String]()
        headerDict["accept"] = "application/json"
        headerDict["Content-Type"] = "application/json"
        return headerDict
    }
    
    public static func generateHeaderWithAuthKey() -> [String: String] {
        var headerDict = [String: String]()
        headerDict["accept"] = "application/json"
        headerDict["Content-Type"] = "application/json"
        headerDict["Authorization"] = "Bearer \(UserDefaultValues.authToken)"
        return headerDict
    }
    
    public static func generateHeaderWithAuthKeyy() -> [String: String] {
        var headerDict = [String: String]()
        headerDict["accept"] = "application/json"
        headerDict["Content-Type"] = "application/json"
        headerDict["Authorization"] = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWJqZWN0IjoiNWY2YjFjZTk1NTg4NTMyODc3YTBiNGMwIiwidXNlciI6IlJFU1RBVVJBTlQiLCJpYXQiOjE2MDQ1ODQ2MjQsImV4cCI6MTQ1NjQ1ODQ2MjR9._5q2xgJ6Pi-LKcktbQ-IQLM1SS4xqeCvTNjVPfoBcVA"
        return headerDict
    }
    
    
    public static func generateHeaderWithAuthKeyForMultiPart() -> [String: String] {
        var headerDict = [String: String]()
        headerDict["accept"] = "application/json"
        headerDict["Content-Type"] = "application/x-www-form-urlencoded"
        headerDict["Authorization"] = "Bearer \(UserDefaultValues.authToken)"
        return headerDict
    }

    public static func generateHeaderWithPublicKey() -> [String: String] {
        var headerDict = [String: String]()
        headerDict["accept"] = "application/json"
        headerDict["Authorization"] = "Bearer \(UserDefaultValues.authToken)"
        return headerDict
    }
    public static func generateHeaderWithPublicKeywith(dataStr : String ) -> [String: String] {
        var headerDict = [String: String]()
//        let timestamp = NSDate().timeIntervalSince1970
        headerDict["accept"] = "application/json"
        headerDict["Authorization"] = "Bearer \(UserDefaultValues.authToken)"

//        headerDict["public_key"] = "\(WebServiceManager.shared.publicKey)\(Int(timestamp))"
        headerDict["data"] = dataStr
        return headerDict
    }
    
    public static func generateHeaderWithPublicKeyAndAuthTokenwith(dataStr : String ) -> [String: String] {
        var headerDict = [String: String]()
        let timestamp = NSDate().timeIntervalSince1970
        headerDict["accept"] = "application/json"
        headerDict["public_key"] = "\(WebServiceManager.shared.publicKey)\(Int(timestamp))"
        headerDict["Authorization"] = "Bearer \(UserDefaultValues.authToken)"
        headerDict["data"] = dataStr
        return headerDict
    }

    
    public static func generateAuthorizedHeader() -> [String: String] {
        var headerDict = [String: String]()
        headerDict["Content-Type"] = "application/json"
        headerDict["authtoken"] = ""
        //add other fields
        return headerDict
    }
    
}

class WebServiceManager: NSObject {
    
    static let shared = WebServiceManager()
    static let passwordEncription = "Aa7F6d7F9cF0bEb238dBbE0d715E3d6B"
    static let ivCode = "9bAd74D61e7E03bB"
    typealias WebServiceCompletionBlock = (Data?, Error?) -> Void
    var publicKey = ""
    /// Performs a API request which is called by any service request class.
    /// It also performs an additional task of validating the auth token and refreshing if necessary
    ///
    /// - Parameters:
    ///     - apiModel: APIModelType which contains the info about api endpath, header & http method type.
    ///     - completion: Request completion handler.
    
    public func requestAPI(url: String, httpHeader: [String: String]?, parameter: [String: Any]? = nil, httpMethodType: HTTPMethodType, completion: @escaping WebServiceCompletionBlock) {
        if Reachability.isConnectedToNetwork(){
            let escapedAddress = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            var request = URLRequest(url: URL(string: escapedAddress!)!)
            request.httpMethod = httpMethodType.rawValue
            request.allHTTPHeaderFields = httpHeader

            
            Utility.log("URL: \(url)")
            Utility.log("Header: \(httpHeader ?? [:])")
            Utility.log("Parameter: ")
            Utility.log(parameter)
            
            if parameter != nil {
                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: parameter as Any, options: .prettyPrinted)
                } catch let error {
                    Utility.log(error.localizedDescription)
                    return
                }
            }
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    completion(nil, error)
                    return
                }
                if let httpStatus = response as? HTTPURLResponse{
                    if httpStatus.statusCode == 403{
                        print(httpStatus)
                        DispatchQueue.main.async {
                            resetUserDefaults()
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "landingScreenbeforeLogin") as! LandingScreenbeforeLogin
                            UIApplication.shared.windows[0].rootViewController = viewController
                            UIApplication.shared.windows.first?.rootViewController = viewController
                            UIApplication.shared.windows.first?.makeKeyAndVisible()
                        }
                    }
                }
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    // check for http errors
//                    Utility.log("Error in fetching response")
                    completion(nil, nil)
                }
                Utility.log("Response: \(String(decoding: data, as: UTF8.self))")
                completion(data, nil)
            }
            task.resume()

        }else{
            completion(nil, ErrorInfo.notConnectedToInternet)
        }
        
    }
    
    //Function with Data
    public func requestAPI(url: String, httpHeader: [String: String], parameter: Data?, httpMethodType: HTTPMethodType, completion: @escaping WebServiceCompletionBlock) {
        if Reachability.isConnectedToNetwork(){
            let escapedAddress = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            var request = URLRequest(url: URL(string: escapedAddress!)!)
            request.httpMethod = httpMethodType.rawValue
            request.allHTTPHeaderFields = httpHeader

            Utility.log("URL: \(url)")
            Utility.log("Header: \(httpHeader)")
            Utility.log("Parameter: ")
            Utility.log(parameter)
            
            if parameter != nil {
                request.httpBody = parameter
            }
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    completion(nil, error)
                    return
                }
                if let httpStatus = response as? HTTPURLResponse{
                    if httpStatus.statusCode == 403{
                        print(httpStatus)
                        DispatchQueue.main.async {
                            resetUserDefaults()
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "landingScreenbeforeLogin") as! LandingScreenbeforeLogin
                            UIApplication.shared.windows[0].rootViewController = viewController
                            UIApplication.shared.windows.first?.rootViewController = viewController
                            UIApplication.shared.windows.first?.makeKeyAndVisible()
                        }
                    }
                }

                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    // check for http errors
//                    Utility.log("Error in fetching response")
                    completion(nil, nil)
                }
//                Utility.log("Response: \(String(decoding: data, as: UTF8.self))")
                completion(data, nil)
            }
            task.resume()

        }else{
            completion(nil, ErrorInfo.notConnectedToInternet)
        }
    }
    
    
    //Multipart data sendfunction
    public func requestMultipartAPI(url: String, parameter: [String: Any]?, httpMethodType: HTTPMethodType, multipartData: Data? = nil, isAuthorized: Bool, authToken: String,imageParamName : String = "image",mimetype : String = "image/jpeg", fname : String = "image.jpeg", completion: @escaping WebServiceCompletionBlock) {
        
        let escapedAddress = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        var request = URLRequest(url: URL(string: escapedAddress!)!)
        request.httpMethod = httpMethodType.rawValue
        
        if( multipartData != nil ) {
            let boundary = generateBoundaryString()
            
            //define the multipart request type
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            if isAuthorized {
//                request.setValue(authToken, forHTTPHeaderField: "authtoken")
                request.setValue(authToken, forHTTPHeaderField: "Authorization")
            }
            
            let body = NSMutableData()
            

            for (key, value) in parameter! {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                //let val = "\(value)".addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: ""))!
                body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
            }
            
            //define the data post parameter
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"\(imageParamName)\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(multipartData!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                completion(nil, error)
                return
            }
            if let httpStatus = response as? HTTPURLResponse{
                if httpStatus.statusCode == 403{
                    print(httpStatus)
                    DispatchQueue.main.async {
                        resetUserDefaults()
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "landingScreenbeforeLogin") as! LandingScreenbeforeLogin
                        UIApplication.shared.windows[0].rootViewController = viewController
                        UIApplication.shared.windows.first?.rootViewController = viewController
                        UIApplication.shared.windows.first?.makeKeyAndVisible()
                    }
                }
            }

            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                Utility.log("Error in fetching response")
                completion(nil, nil)
            }
            Utility.log("Response: \(String(decoding: data, as: UTF8.self))")
            completion(data, nil)
        }
        task.resume()
        
    }
    
    
    //For Two images simultenously
    public func requestMultipartAPIForTwoImages(url: String, parameter: [String: Any]?, httpMethodType: HTTPMethodType, isAuthorized: Bool, authToken: String, completion: @escaping WebServiceCompletionBlock) {
        
        let escapedAddress = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let request = createRequest(param: parameter!, strURL: escapedAddress!, authToken: authToken)
        print("URl = ", url )
        print("parameter = ", parameter as Any )

        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                completion(nil, error)
                return
            }
            if let httpStatus = response as? HTTPURLResponse{
                if httpStatus.statusCode == 403{
                    print(httpStatus)
                    DispatchQueue.main.async {
                        resetUserDefaults()
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "landingScreenbeforeLogin") as! LandingScreenbeforeLogin
                        UIApplication.shared.windows[0].rootViewController = viewController
                        UIApplication.shared.windows.first?.rootViewController = viewController
                        UIApplication.shared.windows.first?.makeKeyAndVisible()
                    }
                }
            }

            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                Utility.log("Error in fetching response")
                completion(nil, nil)
            }
            Utility.log("Response: \(String(decoding: data, as: UTF8.self))")
            completion(data, nil)
        }
        task.resume()
        
    }
    
    
    
    private func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }


    func convertToDictionary(text: String) -> [String: Any]? {
//        var strNew = text.replacingOccurrences(of: "/", with: "")
//        strNew = strNew.replacingOccurrences(of: "\", with: "")
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func encryptJsonToString(json : [String: Any]) -> String{
       var encrypted = ""
//        if let theJSONData = try? JSONSerialization.data(
//            withJSONObject: json,
//            options: [.prettyPrinted]) {
//            let theJSONText = String(data: theJSONData,
//                                     encoding: .ascii)
//            print("JSON string = \(theJSONText!)")
//            encrypted = AES256CBC.encryptString(theJSONText!, password: WebServiceManager.passwordEncription)!
//            print("Encrypted string = \(encrypted)")
//
//        }
        
        
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: json,
            options: [.prettyPrinted]) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .utf8)
            print("JSON string = \(theJSONText!)")
            encrypted = AES256CBC.encryptString(theJSONText!, password: WebServiceManager.passwordEncription)!
            print("Encrypted string = \(encrypted)")
            
            
            let decrypted = AES256CBC.decryptString(encrypted, password: WebServiceManager.passwordEncription)
            print("String decrypted:\t\t\t\(decrypted!)")

        }

        
        
        return encrypted
    }
    
    func decryptResponseDataToData(data : Data) -> Data?{
        do {
            var json: [String : Any] = try JSONSerialization.jsonObject(with: Data.init(referencing: data as NSData), options: JSONSerialization.ReadingOptions(rawValue: 0)) as! [String : Any]
            
            let decrypted = AES256CBC.decryptString(json["data"] as! String, password: WebServiceManager.passwordEncription)
            Utility.log("String decrypted:\t\t\t\(decrypted!)")
            
            json = self.convertToDictionary(text: decrypted!)!
            do {
                let data = try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
                return data
               } catch let myJSONError {
                   print(myJSONError)
                return nil
               }

        } catch {
            Utility.log("")
            return nil
        }
        
    }
    
    func decryptResponseDataToDataForLogin(data : Data) -> Data?{
        do {
            let json: [String : Any] = try JSONSerialization.jsonObject(with: Data.init(referencing: data as NSData), options: JSONSerialization.ReadingOptions(rawValue: 0)) as! [String : Any]
            
            let decrypted = AES256CBC.decryptString(json["data"] as! String, password: WebServiceManager.passwordEncription)
            Utility.log("String decrypted:\t\t\t\(decrypted!)")
            
            if let data = decrypted!.data(using: .utf8) {
                return data
            }else{
                return nil
            }
            

        } catch {
            Utility.log("")
            return nil
        }
        
    }
    
    func decryptResponseDataToJson(data : Data) -> [String:Any]?{
        do {
            let json: [String : Any] = try JSONSerialization.jsonObject(with: Data.init(referencing: data as NSData), options: JSONSerialization.ReadingOptions(rawValue: 0)) as! [String : Any]
            let decrypted = AES256CBC.decryptString( json ["key"] as! String, password: WebServiceManager.passwordEncription)
            Utility.log("String decrypted:\t\t\t\(decrypted!)")
            
            let objjson = self.convertToDictionary(text: decrypted!)!
            return objjson

        } catch {
            Utility.log("")
            return nil
        }
        
    }
    
    // For Getting the public key
//    func getPublicKey(completion : @escaping (_ success : Bool) -> Void){
//        
//        self.requestAPI(url: WebServiceConstants.getPublicKey, httpHeader: WebServiceHeaderGenerator.generateHeader(), httpMethodType: .get) { (data, err) in
//            
//            if let data = data {
//                
//                do {
//                    var json: [String : Any] = try JSONSerialization.jsonObject(with: Data.init(referencing: data as NSData), options: JSONSerialization.ReadingOptions(rawValue: 0)) as! [String : Any]
//                    
//                    let decrypted = AES256CBC.decryptString( json ["key"] as! String, password: WebServiceManager.passwordEncription)
//                    Utility.log("String decrypted:\t\t\t\(decrypted!)")
//                    
//                    json = self.convertToDictionary(text: decrypted!)!
//                    Utility.log(json)
//                    if let key = json["public_key"] as! String?{
//                        self.publicKey = key
//                        completion(true)
//                    }
//                    else{
//                        Utility.log("Key not found")
//                        completion(false)
//                    }
//                } catch {
//                    Utility.log("")
//                    completion(false)
//                }
//            }
//
//        }
//          
//      }
    
    
    
    
    
    
    
    
    
    func createBodyWithParameters(parameters: [String : Any] ,boundary: String) -> Data {
        var body = Data()

            for (key, value) in parameters {

                if(value is String || value is NSString){
                    body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                    body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                    body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
                }
                else if(value is UIImage){
                        let image = value as! UIImage
                        let filename = "image\(key).jpg"
                        let data = image.jpeg(.low)
                        let mimetype = "image/png"

                        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                        body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n".data(using: String.Encoding.utf8)!)
                        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
                        body.append(data!)
                        body.append("\r\n".data(using: String.Encoding.utf8)!)


                }
            }
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        //        NSLog("data %@",NSString(data: body, encoding: NSUTF8StringEncoding)!);
        return body
    }




    func createRequest (param : [String : Any] , strURL : String, authToken : String) -> URLRequest {
        let boundary = generateBoundaryString()
        let url = NSURL(string: strURL)
        var request = URLRequest(url: url! as URL)
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(authToken)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        request.httpBody = createBodyWithParameters(parameters: param, boundary: boundary)
//        request.allHTTPHeaderFields = header
        return request
    }

    
}

