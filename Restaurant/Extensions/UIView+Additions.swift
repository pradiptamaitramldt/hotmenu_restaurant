//
//  UIView+Additions.swift
//  CellularBackupContacts
//
//  Created by Mukesh Singh on 14/10/19.
//  Copyright © 2019 Mukesh Singh. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    func blink() {
        self.alpha = 0.0
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {self.alpha = 1.0}, completion: nil)
    }
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
    }
    
    func applyGradient(colors: [UIColor], isVertical: Bool = false) -> Void {
        self.applyGradient(colors, startPoint: isVertical ? CGPoint(x: 0.0, y: 1.0) : nil, endPoint: isVertical ? CGPoint(x: 1.0, y: 1.0) : nil)
        //self.applyGradient(colors, locations: isVertical ? [0.0 , 1.0] : nil)
    }
    
    func applyGradient(_ colors: [UIColor], startPoint: CGPoint?, endPoint: CGPoint?) -> Void {
        //self.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colors.map { $0.cgColor }
        //gradient.locations = locations
        if startPoint != nil && endPoint != nil {
            gradient.startPoint = startPoint!
            gradient.endPoint = endPoint!
        }
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    //dismiss keyboard on touch outside the textfield
    func addhideKeyboardFeatureOnTouch() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
    
    func makeCircular() {
        self.layer.cornerRadius = self.bounds.size.height / 2
        self.layer.masksToBounds = true
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension CALayer {
    func roundCorners(corners: UIRectCorner, radius: CGFloat, viewBounds: CGRect) {

        let maskPath = UIBezierPath(
                    roundedRect: viewBounds,
                    byRoundingCorners: corners,
                    cornerRadii: CGSize(width: radius, height: radius)
                )

        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        self.mask = shape
    }
}
