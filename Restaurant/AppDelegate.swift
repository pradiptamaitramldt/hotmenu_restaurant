//
//  AppDelegate.swift
//  Restaurant
//
//  Created by Pallab on 21/08/20.
//  Copyright © 2020 brainium. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GoogleApi.shared.initialiseWithKey(GooglePlacesAPIkey)
         IQKeyboardManager.shared.enable = true
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        // iOS 8 support
        else if #available(iOS 8, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        // iOS 7 support
        else {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        #if DEBUG
        UserDefaultValues.pushMode = "S"
        #else
        UserDefaultValues.pushMode = "P"
        #endif
        UserDefaultValues.deviceToken = "testToken"
        
        for family in UIFont.familyNames {
                   print("\(family)")

                   for name in UIFont.fontNames(forFamilyName: family) {
                       print("\(name)")
                   }
               }
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            loadCountriesJson(countryCode: countryCode)
        }
        return true
    }
    func loadCountriesJson(countryCode : String) {
           if let url = Bundle.main.url(forResource: "countryCodes", withExtension: "json") {
               do {
                   let data = try Data(contentsOf: url)
                   let decoder = JSONDecoder()
                   let countries = try decoder.decode([Country].self, from: data)
                   for count in 0...(countries.count - 1) {
                       if countryCode == countries[count].code {
                           print("dialcode",countries[count].dialCode ?? "")
                           SingleToneClass.shared.defaultPhoneCountry = countries[count]
                          // print("def",SingleToneClass.shared.defaultPhoneCountry)
                           break
                       }
                   }
               }
               catch {
                   print("error:\(error)")
               }
           }
       }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        UserDefaultValues.deviceToken = deviceTokenString
        // Print it to console
        print("APNs device token: \(deviceTokenString)")

        // Persist it in your backend in case it's new
    }

    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        
        print("Push notification received: \(data)")
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("Push notification received: \(userInfo)")
        //        gotPust(data: userInfo as NSDictionary)
        
    }

}

